; ModuleID = 'tgsi'
source_filename = "tgsi"
target datalayout = "e-p:64:64-p1:64:64-p2:32:32-p3:32:32-p4:64:64-p5:32:32-p6:32:32-i64:64-v16:16-v24:32-v32:32-v48:64-v96:128-v192:256-v256:256-v512:512-v1024:1024-v2048:2048-n32:64-S32-A5"
target triple = "amdgcn--"

; Function Attrs: alwaysinline
define private amdgpu_vs void @main([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x float] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32 inreg, i32 inreg, i32 inreg, i32 inreg, [0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32, i32, i32, i32, i32) #0 {
main_body:
  %TEMP6.w = alloca float, addrspace(5)
  %TEMP6.z = alloca float, addrspace(5)
  %TEMP6.y = alloca float, addrspace(5)
  %TEMP6.x = alloca float, addrspace(5)
  %TEMP5.w = alloca float, addrspace(5)
  %TEMP5.z = alloca float, addrspace(5)
  %TEMP5.y = alloca float, addrspace(5)
  %TEMP5.x = alloca float, addrspace(5)
  %TEMP4.w = alloca float, addrspace(5)
  %TEMP4.z = alloca float, addrspace(5)
  %TEMP4.y = alloca float, addrspace(5)
  %TEMP4.x = alloca float, addrspace(5)
  %TEMP3.w = alloca float, addrspace(5)
  %TEMP3.z = alloca float, addrspace(5)
  %TEMP3.y = alloca float, addrspace(5)
  %TEMP3.x = alloca float, addrspace(5)
  %TEMP2.w = alloca float, addrspace(5)
  %TEMP2.z = alloca float, addrspace(5)
  %TEMP2.y = alloca float, addrspace(5)
  %TEMP2.x = alloca float, addrspace(5)
  %TEMP1.w = alloca float, addrspace(5)
  %TEMP1.z = alloca float, addrspace(5)
  %TEMP1.y = alloca float, addrspace(5)
  %TEMP1.x = alloca float, addrspace(5)
  %TEMP0.w = alloca float, addrspace(5)
  %TEMP0.z = alloca float, addrspace(5)
  %TEMP0.y = alloca float, addrspace(5)
  %TEMP0.x = alloca float, addrspace(5)
  %OUT1.w = alloca float, addrspace(5)
  %OUT1.z = alloca float, addrspace(5)
  %OUT1.y = alloca float, addrspace(5)
  %OUT1.x = alloca float, addrspace(5)
  %OUT0.w = alloca float, addrspace(5)
  %OUT0.z = alloca float, addrspace(5)
  %OUT0.y = alloca float, addrspace(5)
  %OUT0.x = alloca float, addrspace(5)
  %14 = getelementptr [0 x <4 x i32>], [0 x <4 x i32>] addrspace(6)* %8, i32 0, i32 0, !amdgpu.uniform !0
  %15 = load <4 x i32>, <4 x i32> addrspace(6)* %14, !invariant.load !0
  %16 = call nsz <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32> %15, i32 %13, i32 0, i1 false, i1 false) #2
  %17 = extractelement <4 x float> %16, i32 0
  %18 = extractelement <4 x float> %16, i32 1
  %19 = extractelement <4 x float> %16, i32 2
  %20 = extractelement <4 x float> %16, i32 3
  store float 0xFFFFFFFFE0000000, float addrspace(5)* %TEMP0.x
  store float %17, float addrspace(5)* %OUT0.x
  store float %18, float addrspace(5)* %OUT0.y
  store float %19, float addrspace(5)* %OUT0.z
  store float %20, float addrspace(5)* %OUT0.w
  %21 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %22 = insertelement <4 x i32> undef, i32 %21, i32 0
  %23 = insertelement <4 x i32> %22, i32 0, i32 1
  %24 = insertelement <4 x i32> %23, i32 48, i32 2
  %25 = insertelement <4 x i32> %24, i32 163756, i32 3
  %26 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %25, i32 0)
  %27 = bitcast float %26 to i32
  %28 = icmp eq i32 %27, 1
  %29 = sext i1 %28 to i32
  %30 = bitcast i32 %29 to float
  store float %30, float addrspace(5)* %TEMP1.x
  %31 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %32 = insertelement <4 x i32> undef, i32 %31, i32 0
  %33 = insertelement <4 x i32> %32, i32 0, i32 1
  %34 = insertelement <4 x i32> %33, i32 48, i32 2
  %35 = insertelement <4 x i32> %34, i32 163756, i32 3
  %36 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %35, i32 16)
  %37 = bitcast float %36 to i32
  %38 = icmp eq i32 %37, 1
  %39 = sext i1 %38 to i32
  %40 = bitcast i32 %39 to float
  store float %40, float addrspace(5)* %TEMP2.x
  %41 = load float, float addrspace(5)* %TEMP1.x
  %42 = bitcast float %41 to i32
  %43 = load float, float addrspace(5)* %TEMP2.x
  %44 = bitcast float %43 to i32
  %45 = and i32 %42, %44
  %46 = bitcast i32 %45 to float
  store float %46, float addrspace(5)* %TEMP3.x
  %47 = load float, float addrspace(5)* %TEMP3.x
  %48 = bitcast float %47 to i32
  %49 = icmp ne i32 %48, 0
  %50 = select i1 %49, float 0.000000e+00, float 1.000000e+00
  %51 = load float, float addrspace(5)* %TEMP3.x
  %52 = bitcast float %51 to i32
  %53 = icmp ne i32 %52, 0
  %54 = select i1 %53, float 1.000000e+00, float 0.000000e+00
  %55 = load float, float addrspace(5)* %TEMP3.x
  %56 = bitcast float %55 to i32
  %57 = icmp ne i32 %56, 0
  %58 = select i1 %57, float 0.000000e+00, float 0.000000e+00
  %59 = load float, float addrspace(5)* %TEMP3.x
  %60 = bitcast float %59 to i32
  %61 = icmp ne i32 %60, 0
  %62 = select i1 %61, float 1.000000e+00, float 1.000000e+00
  store float %50, float addrspace(5)* %TEMP4.x
  store float %54, float addrspace(5)* %TEMP4.y
  store float %58, float addrspace(5)* %TEMP4.z
  store float %62, float addrspace(5)* %TEMP4.w
  %63 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %64 = insertelement <4 x i32> undef, i32 %63, i32 0
  %65 = insertelement <4 x i32> %64, i32 0, i32 1
  %66 = insertelement <4 x i32> %65, i32 48, i32 2
  %67 = insertelement <4 x i32> %66, i32 163756, i32 3
  %68 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %67, i32 32)
  %69 = bitcast float %68 to i32
  %70 = icmp eq i32 %69, 1
  %71 = sext i1 %70 to i32
  %72 = bitcast i32 %71 to float
  store float %72, float addrspace(5)* %TEMP5.x
  %73 = load float, float addrspace(5)* %TEMP1.x
  %74 = bitcast float %73 to i32
  %75 = load float, float addrspace(5)* %TEMP5.x
  %76 = bitcast float %75 to i32
  %77 = and i32 %74, %76
  %78 = bitcast i32 %77 to float
  store float %78, float addrspace(5)* %TEMP6.x
  %79 = load float, float addrspace(5)* %TEMP6.x
  %80 = load float, float addrspace(5)* %OUT1.x
  %81 = bitcast float %79 to i32
  %82 = icmp ne i32 %81, 0
  %83 = select i1 %82, float 1.000000e+00, float %80
  %84 = load float, float addrspace(5)* %TEMP6.x
  %85 = load float, float addrspace(5)* %OUT1.y
  %86 = bitcast float %84 to i32
  %87 = icmp ne i32 %86, 0
  %88 = select i1 %87, float 1.000000e+00, float %85
  %89 = load float, float addrspace(5)* %TEMP6.x
  %90 = load float, float addrspace(5)* %OUT1.z
  %91 = bitcast float %89 to i32
  %92 = icmp ne i32 %91, 0
  %93 = select i1 %92, float 0.000000e+00, float %90
  %94 = load float, float addrspace(5)* %TEMP6.x
  %95 = load float, float addrspace(5)* %OUT1.w
  %96 = bitcast float %94 to i32
  %97 = icmp ne i32 %96, 0
  %98 = select i1 %97, float 1.000000e+00, float %95
  store float %83, float addrspace(5)* %OUT1.x
  store float %88, float addrspace(5)* %OUT1.y
  store float %93, float addrspace(5)* %OUT1.z
  store float %98, float addrspace(5)* %OUT1.w
  %99 = load float, float addrspace(5)* %TEMP6.x
  %100 = load float, float addrspace(5)* %TEMP0.x
  %101 = bitcast float %99 to i32
  %102 = icmp ne i32 %101, 0
  %103 = select i1 %102, float 0.000000e+00, float %100
  store float %103, float addrspace(5)* %TEMP0.x
  %104 = load float, float addrspace(5)* %TEMP0.x
  %105 = load float, float addrspace(5)* %TEMP4.x
  %106 = load float, float addrspace(5)* %OUT1.x
  %107 = bitcast float %104 to i32
  %108 = icmp ne i32 %107, 0
  %109 = select i1 %108, float %105, float %106
  %110 = load float, float addrspace(5)* %TEMP0.x
  %111 = load float, float addrspace(5)* %TEMP4.y
  %112 = load float, float addrspace(5)* %OUT1.y
  %113 = bitcast float %110 to i32
  %114 = icmp ne i32 %113, 0
  %115 = select i1 %114, float %111, float %112
  %116 = load float, float addrspace(5)* %TEMP0.x
  %117 = load float, float addrspace(5)* %TEMP4.z
  %118 = load float, float addrspace(5)* %OUT1.z
  %119 = bitcast float %116 to i32
  %120 = icmp ne i32 %119, 0
  %121 = select i1 %120, float %117, float %118
  %122 = load float, float addrspace(5)* %TEMP0.x
  %123 = load float, float addrspace(5)* %TEMP4.w
  %124 = load float, float addrspace(5)* %OUT1.w
  %125 = bitcast float %122 to i32
  %126 = icmp ne i32 %125, 0
  %127 = select i1 %126, float %123, float %124
  store float %109, float addrspace(5)* %OUT1.x
  store float %115, float addrspace(5)* %OUT1.y
  store float %121, float addrspace(5)* %OUT1.z
  store float %127, float addrspace(5)* %OUT1.w
  %128 = trunc i32 %7 to i1
  br i1 %128, label %if-true-block, label %endif-block

if-true-block:                                    ; preds = %main_body
  %129 = load float, float addrspace(5)* %OUT1.x
  %130 = call nsz float @llvm.maxnum.f32(float %129, float 0.000000e+00) #2
  %131 = call nsz float @llvm.minnum.f32(float %130, float 1.000000e+00) #2
  store float %131, float addrspace(5)* %OUT1.x
  %132 = load float, float addrspace(5)* %OUT1.y
  %133 = call nsz float @llvm.maxnum.f32(float %132, float 0.000000e+00) #2
  %134 = call nsz float @llvm.minnum.f32(float %133, float 1.000000e+00) #2
  store float %134, float addrspace(5)* %OUT1.y
  %135 = load float, float addrspace(5)* %OUT1.z
  %136 = call nsz float @llvm.maxnum.f32(float %135, float 0.000000e+00) #2
  %137 = call nsz float @llvm.minnum.f32(float %136, float 1.000000e+00) #2
  store float %137, float addrspace(5)* %OUT1.z
  %138 = load float, float addrspace(5)* %OUT1.w
  %139 = call nsz float @llvm.maxnum.f32(float %138, float 0.000000e+00) #2
  %140 = call nsz float @llvm.minnum.f32(float %139, float 1.000000e+00) #2
  store float %140, float addrspace(5)* %OUT1.w
  br label %endif-block

endif-block:                                      ; preds = %main_body, %if-true-block
  %141 = load float, float addrspace(5)* %OUT0.x
  %142 = load float, float addrspace(5)* %OUT0.y
  %143 = load float, float addrspace(5)* %OUT0.z
  %144 = load float, float addrspace(5)* %OUT0.w
  %145 = load float, float addrspace(5)* %OUT1.x
  %146 = load float, float addrspace(5)* %OUT1.y
  %147 = load float, float addrspace(5)* %OUT1.z
  %148 = load float, float addrspace(5)* %OUT1.w
  call void @llvm.amdgcn.exp.f32(i32 12, i32 15, float %141, float %142, float %143, float %144, i1 true, i1 false) #4
  call void @llvm.amdgcn.exp.f32(i32 32, i32 15, float %145, float %146, float %147, float %148, i1 false, i1 false) #4
  ret void
}

; Function Attrs: nounwind readonly
declare <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32>, i32, i32, i1, i1) #1

; Function Attrs: nounwind readnone
declare float @llvm.SI.load.const.v4i32(<4 x i32>, i32) #2

; Function Attrs: nounwind readnone speculatable
declare float @llvm.maxnum.f32(float, float) #3

; Function Attrs: nounwind readnone speculatable
declare float @llvm.minnum.f32(float, float) #3

; Function Attrs: nounwind
declare void @llvm.amdgcn.exp.f32(i32, i32, float, float, float, float, i1, i1) #4

; Function Attrs: alwaysinline
define private amdgpu_vs <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> @vs_prolog(i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32, i32, i32, i32) #0 {
main_body:
  %13 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> undef, i32 %0, 0
  %14 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %13, i32 %1, 1
  %15 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %14, i32 %2, 2
  %16 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %15, i32 %3, 3
  %17 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %16, i32 %4, 4
  %18 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %17, i32 %5, 5
  %19 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %18, i32 %6, 6
  %20 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %19, i32 %7, 7
  %21 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %20, i32 %8, 8
  %22 = bitcast i32 %9 to float
  %23 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %21, float %22, 9
  %24 = bitcast i32 %10 to float
  %25 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %23, float %24, 10
  %26 = bitcast i32 %11 to float
  %27 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %25, float %26, 11
  %28 = bitcast i32 %12 to float
  %29 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %27, float %28, 12
  %30 = add i32 %9, %4
  %31 = bitcast i32 %30 to float
  %32 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %29, float %31, 13
  ret <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %32
}

define amdgpu_vs void @wrapper([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x float] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32 inreg, i32 inreg, i32 inreg, i32 inreg, [0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32, i32, i32, i32) #5 {
main_body:
  %13 = ptrtoint [0 x <4 x i32>] addrspace(6)* %0 to i32
  %14 = ptrtoint [0 x <8 x i32>] addrspace(6)* %1 to i32
  %15 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %16 = ptrtoint [0 x <8 x i32>] addrspace(6)* %3 to i32
  %17 = ptrtoint [0 x <4 x i32>] addrspace(6)* %8 to i32
  %18 = bitcast i32 %9 to float
  %19 = bitcast i32 %10 to float
  %20 = bitcast i32 %11 to float
  %21 = bitcast i32 %12 to float
  %22 = bitcast float %18 to i32
  %23 = bitcast float %19 to i32
  %24 = bitcast float %20 to i32
  %25 = bitcast float %21 to i32
  %26 = call <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> @vs_prolog(i32 %13, i32 %14, i32 %15, i32 %16, i32 %4, i32 %5, i32 %6, i32 %7, i32 %17, i32 %22, i32 %23, i32 %24, i32 %25)
  %27 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 0
  %28 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 1
  %29 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 2
  %30 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 3
  %31 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 4
  %32 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 5
  %33 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 6
  %34 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 7
  %35 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 8
  %36 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 9
  %37 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 10
  %38 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 11
  %39 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 12
  %40 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 13
  %41 = inttoptr i32 %27 to [0 x <4 x i32>] addrspace(6)*
  %42 = inttoptr i32 %28 to [0 x <8 x i32>] addrspace(6)*
  %43 = inttoptr i32 %29 to [0 x float] addrspace(6)*
  %44 = inttoptr i32 %30 to [0 x <8 x i32>] addrspace(6)*
  %45 = inttoptr i32 %35 to [0 x <4 x i32>] addrspace(6)*
  %46 = bitcast float %36 to i32
  %47 = bitcast float %37 to i32
  %48 = bitcast float %38 to i32
  %49 = bitcast float %39 to i32
  %50 = bitcast float %40 to i32
  call void @main([0 x <4 x i32>] addrspace(6)* %41, [0 x <8 x i32>] addrspace(6)* %42, [0 x float] addrspace(6)* %43, [0 x <8 x i32>] addrspace(6)* %44, i32 %31, i32 %32, i32 %33, i32 %34, [0 x <4 x i32>] addrspace(6)* %45, i32 %46, i32 %47, i32 %48, i32 %49, i32 %50)
  ret void
}

attributes #0 = { alwaysinline "no-signed-zeros-fp-math"="true" }
attributes #1 = { nounwind readonly }
attributes #2 = { nounwind readnone }
attributes #3 = { nounwind readnone speculatable }
attributes #4 = { nounwind }
attributes #5 = { "no-signed-zeros-fp-math"="true" }

!0 = !{}
