; ModuleID = 'tgsi'
source_filename = "tgsi"
target datalayout = "e-p:64:64-p1:64:64-p2:32:32-p3:32:32-p4:64:64-p5:32:32-p6:32:32-i64:64-v16:16-v24:32-v32:32-v48:64-v96:128-v192:256-v256:256-v512:512-v1024:1024-v2048:2048-n32:64-S32-A5"
target triple = "amdgcn--"

; Function Attrs: nounwind readnone speculatable
declare float @llvm.amdgcn.interp.p1(float, i32, i32, i32) #0

; Function Attrs: nounwind readnone speculatable
declare float @llvm.amdgcn.interp.p2(float, float, i32, i32, i32) #0

; Function Attrs: nounwind readnone speculatable
declare <2 x half> @llvm.amdgcn.cvt.pkrtz(float, float) #0

; Function Attrs: nounwind
declare void @llvm.amdgcn.exp.compr.v2i16(i32, i32, <2 x i16>, <2 x i16>, i1, i1) #1

define amdgpu_ps void @wrapper([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), float inreg, i32 inreg, <2 x i32>, <2 x i32>, <2 x i32>, <3 x i32>, <2 x i32>, <2 x i32>, <2 x i32>, float, float, float, float, float, i32, i32, float, i32) #2 {
main_body:
  %22 = bitcast <2 x i32> %7 to <2 x float>
  %23 = extractelement <2 x float> %22, i32 0
  %24 = extractelement <2 x float> %22, i32 1
  %25 = call nsz float @llvm.amdgcn.interp.p1(float %23, i32 0, i32 0, i32 %5) #3
  %26 = call nsz float @llvm.amdgcn.interp.p2(float %25, float %24, i32 0, i32 0, i32 %5) #3
  %27 = call nsz float @llvm.amdgcn.interp.p1(float %23, i32 1, i32 0, i32 %5) #3
  %28 = call nsz float @llvm.amdgcn.interp.p2(float %27, float %24, i32 1, i32 0, i32 %5) #3
  %29 = call nsz float @llvm.amdgcn.interp.p1(float %23, i32 2, i32 0, i32 %5) #3
  %30 = call nsz float @llvm.amdgcn.interp.p2(float %29, float %24, i32 2, i32 0, i32 %5) #3
  %31 = call nsz float @llvm.amdgcn.interp.p1(float %23, i32 3, i32 0, i32 %5) #3
  %32 = call nsz float @llvm.amdgcn.interp.p2(float %31, float %24, i32 3, i32 0, i32 %5) #3
  %33 = call nsz <2 x half> @llvm.amdgcn.cvt.pkrtz(float %26, float %28) #3
  %34 = call nsz <2 x half> @llvm.amdgcn.cvt.pkrtz(float %30, float %32) #3
  %35 = bitcast <2 x half> %33 to <2 x i16>
  %36 = bitcast <2 x half> %34 to <2 x i16>
  call void @llvm.amdgcn.exp.compr.v2i16(i32 0, i32 15, <2 x i16> %35, <2 x i16> %36, i1 true, i1 true) #1
  ret void
}

attributes #0 = { nounwind readnone speculatable }
attributes #1 = { nounwind }
attributes #2 = { "no-signed-zeros-fp-math"="true" }
attributes #3 = { nounwind readnone }
