; ModuleID = 'tgsi'
source_filename = "tgsi"
target datalayout = "e-p:64:64-p1:64:64-p2:32:32-p3:32:32-p4:64:64-p5:32:32-p6:32:32-i64:64-v16:16-v24:32-v32:32-v48:64-v96:128-v192:256-v256:256-v512:512-v1024:1024-v2048:2048-n32:64-S32-A5"
target triple = "amdgcn--"

; Function Attrs: nounwind readonly
declare <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32>, i32, i32, i1, i1) #0

; Function Attrs: nounwind readnone
declare float @llvm.SI.load.const.v4i32(<4 x i32>, i32) #1

; Function Attrs: nounwind readnone speculatable
declare float @llvm.fabs.f32(float) #2

; Function Attrs: nounwind readnone speculatable
declare float @llvm.sqrt.f32(float) #2

; Function Attrs: nounwind readnone speculatable
declare float @llvm.maxnum.f32(float, float) #2

; Function Attrs: nounwind readnone speculatable
declare float @llvm.minnum.f32(float, float) #2

; Function Attrs: nounwind
declare void @llvm.amdgcn.exp.f32(i32, i32, float, float, float, float, i1, i1) #3

define amdgpu_vs void @wrapper([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x float] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32 inreg, i32 inreg, i32 inreg, i32 inreg, [0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32, i32, i32, i32) #4 {
main_body:
  %13 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %14 = add i32 %9, %4
  %15 = getelementptr [0 x <4 x i32>], [0 x <4 x i32>] addrspace(6)* %8, i32 0, i32 0, !amdgpu.uniform !0
  %16 = load <4 x i32>, <4 x i32> addrspace(6)* %15, align 16, !invariant.load !0, !alias.scope !1, !noalias !4
  %17 = call nsz <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32> %16, i32 %14, i32 0, i1 false, i1 false) #1
  %18 = extractelement <4 x float> %17, i32 0
  %19 = extractelement <4 x float> %17, i32 1
  %20 = extractelement <4 x float> %17, i32 2
  %21 = extractelement <4 x float> %17, i32 3
  %22 = getelementptr [0 x <4 x i32>], [0 x <4 x i32>] addrspace(6)* %8, i32 0, i32 1, !amdgpu.uniform !0
  %23 = load <4 x i32>, <4 x i32> addrspace(6)* %22, align 16, !invariant.load !0, !alias.scope !1, !noalias !4
  %24 = call nsz <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32> %23, i32 %14, i32 0, i1 false, i1 false) #1
  %25 = extractelement <4 x float> %24, i32 0
  %26 = extractelement <4 x float> %24, i32 1
  %27 = extractelement <4 x float> %24, i32 2
  %28 = insertelement <4 x i32> <i32 undef, i32 0, i32 160, i32 163756>, i32 %13, i32 0
  %29 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 0)
  %30 = fmul nsz float %18, %29
  %31 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 4)
  %32 = fmul nsz float %18, %31
  %33 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 8)
  %34 = fmul nsz float %18, %33
  %35 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 12)
  %36 = fmul nsz float %18, %35
  %37 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 16)
  %38 = fmul nsz float %19, %37
  %39 = fadd nsz float %38, %30
  %40 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 20)
  %41 = fmul nsz float %19, %40
  %42 = fadd nsz float %41, %32
  %43 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 24)
  %44 = fmul nsz float %19, %43
  %45 = fadd nsz float %44, %34
  %46 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 28)
  %47 = fmul nsz float %19, %46
  %48 = fadd nsz float %47, %36
  %49 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 32)
  %50 = fmul nsz float %20, %49
  %51 = fadd nsz float %50, %39
  %52 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 36)
  %53 = fmul nsz float %20, %52
  %54 = fadd nsz float %53, %42
  %55 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 40)
  %56 = fmul nsz float %20, %55
  %57 = fadd nsz float %56, %45
  %58 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 44)
  %59 = fmul nsz float %20, %58
  %60 = fadd nsz float %59, %48
  %61 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 48)
  %62 = fmul nsz float %21, %61
  %63 = fadd nsz float %62, %51
  %64 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 52)
  %65 = fmul nsz float %21, %64
  %66 = fadd nsz float %65, %54
  %67 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 56)
  %68 = fmul nsz float %21, %67
  %69 = fadd nsz float %68, %57
  %70 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 60)
  %71 = fmul nsz float %21, %70
  %72 = fadd nsz float %71, %60
  %73 = fmul nsz float %25, %25
  %74 = fmul nsz float %26, %26
  %75 = fadd nsz float %74, %73
  %76 = fmul nsz float %27, %27
  %77 = fadd nsz float %75, %76
  %78 = call nsz float @llvm.fabs.f32(float %77) #3
  %79 = call nsz float @llvm.sqrt.f32(float %78) #1
  %80 = fdiv nsz float 1.000000e+00, %79, !fpmath !6
  %81 = fmul nsz float %25, %80
  %82 = fmul nsz float %26, %80
  %83 = fmul nsz float %27, %80
  %84 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 64)
  %85 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 68)
  %86 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 72)
  %87 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 76)
  %88 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 80)
  %89 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 84)
  %90 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 88)
  %91 = fmul nsz float %81, %88
  %92 = fmul nsz float %82, %89
  %93 = fadd nsz float %92, %91
  %94 = fmul nsz float %83, %90
  %95 = fadd nsz float %93, %94
  %96 = call nsz float @llvm.maxnum.f32(float %95, float 0.000000e+00) #1
  %97 = fcmp nsz ogt float %95, 0.000000e+00
  %98 = select i1 %97, float 1.000000e+00, float 0.000000e+00
  %99 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 112)
  %100 = fadd nsz float %99, %84
  %101 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 116)
  %102 = fadd nsz float %101, %85
  %103 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 120)
  %104 = fadd nsz float %103, %86
  %105 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 128)
  %106 = fmul nsz float %96, %105
  %107 = fadd nsz float %106, %100
  %108 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 132)
  %109 = fmul nsz float %96, %108
  %110 = fadd nsz float %109, %102
  %111 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 136)
  %112 = fmul nsz float %96, %111
  %113 = fadd nsz float %112, %104
  %114 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 144)
  %115 = fmul nsz float %98, %114
  %116 = fadd nsz float %115, %107
  %117 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 148)
  %118 = fmul nsz float %98, %117
  %119 = fadd nsz float %118, %110
  %120 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %28, i32 152)
  %121 = fmul nsz float %98, %120
  %122 = fadd nsz float %121, %113
  %123 = and i32 %7, 1
  %124 = icmp eq i32 %123, 0
  br i1 %124, label %main.exit, label %if-true-block.i

if-true-block.i:                                  ; preds = %main_body
  %125 = call nsz float @llvm.maxnum.f32(float %116, float 0.000000e+00) #1
  %126 = call nsz float @llvm.minnum.f32(float %125, float 1.000000e+00) #1
  %127 = call nsz float @llvm.maxnum.f32(float %119, float 0.000000e+00) #1
  %128 = call nsz float @llvm.minnum.f32(float %127, float 1.000000e+00) #1
  %129 = call nsz float @llvm.maxnum.f32(float %122, float 0.000000e+00) #1
  %130 = call nsz float @llvm.minnum.f32(float %129, float 1.000000e+00) #1
  %131 = call nsz float @llvm.maxnum.f32(float %87, float 0.000000e+00) #1
  %132 = call nsz float @llvm.minnum.f32(float %131, float 1.000000e+00) #1
  br label %main.exit

main.exit:                                        ; preds = %main_body, %if-true-block.i
  %OUT1.w.0.i = phi float [ %132, %if-true-block.i ], [ %87, %main_body ]
  %OUT1.z.0.i = phi float [ %130, %if-true-block.i ], [ %122, %main_body ]
  %OUT1.y.0.i = phi float [ %128, %if-true-block.i ], [ %119, %main_body ]
  %OUT1.x.0.i = phi float [ %126, %if-true-block.i ], [ %116, %main_body ]
  call void @llvm.amdgcn.exp.f32(i32 12, i32 15, float %63, float %66, float %69, float %72, i1 true, i1 false) #3, !noalias !1
  call void @llvm.amdgcn.exp.f32(i32 32, i32 15, float %OUT1.x.0.i, float %OUT1.y.0.i, float %OUT1.z.0.i, float %OUT1.w.0.i, i1 false, i1 false) #3, !noalias !1
  ret void
}

attributes #0 = { nounwind readonly }
attributes #1 = { nounwind readnone }
attributes #2 = { nounwind readnone speculatable }
attributes #3 = { nounwind }
attributes #4 = { "no-signed-zeros-fp-math"="true" }

!0 = !{}
!1 = !{!2}
!2 = distinct !{!2, !3, !"main: argument 1"}
!3 = distinct !{!3, !"main"}
!4 = !{!5}
!5 = distinct !{!5, !3, !"main: argument 0"}
!6 = !{float 2.500000e+00}
