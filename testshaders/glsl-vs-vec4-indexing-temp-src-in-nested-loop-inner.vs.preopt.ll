; ModuleID = 'tgsi'
source_filename = "tgsi"
target datalayout = "e-p:64:64-p1:64:64-p2:32:32-p3:32:32-p4:64:64-p5:32:32-p6:32:32-i64:64-v16:16-v24:32-v32:32-v48:64-v96:128-v192:256-v256:256-v512:512-v1024:1024-v2048:2048-n32:64-S32-A5"
target triple = "amdgcn--"

; Function Attrs: alwaysinline
define private amdgpu_vs void @main([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x float] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32 inreg, i32 inreg, i32 inreg, i32 inreg, [0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32, i32, i32, i32, i32) #0 {
main_body:
  %TEMP15.w = alloca float, addrspace(5)
  %TEMP15.z = alloca float, addrspace(5)
  %TEMP15.y = alloca float, addrspace(5)
  %TEMP15.x = alloca float, addrspace(5)
  %TEMP14.w = alloca float, addrspace(5)
  %TEMP14.z = alloca float, addrspace(5)
  %TEMP14.y = alloca float, addrspace(5)
  %TEMP14.x = alloca float, addrspace(5)
  %TEMP13.w = alloca float, addrspace(5)
  %TEMP13.z = alloca float, addrspace(5)
  %TEMP13.y = alloca float, addrspace(5)
  %TEMP13.x = alloca float, addrspace(5)
  %TEMP12.w = alloca float, addrspace(5)
  %TEMP12.z = alloca float, addrspace(5)
  %TEMP12.y = alloca float, addrspace(5)
  %TEMP12.x = alloca float, addrspace(5)
  %TEMP11.w = alloca float, addrspace(5)
  %TEMP11.z = alloca float, addrspace(5)
  %TEMP11.y = alloca float, addrspace(5)
  %TEMP11.x = alloca float, addrspace(5)
  %TEMP10.w = alloca float, addrspace(5)
  %TEMP10.z = alloca float, addrspace(5)
  %TEMP10.y = alloca float, addrspace(5)
  %TEMP10.x = alloca float, addrspace(5)
  %TEMP9.w = alloca float, addrspace(5)
  %TEMP9.z = alloca float, addrspace(5)
  %TEMP9.y = alloca float, addrspace(5)
  %TEMP9.x = alloca float, addrspace(5)
  %TEMP8.w = alloca float, addrspace(5)
  %TEMP8.z = alloca float, addrspace(5)
  %TEMP8.y = alloca float, addrspace(5)
  %TEMP8.x = alloca float, addrspace(5)
  %array = alloca [32 x float], addrspace(5)
  %OUT1.w = alloca float, addrspace(5)
  %OUT1.z = alloca float, addrspace(5)
  %OUT1.y = alloca float, addrspace(5)
  %OUT1.x = alloca float, addrspace(5)
  %OUT0.w = alloca float, addrspace(5)
  %OUT0.z = alloca float, addrspace(5)
  %OUT0.y = alloca float, addrspace(5)
  %OUT0.x = alloca float, addrspace(5)
  %14 = getelementptr [0 x <4 x i32>], [0 x <4 x i32>] addrspace(6)* %8, i32 0, i32 0, !amdgpu.uniform !0
  %15 = load <4 x i32>, <4 x i32> addrspace(6)* %14, !invariant.load !0
  %16 = call nsz <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32> %15, i32 %13, i32 0, i1 false, i1 false) #2
  %17 = extractelement <4 x float> %16, i32 0
  %18 = extractelement <4 x float> %16, i32 1
  %19 = extractelement <4 x float> %16, i32 2
  %20 = extractelement <4 x float> %16, i32 3
  %TEMP0.x = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 0
  %TEMP0.y = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 1
  %TEMP0.z = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 2
  %TEMP0.w = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 3
  %TEMP1.x = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 4
  %TEMP1.y = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 5
  %TEMP1.z = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 6
  %TEMP1.w = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 7
  %TEMP2.x = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 8
  %TEMP2.y = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 9
  %TEMP2.z = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 10
  %TEMP2.w = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 11
  %TEMP3.x = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 12
  %TEMP3.y = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 13
  %TEMP3.z = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 14
  %TEMP3.w = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 15
  %TEMP4.x = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 16
  %TEMP4.y = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 17
  %TEMP4.z = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 18
  %TEMP4.w = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 19
  %TEMP5.x = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 20
  %TEMP5.y = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 21
  %TEMP5.z = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 22
  %TEMP5.w = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 23
  %TEMP6.x = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 24
  %TEMP6.y = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 25
  %TEMP6.z = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 26
  %TEMP6.w = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 27
  %TEMP7.x = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 28
  %TEMP7.y = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 29
  %TEMP7.z = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 30
  %TEMP7.w = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 31
  %21 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %22 = insertelement <4 x i32> undef, i32 %21, i32 0
  %23 = insertelement <4 x i32> %22, i32 0, i32 1
  %24 = insertelement <4 x i32> %23, i32 112, i32 2
  %25 = insertelement <4 x i32> %24, i32 163756, i32 3
  %26 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %25, i32 0)
  %27 = fmul nsz float 0x3FB99999A0000000, %26
  %28 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %29 = insertelement <4 x i32> undef, i32 %28, i32 0
  %30 = insertelement <4 x i32> %29, i32 0, i32 1
  %31 = insertelement <4 x i32> %30, i32 112, i32 2
  %32 = insertelement <4 x i32> %31, i32 163756, i32 3
  %33 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %32, i32 0)
  %34 = fmul nsz float 0.000000e+00, %33
  %35 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %36 = insertelement <4 x i32> undef, i32 %35, i32 0
  %37 = insertelement <4 x i32> %36, i32 0, i32 1
  %38 = insertelement <4 x i32> %37, i32 112, i32 2
  %39 = insertelement <4 x i32> %38, i32 163756, i32 3
  %40 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %39, i32 0)
  %41 = fmul nsz float 0.000000e+00, %40
  %42 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %43 = insertelement <4 x i32> undef, i32 %42, i32 0
  %44 = insertelement <4 x i32> %43, i32 0, i32 1
  %45 = insertelement <4 x i32> %44, i32 112, i32 2
  %46 = insertelement <4 x i32> %45, i32 163756, i32 3
  %47 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %46, i32 0)
  %48 = fmul nsz float 0.000000e+00, %47
  store float %27, float addrspace(5)* %TEMP0.x
  store float %34, float addrspace(5)* %TEMP0.y
  store float %41, float addrspace(5)* %TEMP0.z
  store float %48, float addrspace(5)* %TEMP0.w
  %49 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %50 = insertelement <4 x i32> undef, i32 %49, i32 0
  %51 = insertelement <4 x i32> %50, i32 0, i32 1
  %52 = insertelement <4 x i32> %51, i32 112, i32 2
  %53 = insertelement <4 x i32> %52, i32 163756, i32 3
  %54 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %53, i32 0)
  %55 = fmul nsz float 0.000000e+00, %54
  %56 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %57 = insertelement <4 x i32> undef, i32 %56, i32 0
  %58 = insertelement <4 x i32> %57, i32 0, i32 1
  %59 = insertelement <4 x i32> %58, i32 112, i32 2
  %60 = insertelement <4 x i32> %59, i32 163756, i32 3
  %61 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %60, i32 0)
  %62 = fmul nsz float 0x3FB99999A0000000, %61
  %63 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %64 = insertelement <4 x i32> undef, i32 %63, i32 0
  %65 = insertelement <4 x i32> %64, i32 0, i32 1
  %66 = insertelement <4 x i32> %65, i32 112, i32 2
  %67 = insertelement <4 x i32> %66, i32 163756, i32 3
  %68 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %67, i32 0)
  %69 = fmul nsz float 0.000000e+00, %68
  %70 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %71 = insertelement <4 x i32> undef, i32 %70, i32 0
  %72 = insertelement <4 x i32> %71, i32 0, i32 1
  %73 = insertelement <4 x i32> %72, i32 112, i32 2
  %74 = insertelement <4 x i32> %73, i32 163756, i32 3
  %75 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %74, i32 0)
  %76 = fmul nsz float 0.000000e+00, %75
  store float %55, float addrspace(5)* %TEMP1.x
  store float %62, float addrspace(5)* %TEMP1.y
  store float %69, float addrspace(5)* %TEMP1.z
  store float %76, float addrspace(5)* %TEMP1.w
  %77 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %78 = insertelement <4 x i32> undef, i32 %77, i32 0
  %79 = insertelement <4 x i32> %78, i32 0, i32 1
  %80 = insertelement <4 x i32> %79, i32 112, i32 2
  %81 = insertelement <4 x i32> %80, i32 163756, i32 3
  %82 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %81, i32 0)
  %83 = fmul nsz float 0.000000e+00, %82
  %84 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %85 = insertelement <4 x i32> undef, i32 %84, i32 0
  %86 = insertelement <4 x i32> %85, i32 0, i32 1
  %87 = insertelement <4 x i32> %86, i32 112, i32 2
  %88 = insertelement <4 x i32> %87, i32 163756, i32 3
  %89 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %88, i32 0)
  %90 = fmul nsz float 0.000000e+00, %89
  %91 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %92 = insertelement <4 x i32> undef, i32 %91, i32 0
  %93 = insertelement <4 x i32> %92, i32 0, i32 1
  %94 = insertelement <4 x i32> %93, i32 112, i32 2
  %95 = insertelement <4 x i32> %94, i32 163756, i32 3
  %96 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %95, i32 0)
  %97 = fmul nsz float 0x3FB99999A0000000, %96
  %98 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %99 = insertelement <4 x i32> undef, i32 %98, i32 0
  %100 = insertelement <4 x i32> %99, i32 0, i32 1
  %101 = insertelement <4 x i32> %100, i32 112, i32 2
  %102 = insertelement <4 x i32> %101, i32 163756, i32 3
  %103 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %102, i32 0)
  %104 = fmul nsz float 0.000000e+00, %103
  store float %83, float addrspace(5)* %TEMP2.x
  store float %90, float addrspace(5)* %TEMP2.y
  store float %97, float addrspace(5)* %TEMP2.z
  store float %104, float addrspace(5)* %TEMP2.w
  %105 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %106 = insertelement <4 x i32> undef, i32 %105, i32 0
  %107 = insertelement <4 x i32> %106, i32 0, i32 1
  %108 = insertelement <4 x i32> %107, i32 112, i32 2
  %109 = insertelement <4 x i32> %108, i32 163756, i32 3
  %110 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %109, i32 0)
  %111 = fmul nsz float 0.000000e+00, %110
  %112 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %113 = insertelement <4 x i32> undef, i32 %112, i32 0
  %114 = insertelement <4 x i32> %113, i32 0, i32 1
  %115 = insertelement <4 x i32> %114, i32 112, i32 2
  %116 = insertelement <4 x i32> %115, i32 163756, i32 3
  %117 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %116, i32 0)
  %118 = fmul nsz float 0x3FB99999A0000000, %117
  %119 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %120 = insertelement <4 x i32> undef, i32 %119, i32 0
  %121 = insertelement <4 x i32> %120, i32 0, i32 1
  %122 = insertelement <4 x i32> %121, i32 112, i32 2
  %123 = insertelement <4 x i32> %122, i32 163756, i32 3
  %124 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %123, i32 0)
  %125 = fmul nsz float 0.000000e+00, %124
  %126 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %127 = insertelement <4 x i32> undef, i32 %126, i32 0
  %128 = insertelement <4 x i32> %127, i32 0, i32 1
  %129 = insertelement <4 x i32> %128, i32 112, i32 2
  %130 = insertelement <4 x i32> %129, i32 163756, i32 3
  %131 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %130, i32 0)
  %132 = fmul nsz float 0.000000e+00, %131
  store float %111, float addrspace(5)* %TEMP3.x
  store float %118, float addrspace(5)* %TEMP3.y
  store float %125, float addrspace(5)* %TEMP3.z
  store float %132, float addrspace(5)* %TEMP3.w
  %133 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %134 = insertelement <4 x i32> undef, i32 %133, i32 0
  %135 = insertelement <4 x i32> %134, i32 0, i32 1
  %136 = insertelement <4 x i32> %135, i32 112, i32 2
  %137 = insertelement <4 x i32> %136, i32 163756, i32 3
  %138 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %137, i32 0)
  %139 = fmul nsz float 0.000000e+00, %138
  %140 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %141 = insertelement <4 x i32> undef, i32 %140, i32 0
  %142 = insertelement <4 x i32> %141, i32 0, i32 1
  %143 = insertelement <4 x i32> %142, i32 112, i32 2
  %144 = insertelement <4 x i32> %143, i32 163756, i32 3
  %145 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %144, i32 0)
  %146 = fmul nsz float 0.000000e+00, %145
  %147 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %148 = insertelement <4 x i32> undef, i32 %147, i32 0
  %149 = insertelement <4 x i32> %148, i32 0, i32 1
  %150 = insertelement <4 x i32> %149, i32 112, i32 2
  %151 = insertelement <4 x i32> %150, i32 163756, i32 3
  %152 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %151, i32 0)
  %153 = fmul nsz float 0x3FB99999A0000000, %152
  %154 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %155 = insertelement <4 x i32> undef, i32 %154, i32 0
  %156 = insertelement <4 x i32> %155, i32 0, i32 1
  %157 = insertelement <4 x i32> %156, i32 112, i32 2
  %158 = insertelement <4 x i32> %157, i32 163756, i32 3
  %159 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %158, i32 0)
  %160 = fmul nsz float 0.000000e+00, %159
  store float %139, float addrspace(5)* %TEMP4.x
  store float %146, float addrspace(5)* %TEMP4.y
  store float %153, float addrspace(5)* %TEMP4.z
  store float %160, float addrspace(5)* %TEMP4.w
  %161 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %162 = insertelement <4 x i32> undef, i32 %161, i32 0
  %163 = insertelement <4 x i32> %162, i32 0, i32 1
  %164 = insertelement <4 x i32> %163, i32 112, i32 2
  %165 = insertelement <4 x i32> %164, i32 163756, i32 3
  %166 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %165, i32 0)
  %167 = fmul nsz float 0.000000e+00, %166
  %168 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %169 = insertelement <4 x i32> undef, i32 %168, i32 0
  %170 = insertelement <4 x i32> %169, i32 0, i32 1
  %171 = insertelement <4 x i32> %170, i32 112, i32 2
  %172 = insertelement <4 x i32> %171, i32 163756, i32 3
  %173 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %172, i32 0)
  %174 = fmul nsz float 0.000000e+00, %173
  %175 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %176 = insertelement <4 x i32> undef, i32 %175, i32 0
  %177 = insertelement <4 x i32> %176, i32 0, i32 1
  %178 = insertelement <4 x i32> %177, i32 112, i32 2
  %179 = insertelement <4 x i32> %178, i32 163756, i32 3
  %180 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %179, i32 0)
  %181 = fmul nsz float 0x3FB99999A0000000, %180
  %182 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %183 = insertelement <4 x i32> undef, i32 %182, i32 0
  %184 = insertelement <4 x i32> %183, i32 0, i32 1
  %185 = insertelement <4 x i32> %184, i32 112, i32 2
  %186 = insertelement <4 x i32> %185, i32 163756, i32 3
  %187 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %186, i32 0)
  %188 = fmul nsz float 0.000000e+00, %187
  store float %167, float addrspace(5)* %TEMP5.x
  store float %174, float addrspace(5)* %TEMP5.y
  store float %181, float addrspace(5)* %TEMP5.z
  store float %188, float addrspace(5)* %TEMP5.w
  %189 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %190 = insertelement <4 x i32> undef, i32 %189, i32 0
  %191 = insertelement <4 x i32> %190, i32 0, i32 1
  %192 = insertelement <4 x i32> %191, i32 112, i32 2
  %193 = insertelement <4 x i32> %192, i32 163756, i32 3
  %194 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %193, i32 0)
  %195 = fmul nsz float 0x3FB99999A0000000, %194
  %196 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %197 = insertelement <4 x i32> undef, i32 %196, i32 0
  %198 = insertelement <4 x i32> %197, i32 0, i32 1
  %199 = insertelement <4 x i32> %198, i32 112, i32 2
  %200 = insertelement <4 x i32> %199, i32 163756, i32 3
  %201 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %200, i32 0)
  %202 = fmul nsz float 0.000000e+00, %201
  %203 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %204 = insertelement <4 x i32> undef, i32 %203, i32 0
  %205 = insertelement <4 x i32> %204, i32 0, i32 1
  %206 = insertelement <4 x i32> %205, i32 112, i32 2
  %207 = insertelement <4 x i32> %206, i32 163756, i32 3
  %208 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %207, i32 0)
  %209 = fmul nsz float 0.000000e+00, %208
  %210 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %211 = insertelement <4 x i32> undef, i32 %210, i32 0
  %212 = insertelement <4 x i32> %211, i32 0, i32 1
  %213 = insertelement <4 x i32> %212, i32 112, i32 2
  %214 = insertelement <4 x i32> %213, i32 163756, i32 3
  %215 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %214, i32 0)
  %216 = fmul nsz float 0.000000e+00, %215
  store float %195, float addrspace(5)* %TEMP6.x
  store float %202, float addrspace(5)* %TEMP6.y
  store float %209, float addrspace(5)* %TEMP6.z
  store float %216, float addrspace(5)* %TEMP6.w
  %217 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %218 = insertelement <4 x i32> undef, i32 %217, i32 0
  %219 = insertelement <4 x i32> %218, i32 0, i32 1
  %220 = insertelement <4 x i32> %219, i32 112, i32 2
  %221 = insertelement <4 x i32> %220, i32 163756, i32 3
  %222 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %221, i32 0)
  %223 = fmul nsz float 0.000000e+00, %222
  %224 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %225 = insertelement <4 x i32> undef, i32 %224, i32 0
  %226 = insertelement <4 x i32> %225, i32 0, i32 1
  %227 = insertelement <4 x i32> %226, i32 112, i32 2
  %228 = insertelement <4 x i32> %227, i32 163756, i32 3
  %229 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %228, i32 0)
  %230 = fmul nsz float 0x3FB99999A0000000, %229
  %231 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %232 = insertelement <4 x i32> undef, i32 %231, i32 0
  %233 = insertelement <4 x i32> %232, i32 0, i32 1
  %234 = insertelement <4 x i32> %233, i32 112, i32 2
  %235 = insertelement <4 x i32> %234, i32 163756, i32 3
  %236 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %235, i32 0)
  %237 = fmul nsz float 0.000000e+00, %236
  %238 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %239 = insertelement <4 x i32> undef, i32 %238, i32 0
  %240 = insertelement <4 x i32> %239, i32 0, i32 1
  %241 = insertelement <4 x i32> %240, i32 112, i32 2
  %242 = insertelement <4 x i32> %241, i32 163756, i32 3
  %243 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %242, i32 0)
  %244 = fmul nsz float 0.000000e+00, %243
  store float %223, float addrspace(5)* %TEMP7.x
  store float %230, float addrspace(5)* %TEMP7.y
  store float %237, float addrspace(5)* %TEMP7.z
  store float %244, float addrspace(5)* %TEMP7.w
  %245 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %246 = insertelement <4 x i32> undef, i32 %245, i32 0
  %247 = insertelement <4 x i32> %246, i32 0, i32 1
  %248 = insertelement <4 x i32> %247, i32 112, i32 2
  %249 = insertelement <4 x i32> %248, i32 163756, i32 3
  %250 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %249, i32 48)
  %251 = fmul nsz float %250, %17
  %252 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %253 = insertelement <4 x i32> undef, i32 %252, i32 0
  %254 = insertelement <4 x i32> %253, i32 0, i32 1
  %255 = insertelement <4 x i32> %254, i32 112, i32 2
  %256 = insertelement <4 x i32> %255, i32 163756, i32 3
  %257 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %256, i32 52)
  %258 = fmul nsz float %257, %17
  %259 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %260 = insertelement <4 x i32> undef, i32 %259, i32 0
  %261 = insertelement <4 x i32> %260, i32 0, i32 1
  %262 = insertelement <4 x i32> %261, i32 112, i32 2
  %263 = insertelement <4 x i32> %262, i32 163756, i32 3
  %264 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %263, i32 56)
  %265 = fmul nsz float %264, %17
  %266 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %267 = insertelement <4 x i32> undef, i32 %266, i32 0
  %268 = insertelement <4 x i32> %267, i32 0, i32 1
  %269 = insertelement <4 x i32> %268, i32 112, i32 2
  %270 = insertelement <4 x i32> %269, i32 163756, i32 3
  %271 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %270, i32 60)
  %272 = fmul nsz float %271, %17
  store float %251, float addrspace(5)* %TEMP8.x
  store float %258, float addrspace(5)* %TEMP8.y
  store float %265, float addrspace(5)* %TEMP8.z
  store float %272, float addrspace(5)* %TEMP8.w
  %273 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %274 = insertelement <4 x i32> undef, i32 %273, i32 0
  %275 = insertelement <4 x i32> %274, i32 0, i32 1
  %276 = insertelement <4 x i32> %275, i32 112, i32 2
  %277 = insertelement <4 x i32> %276, i32 163756, i32 3
  %278 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %277, i32 64)
  %279 = load float, float addrspace(5)* %TEMP8.x
  %280 = fmul nsz float %278, %18
  %281 = fadd nsz float %280, %279
  %282 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %283 = insertelement <4 x i32> undef, i32 %282, i32 0
  %284 = insertelement <4 x i32> %283, i32 0, i32 1
  %285 = insertelement <4 x i32> %284, i32 112, i32 2
  %286 = insertelement <4 x i32> %285, i32 163756, i32 3
  %287 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %286, i32 68)
  %288 = load float, float addrspace(5)* %TEMP8.y
  %289 = fmul nsz float %287, %18
  %290 = fadd nsz float %289, %288
  %291 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %292 = insertelement <4 x i32> undef, i32 %291, i32 0
  %293 = insertelement <4 x i32> %292, i32 0, i32 1
  %294 = insertelement <4 x i32> %293, i32 112, i32 2
  %295 = insertelement <4 x i32> %294, i32 163756, i32 3
  %296 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %295, i32 72)
  %297 = load float, float addrspace(5)* %TEMP8.z
  %298 = fmul nsz float %296, %18
  %299 = fadd nsz float %298, %297
  %300 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %301 = insertelement <4 x i32> undef, i32 %300, i32 0
  %302 = insertelement <4 x i32> %301, i32 0, i32 1
  %303 = insertelement <4 x i32> %302, i32 112, i32 2
  %304 = insertelement <4 x i32> %303, i32 163756, i32 3
  %305 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %304, i32 76)
  %306 = load float, float addrspace(5)* %TEMP8.w
  %307 = fmul nsz float %305, %18
  %308 = fadd nsz float %307, %306
  store float %281, float addrspace(5)* %TEMP9.x
  store float %290, float addrspace(5)* %TEMP9.y
  store float %299, float addrspace(5)* %TEMP9.z
  store float %308, float addrspace(5)* %TEMP9.w
  %309 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %310 = insertelement <4 x i32> undef, i32 %309, i32 0
  %311 = insertelement <4 x i32> %310, i32 0, i32 1
  %312 = insertelement <4 x i32> %311, i32 112, i32 2
  %313 = insertelement <4 x i32> %312, i32 163756, i32 3
  %314 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %313, i32 80)
  %315 = load float, float addrspace(5)* %TEMP9.x
  %316 = fmul nsz float %314, %19
  %317 = fadd nsz float %316, %315
  %318 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %319 = insertelement <4 x i32> undef, i32 %318, i32 0
  %320 = insertelement <4 x i32> %319, i32 0, i32 1
  %321 = insertelement <4 x i32> %320, i32 112, i32 2
  %322 = insertelement <4 x i32> %321, i32 163756, i32 3
  %323 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %322, i32 84)
  %324 = load float, float addrspace(5)* %TEMP9.y
  %325 = fmul nsz float %323, %19
  %326 = fadd nsz float %325, %324
  %327 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %328 = insertelement <4 x i32> undef, i32 %327, i32 0
  %329 = insertelement <4 x i32> %328, i32 0, i32 1
  %330 = insertelement <4 x i32> %329, i32 112, i32 2
  %331 = insertelement <4 x i32> %330, i32 163756, i32 3
  %332 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %331, i32 88)
  %333 = load float, float addrspace(5)* %TEMP9.z
  %334 = fmul nsz float %332, %19
  %335 = fadd nsz float %334, %333
  %336 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %337 = insertelement <4 x i32> undef, i32 %336, i32 0
  %338 = insertelement <4 x i32> %337, i32 0, i32 1
  %339 = insertelement <4 x i32> %338, i32 112, i32 2
  %340 = insertelement <4 x i32> %339, i32 163756, i32 3
  %341 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %340, i32 92)
  %342 = load float, float addrspace(5)* %TEMP9.w
  %343 = fmul nsz float %341, %19
  %344 = fadd nsz float %343, %342
  store float %317, float addrspace(5)* %TEMP10.x
  store float %326, float addrspace(5)* %TEMP10.y
  store float %335, float addrspace(5)* %TEMP10.z
  store float %344, float addrspace(5)* %TEMP10.w
  %345 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %346 = insertelement <4 x i32> undef, i32 %345, i32 0
  %347 = insertelement <4 x i32> %346, i32 0, i32 1
  %348 = insertelement <4 x i32> %347, i32 112, i32 2
  %349 = insertelement <4 x i32> %348, i32 163756, i32 3
  %350 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %349, i32 96)
  %351 = load float, float addrspace(5)* %TEMP10.x
  %352 = fmul nsz float %350, %20
  %353 = fadd nsz float %352, %351
  %354 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %355 = insertelement <4 x i32> undef, i32 %354, i32 0
  %356 = insertelement <4 x i32> %355, i32 0, i32 1
  %357 = insertelement <4 x i32> %356, i32 112, i32 2
  %358 = insertelement <4 x i32> %357, i32 163756, i32 3
  %359 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %358, i32 100)
  %360 = load float, float addrspace(5)* %TEMP10.y
  %361 = fmul nsz float %359, %20
  %362 = fadd nsz float %361, %360
  %363 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %364 = insertelement <4 x i32> undef, i32 %363, i32 0
  %365 = insertelement <4 x i32> %364, i32 0, i32 1
  %366 = insertelement <4 x i32> %365, i32 112, i32 2
  %367 = insertelement <4 x i32> %366, i32 163756, i32 3
  %368 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %367, i32 104)
  %369 = load float, float addrspace(5)* %TEMP10.z
  %370 = fmul nsz float %368, %20
  %371 = fadd nsz float %370, %369
  %372 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %373 = insertelement <4 x i32> undef, i32 %372, i32 0
  %374 = insertelement <4 x i32> %373, i32 0, i32 1
  %375 = insertelement <4 x i32> %374, i32 112, i32 2
  %376 = insertelement <4 x i32> %375, i32 163756, i32 3
  %377 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %376, i32 108)
  %378 = load float, float addrspace(5)* %TEMP10.w
  %379 = fmul nsz float %377, %20
  %380 = fadd nsz float %379, %378
  store float %353, float addrspace(5)* %OUT0.x
  store float %362, float addrspace(5)* %OUT0.y
  store float %371, float addrspace(5)* %OUT0.z
  store float %380, float addrspace(5)* %OUT0.w
  store float 0.000000e+00, float addrspace(5)* %TEMP11.x
  store float 0.000000e+00, float addrspace(5)* %TEMP11.y
  store float 0.000000e+00, float addrspace(5)* %TEMP11.z
  store float 0.000000e+00, float addrspace(5)* %TEMP11.w
  store float 0.000000e+00, float addrspace(5)* %TEMP12.x
  br label %loop14

loop14:                                           ; preds = %endloop27, %main_body
  %381 = load float, float addrspace(5)* %TEMP12.x
  %382 = bitcast float %381 to i32
  %383 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %384 = insertelement <4 x i32> undef, i32 %383, i32 0
  %385 = insertelement <4 x i32> %384, i32 0, i32 1
  %386 = insertelement <4 x i32> %385, i32 112, i32 2
  %387 = insertelement <4 x i32> %386, i32 163756, i32 3
  %388 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %387, i32 16)
  %389 = bitcast float %388 to i32
  %390 = icmp sge i32 %382, %389
  %391 = sext i1 %390 to i32
  %392 = bitcast i32 %391 to float
  store float %392, float addrspace(5)* %TEMP13.x
  %393 = load float, float addrspace(5)* %TEMP13.x
  %394 = bitcast float %393 to i32
  %395 = icmp ne i32 %394, 0
  br i1 %395, label %if16, label %endif18

if16:                                             ; preds = %loop14
  br label %endloop29

endif18:                                          ; preds = %loop14
  store float 0.000000e+00, float addrspace(5)* %TEMP14.x
  br label %loop20

loop20:                                           ; preds = %endif24, %endif18
  %396 = load float, float addrspace(5)* %TEMP14.x
  %397 = bitcast float %396 to i32
  %398 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %399 = insertelement <4 x i32> undef, i32 %398, i32 0
  %400 = insertelement <4 x i32> %399, i32 0, i32 1
  %401 = insertelement <4 x i32> %400, i32 112, i32 2
  %402 = insertelement <4 x i32> %401, i32 163756, i32 3
  %403 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %402, i32 32)
  %404 = bitcast float %403 to i32
  %405 = icmp sge i32 %397, %404
  %406 = sext i1 %405 to i32
  %407 = bitcast i32 %406 to float
  store float %407, float addrspace(5)* %TEMP15.x
  %408 = load float, float addrspace(5)* %TEMP15.x
  %409 = bitcast float %408 to i32
  %410 = icmp ne i32 %409, 0
  br i1 %410, label %if22, label %endif24

if22:                                             ; preds = %loop20
  br label %endloop27

endif24:                                          ; preds = %loop20
  %411 = load float, float addrspace(5)* %TEMP11.x
  %412 = load float, float addrspace(5)* %TEMP14.x
  %413 = bitcast float %412 to i32
  %414 = add i32 %413, 0
  %415 = and i32 %414, 7
  %416 = mul i32 %415, 4
  %417 = add i32 %416, 0
  %418 = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 %417
  %419 = load float, float addrspace(5)* %418
  %420 = fadd nsz float %411, %419
  %421 = load float, float addrspace(5)* %TEMP11.y
  %422 = load float, float addrspace(5)* %TEMP14.x
  %423 = bitcast float %422 to i32
  %424 = add i32 %423, 0
  %425 = and i32 %424, 7
  %426 = mul i32 %425, 4
  %427 = add i32 %426, 1
  %428 = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 %427
  %429 = load float, float addrspace(5)* %428
  %430 = fadd nsz float %421, %429
  %431 = load float, float addrspace(5)* %TEMP11.z
  %432 = load float, float addrspace(5)* %TEMP14.x
  %433 = bitcast float %432 to i32
  %434 = add i32 %433, 0
  %435 = and i32 %434, 7
  %436 = mul i32 %435, 4
  %437 = add i32 %436, 2
  %438 = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 %437
  %439 = load float, float addrspace(5)* %438
  %440 = fadd nsz float %431, %439
  %441 = load float, float addrspace(5)* %TEMP11.w
  %442 = load float, float addrspace(5)* %TEMP14.x
  %443 = bitcast float %442 to i32
  %444 = add i32 %443, 0
  %445 = and i32 %444, 7
  %446 = mul i32 %445, 4
  %447 = add i32 %446, 3
  %448 = getelementptr [32 x float], [32 x float] addrspace(5)* %array, i32 0, i32 %447
  %449 = load float, float addrspace(5)* %448
  %450 = fadd nsz float %441, %449
  store float %420, float addrspace(5)* %TEMP11.x
  store float %430, float addrspace(5)* %TEMP11.y
  store float %440, float addrspace(5)* %TEMP11.z
  store float %450, float addrspace(5)* %TEMP11.w
  %451 = load float, float addrspace(5)* %TEMP14.x
  %452 = bitcast float %451 to i32
  %453 = add i32 %452, 1
  %454 = bitcast i32 %453 to float
  store float %454, float addrspace(5)* %TEMP14.x
  br label %loop20

endloop27:                                        ; preds = %if22
  %455 = load float, float addrspace(5)* %TEMP12.x
  %456 = bitcast float %455 to i32
  %457 = add i32 %456, 1
  %458 = bitcast i32 %457 to float
  store float %458, float addrspace(5)* %TEMP12.x
  br label %loop14

endloop29:                                        ; preds = %if16
  %459 = load float, float addrspace(5)* %TEMP11.x
  %460 = load float, float addrspace(5)* %TEMP11.y
  %461 = load float, float addrspace(5)* %TEMP11.z
  %462 = load float, float addrspace(5)* %TEMP11.w
  store float %459, float addrspace(5)* %OUT1.x
  store float %460, float addrspace(5)* %OUT1.y
  store float %461, float addrspace(5)* %OUT1.z
  store float %462, float addrspace(5)* %OUT1.w
  %463 = trunc i32 %7 to i1
  br i1 %463, label %if-true-block, label %endif-block

if-true-block:                                    ; preds = %endloop29
  %464 = load float, float addrspace(5)* %OUT1.x
  %465 = call nsz float @llvm.maxnum.f32(float %464, float 0.000000e+00) #2
  %466 = call nsz float @llvm.minnum.f32(float %465, float 1.000000e+00) #2
  store float %466, float addrspace(5)* %OUT1.x
  %467 = load float, float addrspace(5)* %OUT1.y
  %468 = call nsz float @llvm.maxnum.f32(float %467, float 0.000000e+00) #2
  %469 = call nsz float @llvm.minnum.f32(float %468, float 1.000000e+00) #2
  store float %469, float addrspace(5)* %OUT1.y
  %470 = load float, float addrspace(5)* %OUT1.z
  %471 = call nsz float @llvm.maxnum.f32(float %470, float 0.000000e+00) #2
  %472 = call nsz float @llvm.minnum.f32(float %471, float 1.000000e+00) #2
  store float %472, float addrspace(5)* %OUT1.z
  %473 = load float, float addrspace(5)* %OUT1.w
  %474 = call nsz float @llvm.maxnum.f32(float %473, float 0.000000e+00) #2
  %475 = call nsz float @llvm.minnum.f32(float %474, float 1.000000e+00) #2
  store float %475, float addrspace(5)* %OUT1.w
  br label %endif-block

endif-block:                                      ; preds = %endloop29, %if-true-block
  %476 = load float, float addrspace(5)* %OUT0.x
  %477 = load float, float addrspace(5)* %OUT0.y
  %478 = load float, float addrspace(5)* %OUT0.z
  %479 = load float, float addrspace(5)* %OUT0.w
  %480 = load float, float addrspace(5)* %OUT1.x
  %481 = load float, float addrspace(5)* %OUT1.y
  %482 = load float, float addrspace(5)* %OUT1.z
  %483 = load float, float addrspace(5)* %OUT1.w
  call void @llvm.amdgcn.exp.f32(i32 12, i32 15, float %476, float %477, float %478, float %479, i1 true, i1 false) #4
  call void @llvm.amdgcn.exp.f32(i32 32, i32 15, float %480, float %481, float %482, float %483, i1 false, i1 false) #4
  ret void
}

; Function Attrs: nounwind readonly
declare <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32>, i32, i32, i1, i1) #1

; Function Attrs: nounwind readnone
declare float @llvm.SI.load.const.v4i32(<4 x i32>, i32) #2

; Function Attrs: nounwind readnone speculatable
declare float @llvm.maxnum.f32(float, float) #3

; Function Attrs: nounwind readnone speculatable
declare float @llvm.minnum.f32(float, float) #3

; Function Attrs: nounwind
declare void @llvm.amdgcn.exp.f32(i32, i32, float, float, float, float, i1, i1) #4

; Function Attrs: alwaysinline
define private amdgpu_vs <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> @vs_prolog(i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32, i32, i32, i32) #0 {
main_body:
  %13 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> undef, i32 %0, 0
  %14 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %13, i32 %1, 1
  %15 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %14, i32 %2, 2
  %16 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %15, i32 %3, 3
  %17 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %16, i32 %4, 4
  %18 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %17, i32 %5, 5
  %19 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %18, i32 %6, 6
  %20 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %19, i32 %7, 7
  %21 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %20, i32 %8, 8
  %22 = bitcast i32 %9 to float
  %23 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %21, float %22, 9
  %24 = bitcast i32 %10 to float
  %25 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %23, float %24, 10
  %26 = bitcast i32 %11 to float
  %27 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %25, float %26, 11
  %28 = bitcast i32 %12 to float
  %29 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %27, float %28, 12
  %30 = add i32 %9, %4
  %31 = bitcast i32 %30 to float
  %32 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %29, float %31, 13
  ret <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %32
}

define amdgpu_vs void @wrapper([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x float] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32 inreg, i32 inreg, i32 inreg, i32 inreg, [0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32, i32, i32, i32) #5 {
main_body:
  %13 = ptrtoint [0 x <4 x i32>] addrspace(6)* %0 to i32
  %14 = ptrtoint [0 x <8 x i32>] addrspace(6)* %1 to i32
  %15 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %16 = ptrtoint [0 x <8 x i32>] addrspace(6)* %3 to i32
  %17 = ptrtoint [0 x <4 x i32>] addrspace(6)* %8 to i32
  %18 = bitcast i32 %9 to float
  %19 = bitcast i32 %10 to float
  %20 = bitcast i32 %11 to float
  %21 = bitcast i32 %12 to float
  %22 = bitcast float %18 to i32
  %23 = bitcast float %19 to i32
  %24 = bitcast float %20 to i32
  %25 = bitcast float %21 to i32
  %26 = call <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> @vs_prolog(i32 %13, i32 %14, i32 %15, i32 %16, i32 %4, i32 %5, i32 %6, i32 %7, i32 %17, i32 %22, i32 %23, i32 %24, i32 %25)
  %27 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 0
  %28 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 1
  %29 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 2
  %30 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 3
  %31 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 4
  %32 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 5
  %33 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 6
  %34 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 7
  %35 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 8
  %36 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 9
  %37 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 10
  %38 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 11
  %39 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 12
  %40 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 13
  %41 = inttoptr i32 %27 to [0 x <4 x i32>] addrspace(6)*
  %42 = inttoptr i32 %28 to [0 x <8 x i32>] addrspace(6)*
  %43 = inttoptr i32 %29 to [0 x float] addrspace(6)*
  %44 = inttoptr i32 %30 to [0 x <8 x i32>] addrspace(6)*
  %45 = inttoptr i32 %35 to [0 x <4 x i32>] addrspace(6)*
  %46 = bitcast float %36 to i32
  %47 = bitcast float %37 to i32
  %48 = bitcast float %38 to i32
  %49 = bitcast float %39 to i32
  %50 = bitcast float %40 to i32
  call void @main([0 x <4 x i32>] addrspace(6)* %41, [0 x <8 x i32>] addrspace(6)* %42, [0 x float] addrspace(6)* %43, [0 x <8 x i32>] addrspace(6)* %44, i32 %31, i32 %32, i32 %33, i32 %34, [0 x <4 x i32>] addrspace(6)* %45, i32 %46, i32 %47, i32 %48, i32 %49, i32 %50)
  ret void
}

attributes #0 = { alwaysinline "no-signed-zeros-fp-math"="true" }
attributes #1 = { nounwind readonly }
attributes #2 = { nounwind readnone }
attributes #3 = { nounwind readnone speculatable }
attributes #4 = { nounwind }
attributes #5 = { "no-signed-zeros-fp-math"="true" }

!0 = !{}
