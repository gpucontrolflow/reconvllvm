; ModuleID = 'tgsi'
source_filename = "tgsi"
target datalayout = "e-p:64:64-p1:64:64-p2:32:32-p3:32:32-p4:64:64-p5:32:32-p6:32:32-i64:64-v16:16-v24:32-v32:32-v48:64-v96:128-v192:256-v256:256-v512:512-v1024:1024-v2048:2048-n32:64-S32-A5"
target triple = "amdgcn--"

; Function Attrs: alwaysinline
define private amdgpu_ps <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> @main([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), float inreg, i32 inreg, <2 x i32>, <2 x i32>, <2 x i32>, <3 x i32>, <2 x i32>, <2 x i32>, <2 x i32>, float, float, float, float, float, i32, i32, float, i32, float, float, float, float) #0 {
main_body:
  %OUT0.w = alloca float, addrspace(5)
  %OUT0.z = alloca float, addrspace(5)
  %OUT0.y = alloca float, addrspace(5)
  %OUT0.x = alloca float, addrspace(5)
  store float %22, float addrspace(5)* %OUT0.x
  store float %23, float addrspace(5)* %OUT0.y
  store float %24, float addrspace(5)* %OUT0.z
  store float %25, float addrspace(5)* %OUT0.w
  %26 = load float, float addrspace(5)* %OUT0.x
  %27 = load float, float addrspace(5)* %OUT0.y
  %28 = load float, float addrspace(5)* %OUT0.z
  %29 = load float, float addrspace(5)* %OUT0.w
  %30 = bitcast float %4 to i32
  %31 = insertvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> undef, i32 %30, 4
  %32 = insertvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %31, float %26, 5
  %33 = insertvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %32, float %27, 6
  %34 = insertvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %33, float %28, 7
  %35 = insertvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %34, float %29, 8
  %36 = insertvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %35, float %20, 19
  ret <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %36
}

; Function Attrs: alwaysinline
define private amdgpu_ps <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> @ps_prolog(i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float) #0 {
main_body:
  %30 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> undef, i32 %0, 0
  %31 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %30, i32 %1, 1
  %32 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %31, i32 %2, 2
  %33 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %32, i32 %3, 3
  %34 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %33, i32 %4, 4
  %35 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %34, i32 %5, 5
  %36 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %35, float %6, 6
  %37 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %36, float %7, 7
  %38 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %37, float %8, 8
  %39 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %38, float %9, 9
  %40 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %39, float %10, 10
  %41 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %40, float %11, 11
  %42 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %41, float %12, 12
  %43 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %42, float %13, 13
  %44 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %43, float %14, 14
  %45 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %44, float %15, 15
  %46 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %45, float %16, 16
  %47 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %46, float %17, 17
  %48 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %47, float %18, 18
  %49 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %48, float %19, 19
  %50 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %49, float %20, 20
  %51 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %50, float %21, 21
  %52 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %51, float %22, 22
  %53 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, float %23, 23
  %54 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %53, float %24, 24
  %55 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %54, float %25, 25
  %56 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %55, float %26, 26
  %57 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %56, float %27, 27
  %58 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %57, float %28, 28
  %59 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %58, float %29, 29
  %60 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %59, 8
  %61 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %59, 9
  %62 = insertelement <2 x float> undef, float %60, i32 0
  %63 = insertelement <2 x float> %62, float %61, i32 1
  %64 = extractelement <2 x float> %63, i32 0
  %65 = extractelement <2 x float> %63, i32 1
  %66 = call nsz float @llvm.amdgcn.interp.p1(float %64, i32 0, i32 0, i32 %5) #5
  %67 = call nsz float @llvm.amdgcn.interp.p2(float %66, float %65, i32 0, i32 0, i32 %5) #5
  %68 = call nsz float @llvm.amdgcn.interp.p1(float %64, i32 1, i32 0, i32 %5) #5
  %69 = call nsz float @llvm.amdgcn.interp.p2(float %68, float %65, i32 1, i32 0, i32 %5) #5
  %70 = call nsz float @llvm.amdgcn.interp.p1(float %64, i32 2, i32 0, i32 %5) #5
  %71 = call nsz float @llvm.amdgcn.interp.p2(float %70, float %65, i32 2, i32 0, i32 %5) #5
  %72 = call nsz float @llvm.amdgcn.interp.p1(float %64, i32 3, i32 0, i32 %5) #5
  %73 = call nsz float @llvm.amdgcn.interp.p2(float %72, float %65, i32 3, i32 0, i32 %5) #5
  %74 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %59, float %67, 30
  %75 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %74, float %69, 31
  %76 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %75, float %71, 32
  %77 = insertvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %76, float %73, 33
  ret <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %77
}

; Function Attrs: nounwind readnone speculatable
declare float @llvm.amdgcn.interp.p1(float, i32, i32, i32) #1

; Function Attrs: nounwind readnone speculatable
declare float @llvm.amdgcn.interp.p2(float, float, i32, i32, i32) #1

; Function Attrs: alwaysinline
define private amdgpu_ps void @ps_epilog(i32 inreg, i32 inreg, i32 inreg, i32 inreg, float inreg, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float) #2 {
main_body:
  %20 = call nsz <2 x half> @llvm.amdgcn.cvt.pkrtz(float %5, float %6) #5
  %21 = bitcast <2 x half> %20 to i32
  %22 = bitcast i32 %21 to float
  %23 = call nsz <2 x half> @llvm.amdgcn.cvt.pkrtz(float %7, float %8) #5
  %24 = bitcast <2 x half> %23 to i32
  %25 = bitcast i32 %24 to float
  %26 = bitcast float %22 to <2 x i16>
  %27 = bitcast float %25 to <2 x i16>
  call void @llvm.amdgcn.exp.compr.v2i16(i32 0, i32 15, <2 x i16> %26, <2 x i16> %27, i1 true, i1 true) #3
  ret void
}

; Function Attrs: nounwind readnone speculatable
declare <2 x half> @llvm.amdgcn.cvt.pkrtz(float, float) #1

; Function Attrs: nounwind
declare void @llvm.amdgcn.exp.compr.v2i16(i32, i32, <2 x i16>, <2 x i16>, i1, i1) #3

define amdgpu_ps void @wrapper([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), float inreg, i32 inreg, <2 x i32>, <2 x i32>, <2 x i32>, <3 x i32>, <2 x i32>, <2 x i32>, <2 x i32>, float, float, float, float, float, i32, i32, float, i32) #4 {
main_body:
  %22 = ptrtoint [0 x <4 x i32>] addrspace(6)* %0 to i32
  %23 = ptrtoint [0 x <8 x i32>] addrspace(6)* %1 to i32
  %24 = ptrtoint [0 x <4 x i32>] addrspace(6)* %2 to i32
  %25 = ptrtoint [0 x <8 x i32>] addrspace(6)* %3 to i32
  %26 = bitcast float %4 to i32
  %27 = bitcast <2 x i32> %6 to <2 x float>
  %28 = extractelement <2 x float> %27, i32 0
  %29 = extractelement <2 x float> %27, i32 1
  %30 = bitcast <2 x i32> %7 to <2 x float>
  %31 = extractelement <2 x float> %30, i32 0
  %32 = extractelement <2 x float> %30, i32 1
  %33 = bitcast <2 x i32> %8 to <2 x float>
  %34 = extractelement <2 x float> %33, i32 0
  %35 = extractelement <2 x float> %33, i32 1
  %36 = bitcast <3 x i32> %9 to <3 x float>
  %37 = extractelement <3 x float> %36, i32 0
  %38 = extractelement <3 x float> %36, i32 1
  %39 = extractelement <3 x float> %36, i32 2
  %40 = bitcast <2 x i32> %10 to <2 x float>
  %41 = extractelement <2 x float> %40, i32 0
  %42 = extractelement <2 x float> %40, i32 1
  %43 = bitcast <2 x i32> %11 to <2 x float>
  %44 = extractelement <2 x float> %43, i32 0
  %45 = extractelement <2 x float> %43, i32 1
  %46 = bitcast <2 x i32> %12 to <2 x float>
  %47 = extractelement <2 x float> %46, i32 0
  %48 = extractelement <2 x float> %46, i32 1
  %49 = bitcast i32 %18 to float
  %50 = bitcast i32 %19 to float
  %51 = bitcast i32 %21 to float
  %52 = call <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> @ps_prolog(i32 %22, i32 %23, i32 %24, i32 %25, i32 %26, i32 %5, float %28, float %29, float %31, float %32, float %34, float %35, float %37, float %38, float %39, float %41, float %42, float %44, float %45, float %47, float %48, float %13, float %14, float %15, float %16, float %17, float %49, float %50, float %20, float %51)
  %53 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 0
  %54 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 1
  %55 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 2
  %56 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 3
  %57 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 4
  %58 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 5
  %59 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 6
  %60 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 7
  %61 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 8
  %62 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 9
  %63 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 10
  %64 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 11
  %65 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 12
  %66 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 13
  %67 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 14
  %68 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 15
  %69 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 16
  %70 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 17
  %71 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 18
  %72 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 19
  %73 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 20
  %74 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 21
  %75 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 22
  %76 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 23
  %77 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 24
  %78 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 25
  %79 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 26
  %80 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 27
  %81 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 28
  %82 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 29
  %83 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 30
  %84 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 31
  %85 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 32
  %86 = extractvalue <{ i32, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %52, 33
  %87 = inttoptr i32 %53 to [0 x <4 x i32>] addrspace(6)*
  %88 = inttoptr i32 %54 to [0 x <8 x i32>] addrspace(6)*
  %89 = inttoptr i32 %55 to [0 x <4 x i32>] addrspace(6)*
  %90 = inttoptr i32 %56 to [0 x <8 x i32>] addrspace(6)*
  %91 = bitcast i32 %57 to float
  %92 = insertelement <2 x float> undef, float %59, i32 0
  %93 = insertelement <2 x float> %92, float %60, i32 1
  %94 = bitcast <2 x float> %93 to <2 x i32>
  %95 = insertelement <2 x float> undef, float %61, i32 0
  %96 = insertelement <2 x float> %95, float %62, i32 1
  %97 = bitcast <2 x float> %96 to <2 x i32>
  %98 = insertelement <2 x float> undef, float %63, i32 0
  %99 = insertelement <2 x float> %98, float %64, i32 1
  %100 = bitcast <2 x float> %99 to <2 x i32>
  %101 = insertelement <3 x float> undef, float %65, i32 0
  %102 = insertelement <3 x float> %101, float %66, i32 1
  %103 = insertelement <3 x float> %102, float %67, i32 2
  %104 = bitcast <3 x float> %103 to <3 x i32>
  %105 = insertelement <2 x float> undef, float %68, i32 0
  %106 = insertelement <2 x float> %105, float %69, i32 1
  %107 = bitcast <2 x float> %106 to <2 x i32>
  %108 = insertelement <2 x float> undef, float %70, i32 0
  %109 = insertelement <2 x float> %108, float %71, i32 1
  %110 = bitcast <2 x float> %109 to <2 x i32>
  %111 = insertelement <2 x float> undef, float %72, i32 0
  %112 = insertelement <2 x float> %111, float %73, i32 1
  %113 = bitcast <2 x float> %112 to <2 x i32>
  %114 = bitcast float %79 to i32
  %115 = bitcast float %80 to i32
  %116 = bitcast float %82 to i32
  %117 = call <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> @main([0 x <4 x i32>] addrspace(6)* %87, [0 x <8 x i32>] addrspace(6)* %88, [0 x <4 x i32>] addrspace(6)* %89, [0 x <8 x i32>] addrspace(6)* %90, float %91, i32 %58, <2 x i32> %94, <2 x i32> %97, <2 x i32> %100, <3 x i32> %104, <2 x i32> %107, <2 x i32> %110, <2 x i32> %113, float %74, float %75, float %76, float %77, float %78, i32 %114, i32 %115, float %81, i32 %116, float %83, float %84, float %85, float %86)
  %118 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 0
  %119 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 1
  %120 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 2
  %121 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 3
  %122 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 4
  %123 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 5
  %124 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 6
  %125 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 7
  %126 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 8
  %127 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 9
  %128 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 10
  %129 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 11
  %130 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 12
  %131 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 13
  %132 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 14
  %133 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 15
  %134 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 16
  %135 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 17
  %136 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 18
  %137 = extractvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %117, 19
  %138 = bitcast i32 %122 to float
  call void @ps_epilog(i32 %118, i32 %119, i32 %120, i32 %121, float %138, float %123, float %124, float %125, float %126, float %127, float %128, float %129, float %130, float %131, float %132, float %133, float %134, float %135, float %136, float %137)
  ret void
}

attributes #0 = { alwaysinline "no-signed-zeros-fp-math"="true" }
attributes #1 = { nounwind readnone speculatable }
attributes #2 = { alwaysinline "InitialPSInputAddr"="0xffffff" "no-signed-zeros-fp-math"="true" }
attributes #3 = { nounwind }
attributes #4 = { "no-signed-zeros-fp-math"="true" }
attributes #5 = { nounwind readnone }
