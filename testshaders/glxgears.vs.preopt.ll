; ModuleID = 'tgsi'
source_filename = "tgsi"
target datalayout = "e-p:64:64-p1:64:64-p2:32:32-p3:32:32-p4:64:64-p5:32:32-p6:32:32-i64:64-v16:16-v24:32-v32:32-v48:64-v96:128-v192:256-v256:256-v512:512-v1024:1024-v2048:2048-n32:64-S32-A5"
target triple = "amdgcn--"

; Function Attrs: alwaysinline
define private amdgpu_vs void @main([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x float] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32 inreg, i32 inreg, i32 inreg, i32 inreg, [0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32, i32, i32, i32, i32) #0 {
main_body:
  %TEMP3.w = alloca float, addrspace(5)
  %TEMP3.z = alloca float, addrspace(5)
  %TEMP3.y = alloca float, addrspace(5)
  %TEMP3.x = alloca float, addrspace(5)
  %TEMP2.w = alloca float, addrspace(5)
  %TEMP2.z = alloca float, addrspace(5)
  %TEMP2.y = alloca float, addrspace(5)
  %TEMP2.x = alloca float, addrspace(5)
  %TEMP1.w = alloca float, addrspace(5)
  %TEMP1.z = alloca float, addrspace(5)
  %TEMP1.y = alloca float, addrspace(5)
  %TEMP1.x = alloca float, addrspace(5)
  %TEMP0.w = alloca float, addrspace(5)
  %TEMP0.z = alloca float, addrspace(5)
  %TEMP0.y = alloca float, addrspace(5)
  %TEMP0.x = alloca float, addrspace(5)
  %OUT1.w = alloca float, addrspace(5)
  %OUT1.z = alloca float, addrspace(5)
  %OUT1.y = alloca float, addrspace(5)
  %OUT1.x = alloca float, addrspace(5)
  %OUT0.w = alloca float, addrspace(5)
  %OUT0.z = alloca float, addrspace(5)
  %OUT0.y = alloca float, addrspace(5)
  %OUT0.x = alloca float, addrspace(5)
  %14 = getelementptr [0 x <4 x i32>], [0 x <4 x i32>] addrspace(6)* %8, i32 0, i32 0, !amdgpu.uniform !0
  %15 = load <4 x i32>, <4 x i32> addrspace(6)* %14, !invariant.load !0
  %16 = call nsz <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32> %15, i32 %13, i32 0, i1 false, i1 false) #2
  %17 = extractelement <4 x float> %16, i32 0
  %18 = extractelement <4 x float> %16, i32 1
  %19 = extractelement <4 x float> %16, i32 2
  %20 = extractelement <4 x float> %16, i32 3
  %21 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %22 = insertelement <4 x i32> undef, i32 %21, i32 0
  %23 = insertelement <4 x i32> %22, i32 0, i32 1
  %24 = insertelement <4 x i32> %23, i32 176, i32 2
  %25 = insertelement <4 x i32> %24, i32 163756, i32 3
  %26 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %25, i32 0)
  %27 = fmul nsz float %17, %26
  %28 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %29 = insertelement <4 x i32> undef, i32 %28, i32 0
  %30 = insertelement <4 x i32> %29, i32 0, i32 1
  %31 = insertelement <4 x i32> %30, i32 176, i32 2
  %32 = insertelement <4 x i32> %31, i32 163756, i32 3
  %33 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %32, i32 4)
  %34 = fmul nsz float %17, %33
  %35 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %36 = insertelement <4 x i32> undef, i32 %35, i32 0
  %37 = insertelement <4 x i32> %36, i32 0, i32 1
  %38 = insertelement <4 x i32> %37, i32 176, i32 2
  %39 = insertelement <4 x i32> %38, i32 163756, i32 3
  %40 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %39, i32 8)
  %41 = fmul nsz float %17, %40
  %42 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %43 = insertelement <4 x i32> undef, i32 %42, i32 0
  %44 = insertelement <4 x i32> %43, i32 0, i32 1
  %45 = insertelement <4 x i32> %44, i32 176, i32 2
  %46 = insertelement <4 x i32> %45, i32 163756, i32 3
  %47 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %46, i32 12)
  %48 = fmul nsz float %17, %47
  store float %27, float addrspace(5)* %TEMP0.x
  store float %34, float addrspace(5)* %TEMP0.y
  store float %41, float addrspace(5)* %TEMP0.z
  store float %48, float addrspace(5)* %TEMP0.w
  %49 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %50 = insertelement <4 x i32> undef, i32 %49, i32 0
  %51 = insertelement <4 x i32> %50, i32 0, i32 1
  %52 = insertelement <4 x i32> %51, i32 176, i32 2
  %53 = insertelement <4 x i32> %52, i32 163756, i32 3
  %54 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %53, i32 16)
  %55 = load float, float addrspace(5)* %TEMP0.x
  %56 = fmul nsz float %18, %54
  %57 = fadd nsz float %56, %55
  %58 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %59 = insertelement <4 x i32> undef, i32 %58, i32 0
  %60 = insertelement <4 x i32> %59, i32 0, i32 1
  %61 = insertelement <4 x i32> %60, i32 176, i32 2
  %62 = insertelement <4 x i32> %61, i32 163756, i32 3
  %63 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %62, i32 20)
  %64 = load float, float addrspace(5)* %TEMP0.y
  %65 = fmul nsz float %18, %63
  %66 = fadd nsz float %65, %64
  %67 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %68 = insertelement <4 x i32> undef, i32 %67, i32 0
  %69 = insertelement <4 x i32> %68, i32 0, i32 1
  %70 = insertelement <4 x i32> %69, i32 176, i32 2
  %71 = insertelement <4 x i32> %70, i32 163756, i32 3
  %72 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %71, i32 24)
  %73 = load float, float addrspace(5)* %TEMP0.z
  %74 = fmul nsz float %18, %72
  %75 = fadd nsz float %74, %73
  %76 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %77 = insertelement <4 x i32> undef, i32 %76, i32 0
  %78 = insertelement <4 x i32> %77, i32 0, i32 1
  %79 = insertelement <4 x i32> %78, i32 176, i32 2
  %80 = insertelement <4 x i32> %79, i32 163756, i32 3
  %81 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %80, i32 28)
  %82 = load float, float addrspace(5)* %TEMP0.w
  %83 = fmul nsz float %18, %81
  %84 = fadd nsz float %83, %82
  store float %57, float addrspace(5)* %TEMP0.x
  store float %66, float addrspace(5)* %TEMP0.y
  store float %75, float addrspace(5)* %TEMP0.z
  store float %84, float addrspace(5)* %TEMP0.w
  %85 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %86 = insertelement <4 x i32> undef, i32 %85, i32 0
  %87 = insertelement <4 x i32> %86, i32 0, i32 1
  %88 = insertelement <4 x i32> %87, i32 176, i32 2
  %89 = insertelement <4 x i32> %88, i32 163756, i32 3
  %90 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %89, i32 32)
  %91 = load float, float addrspace(5)* %TEMP0.x
  %92 = fmul nsz float %19, %90
  %93 = fadd nsz float %92, %91
  %94 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %95 = insertelement <4 x i32> undef, i32 %94, i32 0
  %96 = insertelement <4 x i32> %95, i32 0, i32 1
  %97 = insertelement <4 x i32> %96, i32 176, i32 2
  %98 = insertelement <4 x i32> %97, i32 163756, i32 3
  %99 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %98, i32 36)
  %100 = load float, float addrspace(5)* %TEMP0.y
  %101 = fmul nsz float %19, %99
  %102 = fadd nsz float %101, %100
  %103 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %104 = insertelement <4 x i32> undef, i32 %103, i32 0
  %105 = insertelement <4 x i32> %104, i32 0, i32 1
  %106 = insertelement <4 x i32> %105, i32 176, i32 2
  %107 = insertelement <4 x i32> %106, i32 163756, i32 3
  %108 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %107, i32 40)
  %109 = load float, float addrspace(5)* %TEMP0.z
  %110 = fmul nsz float %19, %108
  %111 = fadd nsz float %110, %109
  %112 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %113 = insertelement <4 x i32> undef, i32 %112, i32 0
  %114 = insertelement <4 x i32> %113, i32 0, i32 1
  %115 = insertelement <4 x i32> %114, i32 176, i32 2
  %116 = insertelement <4 x i32> %115, i32 163756, i32 3
  %117 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %116, i32 44)
  %118 = load float, float addrspace(5)* %TEMP0.w
  %119 = fmul nsz float %19, %117
  %120 = fadd nsz float %119, %118
  store float %93, float addrspace(5)* %TEMP0.x
  store float %102, float addrspace(5)* %TEMP0.y
  store float %111, float addrspace(5)* %TEMP0.z
  store float %120, float addrspace(5)* %TEMP0.w
  %121 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %122 = insertelement <4 x i32> undef, i32 %121, i32 0
  %123 = insertelement <4 x i32> %122, i32 0, i32 1
  %124 = insertelement <4 x i32> %123, i32 176, i32 2
  %125 = insertelement <4 x i32> %124, i32 163756, i32 3
  %126 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %125, i32 48)
  %127 = load float, float addrspace(5)* %TEMP0.x
  %128 = fmul nsz float %20, %126
  %129 = fadd nsz float %128, %127
  %130 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %131 = insertelement <4 x i32> undef, i32 %130, i32 0
  %132 = insertelement <4 x i32> %131, i32 0, i32 1
  %133 = insertelement <4 x i32> %132, i32 176, i32 2
  %134 = insertelement <4 x i32> %133, i32 163756, i32 3
  %135 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %134, i32 52)
  %136 = load float, float addrspace(5)* %TEMP0.y
  %137 = fmul nsz float %20, %135
  %138 = fadd nsz float %137, %136
  %139 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %140 = insertelement <4 x i32> undef, i32 %139, i32 0
  %141 = insertelement <4 x i32> %140, i32 0, i32 1
  %142 = insertelement <4 x i32> %141, i32 176, i32 2
  %143 = insertelement <4 x i32> %142, i32 163756, i32 3
  %144 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %143, i32 56)
  %145 = load float, float addrspace(5)* %TEMP0.z
  %146 = fmul nsz float %20, %144
  %147 = fadd nsz float %146, %145
  %148 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %149 = insertelement <4 x i32> undef, i32 %148, i32 0
  %150 = insertelement <4 x i32> %149, i32 0, i32 1
  %151 = insertelement <4 x i32> %150, i32 176, i32 2
  %152 = insertelement <4 x i32> %151, i32 163756, i32 3
  %153 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %152, i32 60)
  %154 = load float, float addrspace(5)* %TEMP0.w
  %155 = fmul nsz float %20, %153
  %156 = fadd nsz float %155, %154
  store float %129, float addrspace(5)* %OUT0.x
  store float %138, float addrspace(5)* %OUT0.y
  store float %147, float addrspace(5)* %OUT0.z
  store float %156, float addrspace(5)* %OUT0.w
  %157 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %158 = insertelement <4 x i32> undef, i32 %157, i32 0
  %159 = insertelement <4 x i32> %158, i32 0, i32 1
  %160 = insertelement <4 x i32> %159, i32 176, i32 2
  %161 = insertelement <4 x i32> %160, i32 163756, i32 3
  %162 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %161, i32 64)
  %163 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %164 = insertelement <4 x i32> undef, i32 %163, i32 0
  %165 = insertelement <4 x i32> %164, i32 0, i32 1
  %166 = insertelement <4 x i32> %165, i32 176, i32 2
  %167 = insertelement <4 x i32> %166, i32 163756, i32 3
  %168 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %167, i32 68)
  %169 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %170 = insertelement <4 x i32> undef, i32 %169, i32 0
  %171 = insertelement <4 x i32> %170, i32 0, i32 1
  %172 = insertelement <4 x i32> %171, i32 176, i32 2
  %173 = insertelement <4 x i32> %172, i32 163756, i32 3
  %174 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %173, i32 72)
  %175 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %176 = insertelement <4 x i32> undef, i32 %175, i32 0
  %177 = insertelement <4 x i32> %176, i32 0, i32 1
  %178 = insertelement <4 x i32> %177, i32 176, i32 2
  %179 = insertelement <4 x i32> %178, i32 163756, i32 3
  %180 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %179, i32 64)
  %181 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %182 = insertelement <4 x i32> undef, i32 %181, i32 0
  %183 = insertelement <4 x i32> %182, i32 0, i32 1
  %184 = insertelement <4 x i32> %183, i32 176, i32 2
  %185 = insertelement <4 x i32> %184, i32 163756, i32 3
  %186 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %185, i32 68)
  %187 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %188 = insertelement <4 x i32> undef, i32 %187, i32 0
  %189 = insertelement <4 x i32> %188, i32 0, i32 1
  %190 = insertelement <4 x i32> %189, i32 176, i32 2
  %191 = insertelement <4 x i32> %190, i32 163756, i32 3
  %192 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %191, i32 72)
  %193 = fmul nsz float %162, %180
  %194 = fmul nsz float %168, %186
  %195 = fadd nsz float %194, %193
  %196 = fmul nsz float %174, %192
  %197 = fadd nsz float %195, %196
  store float %197, float addrspace(5)* %TEMP1.x
  %198 = load float, float addrspace(5)* %TEMP1.x
  %199 = call nsz float @llvm.fabs.f32(float %198) #4
  %200 = call nsz float @llvm.sqrt.f32(float %199) #2
  %201 = fdiv nsz float 1.000000e+00, %200, !fpmath !1
  store float %201, float addrspace(5)* %TEMP1.x
  %202 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %203 = insertelement <4 x i32> undef, i32 %202, i32 0
  %204 = insertelement <4 x i32> %203, i32 0, i32 1
  %205 = insertelement <4 x i32> %204, i32 176, i32 2
  %206 = insertelement <4 x i32> %205, i32 163756, i32 3
  %207 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %206, i32 64)
  %208 = load float, float addrspace(5)* %TEMP1.x
  %209 = fmul nsz float %207, %208
  %210 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %211 = insertelement <4 x i32> undef, i32 %210, i32 0
  %212 = insertelement <4 x i32> %211, i32 0, i32 1
  %213 = insertelement <4 x i32> %212, i32 176, i32 2
  %214 = insertelement <4 x i32> %213, i32 163756, i32 3
  %215 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %214, i32 68)
  %216 = load float, float addrspace(5)* %TEMP1.x
  %217 = fmul nsz float %215, %216
  %218 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %219 = insertelement <4 x i32> undef, i32 %218, i32 0
  %220 = insertelement <4 x i32> %219, i32 0, i32 1
  %221 = insertelement <4 x i32> %220, i32 176, i32 2
  %222 = insertelement <4 x i32> %221, i32 163756, i32 3
  %223 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %222, i32 72)
  %224 = load float, float addrspace(5)* %TEMP1.x
  %225 = fmul nsz float %223, %224
  %226 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %227 = insertelement <4 x i32> undef, i32 %226, i32 0
  %228 = insertelement <4 x i32> %227, i32 0, i32 1
  %229 = insertelement <4 x i32> %228, i32 176, i32 2
  %230 = insertelement <4 x i32> %229, i32 163756, i32 3
  %231 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %230, i32 76)
  %232 = load float, float addrspace(5)* %TEMP1.x
  %233 = fmul nsz float %231, %232
  store float %209, float addrspace(5)* %TEMP0.x
  store float %217, float addrspace(5)* %TEMP0.y
  store float %225, float addrspace(5)* %TEMP0.z
  store float %233, float addrspace(5)* %TEMP0.w
  %234 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %235 = insertelement <4 x i32> undef, i32 %234, i32 0
  %236 = insertelement <4 x i32> %235, i32 0, i32 1
  %237 = insertelement <4 x i32> %236, i32 176, i32 2
  %238 = insertelement <4 x i32> %237, i32 163756, i32 3
  %239 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %238, i32 80)
  %240 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %241 = insertelement <4 x i32> undef, i32 %240, i32 0
  %242 = insertelement <4 x i32> %241, i32 0, i32 1
  %243 = insertelement <4 x i32> %242, i32 176, i32 2
  %244 = insertelement <4 x i32> %243, i32 163756, i32 3
  %245 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %244, i32 84)
  %246 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %247 = insertelement <4 x i32> undef, i32 %246, i32 0
  %248 = insertelement <4 x i32> %247, i32 0, i32 1
  %249 = insertelement <4 x i32> %248, i32 176, i32 2
  %250 = insertelement <4 x i32> %249, i32 163756, i32 3
  %251 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %250, i32 88)
  %252 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %253 = insertelement <4 x i32> undef, i32 %252, i32 0
  %254 = insertelement <4 x i32> %253, i32 0, i32 1
  %255 = insertelement <4 x i32> %254, i32 176, i32 2
  %256 = insertelement <4 x i32> %255, i32 163756, i32 3
  %257 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %256, i32 92)
  store float %239, float addrspace(5)* %TEMP2.x
  store float %245, float addrspace(5)* %TEMP2.y
  store float %251, float addrspace(5)* %TEMP2.z
  store float %257, float addrspace(5)* %TEMP2.w
  %258 = load float, float addrspace(5)* %TEMP2.x
  %259 = load float, float addrspace(5)* %TEMP2.y
  %260 = load float, float addrspace(5)* %TEMP2.z
  %261 = load float, float addrspace(5)* %TEMP2.w
  store float %258, float addrspace(5)* %OUT1.x
  store float %259, float addrspace(5)* %OUT1.y
  store float %260, float addrspace(5)* %OUT1.z
  store float %261, float addrspace(5)* %OUT1.w
  %262 = load float, float addrspace(5)* %TEMP0.x
  %263 = load float, float addrspace(5)* %TEMP0.y
  %264 = load float, float addrspace(5)* %TEMP0.z
  %265 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %266 = insertelement <4 x i32> undef, i32 %265, i32 0
  %267 = insertelement <4 x i32> %266, i32 0, i32 1
  %268 = insertelement <4 x i32> %267, i32 176, i32 2
  %269 = insertelement <4 x i32> %268, i32 163756, i32 3
  %270 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %269, i32 96)
  %271 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %272 = insertelement <4 x i32> undef, i32 %271, i32 0
  %273 = insertelement <4 x i32> %272, i32 0, i32 1
  %274 = insertelement <4 x i32> %273, i32 176, i32 2
  %275 = insertelement <4 x i32> %274, i32 163756, i32 3
  %276 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %275, i32 100)
  %277 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %278 = insertelement <4 x i32> undef, i32 %277, i32 0
  %279 = insertelement <4 x i32> %278, i32 0, i32 1
  %280 = insertelement <4 x i32> %279, i32 176, i32 2
  %281 = insertelement <4 x i32> %280, i32 163756, i32 3
  %282 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %281, i32 104)
  %283 = fmul nsz float %262, %270
  %284 = fmul nsz float %263, %276
  %285 = fadd nsz float %284, %283
  %286 = fmul nsz float %264, %282
  %287 = fadd nsz float %285, %286
  store float %287, float addrspace(5)* %TEMP3.x
  store float %287, float addrspace(5)* %TEMP3.y
  store float %287, float addrspace(5)* %TEMP3.z
  store float %287, float addrspace(5)* %TEMP3.w
  %288 = load float, float addrspace(5)* %TEMP3.x
  %289 = call nsz float @llvm.maxnum.f32(float 0.000000e+00, float %288) #2
  %290 = load float, float addrspace(5)* %TEMP3.y
  %291 = call nsz float @llvm.maxnum.f32(float 0.000000e+00, float %290) #2
  %292 = load float, float addrspace(5)* %TEMP3.z
  %293 = call nsz float @llvm.maxnum.f32(float 0.000000e+00, float %292) #2
  %294 = load float, float addrspace(5)* %TEMP3.w
  %295 = call nsz float @llvm.maxnum.f32(float 1.000000e+00, float %294) #2
  store float %289, float addrspace(5)* %TEMP1.x
  store float %291, float addrspace(5)* %TEMP1.y
  store float %293, float addrspace(5)* %TEMP1.z
  store float %295, float addrspace(5)* %TEMP1.w
  %296 = load float, float addrspace(5)* %TEMP3.z
  %297 = fcmp nsz olt float 0.000000e+00, %296
  %298 = select i1 %297, float 1.000000e+00, float 0.000000e+00
  store float %298, float addrspace(5)* %TEMP1.z
  %299 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %300 = insertelement <4 x i32> undef, i32 %299, i32 0
  %301 = insertelement <4 x i32> %300, i32 0, i32 1
  %302 = insertelement <4 x i32> %301, i32 176, i32 2
  %303 = insertelement <4 x i32> %302, i32 163756, i32 3
  %304 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %303, i32 128)
  %305 = load float, float addrspace(5)* %TEMP2.x
  %306 = fadd nsz float %304, %305
  %307 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %308 = insertelement <4 x i32> undef, i32 %307, i32 0
  %309 = insertelement <4 x i32> %308, i32 0, i32 1
  %310 = insertelement <4 x i32> %309, i32 176, i32 2
  %311 = insertelement <4 x i32> %310, i32 163756, i32 3
  %312 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %311, i32 132)
  %313 = load float, float addrspace(5)* %TEMP2.y
  %314 = fadd nsz float %312, %313
  %315 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %316 = insertelement <4 x i32> undef, i32 %315, i32 0
  %317 = insertelement <4 x i32> %316, i32 0, i32 1
  %318 = insertelement <4 x i32> %317, i32 176, i32 2
  %319 = insertelement <4 x i32> %318, i32 163756, i32 3
  %320 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %319, i32 136)
  %321 = load float, float addrspace(5)* %TEMP2.z
  %322 = fadd nsz float %320, %321
  %323 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %324 = insertelement <4 x i32> undef, i32 %323, i32 0
  %325 = insertelement <4 x i32> %324, i32 0, i32 1
  %326 = insertelement <4 x i32> %325, i32 176, i32 2
  %327 = insertelement <4 x i32> %326, i32 163756, i32 3
  %328 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %327, i32 140)
  %329 = load float, float addrspace(5)* %TEMP2.w
  %330 = fadd nsz float %328, %329
  store float %306, float addrspace(5)* %TEMP2.x
  store float %314, float addrspace(5)* %TEMP2.y
  store float %322, float addrspace(5)* %TEMP2.z
  store float %330, float addrspace(5)* %TEMP2.w
  %331 = load float, float addrspace(5)* %TEMP1.y
  %332 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %333 = insertelement <4 x i32> undef, i32 %332, i32 0
  %334 = insertelement <4 x i32> %333, i32 0, i32 1
  %335 = insertelement <4 x i32> %334, i32 176, i32 2
  %336 = insertelement <4 x i32> %335, i32 163756, i32 3
  %337 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %336, i32 144)
  %338 = load float, float addrspace(5)* %TEMP2.x
  %339 = fmul nsz float %331, %337
  %340 = fadd nsz float %339, %338
  %341 = load float, float addrspace(5)* %TEMP1.y
  %342 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %343 = insertelement <4 x i32> undef, i32 %342, i32 0
  %344 = insertelement <4 x i32> %343, i32 0, i32 1
  %345 = insertelement <4 x i32> %344, i32 176, i32 2
  %346 = insertelement <4 x i32> %345, i32 163756, i32 3
  %347 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %346, i32 148)
  %348 = load float, float addrspace(5)* %TEMP2.y
  %349 = fmul nsz float %341, %347
  %350 = fadd nsz float %349, %348
  %351 = load float, float addrspace(5)* %TEMP1.y
  %352 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %353 = insertelement <4 x i32> undef, i32 %352, i32 0
  %354 = insertelement <4 x i32> %353, i32 0, i32 1
  %355 = insertelement <4 x i32> %354, i32 176, i32 2
  %356 = insertelement <4 x i32> %355, i32 163756, i32 3
  %357 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %356, i32 152)
  %358 = load float, float addrspace(5)* %TEMP2.z
  %359 = fmul nsz float %351, %357
  %360 = fadd nsz float %359, %358
  %361 = load float, float addrspace(5)* %TEMP1.y
  %362 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %363 = insertelement <4 x i32> undef, i32 %362, i32 0
  %364 = insertelement <4 x i32> %363, i32 0, i32 1
  %365 = insertelement <4 x i32> %364, i32 176, i32 2
  %366 = insertelement <4 x i32> %365, i32 163756, i32 3
  %367 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %366, i32 156)
  %368 = load float, float addrspace(5)* %TEMP2.w
  %369 = fmul nsz float %361, %367
  %370 = fadd nsz float %369, %368
  store float %340, float addrspace(5)* %TEMP2.x
  store float %350, float addrspace(5)* %TEMP2.y
  store float %360, float addrspace(5)* %TEMP2.z
  store float %370, float addrspace(5)* %TEMP2.w
  %371 = load float, float addrspace(5)* %TEMP1.z
  %372 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %373 = insertelement <4 x i32> undef, i32 %372, i32 0
  %374 = insertelement <4 x i32> %373, i32 0, i32 1
  %375 = insertelement <4 x i32> %374, i32 176, i32 2
  %376 = insertelement <4 x i32> %375, i32 163756, i32 3
  %377 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %376, i32 160)
  %378 = load float, float addrspace(5)* %TEMP2.x
  %379 = fmul nsz float %371, %377
  %380 = fadd nsz float %379, %378
  %381 = load float, float addrspace(5)* %TEMP1.z
  %382 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %383 = insertelement <4 x i32> undef, i32 %382, i32 0
  %384 = insertelement <4 x i32> %383, i32 0, i32 1
  %385 = insertelement <4 x i32> %384, i32 176, i32 2
  %386 = insertelement <4 x i32> %385, i32 163756, i32 3
  %387 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %386, i32 164)
  %388 = load float, float addrspace(5)* %TEMP2.y
  %389 = fmul nsz float %381, %387
  %390 = fadd nsz float %389, %388
  %391 = load float, float addrspace(5)* %TEMP1.z
  %392 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %393 = insertelement <4 x i32> undef, i32 %392, i32 0
  %394 = insertelement <4 x i32> %393, i32 0, i32 1
  %395 = insertelement <4 x i32> %394, i32 176, i32 2
  %396 = insertelement <4 x i32> %395, i32 163756, i32 3
  %397 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %396, i32 168)
  %398 = load float, float addrspace(5)* %TEMP2.z
  %399 = fmul nsz float %391, %397
  %400 = fadd nsz float %399, %398
  store float %380, float addrspace(5)* %OUT1.x
  store float %390, float addrspace(5)* %OUT1.y
  store float %400, float addrspace(5)* %OUT1.z
  %401 = trunc i32 %7 to i1
  br i1 %401, label %if-true-block, label %endif-block

if-true-block:                                    ; preds = %main_body
  %402 = load float, float addrspace(5)* %OUT1.x
  %403 = call nsz float @llvm.maxnum.f32(float %402, float 0.000000e+00) #2
  %404 = call nsz float @llvm.minnum.f32(float %403, float 1.000000e+00) #2
  store float %404, float addrspace(5)* %OUT1.x
  %405 = load float, float addrspace(5)* %OUT1.y
  %406 = call nsz float @llvm.maxnum.f32(float %405, float 0.000000e+00) #2
  %407 = call nsz float @llvm.minnum.f32(float %406, float 1.000000e+00) #2
  store float %407, float addrspace(5)* %OUT1.y
  %408 = load float, float addrspace(5)* %OUT1.z
  %409 = call nsz float @llvm.maxnum.f32(float %408, float 0.000000e+00) #2
  %410 = call nsz float @llvm.minnum.f32(float %409, float 1.000000e+00) #2
  store float %410, float addrspace(5)* %OUT1.z
  %411 = load float, float addrspace(5)* %OUT1.w
  %412 = call nsz float @llvm.maxnum.f32(float %411, float 0.000000e+00) #2
  %413 = call nsz float @llvm.minnum.f32(float %412, float 1.000000e+00) #2
  store float %413, float addrspace(5)* %OUT1.w
  br label %endif-block

endif-block:                                      ; preds = %main_body, %if-true-block
  %414 = load float, float addrspace(5)* %OUT0.x
  %415 = load float, float addrspace(5)* %OUT0.y
  %416 = load float, float addrspace(5)* %OUT0.z
  %417 = load float, float addrspace(5)* %OUT0.w
  %418 = load float, float addrspace(5)* %OUT1.x
  %419 = load float, float addrspace(5)* %OUT1.y
  %420 = load float, float addrspace(5)* %OUT1.z
  %421 = load float, float addrspace(5)* %OUT1.w
  call void @llvm.amdgcn.exp.f32(i32 12, i32 15, float %414, float %415, float %416, float %417, i1 true, i1 false) #4
  call void @llvm.amdgcn.exp.f32(i32 32, i32 15, float %418, float %419, float %420, float %421, i1 false, i1 false) #4
  ret void
}

; Function Attrs: nounwind readonly
declare <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32>, i32, i32, i1, i1) #1

; Function Attrs: nounwind readnone
declare float @llvm.SI.load.const.v4i32(<4 x i32>, i32) #2

; Function Attrs: nounwind readnone speculatable
declare float @llvm.fabs.f32(float) #3

; Function Attrs: nounwind readnone speculatable
declare float @llvm.sqrt.f32(float) #3

; Function Attrs: nounwind readnone speculatable
declare float @llvm.maxnum.f32(float, float) #3

; Function Attrs: nounwind readnone speculatable
declare float @llvm.minnum.f32(float, float) #3

; Function Attrs: nounwind
declare void @llvm.amdgcn.exp.f32(i32, i32, float, float, float, float, i1, i1) #4

; Function Attrs: alwaysinline
define private amdgpu_vs <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> @vs_prolog(i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32 inreg, i32, i32, i32, i32) #0 {
main_body:
  %13 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> undef, i32 %0, 0
  %14 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %13, i32 %1, 1
  %15 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %14, i32 %2, 2
  %16 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %15, i32 %3, 3
  %17 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %16, i32 %4, 4
  %18 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %17, i32 %5, 5
  %19 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %18, i32 %6, 6
  %20 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %19, i32 %7, 7
  %21 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %20, i32 %8, 8
  %22 = bitcast i32 %9 to float
  %23 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %21, float %22, 9
  %24 = bitcast i32 %10 to float
  %25 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %23, float %24, 10
  %26 = bitcast i32 %11 to float
  %27 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %25, float %26, 11
  %28 = bitcast i32 %12 to float
  %29 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %27, float %28, 12
  %30 = add i32 %9, %4
  %31 = bitcast i32 %30 to float
  %32 = insertvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %29, float %31, 13
  ret <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %32
}

define amdgpu_vs void @wrapper([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x float] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32 inreg, i32 inreg, i32 inreg, i32 inreg, [0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32, i32, i32, i32) #5 {
main_body:
  %13 = ptrtoint [0 x <4 x i32>] addrspace(6)* %0 to i32
  %14 = ptrtoint [0 x <8 x i32>] addrspace(6)* %1 to i32
  %15 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %16 = ptrtoint [0 x <8 x i32>] addrspace(6)* %3 to i32
  %17 = ptrtoint [0 x <4 x i32>] addrspace(6)* %8 to i32
  %18 = bitcast i32 %9 to float
  %19 = bitcast i32 %10 to float
  %20 = bitcast i32 %11 to float
  %21 = bitcast i32 %12 to float
  %22 = bitcast float %18 to i32
  %23 = bitcast float %19 to i32
  %24 = bitcast float %20 to i32
  %25 = bitcast float %21 to i32
  %26 = call <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> @vs_prolog(i32 %13, i32 %14, i32 %15, i32 %16, i32 %4, i32 %5, i32 %6, i32 %7, i32 %17, i32 %22, i32 %23, i32 %24, i32 %25)
  %27 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 0
  %28 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 1
  %29 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 2
  %30 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 3
  %31 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 4
  %32 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 5
  %33 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 6
  %34 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 7
  %35 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 8
  %36 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 9
  %37 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 10
  %38 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 11
  %39 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 12
  %40 = extractvalue <{ i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float }> %26, 13
  %41 = inttoptr i32 %27 to [0 x <4 x i32>] addrspace(6)*
  %42 = inttoptr i32 %28 to [0 x <8 x i32>] addrspace(6)*
  %43 = inttoptr i32 %29 to [0 x float] addrspace(6)*
  %44 = inttoptr i32 %30 to [0 x <8 x i32>] addrspace(6)*
  %45 = inttoptr i32 %35 to [0 x <4 x i32>] addrspace(6)*
  %46 = bitcast float %36 to i32
  %47 = bitcast float %37 to i32
  %48 = bitcast float %38 to i32
  %49 = bitcast float %39 to i32
  %50 = bitcast float %40 to i32
  call void @main([0 x <4 x i32>] addrspace(6)* %41, [0 x <8 x i32>] addrspace(6)* %42, [0 x float] addrspace(6)* %43, [0 x <8 x i32>] addrspace(6)* %44, i32 %31, i32 %32, i32 %33, i32 %34, [0 x <4 x i32>] addrspace(6)* %45, i32 %46, i32 %47, i32 %48, i32 %49, i32 %50)
  ret void
}

attributes #0 = { alwaysinline "no-signed-zeros-fp-math"="true" }
attributes #1 = { nounwind readonly }
attributes #2 = { nounwind readnone }
attributes #3 = { nounwind readnone speculatable }
attributes #4 = { nounwind }
attributes #5 = { "no-signed-zeros-fp-math"="true" }

!0 = !{}
!1 = !{float 2.500000e+00}
