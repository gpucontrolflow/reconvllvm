; ModuleID = 'tgsi'
source_filename = "tgsi"
target datalayout = "e-p:64:64-p1:64:64-p2:32:32-p3:32:32-p4:64:64-p5:32:32-p6:32:32-i64:64-v16:16-v24:32-v32:32-v48:64-v96:128-v192:256-v256:256-v512:512-v1024:1024-v2048:2048-n32:64-S32-A5"
target triple = "amdgcn--"

; Function Attrs: nounwind readonly
declare <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32>, i32, i32, i1, i1) #0

; Function Attrs: nounwind readnone
declare float @llvm.SI.load.const.v4i32(<4 x i32>, i32) #1

; Function Attrs: nounwind readnone speculatable
declare float @llvm.maxnum.f32(float, float) #2

; Function Attrs: nounwind readnone speculatable
declare float @llvm.minnum.f32(float, float) #2

; Function Attrs: nounwind
declare void @llvm.amdgcn.exp.f32(i32, i32, float, float, float, float, i1, i1) #3

define amdgpu_vs void @wrapper([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x float] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32 inreg, i32 inreg, i32 inreg, i32 inreg, [0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32, i32, i32, i32) #4 {
main_body:
  %13 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %14 = add i32 %9, %4
  %15 = getelementptr [0 x <4 x i32>], [0 x <4 x i32>] addrspace(6)* %8, i32 0, i32 0, !amdgpu.uniform !0
  %16 = load <4 x i32>, <4 x i32> addrspace(6)* %15, align 16, !invariant.load !0, !alias.scope !1, !noalias !4
  %17 = call nsz <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32> %16, i32 %14, i32 0, i1 false, i1 false) #1
  %18 = extractelement <4 x float> %17, i32 0
  %19 = extractelement <4 x float> %17, i32 1
  %20 = extractelement <4 x float> %17, i32 2
  %21 = extractelement <4 x float> %17, i32 3
  %22 = insertelement <4 x i32> <i32 undef, i32 0, i32 48, i32 163756>, i32 %13, i32 0
  %23 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %22, i32 0)
  %24 = bitcast float %23 to i32
  %25 = icmp eq i32 %24, 1
  %26 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %22, i32 16)
  %27 = bitcast float %26 to i32
  %28 = icmp eq i32 %27, 1
  %29 = and i1 %25, %28
  %30 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %22, i32 32)
  %31 = bitcast float %30 to i32
  %32 = icmp eq i32 %31, 1
  %33 = and i1 %25, %32
  %not..i = xor i1 %33, true
  %34 = and i1 %29, %not..i
  %35 = select i1 %34, float 0.000000e+00, float 1.000000e+00
  %36 = or i1 %33, %29
  %37 = select i1 %36, float 1.000000e+00, float 0.000000e+00
  %38 = and i32 %7, 1
  %39 = icmp eq i32 %38, 0
  br i1 %39, label %main.exit, label %if-true-block.i

if-true-block.i:                                  ; preds = %main_body
  %40 = call nsz float @llvm.maxnum.f32(float %35, float 0.000000e+00) #1
  %41 = call nsz float @llvm.minnum.f32(float %40, float 1.000000e+00) #1
  %42 = call nsz float @llvm.maxnum.f32(float %37, float 0.000000e+00) #1
  %43 = call nsz float @llvm.minnum.f32(float %42, float 1.000000e+00) #1
  br label %main.exit

main.exit:                                        ; preds = %main_body, %if-true-block.i
  %OUT1.y.0.i = phi float [ %43, %if-true-block.i ], [ %37, %main_body ]
  %OUT1.x.0.i = phi float [ %41, %if-true-block.i ], [ %35, %main_body ]
  call void @llvm.amdgcn.exp.f32(i32 12, i32 15, float %18, float %19, float %20, float %21, i1 true, i1 false) #3, !noalias !1
  call void @llvm.amdgcn.exp.f32(i32 32, i32 15, float %OUT1.x.0.i, float %OUT1.y.0.i, float 0.000000e+00, float 1.000000e+00, i1 false, i1 false) #3, !noalias !1
  ret void
}

attributes #0 = { nounwind readonly }
attributes #1 = { nounwind readnone }
attributes #2 = { nounwind readnone speculatable }
attributes #3 = { nounwind }
attributes #4 = { "no-signed-zeros-fp-math"="true" }

!0 = !{}
!1 = !{!2}
!2 = distinct !{!2, !3, !"main: argument 1"}
!3 = distinct !{!3, !"main"}
!4 = !{!5}
!5 = distinct !{!5, !3, !"main: argument 0"}
