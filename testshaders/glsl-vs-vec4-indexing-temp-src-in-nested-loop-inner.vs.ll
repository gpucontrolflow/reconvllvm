; ModuleID = 'tgsi'
source_filename = "tgsi"
target datalayout = "e-p:64:64-p1:64:64-p2:32:32-p3:32:32-p4:64:64-p5:32:32-p6:32:32-i64:64-v16:16-v24:32-v32:32-v48:64-v96:128-v192:256-v256:256-v512:512-v1024:1024-v2048:2048-n32:64-S32-A5"
target triple = "amdgcn--"

; Function Attrs: nounwind readonly
declare <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32>, i32, i32, i1, i1) #0

; Function Attrs: nounwind readnone
declare float @llvm.SI.load.const.v4i32(<4 x i32>, i32) #1

; Function Attrs: nounwind readnone speculatable
declare float @llvm.maxnum.f32(float, float) #2

; Function Attrs: nounwind readnone speculatable
declare float @llvm.minnum.f32(float, float) #2

; Function Attrs: nounwind
declare void @llvm.amdgcn.exp.f32(i32, i32, float, float, float, float, i1, i1) #3

define amdgpu_vs void @wrapper([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x float] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32 inreg, i32 inreg, i32 inreg, i32 inreg, [0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), i32, i32, i32, i32) #4 {
main_body:
  %array.i = alloca [32 x float], align 4, addrspace(5)
  %13 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %14 = add i32 %9, %4
  %15 = bitcast [32 x float] addrspace(5)* %array.i to i8 addrspace(5)*
  call void @llvm.lifetime.start.p5i8(i64 128, i8 addrspace(5)* %15)
  %16 = getelementptr [0 x <4 x i32>], [0 x <4 x i32>] addrspace(6)* %8, i32 0, i32 0, !amdgpu.uniform !0
  %17 = load <4 x i32>, <4 x i32> addrspace(6)* %16, align 16, !invariant.load !0, !alias.scope !1, !noalias !4
  %18 = call nsz <4 x float> @llvm.amdgcn.buffer.load.format.v4f32(<4 x i32> %17, i32 %14, i32 0, i1 false, i1 false) #1
  %19 = extractelement <4 x float> %18, i32 0
  %20 = extractelement <4 x float> %18, i32 1
  %21 = extractelement <4 x float> %18, i32 2
  %22 = extractelement <4 x float> %18, i32 3
  %TEMP0.x.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 0
  %TEMP0.y.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 1
  %TEMP0.z.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 2
  %TEMP0.w.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 3
  %TEMP1.x.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 4
  %TEMP1.y.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 5
  %TEMP1.z.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 6
  %TEMP1.w.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 7
  %TEMP2.x.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 8
  %TEMP2.y.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 9
  %TEMP2.z.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 10
  %TEMP2.w.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 11
  %TEMP3.x.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 12
  %TEMP3.y.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 13
  %TEMP3.z.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 14
  %TEMP3.w.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 15
  %TEMP4.x.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 16
  %TEMP4.y.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 17
  %TEMP4.z.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 18
  %TEMP4.w.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 19
  %TEMP5.x.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 20
  %TEMP5.y.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 21
  %TEMP5.z.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 22
  %TEMP5.w.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 23
  %TEMP6.x.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 24
  %TEMP6.y.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 25
  %TEMP6.z.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 26
  %TEMP6.w.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 27
  %TEMP7.x.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 28
  %TEMP7.y.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 29
  %TEMP7.z.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 30
  %TEMP7.w.i = getelementptr inbounds [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 31
  %23 = insertelement <4 x i32> <i32 undef, i32 0, i32 112, i32 163756>, i32 %13, i32 0
  %24 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 0)
  %25 = fmul nsz float %24, 0x3FB99999A0000000
  %26 = fmul nsz float %24, 0.000000e+00
  store float %25, float addrspace(5)* %TEMP0.x.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP0.y.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP0.z.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP0.w.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP1.x.i, align 4, !noalias !6
  store float %25, float addrspace(5)* %TEMP1.y.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP1.z.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP1.w.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP2.x.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP2.y.i, align 4, !noalias !6
  store float %25, float addrspace(5)* %TEMP2.z.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP2.w.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP3.x.i, align 4, !noalias !6
  store float %25, float addrspace(5)* %TEMP3.y.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP3.z.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP3.w.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP4.x.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP4.y.i, align 4, !noalias !6
  store float %25, float addrspace(5)* %TEMP4.z.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP4.w.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP5.x.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP5.y.i, align 4, !noalias !6
  store float %25, float addrspace(5)* %TEMP5.z.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP5.w.i, align 4, !noalias !6
  store float %25, float addrspace(5)* %TEMP6.x.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP6.y.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP6.z.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP6.w.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP7.x.i, align 4, !noalias !6
  store float %25, float addrspace(5)* %TEMP7.y.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP7.z.i, align 4, !noalias !6
  store float %26, float addrspace(5)* %TEMP7.w.i, align 4, !noalias !6
  %27 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 48)
  %28 = fmul nsz float %27, %19
  %29 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 52)
  %30 = fmul nsz float %29, %19
  %31 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 56)
  %32 = fmul nsz float %31, %19
  %33 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 60)
  %34 = fmul nsz float %33, %19
  %35 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 64)
  %36 = fmul nsz float %35, %20
  %37 = fadd nsz float %36, %28
  %38 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 68)
  %39 = fmul nsz float %38, %20
  %40 = fadd nsz float %39, %30
  %41 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 72)
  %42 = fmul nsz float %41, %20
  %43 = fadd nsz float %42, %32
  %44 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 76)
  %45 = fmul nsz float %44, %20
  %46 = fadd nsz float %45, %34
  %47 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 80)
  %48 = fmul nsz float %47, %21
  %49 = fadd nsz float %48, %37
  %50 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 84)
  %51 = fmul nsz float %50, %21
  %52 = fadd nsz float %51, %40
  %53 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 88)
  %54 = fmul nsz float %53, %21
  %55 = fadd nsz float %54, %43
  %56 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 92)
  %57 = fmul nsz float %56, %21
  %58 = fadd nsz float %57, %46
  %59 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 96)
  %60 = fmul nsz float %59, %22
  %61 = fadd nsz float %60, %49
  %62 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 100)
  %63 = fmul nsz float %62, %22
  %64 = fadd nsz float %63, %52
  %65 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 104)
  %66 = fmul nsz float %65, %22
  %67 = fadd nsz float %66, %55
  %68 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 108)
  %69 = fmul nsz float %68, %22
  %70 = fadd nsz float %69, %58
  %71 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 16)
  %72 = bitcast float %71 to i32
  br label %loop14.i

loop14.i:                                         ; preds = %endloop27.i, %main_body
  %73 = phi i32 [ 0, %main_body ], [ %97, %endloop27.i ]
  %TEMP11.w.0.i = phi float [ 0.000000e+00, %main_body ], [ %TEMP11.w.1.i, %endloop27.i ]
  %TEMP11.z.0.i = phi float [ 0.000000e+00, %main_body ], [ %TEMP11.z.1.i, %endloop27.i ]
  %TEMP11.y.0.i = phi float [ 0.000000e+00, %main_body ], [ %TEMP11.y.1.i, %endloop27.i ]
  %TEMP11.x.0.i = phi float [ 0.000000e+00, %main_body ], [ %TEMP11.x.1.i, %endloop27.i ]
  %74 = icmp slt i32 %73, %72
  br i1 %74, label %endif18.i, label %endloop29.i

endif18.i:                                        ; preds = %loop14.i
  %75 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %23, i32 32)
  %76 = bitcast float %75 to i32
  br label %loop20.i

loop20.i:                                         ; preds = %endif24.i, %endif18.i
  %TEMP11.w.1.i = phi float [ %TEMP11.w.0.i, %endif18.i ], [ %95, %endif24.i ]
  %77 = phi i32 [ 0, %endif18.i ], [ %96, %endif24.i ]
  %TEMP11.z.1.i = phi float [ %TEMP11.z.0.i, %endif18.i ], [ %91, %endif24.i ]
  %TEMP11.y.1.i = phi float [ %TEMP11.y.0.i, %endif18.i ], [ %87, %endif24.i ]
  %TEMP11.x.1.i = phi float [ %TEMP11.x.0.i, %endif18.i ], [ %83, %endif24.i ]
  %78 = icmp slt i32 %77, %76
  br i1 %78, label %endif24.i, label %endloop27.i

endif24.i:                                        ; preds = %loop20.i
  %79 = shl i32 %77, 2
  %80 = and i32 %79, 28
  %81 = getelementptr [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 %80
  %82 = load float, float addrspace(5)* %81, align 4, !noalias !6
  %83 = fadd nsz float %TEMP11.x.1.i, %82
  %84 = or i32 %80, 1
  %85 = getelementptr [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 %84
  %86 = load float, float addrspace(5)* %85, align 4, !noalias !6
  %87 = fadd nsz float %TEMP11.y.1.i, %86
  %88 = or i32 %80, 2
  %89 = getelementptr [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 %88
  %90 = load float, float addrspace(5)* %89, align 4, !noalias !6
  %91 = fadd nsz float %TEMP11.z.1.i, %90
  %92 = or i32 %80, 3
  %93 = getelementptr [32 x float], [32 x float] addrspace(5)* %array.i, i32 0, i32 %92
  %94 = load float, float addrspace(5)* %93, align 4, !noalias !6
  %95 = fadd nsz float %TEMP11.w.1.i, %94
  %96 = add i32 %77, 1
  br label %loop20.i

endloop27.i:                                      ; preds = %loop20.i
  %97 = add i32 %73, 1
  br label %loop14.i

endloop29.i:                                      ; preds = %loop14.i
  %98 = and i32 %7, 1
  %99 = icmp eq i32 %98, 0
  br i1 %99, label %main.exit, label %if-true-block.i

if-true-block.i:                                  ; preds = %endloop29.i
  %100 = call nsz float @llvm.maxnum.f32(float %TEMP11.x.0.i, float 0.000000e+00) #1
  %101 = call nsz float @llvm.minnum.f32(float %100, float 1.000000e+00) #1
  %102 = call nsz float @llvm.maxnum.f32(float %TEMP11.y.0.i, float 0.000000e+00) #1
  %103 = call nsz float @llvm.minnum.f32(float %102, float 1.000000e+00) #1
  %104 = call nsz float @llvm.maxnum.f32(float %TEMP11.z.0.i, float 0.000000e+00) #1
  %105 = call nsz float @llvm.minnum.f32(float %104, float 1.000000e+00) #1
  %106 = call nsz float @llvm.maxnum.f32(float %TEMP11.w.0.i, float 0.000000e+00) #1
  %107 = call nsz float @llvm.minnum.f32(float %106, float 1.000000e+00) #1
  br label %main.exit

main.exit:                                        ; preds = %endloop29.i, %if-true-block.i
  %OUT1.w.0.i = phi float [ %107, %if-true-block.i ], [ %TEMP11.w.0.i, %endloop29.i ]
  %OUT1.z.0.i = phi float [ %105, %if-true-block.i ], [ %TEMP11.z.0.i, %endloop29.i ]
  %OUT1.y.0.i = phi float [ %103, %if-true-block.i ], [ %TEMP11.y.0.i, %endloop29.i ]
  %OUT1.x.0.i = phi float [ %101, %if-true-block.i ], [ %TEMP11.x.0.i, %endloop29.i ]
  call void @llvm.amdgcn.exp.f32(i32 12, i32 15, float %61, float %64, float %67, float %70, i1 true, i1 false) #3, !noalias !1
  call void @llvm.amdgcn.exp.f32(i32 32, i32 15, float %OUT1.x.0.i, float %OUT1.y.0.i, float %OUT1.z.0.i, float %OUT1.w.0.i, i1 false, i1 false) #3, !noalias !1
  call void @llvm.lifetime.end.p5i8(i64 128, i8 addrspace(5)* %15)
  ret void
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start.p5i8(i64, i8 addrspace(5)* nocapture) #5

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end.p5i8(i64, i8 addrspace(5)* nocapture) #5

attributes #0 = { nounwind readonly }
attributes #1 = { nounwind readnone }
attributes #2 = { nounwind readnone speculatable }
attributes #3 = { nounwind }
attributes #4 = { "no-signed-zeros-fp-math"="true" }
attributes #5 = { argmemonly nounwind }

!0 = !{}
!1 = !{!2}
!2 = distinct !{!2, !3, !"main: argument 1"}
!3 = distinct !{!3, !"main"}
!4 = !{!5}
!5 = distinct !{!5, !3, !"main: argument 0"}
!6 = !{!5, !2}
