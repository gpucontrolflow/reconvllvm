; ModuleID = 'tgsi'
source_filename = "tgsi"
target datalayout = "e-p:64:64-p1:64:64-p2:32:32-p3:32:32-p4:64:64-p5:32:32-p6:32:32-i64:64-v16:16-v24:32-v32:32-v48:64-v96:128-v192:256-v256:256-v512:512-v1024:1024-v2048:2048-n32:64-S32-A5"
target triple = "amdgcn--"

define amdgpu_ps <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> @main([0 x <4 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x float] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), [0 x <8 x i32>] addrspace(6)* inreg noalias dereferenceable(18446744073709551615), float inreg, i32 inreg, <2 x i32>, <2 x i32>, <2 x i32>, <3 x i32>, <2 x i32>, <2 x i32>, <2 x i32>, float, float, float, float, float, i32, i32, float, i32) #0 {
main_body:
  %22 = bitcast <2 x i32> %7 to <2 x float>
  %23 = extractelement <2 x float> %22, i32 0
  %24 = extractelement <2 x float> %22, i32 1
  %25 = call nsz float @llvm.amdgcn.interp.p1(float %23, i32 0, i32 0, i32 %5) #2
  %26 = call nsz float @llvm.amdgcn.interp.p2(float %25, float %24, i32 0, i32 0, i32 %5) #2
  %27 = fadd nsz float %26, -5.000000e-01
  %28 = call nsz float @llvm.amdgcn.interp.p1(float %23, i32 1, i32 0, i32 %5) #2
  %29 = call nsz float @llvm.amdgcn.interp.p2(float %28, float %24, i32 1, i32 0, i32 %5) #2
  %30 = fadd nsz float %29, -5.000000e-01
  %31 = ptrtoint [0 x float] addrspace(6)* %2 to i32
  %32 = insertelement <4 x i32> <i32 undef, i32 0, i32 80, i32 163756>, i32 %31, i32 0
  %33 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %32, i32 64)
  %34 = fmul nsz float %27, %33
  %35 = fmul nsz float %30, %33
  %36 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %32, i32 0)
  %37 = fadd nsz float %34, %36
  %38 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %32, i32 16)
  %39 = fadd nsz float %35, %38
  %40 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %32, i32 32)
  %41 = bitcast float %40 to i32
  br label %loop7

loop7:                                            ; preds = %endif17, %main_body
  %42 = phi i32 [ 0, %main_body ], [ %56, %endif17 ]
  %43 = phi float [ 0.000000e+00, %main_body ], [ %phitmp, %endif17 ]
  %.01 = phi float [ %39, %main_body ], [ %55, %endif17 ]
  %.0 = phi float [ %37, %main_body ], [ %53, %endif17 ]
  %44 = icmp slt i32 %42, %41
  br i1 %44, label %endif11, label %endloop24

endif11:                                          ; preds = %loop7
  %45 = fmul nsz float %.0, %.0
  %46 = fmul nsz float %.01, %.01
  %47 = fadd nsz float %45, %46
  %48 = fcmp nsz ogt float %47, 4.000000e+00
  br i1 %48, label %endloop24, label %endif17

endif17:                                          ; preds = %endif11
  %49 = fsub nsz float %45, %46
  %50 = fmul nsz float %.0, 2.000000e+00
  %51 = fmul nsz float %50, %.01
  %52 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %32, i32 48)
  %53 = fadd nsz float %49, %52
  %54 = call nsz float @llvm.SI.load.const.v4i32(<4 x i32> %32, i32 52)
  %55 = fadd nsz float %51, %54
  %56 = add i32 %42, 1
  %phitmp = sitofp i32 %56 to float
  br label %loop7

endloop24:                                        ; preds = %loop7, %endif11
  %57 = icmp eq i32 %42, %41
  %58 = select i1 %57, float 0.000000e+00, float %43
  %59 = sitofp i32 %41 to float
  %60 = fdiv nsz float 1.000000e+00, %59, !fpmath !0
  %61 = fmul nsz float %58, %60
  %62 = getelementptr [0 x <8 x i32>], [0 x <8 x i32>] addrspace(6)* %3, i32 0, i32 16, !amdgpu.uniform !1
  %63 = load <8 x i32>, <8 x i32> addrspace(6)* %62, align 32, !invariant.load !1
  %64 = bitcast [0 x <8 x i32>] addrspace(6)* %3 to [0 x <4 x i32>] addrspace(6)*
  %65 = getelementptr [0 x <4 x i32>], [0 x <4 x i32>] addrspace(6)* %64, i32 0, i32 35, !amdgpu.uniform !1
  %66 = load <4 x i32>, <4 x i32> addrspace(6)* %65, align 16, !invariant.load !1
  %67 = call nsz <4 x float> @llvm.amdgcn.image.sample.v4f32.f32.v8i32(float %61, <8 x i32> %63, <4 x i32> %66, i32 15, i1 false, i1 false, i1 false, i1 false, i1 false) #2
  %68 = extractelement <4 x float> %67, i32 0
  %69 = extractelement <4 x float> %67, i32 1
  %70 = extractelement <4 x float> %67, i32 2
  %71 = extractelement <4 x float> %67, i32 3
  %72 = bitcast float %4 to i32
  %73 = insertvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> undef, i32 %72, 4
  %74 = insertvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %73, float %68, 5
  %75 = insertvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %74, float %69, 6
  %76 = insertvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %75, float %70, 7
  %77 = insertvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %76, float %71, 8
  %78 = insertvalue <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %77, float %20, 19
  ret <{ i32, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float }> %78
}

; Function Attrs: nounwind readnone speculatable
declare float @llvm.amdgcn.interp.p1(float, i32, i32, i32) #1

; Function Attrs: nounwind readnone speculatable
declare float @llvm.amdgcn.interp.p2(float, float, i32, i32, i32) #1

; Function Attrs: nounwind readnone
declare float @llvm.SI.load.const.v4i32(<4 x i32>, i32) #2

; Function Attrs: nounwind readonly
declare <4 x float> @llvm.amdgcn.image.sample.v4f32.f32.v8i32(float, <8 x i32>, <4 x i32>, i32, i1, i1, i1, i1, i1) #3

attributes #0 = { "InitialPSInputAddr"="0xb077" "no-signed-zeros-fp-math"="true" }
attributes #1 = { nounwind readnone speculatable }
attributes #2 = { nounwind readnone }
attributes #3 = { nounwind readonly }

!0 = !{float 2.500000e+00}
!1 = !{}
