@echo off
copy /y .\add_shl.ll ..\LLVM\test\CodeGen\AMDGPU\add_shl.ll
python.exe ..\llvmbuild\Release\bin\llvm-lit.py ..\LLVM\test\CodeGen\AMDGPU\add_shl.ll -vv

copy /y .\add3.ll ..\LLVM\test\CodeGen\AMDGPU\add3.ll
python.exe ..\llvmbuild\Release\bin\llvm-lit.py ..\LLVM\test\CodeGen\AMDGPU\add3.ll -vv

copy /y .\and_or.ll ..\LLVM\test\CodeGen\AMDGPU\and_or.ll
python.exe ..\llvmbuild\Release\bin\llvm-lit.py ..\LLVM\test\CodeGen\AMDGPU\and_or.ll -vv

copy /y .\or3.ll ..\LLVM\test\CodeGen\AMDGPU\or3.ll
python.exe ..\llvmbuild\Release\bin\llvm-lit.py ..\LLVM\test\CodeGen\AMDGPU\or3.ll -vv

copy /y .\shl_add.ll ..\LLVM\test\CodeGen\AMDGPU\shl_add.ll
python.exe ..\llvmbuild\Release\bin\llvm-lit.py ..\LLVM\test\CodeGen\AMDGPU\shl_add.ll -vv

copy /y .\shl_or.ll ..\LLVM\test\CodeGen\AMDGPU\shl_or.ll
python.exe ..\llvmbuild\Release\bin\llvm-lit.py ..\LLVM\test\CodeGen\AMDGPU\shl_or.ll -vv
pause
