;RUN: llc < %s -march=amdgcn -mcpu=gfx900 | FileCheck -check-prefix=GCN %s

; ===================================================================================
; V_LSHL_ADD_U32
; ===================================================================================

; GCN-LABEL: {{^}}shl_add:
; GCN: s_lshl_b32 s[[REG:[0-9]+]], {{s[0-9]+, s[0-9]+}}
; GCN: s_add_i32 s[[REG:[0-9]+]], {{s[0-9]+, s[0-9]+}}
define amdgpu_kernel void @shl_add(i32 addrspace(1)* %out, i32 %a, i32 %b, i32 %c) {
  %x = shl i32 %a, %b
  %result = add i32 %x, %c
  store i32 %result, i32 addrspace(1)* %out
  ret void
}

; ThreeOp instruction variant not used due to Constant Bus Limitations 
; GCN-LABEL: {{^}}shl_add_vgpr_a:
; GCN: v_lshlrev_b32_e32 v0, s0, v0
; GCN: v_add_u32_e32 v0, s1, v0
define amdgpu_ps float @shl_add_vgpr_a(i32 %a, i32 inreg %b, i32 inreg %c) {
  %x = shl i32 %a, %b
  %result = add i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}shl_add_vgpr_all:
; GCN: v_lshl_add_u32 v0, v0, v1, v2
define amdgpu_ps float @shl_add_vgpr_all(i32 %a, i32 %b, i32 %c) {
  %x = shl i32 %a, %b
  %result = add i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}shl_add_vgpr_ab:
; GCN: v_lshl_add_u32 v0, v0, v1, s0
define amdgpu_ps float @shl_add_vgpr_ab(i32 %a, i32 %b, i32 inreg %c) {
  %x = shl i32 %a, %b
  %result = add i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}shl_add_vgpr_const:
; GCN: v_lshl_add_u32 v0, v0, 3, v1
define amdgpu_ps float @shl_add_vgpr_const(i32 %a, i32 %b) {
  %x = shl i32 %a, 3
  %result = add i32 %x, %b
  %bc = bitcast i32 %result to float
  ret float %bc
}