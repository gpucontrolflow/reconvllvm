;RUN: llc < %s -march=amdgcn -mcpu=gfx900 | FileCheck -check-prefix=GCN %s

; ===================================================================================
; V_OR3_B32
; ===================================================================================

; GCN-LABEL: {{^}}or3:
; GCN: s_or_b32 s[[REG:[0-9]+]], {{s[0-9]+, s[0-9]+}}
; GCN: s_or_b32 s[[REG:[0-9]+]], {{s[0-9]+, s[0-9]+}}
define amdgpu_kernel void @or3(i32 addrspace(1)* %out, i32 %a, i32 %b, i32 %c) {
  %x = or i32 %a, %b
  %result = or i32 %x, %c
  store i32 %result, i32 addrspace(1)* %out
  ret void
}

; ThreeOp instruction variant not used due to Constant Bus Limitations
; TODO: with reassociation it is possible to replace a v_or_b32_e32 with a s_or_b32
; GCN-LABEL: {{^}}or3_vgpr_a:
; GCN: v_or_b32_e32 v0, s0, v0
; GCN: v_or_b32_e32 v0, s1, v0
define amdgpu_ps float @or3_vgpr_a(i32 %a, i32 inreg %b, i32 inreg %c) {
  %x = or i32 %a, %b
  %result = or i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}or3_vgpr_all:
; GCN: v_or3_b32 v0, v0, v1, v2
define amdgpu_ps float @or3_vgpr_all(i32 %a, i32 %b, i32 %c) {
  %x = or i32 %a, %b
  %result = or i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}or3_vgpr_all2:
; GCN: v_or3_b32 v0, v1, v2, v0
define amdgpu_ps float @or3_vgpr_all2(i32 %a, i32 %b, i32 %c) {
  %x = or i32 %b, %c
  %result = or i32 %x, %a
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}or3_vgpr_bc:
; GCN: v_or3_b32 v0, s0, v0, v1
define amdgpu_ps float @or3_vgpr_bc(i32 inreg %a, i32 %b, i32 %c) {
  %x = or i32 %a, %b
  %result = or i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}or3_vgpr_const:
; GCN: v_or3_b32 v0, v1, v0, 64
define amdgpu_ps float @or3_vgpr_const(i32 %a, i32 %b) {
  %x = or i32 64, %b
  %result = or i32 %x, %a
  %bc = bitcast i32 %result to float
  ret float %bc
}