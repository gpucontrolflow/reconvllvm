;RUN: llc < %s -march=amdgcn -mcpu=gfx900 | FileCheck -check-prefix=GCN %s

; ===================================================================================
; V_ADD3_U32
; ===================================================================================

; GCN-LABEL: {{^}}add3:
; GCN: s_add_i32 s[[REG:[0-9]+]], {{s[0-9]+, s[0-9]+}}
; GCN: s_add_i32 s[[REG:[0-9]+]], {{s[0-9]+, s[0-9]+}}
define amdgpu_kernel void @add3(i32 addrspace(1)* %out, i32 %a, i32 %b, i32 %c) {
  %x = add i32 %a, %b
  %result = add i32 %x, %c
  store i32 %result, i32 addrspace(1)* %out
  ret void
}

; ThreeOp instruction variant not used due to Constant Bus Limitations
; TODO: with reassociation it is possible to replace a v_add_u32_e32 with a s_add_i32
; GCN-LABEL: {{^}}add3_vgpr_b:
; GCN: v_add_u32_e32 v0, s0, v0
; GCN: v_add_u32_e32 v0, s1, v0
define amdgpu_ps float @add3_vgpr_b(i32 inreg %a, i32 %b, i32 inreg %c) {
  %x = add i32 %a, %b
  %result = add i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}add3_vgpr_all:
; GCN: v_add3_u32 v0, v0, v1, v2
define amdgpu_ps float @add3_vgpr_all(i32 %a, i32 %b, i32 %c) {
  %x = add i32 %a, %b
  %result = add i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}add3_vgpr_all2:
; GCN: v_add3_u32 v0, v1, v2, v0
define amdgpu_ps float @add3_vgpr_all2(i32 %a, i32 %b, i32 %c) {
  %x = add i32 %b, %c
  %result = add i32 %x, %a
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}add3_vgpr_bc:
; GCN: v_add3_u32 v0, s0, v0, v1
define amdgpu_ps float @add3_vgpr_bc(i32 inreg %a, i32 %b, i32 %c) {
  %x = add i32 %a, %b
  %result = add i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}add3_vgpr_const:
; GCN: v_add3_u32 v0, v0, v1, 16
define amdgpu_ps float @add3_vgpr_const(i32 %a, i32 %b) {
  %x = add i32 %a, %b
  %result = add i32 %x, 16
  %bc = bitcast i32 %result to float
  ret float %bc
}