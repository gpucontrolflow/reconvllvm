;RUN: llc < %s -march=amdgcn -mcpu=gfx900 | FileCheck -check-prefix=GCN %s

; ===================================================================================
; V_AND_OR_B32
; ===================================================================================

; GCN-LABEL: {{^}}and_or:
; GCN: s_and_b32 s[[REG:[0-9]+]], {{s[0-9]+, s[0-9]+}}
; GCN: s_or_b32 s[[REG:[0-9]+]], {{s[0-9]+, s[0-9]+}}
define amdgpu_kernel void @and_or(i32 addrspace(1)* %out, i32 %a, i32 %b, i32 %c) {
  %x = and i32 %a, %b
  %result = or i32 %x, %c
  store i32 %result, i32 addrspace(1)* %out
  ret void
}

; ThreeOp instruction variant not used due to Constant Bus Limitations
; GCN-LABEL: {{^}}and_or_vgpr_b:
; GCN: v_and_b32_e32 v0, s0, v0
; GCN: v_or_b32_e32 v0, s1, v0
define amdgpu_ps float @and_or_vgpr_b(i32 inreg %a, i32 %b, i32 inreg %c) {
  %x = and i32 %a, %b
  %result = or i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}and_or_vgpr_all:
; GCN: v_and_or_b32 v0, v0, v1, v2
define amdgpu_ps float @and_or_vgpr_all(i32 %a, i32 %b, i32 %c) {
  %x = and i32 %a, %b
  %result = or i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}and_or_vgpr_ab:
; GCN: v_and_or_b32 v0, v0, v1, s0
define amdgpu_ps float @and_or_vgpr_ab(i32 %a, i32 %b, i32 inreg %c) {
  %x = and i32 %a, %b
  %result = or i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}and_or_vgpr_const:
; GCN: v_and_or_b32 v0, v0, 4, v1
define amdgpu_ps float @and_or_vgpr_const(i32 %a, i32 %b) {
  %x = and i32 4, %a
  %result = or i32 %x, %b
  %bc = bitcast i32 %result to float
  ret float %bc
}