;RUN: llc < %s -march=amdgcn -mcpu=gfx900 | FileCheck -check-prefix=GCN %s

; ===================================================================================
; V_LSHL_OR_B32
; ===================================================================================

; GCN-LABEL: {{^}}shl_or:
; GCN: s_lshl_b32 s[[REG:[0-9]+]], {{s[0-9]+, s[0-9]+}}
; GCN: s_or_b32 s[[REG:[0-9]+]], {{s[0-9]+, s[0-9]+}}
define amdgpu_kernel void @shl_or(i32 addrspace(1)* %out, i32 %a, i32 %b, i32 %c) {
  %x = shl i32 %a, %b
  %result = or i32 %x, %c
  store i32 %result, i32 addrspace(1)* %out
  ret void
}

; GCN-LABEL: {{^}}shl_or_vgpr_c:
; GCN: s_lshl_b32 s0, s0, s1
; GCN: v_or_b32_e32 v0, s0, v0
define amdgpu_ps float @shl_or_vgpr_c(i32 inreg %a, i32 inreg %b, i32 %c) {
  %x = shl i32 %a, %b
  %result = or i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}shl_or_vgpr_all:
; GCN: v_lshl_or_b32 v0, v0, v1, v2
define amdgpu_ps float @shl_or_vgpr_all(i32 %a, i32 %b, i32 %c) {
  %x = shl i32 %a, %b
  %result = or i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}shl_or_vgpr_ac:
; GCN: v_lshl_or_b32 v0, v0, s0, v1
define amdgpu_ps float @shl_or_vgpr_ac(i32 %a, i32 inreg %b, i32 %c) {
  %x = shl i32 %a, %b
  %result = or i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}shl_or_vgpr_const:
; GCN: v_lshl_or_b32 v0, v0, v1, 6
define amdgpu_ps float @shl_or_vgpr_const(i32 %a, i32 %b) {
  %x = shl i32 %a, %b
  %result = or i32 %x, 6
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}shl_or_vgpr_const2:
; GCN: v_lshl_or_b32 v0, v0, 6, v1
define amdgpu_ps float @shl_or_vgpr_const2(i32 %a, i32 %b) {
  %x = shl i32 %a, 6
  %result = or i32 %x, %b
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}shl_or_vgpr_const_scalar1:
; GCN: s_lshl_b32 s0, s0, 6
; GCN: v_or_b32_e32 v0, s0, v0
define amdgpu_ps float @shl_or_vgpr_const_scalar1(i32 inreg %a, i32 %b) {
  %x = shl i32 %a, 6
  %result = or i32 %x, %b
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}shl_or_vgpr_const_scalar2:
; GCN: v_lshlrev_b32_e32 v0, 6, v0
; GCN: v_or_b32_e32 v0, s0, v0
define amdgpu_ps float @shl_or_vgpr_const_scalar2(i32 %a, i32 inreg %b) {
  %x = shl i32 %a, 6
  %result = or i32 %x, %b
  %bc = bitcast i32 %result to float
  ret float %bc
}