;RUN: llc < %s -march=amdgcn -mcpu=gfx900 | FileCheck -check-prefix=GCN %s

; ===================================================================================
; V_ADD_LSHL_U32
; ===================================================================================

; GCN-LABEL: {{^}}add_shl:
; GCN: s_add_i32 s[[REG:[0-9]+]], {{s[0-9]+, s[0-9]+}}
; GCN: s_lshl_b32 s[[REG:[0-9]+]], {{s[0-9]+, s[0-9]+}}
define amdgpu_kernel void @add_shl(i32 addrspace(1)* %out, i32 %a, i32 %b, i32 %c) {
  %x = add i32 %a, %b
  %result = shl i32 %x, %c
  store i32 %result, i32 addrspace(1)* %out
  ret void
}

; GCN-LABEL: {{^}}add_shl_vgpr_c:
; GCN: s_add_i32 s0, s0, s1
; GCN: v_lshlrev_b32_e64 v0, v0, s0
define amdgpu_ps float @add_shl_vgpr_c(i32 inreg %a, i32 inreg %b, i32 %c) {
  %x = add i32 %a, %b
  %result = shl i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}add_shl_vgpr_all:
; GCN: v_add_lshl_u32 v0, v0, v1, v2
define amdgpu_ps float @add_shl_vgpr_all(i32 %a, i32 %b, i32 %c) {
  %x = add i32 %a, %b
  %result = shl i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}add_shl_vgpr_ac:
; GCN: v_add_lshl_u32 v0, v0, s0, v1
define amdgpu_ps float @add_shl_vgpr_ac(i32 %a, i32 inreg %b, i32 %c) {
  %x = add i32 %a, %b
  %result = shl i32 %x, %c
  %bc = bitcast i32 %result to float
  ret float %bc
}

; GCN-LABEL: {{^}}add_shl_vgpr_const:
; GCN: v_add_lshl_u32 v0, v0, v1, 9
define amdgpu_ps float @add_shl_vgpr_const(i32 %a, i32 %b) {
  %x = add i32 %a, %b
  %result = shl i32 %x, 9
  %bc = bitcast i32 %result to float
  ret float %bc
}