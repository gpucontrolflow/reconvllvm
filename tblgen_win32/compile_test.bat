@echo off
..\llvmbuild\Release\bin\llc.exe -march=amdgcn -mcpu=gfx900 add_shl.ll
..\llvmbuild\Release\bin\llc.exe -march=amdgcn -mcpu=gfx900 add3.ll
..\llvmbuild\Release\bin\llc.exe -march=amdgcn -mcpu=gfx900 and_or.ll
..\llvmbuild\Release\bin\llc.exe -march=amdgcn -mcpu=gfx900 or3.ll
..\llvmbuild\Release\bin\llc.exe -march=amdgcn -mcpu=gfx900 shl_add.ll
..\llvmbuild\Release\bin\llc.exe -march=amdgcn -mcpu=gfx900 shl_or.ll
pause
