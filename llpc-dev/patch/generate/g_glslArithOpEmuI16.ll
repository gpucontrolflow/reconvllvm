;**********************************************************************************************************************
;*
;*  Trade secret of Advanced Micro Devices, Inc.
;*  Copyright (c) 2017, Advanced Micro Devices, Inc., (unpublished)
;*
;*  All rights reserved. This notice is intended as a precaution against inadvertent publication and does not imply
;*  publication or any waiver of confidentiality. The year included in the foregoing notice is the year of creation of
;*  the work.
;*
;**********************************************************************************************************************

;**********************************************************************************************************************
;* @file  g_glslArithOpEmuI16.ll
;* @brief LLPC LLVM-IR file: contains emulation codes for GLSL arithmetic operations (int16).
;*
;* @note  This file has been generated automatically. Do not hand-modify this file. When changes are needed, modify the
;*        generating script genGlslArithOpEmuCode.py.
;**********************************************************************************************************************

target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v16:16:16-v24:32:32-v32:32:32-v48:64:64-v64:64:64-v96:128:128-v128:128:128-v192:256:256-v256:256:256-v512:512:512-v1024:1024:1024"
target triple = "spir64-unknown-unknown"

; =====================================================================================================================
; >>>  Operators
; =====================================================================================================================

; i16 smod()  =>  llpc.mod.i16
define spir_func i16 @_Z4smodss(
    i16 %x, i16 %y) #0
{
    %1 = call i16 @llpc.mod.i16(i16 %x, i16 %y)

    ret i16 %1
}

; <2 x i16> smod()  =>  llpc.mod.i16
define spir_func <2 x i16> @_Z4smodDv2_sDv2_s(
    <2 x i16> %x, <2 x i16> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i16> %x, i32 0
    %x1 = extractelement <2 x i16> %x, i32 1

    %y0 = extractelement <2 x i16> %y, i32 0
    %y1 = extractelement <2 x i16> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.mod.i16(i16 %x0, i16 %y0)
    %2 = call i16 @llpc.mod.i16(i16 %x1, i16 %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i16>
    %4 = load <2 x i16>, <2 x i16>* %3
    %5 = insertelement <2 x i16> %4, i16 %1, i32 0
    %6 = insertelement <2 x i16> %5, i16 %2, i32 1

    ret <2 x i16> %6
}

; <3 x i16> smod()  =>  llpc.mod.i16
define spir_func <3 x i16> @_Z4smodDv3_sDv3_s(
    <3 x i16> %x, <3 x i16> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i16> %x, i32 0
    %x1 = extractelement <3 x i16> %x, i32 1
    %x2 = extractelement <3 x i16> %x, i32 2

    %y0 = extractelement <3 x i16> %y, i32 0
    %y1 = extractelement <3 x i16> %y, i32 1
    %y2 = extractelement <3 x i16> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.mod.i16(i16 %x0, i16 %y0)
    %2 = call i16 @llpc.mod.i16(i16 %x1, i16 %y1)
    %3 = call i16 @llpc.mod.i16(i16 %x2, i16 %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i16>
    %5 = load <3 x i16>, <3 x i16>* %4
    %6 = insertelement <3 x i16> %5, i16 %1, i32 0
    %7 = insertelement <3 x i16> %6, i16 %2, i32 1
    %8 = insertelement <3 x i16> %7, i16 %3, i32 2

    ret <3 x i16> %8
}

; <4 x i16> smod()  =>  llpc.mod.i16
define spir_func <4 x i16> @_Z4smodDv4_sDv4_s(
    <4 x i16> %x, <4 x i16> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i16> %x, i32 0
    %x1 = extractelement <4 x i16> %x, i32 1
    %x2 = extractelement <4 x i16> %x, i32 2
    %x3 = extractelement <4 x i16> %x, i32 3

    %y0 = extractelement <4 x i16> %y, i32 0
    %y1 = extractelement <4 x i16> %y, i32 1
    %y2 = extractelement <4 x i16> %y, i32 2
    %y3 = extractelement <4 x i16> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.mod.i16(i16 %x0, i16 %y0)
    %2 = call i16 @llpc.mod.i16(i16 %x1, i16 %y1)
    %3 = call i16 @llpc.mod.i16(i16 %x2, i16 %y2)
    %4 = call i16 @llpc.mod.i16(i16 %x3, i16 %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i16>
    %6 = load <4 x i16>, <4 x i16>* %5
    %7 = insertelement <4 x i16> %6, i16 %1, i32 0
    %8 = insertelement <4 x i16> %7, i16 %2, i32 1
    %9 = insertelement <4 x i16> %8, i16 %3, i32 2
    %10 = insertelement <4 x i16> %9, i16 %4, i32 3

    ret <4 x i16> %10
}

declare i16 @llpc.mod.i16(i16, i16) #0

; =====================================================================================================================
; >>>  Common Functions
; =====================================================================================================================

; i16 sabs()  =>  llpc.sabs.i16
define spir_func i16 @_Z4sabss(
    i16 %x) #0
{
    %1 = call i16 @llpc.sabs.i16(i16 %x)

    ret i16 %1
}

; <2 x i16> sabs()  =>  llpc.sabs.i16
define spir_func <2 x i16> @_Z4sabsDv2_s(
    <2 x i16> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i16> %x, i32 0
    %x1 = extractelement <2 x i16> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.sabs.i16(i16 %x0)
    %2 = call i16 @llpc.sabs.i16(i16 %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i16>
    %4 = load <2 x i16>, <2 x i16>* %3
    %5 = insertelement <2 x i16> %4, i16 %1, i32 0
    %6 = insertelement <2 x i16> %5, i16 %2, i32 1

    ret <2 x i16> %6
}

; <3 x i16> sabs()  =>  llpc.sabs.i16
define spir_func <3 x i16> @_Z4sabsDv3_s(
    <3 x i16> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i16> %x, i32 0
    %x1 = extractelement <3 x i16> %x, i32 1
    %x2 = extractelement <3 x i16> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.sabs.i16(i16 %x0)
    %2 = call i16 @llpc.sabs.i16(i16 %x1)
    %3 = call i16 @llpc.sabs.i16(i16 %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i16>
    %5 = load <3 x i16>, <3 x i16>* %4
    %6 = insertelement <3 x i16> %5, i16 %1, i32 0
    %7 = insertelement <3 x i16> %6, i16 %2, i32 1
    %8 = insertelement <3 x i16> %7, i16 %3, i32 2

    ret <3 x i16> %8
}

; <4 x i16> sabs()  =>  llpc.sabs.i16
define spir_func <4 x i16> @_Z4sabsDv4_s(
    <4 x i16> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i16> %x, i32 0
    %x1 = extractelement <4 x i16> %x, i32 1
    %x2 = extractelement <4 x i16> %x, i32 2
    %x3 = extractelement <4 x i16> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.sabs.i16(i16 %x0)
    %2 = call i16 @llpc.sabs.i16(i16 %x1)
    %3 = call i16 @llpc.sabs.i16(i16 %x2)
    %4 = call i16 @llpc.sabs.i16(i16 %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i16>
    %6 = load <4 x i16>, <4 x i16>* %5
    %7 = insertelement <4 x i16> %6, i16 %1, i32 0
    %8 = insertelement <4 x i16> %7, i16 %2, i32 1
    %9 = insertelement <4 x i16> %8, i16 %3, i32 2
    %10 = insertelement <4 x i16> %9, i16 %4, i32 3

    ret <4 x i16> %10
}

declare i16 @llpc.sabs.i16(i16) #0

; i16 ssign()  =>  llpc.ssign.i16
define spir_func i16 @_Z5ssigns(
    i16 %x) #0
{
    %1 = call i16 @llpc.ssign.i16(i16 %x)

    ret i16 %1
}

; <2 x i16> ssign()  =>  llpc.ssign.i16
define spir_func <2 x i16> @_Z5ssignDv2_s(
    <2 x i16> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i16> %x, i32 0
    %x1 = extractelement <2 x i16> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.ssign.i16(i16 %x0)
    %2 = call i16 @llpc.ssign.i16(i16 %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i16>
    %4 = load <2 x i16>, <2 x i16>* %3
    %5 = insertelement <2 x i16> %4, i16 %1, i32 0
    %6 = insertelement <2 x i16> %5, i16 %2, i32 1

    ret <2 x i16> %6
}

; <3 x i16> ssign()  =>  llpc.ssign.i16
define spir_func <3 x i16> @_Z5ssignDv3_s(
    <3 x i16> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i16> %x, i32 0
    %x1 = extractelement <3 x i16> %x, i32 1
    %x2 = extractelement <3 x i16> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.ssign.i16(i16 %x0)
    %2 = call i16 @llpc.ssign.i16(i16 %x1)
    %3 = call i16 @llpc.ssign.i16(i16 %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i16>
    %5 = load <3 x i16>, <3 x i16>* %4
    %6 = insertelement <3 x i16> %5, i16 %1, i32 0
    %7 = insertelement <3 x i16> %6, i16 %2, i32 1
    %8 = insertelement <3 x i16> %7, i16 %3, i32 2

    ret <3 x i16> %8
}

; <4 x i16> ssign()  =>  llpc.ssign.i16
define spir_func <4 x i16> @_Z5ssignDv4_s(
    <4 x i16> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i16> %x, i32 0
    %x1 = extractelement <4 x i16> %x, i32 1
    %x2 = extractelement <4 x i16> %x, i32 2
    %x3 = extractelement <4 x i16> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.ssign.i16(i16 %x0)
    %2 = call i16 @llpc.ssign.i16(i16 %x1)
    %3 = call i16 @llpc.ssign.i16(i16 %x2)
    %4 = call i16 @llpc.ssign.i16(i16 %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i16>
    %6 = load <4 x i16>, <4 x i16>* %5
    %7 = insertelement <4 x i16> %6, i16 %1, i32 0
    %8 = insertelement <4 x i16> %7, i16 %2, i32 1
    %9 = insertelement <4 x i16> %8, i16 %3, i32 2
    %10 = insertelement <4 x i16> %9, i16 %4, i32 3

    ret <4 x i16> %10
}

declare i16 @llpc.ssign.i16(i16) #0

; =====================================================================================================================
; >>>  Functions of extension AMD_shader_trinary_minmax
; =====================================================================================================================

; i16 SMin3AMD()  =>  llpc.smin3.i16
define spir_func i16 @_Z8SMin3AMDsss(
    i16 %x, i16 %y, i16 %z) #0
{
    %1 = call i16 @llpc.smin3.i16(i16 %x, i16 %y, i16 %z)

    ret i16 %1
}

; <2 x i16> SMin3AMD()  =>  llpc.smin3.i16
define spir_func <2 x i16> @_Z8SMin3AMDDv2_sDv2_sDv2_s(
    <2 x i16> %x, <2 x i16> %y, <2 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i16> %x, i32 0
    %x1 = extractelement <2 x i16> %x, i32 1

    %y0 = extractelement <2 x i16> %y, i32 0
    %y1 = extractelement <2 x i16> %y, i32 1

    %z0 = extractelement <2 x i16> %z, i32 0
    %z1 = extractelement <2 x i16> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.smin3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.smin3.i16(i16 %x1, i16 %y1, i16 %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i16>
    %4 = load <2 x i16>, <2 x i16>* %3
    %5 = insertelement <2 x i16> %4, i16 %1, i32 0
    %6 = insertelement <2 x i16> %5, i16 %2, i32 1

    ret <2 x i16> %6
}

; <3 x i16> SMin3AMD()  =>  llpc.smin3.i16
define spir_func <3 x i16> @_Z8SMin3AMDDv3_sDv3_sDv3_s(
    <3 x i16> %x, <3 x i16> %y, <3 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i16> %x, i32 0
    %x1 = extractelement <3 x i16> %x, i32 1
    %x2 = extractelement <3 x i16> %x, i32 2

    %y0 = extractelement <3 x i16> %y, i32 0
    %y1 = extractelement <3 x i16> %y, i32 1
    %y2 = extractelement <3 x i16> %y, i32 2

    %z0 = extractelement <3 x i16> %z, i32 0
    %z1 = extractelement <3 x i16> %z, i32 1
    %z2 = extractelement <3 x i16> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.smin3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.smin3.i16(i16 %x1, i16 %y1, i16 %z1)
    %3 = call i16 @llpc.smin3.i16(i16 %x2, i16 %y2, i16 %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i16>
    %5 = load <3 x i16>, <3 x i16>* %4
    %6 = insertelement <3 x i16> %5, i16 %1, i32 0
    %7 = insertelement <3 x i16> %6, i16 %2, i32 1
    %8 = insertelement <3 x i16> %7, i16 %3, i32 2

    ret <3 x i16> %8
}

; <4 x i16> SMin3AMD()  =>  llpc.smin3.i16
define spir_func <4 x i16> @_Z8SMin3AMDDv4_sDv4_sDv4_s(
    <4 x i16> %x, <4 x i16> %y, <4 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i16> %x, i32 0
    %x1 = extractelement <4 x i16> %x, i32 1
    %x2 = extractelement <4 x i16> %x, i32 2
    %x3 = extractelement <4 x i16> %x, i32 3

    %y0 = extractelement <4 x i16> %y, i32 0
    %y1 = extractelement <4 x i16> %y, i32 1
    %y2 = extractelement <4 x i16> %y, i32 2
    %y3 = extractelement <4 x i16> %y, i32 3

    %z0 = extractelement <4 x i16> %z, i32 0
    %z1 = extractelement <4 x i16> %z, i32 1
    %z2 = extractelement <4 x i16> %z, i32 2
    %z3 = extractelement <4 x i16> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.smin3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.smin3.i16(i16 %x1, i16 %y1, i16 %z1)
    %3 = call i16 @llpc.smin3.i16(i16 %x2, i16 %y2, i16 %z2)
    %4 = call i16 @llpc.smin3.i16(i16 %x3, i16 %y3, i16 %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i16>
    %6 = load <4 x i16>, <4 x i16>* %5
    %7 = insertelement <4 x i16> %6, i16 %1, i32 0
    %8 = insertelement <4 x i16> %7, i16 %2, i32 1
    %9 = insertelement <4 x i16> %8, i16 %3, i32 2
    %10 = insertelement <4 x i16> %9, i16 %4, i32 3

    ret <4 x i16> %10
}

declare i16 @llpc.smin3.i16(i16, i16, i16) #0

; i16 SMax3AMD()  =>  llpc.smax3.i16
define spir_func i16 @_Z8SMax3AMDsss(
    i16 %x, i16 %y, i16 %z) #0
{
    %1 = call i16 @llpc.smax3.i16(i16 %x, i16 %y, i16 %z)

    ret i16 %1
}

; <2 x i16> SMax3AMD()  =>  llpc.smax3.i16
define spir_func <2 x i16> @_Z8SMax3AMDDv2_sDv2_sDv2_s(
    <2 x i16> %x, <2 x i16> %y, <2 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i16> %x, i32 0
    %x1 = extractelement <2 x i16> %x, i32 1

    %y0 = extractelement <2 x i16> %y, i32 0
    %y1 = extractelement <2 x i16> %y, i32 1

    %z0 = extractelement <2 x i16> %z, i32 0
    %z1 = extractelement <2 x i16> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.smax3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.smax3.i16(i16 %x1, i16 %y1, i16 %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i16>
    %4 = load <2 x i16>, <2 x i16>* %3
    %5 = insertelement <2 x i16> %4, i16 %1, i32 0
    %6 = insertelement <2 x i16> %5, i16 %2, i32 1

    ret <2 x i16> %6
}

; <3 x i16> SMax3AMD()  =>  llpc.smax3.i16
define spir_func <3 x i16> @_Z8SMax3AMDDv3_sDv3_sDv3_s(
    <3 x i16> %x, <3 x i16> %y, <3 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i16> %x, i32 0
    %x1 = extractelement <3 x i16> %x, i32 1
    %x2 = extractelement <3 x i16> %x, i32 2

    %y0 = extractelement <3 x i16> %y, i32 0
    %y1 = extractelement <3 x i16> %y, i32 1
    %y2 = extractelement <3 x i16> %y, i32 2

    %z0 = extractelement <3 x i16> %z, i32 0
    %z1 = extractelement <3 x i16> %z, i32 1
    %z2 = extractelement <3 x i16> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.smax3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.smax3.i16(i16 %x1, i16 %y1, i16 %z1)
    %3 = call i16 @llpc.smax3.i16(i16 %x2, i16 %y2, i16 %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i16>
    %5 = load <3 x i16>, <3 x i16>* %4
    %6 = insertelement <3 x i16> %5, i16 %1, i32 0
    %7 = insertelement <3 x i16> %6, i16 %2, i32 1
    %8 = insertelement <3 x i16> %7, i16 %3, i32 2

    ret <3 x i16> %8
}

; <4 x i16> SMax3AMD()  =>  llpc.smax3.i16
define spir_func <4 x i16> @_Z8SMax3AMDDv4_sDv4_sDv4_s(
    <4 x i16> %x, <4 x i16> %y, <4 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i16> %x, i32 0
    %x1 = extractelement <4 x i16> %x, i32 1
    %x2 = extractelement <4 x i16> %x, i32 2
    %x3 = extractelement <4 x i16> %x, i32 3

    %y0 = extractelement <4 x i16> %y, i32 0
    %y1 = extractelement <4 x i16> %y, i32 1
    %y2 = extractelement <4 x i16> %y, i32 2
    %y3 = extractelement <4 x i16> %y, i32 3

    %z0 = extractelement <4 x i16> %z, i32 0
    %z1 = extractelement <4 x i16> %z, i32 1
    %z2 = extractelement <4 x i16> %z, i32 2
    %z3 = extractelement <4 x i16> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.smax3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.smax3.i16(i16 %x1, i16 %y1, i16 %z1)
    %3 = call i16 @llpc.smax3.i16(i16 %x2, i16 %y2, i16 %z2)
    %4 = call i16 @llpc.smax3.i16(i16 %x3, i16 %y3, i16 %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i16>
    %6 = load <4 x i16>, <4 x i16>* %5
    %7 = insertelement <4 x i16> %6, i16 %1, i32 0
    %8 = insertelement <4 x i16> %7, i16 %2, i32 1
    %9 = insertelement <4 x i16> %8, i16 %3, i32 2
    %10 = insertelement <4 x i16> %9, i16 %4, i32 3

    ret <4 x i16> %10
}

declare i16 @llpc.smax3.i16(i16, i16, i16) #0

; i16 SMid3AMD()  =>  llpc.smid3.i16
define spir_func i16 @_Z8SMid3AMDsss(
    i16 %x, i16 %y, i16 %z) #0
{
    %1 = call i16 @llpc.smid3.i16(i16 %x, i16 %y, i16 %z)

    ret i16 %1
}

; <2 x i16> SMid3AMD()  =>  llpc.smid3.i16
define spir_func <2 x i16> @_Z8SMid3AMDDv2_sDv2_sDv2_s(
    <2 x i16> %x, <2 x i16> %y, <2 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i16> %x, i32 0
    %x1 = extractelement <2 x i16> %x, i32 1

    %y0 = extractelement <2 x i16> %y, i32 0
    %y1 = extractelement <2 x i16> %y, i32 1

    %z0 = extractelement <2 x i16> %z, i32 0
    %z1 = extractelement <2 x i16> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.smid3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.smid3.i16(i16 %x1, i16 %y1, i16 %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i16>
    %4 = load <2 x i16>, <2 x i16>* %3
    %5 = insertelement <2 x i16> %4, i16 %1, i32 0
    %6 = insertelement <2 x i16> %5, i16 %2, i32 1

    ret <2 x i16> %6
}

; <3 x i16> SMid3AMD()  =>  llpc.smid3.i16
define spir_func <3 x i16> @_Z8SMid3AMDDv3_sDv3_sDv3_s(
    <3 x i16> %x, <3 x i16> %y, <3 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i16> %x, i32 0
    %x1 = extractelement <3 x i16> %x, i32 1
    %x2 = extractelement <3 x i16> %x, i32 2

    %y0 = extractelement <3 x i16> %y, i32 0
    %y1 = extractelement <3 x i16> %y, i32 1
    %y2 = extractelement <3 x i16> %y, i32 2

    %z0 = extractelement <3 x i16> %z, i32 0
    %z1 = extractelement <3 x i16> %z, i32 1
    %z2 = extractelement <3 x i16> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.smid3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.smid3.i16(i16 %x1, i16 %y1, i16 %z1)
    %3 = call i16 @llpc.smid3.i16(i16 %x2, i16 %y2, i16 %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i16>
    %5 = load <3 x i16>, <3 x i16>* %4
    %6 = insertelement <3 x i16> %5, i16 %1, i32 0
    %7 = insertelement <3 x i16> %6, i16 %2, i32 1
    %8 = insertelement <3 x i16> %7, i16 %3, i32 2

    ret <3 x i16> %8
}

; <4 x i16> SMid3AMD()  =>  llpc.smid3.i16
define spir_func <4 x i16> @_Z8SMid3AMDDv4_sDv4_sDv4_s(
    <4 x i16> %x, <4 x i16> %y, <4 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i16> %x, i32 0
    %x1 = extractelement <4 x i16> %x, i32 1
    %x2 = extractelement <4 x i16> %x, i32 2
    %x3 = extractelement <4 x i16> %x, i32 3

    %y0 = extractelement <4 x i16> %y, i32 0
    %y1 = extractelement <4 x i16> %y, i32 1
    %y2 = extractelement <4 x i16> %y, i32 2
    %y3 = extractelement <4 x i16> %y, i32 3

    %z0 = extractelement <4 x i16> %z, i32 0
    %z1 = extractelement <4 x i16> %z, i32 1
    %z2 = extractelement <4 x i16> %z, i32 2
    %z3 = extractelement <4 x i16> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.smid3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.smid3.i16(i16 %x1, i16 %y1, i16 %z1)
    %3 = call i16 @llpc.smid3.i16(i16 %x2, i16 %y2, i16 %z2)
    %4 = call i16 @llpc.smid3.i16(i16 %x3, i16 %y3, i16 %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i16>
    %6 = load <4 x i16>, <4 x i16>* %5
    %7 = insertelement <4 x i16> %6, i16 %1, i32 0
    %8 = insertelement <4 x i16> %7, i16 %2, i32 1
    %9 = insertelement <4 x i16> %8, i16 %3, i32 2
    %10 = insertelement <4 x i16> %9, i16 %4, i32 3

    ret <4 x i16> %10
}

declare i16 @llpc.smid3.i16(i16, i16, i16) #0

; i16 UMin3AMD()  =>  llpc.umin3.i16
define spir_func i16 @_Z8UMin3AMDsss(
    i16 %x, i16 %y, i16 %z) #0
{
    %1 = call i16 @llpc.umin3.i16(i16 %x, i16 %y, i16 %z)

    ret i16 %1
}

; <2 x i16> UMin3AMD()  =>  llpc.umin3.i16
define spir_func <2 x i16> @_Z8UMin3AMDDv2_sDv2_sDv2_s(
    <2 x i16> %x, <2 x i16> %y, <2 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i16> %x, i32 0
    %x1 = extractelement <2 x i16> %x, i32 1

    %y0 = extractelement <2 x i16> %y, i32 0
    %y1 = extractelement <2 x i16> %y, i32 1

    %z0 = extractelement <2 x i16> %z, i32 0
    %z1 = extractelement <2 x i16> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.umin3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.umin3.i16(i16 %x1, i16 %y1, i16 %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i16>
    %4 = load <2 x i16>, <2 x i16>* %3
    %5 = insertelement <2 x i16> %4, i16 %1, i32 0
    %6 = insertelement <2 x i16> %5, i16 %2, i32 1

    ret <2 x i16> %6
}

; <3 x i16> UMin3AMD()  =>  llpc.umin3.i16
define spir_func <3 x i16> @_Z8UMin3AMDDv3_sDv3_sDv3_s(
    <3 x i16> %x, <3 x i16> %y, <3 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i16> %x, i32 0
    %x1 = extractelement <3 x i16> %x, i32 1
    %x2 = extractelement <3 x i16> %x, i32 2

    %y0 = extractelement <3 x i16> %y, i32 0
    %y1 = extractelement <3 x i16> %y, i32 1
    %y2 = extractelement <3 x i16> %y, i32 2

    %z0 = extractelement <3 x i16> %z, i32 0
    %z1 = extractelement <3 x i16> %z, i32 1
    %z2 = extractelement <3 x i16> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.umin3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.umin3.i16(i16 %x1, i16 %y1, i16 %z1)
    %3 = call i16 @llpc.umin3.i16(i16 %x2, i16 %y2, i16 %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i16>
    %5 = load <3 x i16>, <3 x i16>* %4
    %6 = insertelement <3 x i16> %5, i16 %1, i32 0
    %7 = insertelement <3 x i16> %6, i16 %2, i32 1
    %8 = insertelement <3 x i16> %7, i16 %3, i32 2

    ret <3 x i16> %8
}

; <4 x i16> UMin3AMD()  =>  llpc.umin3.i16
define spir_func <4 x i16> @_Z8UMin3AMDDv4_sDv4_sDv4_s(
    <4 x i16> %x, <4 x i16> %y, <4 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i16> %x, i32 0
    %x1 = extractelement <4 x i16> %x, i32 1
    %x2 = extractelement <4 x i16> %x, i32 2
    %x3 = extractelement <4 x i16> %x, i32 3

    %y0 = extractelement <4 x i16> %y, i32 0
    %y1 = extractelement <4 x i16> %y, i32 1
    %y2 = extractelement <4 x i16> %y, i32 2
    %y3 = extractelement <4 x i16> %y, i32 3

    %z0 = extractelement <4 x i16> %z, i32 0
    %z1 = extractelement <4 x i16> %z, i32 1
    %z2 = extractelement <4 x i16> %z, i32 2
    %z3 = extractelement <4 x i16> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.umin3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.umin3.i16(i16 %x1, i16 %y1, i16 %z1)
    %3 = call i16 @llpc.umin3.i16(i16 %x2, i16 %y2, i16 %z2)
    %4 = call i16 @llpc.umin3.i16(i16 %x3, i16 %y3, i16 %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i16>
    %6 = load <4 x i16>, <4 x i16>* %5
    %7 = insertelement <4 x i16> %6, i16 %1, i32 0
    %8 = insertelement <4 x i16> %7, i16 %2, i32 1
    %9 = insertelement <4 x i16> %8, i16 %3, i32 2
    %10 = insertelement <4 x i16> %9, i16 %4, i32 3

    ret <4 x i16> %10
}

declare i16 @llpc.umin3.i16(i16, i16, i16) #0

; i16 UMax3AMD()  =>  llpc.umax3.i16
define spir_func i16 @_Z8UMax3AMDsss(
    i16 %x, i16 %y, i16 %z) #0
{
    %1 = call i16 @llpc.umax3.i16(i16 %x, i16 %y, i16 %z)

    ret i16 %1
}

; <2 x i16> UMax3AMD()  =>  llpc.umax3.i16
define spir_func <2 x i16> @_Z8UMax3AMDDv2_sDv2_sDv2_s(
    <2 x i16> %x, <2 x i16> %y, <2 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i16> %x, i32 0
    %x1 = extractelement <2 x i16> %x, i32 1

    %y0 = extractelement <2 x i16> %y, i32 0
    %y1 = extractelement <2 x i16> %y, i32 1

    %z0 = extractelement <2 x i16> %z, i32 0
    %z1 = extractelement <2 x i16> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.umax3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.umax3.i16(i16 %x1, i16 %y1, i16 %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i16>
    %4 = load <2 x i16>, <2 x i16>* %3
    %5 = insertelement <2 x i16> %4, i16 %1, i32 0
    %6 = insertelement <2 x i16> %5, i16 %2, i32 1

    ret <2 x i16> %6
}

; <3 x i16> UMax3AMD()  =>  llpc.umax3.i16
define spir_func <3 x i16> @_Z8UMax3AMDDv3_sDv3_sDv3_s(
    <3 x i16> %x, <3 x i16> %y, <3 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i16> %x, i32 0
    %x1 = extractelement <3 x i16> %x, i32 1
    %x2 = extractelement <3 x i16> %x, i32 2

    %y0 = extractelement <3 x i16> %y, i32 0
    %y1 = extractelement <3 x i16> %y, i32 1
    %y2 = extractelement <3 x i16> %y, i32 2

    %z0 = extractelement <3 x i16> %z, i32 0
    %z1 = extractelement <3 x i16> %z, i32 1
    %z2 = extractelement <3 x i16> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.umax3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.umax3.i16(i16 %x1, i16 %y1, i16 %z1)
    %3 = call i16 @llpc.umax3.i16(i16 %x2, i16 %y2, i16 %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i16>
    %5 = load <3 x i16>, <3 x i16>* %4
    %6 = insertelement <3 x i16> %5, i16 %1, i32 0
    %7 = insertelement <3 x i16> %6, i16 %2, i32 1
    %8 = insertelement <3 x i16> %7, i16 %3, i32 2

    ret <3 x i16> %8
}

; <4 x i16> UMax3AMD()  =>  llpc.umax3.i16
define spir_func <4 x i16> @_Z8UMax3AMDDv4_sDv4_sDv4_s(
    <4 x i16> %x, <4 x i16> %y, <4 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i16> %x, i32 0
    %x1 = extractelement <4 x i16> %x, i32 1
    %x2 = extractelement <4 x i16> %x, i32 2
    %x3 = extractelement <4 x i16> %x, i32 3

    %y0 = extractelement <4 x i16> %y, i32 0
    %y1 = extractelement <4 x i16> %y, i32 1
    %y2 = extractelement <4 x i16> %y, i32 2
    %y3 = extractelement <4 x i16> %y, i32 3

    %z0 = extractelement <4 x i16> %z, i32 0
    %z1 = extractelement <4 x i16> %z, i32 1
    %z2 = extractelement <4 x i16> %z, i32 2
    %z3 = extractelement <4 x i16> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.umax3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.umax3.i16(i16 %x1, i16 %y1, i16 %z1)
    %3 = call i16 @llpc.umax3.i16(i16 %x2, i16 %y2, i16 %z2)
    %4 = call i16 @llpc.umax3.i16(i16 %x3, i16 %y3, i16 %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i16>
    %6 = load <4 x i16>, <4 x i16>* %5
    %7 = insertelement <4 x i16> %6, i16 %1, i32 0
    %8 = insertelement <4 x i16> %7, i16 %2, i32 1
    %9 = insertelement <4 x i16> %8, i16 %3, i32 2
    %10 = insertelement <4 x i16> %9, i16 %4, i32 3

    ret <4 x i16> %10
}

declare i16 @llpc.umax3.i16(i16, i16, i16) #0

; i16 UMid3AMD()  =>  llpc.umid3.i16
define spir_func i16 @_Z8UMid3AMDsss(
    i16 %x, i16 %y, i16 %z) #0
{
    %1 = call i16 @llpc.umid3.i16(i16 %x, i16 %y, i16 %z)

    ret i16 %1
}

; <2 x i16> UMid3AMD()  =>  llpc.umid3.i16
define spir_func <2 x i16> @_Z8UMid3AMDDv2_sDv2_sDv2_s(
    <2 x i16> %x, <2 x i16> %y, <2 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i16> %x, i32 0
    %x1 = extractelement <2 x i16> %x, i32 1

    %y0 = extractelement <2 x i16> %y, i32 0
    %y1 = extractelement <2 x i16> %y, i32 1

    %z0 = extractelement <2 x i16> %z, i32 0
    %z1 = extractelement <2 x i16> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.umid3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.umid3.i16(i16 %x1, i16 %y1, i16 %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i16>
    %4 = load <2 x i16>, <2 x i16>* %3
    %5 = insertelement <2 x i16> %4, i16 %1, i32 0
    %6 = insertelement <2 x i16> %5, i16 %2, i32 1

    ret <2 x i16> %6
}

; <3 x i16> UMid3AMD()  =>  llpc.umid3.i16
define spir_func <3 x i16> @_Z8UMid3AMDDv3_sDv3_sDv3_s(
    <3 x i16> %x, <3 x i16> %y, <3 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i16> %x, i32 0
    %x1 = extractelement <3 x i16> %x, i32 1
    %x2 = extractelement <3 x i16> %x, i32 2

    %y0 = extractelement <3 x i16> %y, i32 0
    %y1 = extractelement <3 x i16> %y, i32 1
    %y2 = extractelement <3 x i16> %y, i32 2

    %z0 = extractelement <3 x i16> %z, i32 0
    %z1 = extractelement <3 x i16> %z, i32 1
    %z2 = extractelement <3 x i16> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.umid3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.umid3.i16(i16 %x1, i16 %y1, i16 %z1)
    %3 = call i16 @llpc.umid3.i16(i16 %x2, i16 %y2, i16 %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i16>
    %5 = load <3 x i16>, <3 x i16>* %4
    %6 = insertelement <3 x i16> %5, i16 %1, i32 0
    %7 = insertelement <3 x i16> %6, i16 %2, i32 1
    %8 = insertelement <3 x i16> %7, i16 %3, i32 2

    ret <3 x i16> %8
}

; <4 x i16> UMid3AMD()  =>  llpc.umid3.i16
define spir_func <4 x i16> @_Z8UMid3AMDDv4_sDv4_sDv4_s(
    <4 x i16> %x, <4 x i16> %y, <4 x i16> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i16> %x, i32 0
    %x1 = extractelement <4 x i16> %x, i32 1
    %x2 = extractelement <4 x i16> %x, i32 2
    %x3 = extractelement <4 x i16> %x, i32 3

    %y0 = extractelement <4 x i16> %y, i32 0
    %y1 = extractelement <4 x i16> %y, i32 1
    %y2 = extractelement <4 x i16> %y, i32 2
    %y3 = extractelement <4 x i16> %y, i32 3

    %z0 = extractelement <4 x i16> %z, i32 0
    %z1 = extractelement <4 x i16> %z, i32 1
    %z2 = extractelement <4 x i16> %z, i32 2
    %z3 = extractelement <4 x i16> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i16 @llpc.umid3.i16(i16 %x0, i16 %y0, i16 %z0)
    %2 = call i16 @llpc.umid3.i16(i16 %x1, i16 %y1, i16 %z1)
    %3 = call i16 @llpc.umid3.i16(i16 %x2, i16 %y2, i16 %z2)
    %4 = call i16 @llpc.umid3.i16(i16 %x3, i16 %y3, i16 %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i16>
    %6 = load <4 x i16>, <4 x i16>* %5
    %7 = insertelement <4 x i16> %6, i16 %1, i32 0
    %8 = insertelement <4 x i16> %7, i16 %2, i32 1
    %9 = insertelement <4 x i16> %8, i16 %3, i32 2
    %10 = insertelement <4 x i16> %9, i16 %4, i32 3

    ret <4 x i16> %10
}

declare i16 @llpc.umid3.i16(i16, i16, i16) #0

attributes #0 = { nounwind }
attributes #1 = { nounwind readnone }
