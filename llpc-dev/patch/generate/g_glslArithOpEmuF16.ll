;**********************************************************************************************************************
;*
;*  Trade secret of Advanced Micro Devices, Inc.
;*  Copyright (c) 2017, Advanced Micro Devices, Inc., (unpublished)
;*
;*  All rights reserved. This notice is intended as a precaution against inadvertent publication and does not imply
;*  publication or any waiver of confidentiality. The year included in the foregoing notice is the year of creation of
;*  the work.
;*
;**********************************************************************************************************************

;**********************************************************************************************************************
;* @file  g_glslArithOpEmuF16.ll
;* @brief LLPC LLVM-IR file: contains emulation codes for GLSL arithmetic operations (float16).
;*
;* @note  This file has been generated automatically. Do not hand-modify this file. When changes are needed, modify the
;*        generating script genGlslArithOpEmuCode.py.
;**********************************************************************************************************************

target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v16:16:16-v24:32:32-v32:32:32-v48:64:64-v64:64:64-v96:128:128-v128:128:128-v192:256:256-v256:256:256-v512:512:512-v1024:1024:1024"
target triple = "spir64-unknown-unknown"

; =====================================================================================================================
; >>>  Operators
; =====================================================================================================================

; half fdiv()  =>  llpc.fdiv.f16
define spir_func half @_Z4fdivDhDh(
    half %y, half %x) #0
{
    %1 = call half @llpc.fdiv.f16(half %y, half %x)

    ret half %1
}

; <2 x half> fdiv()  =>  llpc.fdiv.f16
define spir_func <2 x half> @_Z4fdivDv2_DhDv2_Dh(
    <2 x half> %y, <2 x half> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <2 x half> %y, i32 0
    %y1 = extractelement <2 x half> %y, i32 1

    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fdiv.f16(half %y0, half %x0)
    %2 = call half @llpc.fdiv.f16(half %y1, half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> fdiv()  =>  llpc.fdiv.f16
define spir_func <3 x half> @_Z4fdivDv3_DhDv3_Dh(
    <3 x half> %y, <3 x half> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <3 x half> %y, i32 0
    %y1 = extractelement <3 x half> %y, i32 1
    %y2 = extractelement <3 x half> %y, i32 2

    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fdiv.f16(half %y0, half %x0)
    %2 = call half @llpc.fdiv.f16(half %y1, half %x1)
    %3 = call half @llpc.fdiv.f16(half %y2, half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> fdiv()  =>  llpc.fdiv.f16
define spir_func <4 x half> @_Z4fdivDv4_DhDv4_Dh(
    <4 x half> %y, <4 x half> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <4 x half> %y, i32 0
    %y1 = extractelement <4 x half> %y, i32 1
    %y2 = extractelement <4 x half> %y, i32 2
    %y3 = extractelement <4 x half> %y, i32 3

    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fdiv.f16(half %y0, half %x0)
    %2 = call half @llpc.fdiv.f16(half %y1, half %x1)
    %3 = call half @llpc.fdiv.f16(half %y2, half %x2)
    %4 = call half @llpc.fdiv.f16(half %y3, half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.fdiv.f16(half, half) #0

; =====================================================================================================================
; >>>  Angle and Trigonometry Functions
; =====================================================================================================================

; half sin()  =>  llvm.sin.f16
define spir_func half @_Z3sinDh(
    half %angle) #0
{
    %1 = call half @llvm.sin.f16(half %angle)

    ret half %1
}

; <2 x half> sin()  =>  llvm.sin.f16
define spir_func <2 x half> @_Z3sinDv2_Dh(
    <2 x half> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <2 x half> %angle, i32 0
    %angle1 = extractelement <2 x half> %angle, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.sin.f16(half %angle0)
    %2 = call half @llvm.sin.f16(half %angle1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> sin()  =>  llvm.sin.f16
define spir_func <3 x half> @_Z3sinDv3_Dh(
    <3 x half> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <3 x half> %angle, i32 0
    %angle1 = extractelement <3 x half> %angle, i32 1
    %angle2 = extractelement <3 x half> %angle, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.sin.f16(half %angle0)
    %2 = call half @llvm.sin.f16(half %angle1)
    %3 = call half @llvm.sin.f16(half %angle2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> sin()  =>  llvm.sin.f16
define spir_func <4 x half> @_Z3sinDv4_Dh(
    <4 x half> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <4 x half> %angle, i32 0
    %angle1 = extractelement <4 x half> %angle, i32 1
    %angle2 = extractelement <4 x half> %angle, i32 2
    %angle3 = extractelement <4 x half> %angle, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.sin.f16(half %angle0)
    %2 = call half @llvm.sin.f16(half %angle1)
    %3 = call half @llvm.sin.f16(half %angle2)
    %4 = call half @llvm.sin.f16(half %angle3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llvm.sin.f16(half) #0

; half cos()  =>  llvm.cos.f16
define spir_func half @_Z3cosDh(
    half %angle) #0
{
    %1 = call half @llvm.cos.f16(half %angle)

    ret half %1
}

; <2 x half> cos()  =>  llvm.cos.f16
define spir_func <2 x half> @_Z3cosDv2_Dh(
    <2 x half> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <2 x half> %angle, i32 0
    %angle1 = extractelement <2 x half> %angle, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.cos.f16(half %angle0)
    %2 = call half @llvm.cos.f16(half %angle1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> cos()  =>  llvm.cos.f16
define spir_func <3 x half> @_Z3cosDv3_Dh(
    <3 x half> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <3 x half> %angle, i32 0
    %angle1 = extractelement <3 x half> %angle, i32 1
    %angle2 = extractelement <3 x half> %angle, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.cos.f16(half %angle0)
    %2 = call half @llvm.cos.f16(half %angle1)
    %3 = call half @llvm.cos.f16(half %angle2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> cos()  =>  llvm.cos.f16
define spir_func <4 x half> @_Z3cosDv4_Dh(
    <4 x half> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <4 x half> %angle, i32 0
    %angle1 = extractelement <4 x half> %angle, i32 1
    %angle2 = extractelement <4 x half> %angle, i32 2
    %angle3 = extractelement <4 x half> %angle, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.cos.f16(half %angle0)
    %2 = call half @llvm.cos.f16(half %angle1)
    %3 = call half @llvm.cos.f16(half %angle2)
    %4 = call half @llvm.cos.f16(half %angle3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llvm.cos.f16(half) #0

; half tan()  =>  llpc.tan.f16
define spir_func half @_Z3tanDh(
    half %angle) #0
{
    %1 = call half @llpc.tan.f16(half %angle)

    ret half %1
}

; <2 x half> tan()  =>  llpc.tan.f16
define spir_func <2 x half> @_Z3tanDv2_Dh(
    <2 x half> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <2 x half> %angle, i32 0
    %angle1 = extractelement <2 x half> %angle, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.tan.f16(half %angle0)
    %2 = call half @llpc.tan.f16(half %angle1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> tan()  =>  llpc.tan.f16
define spir_func <3 x half> @_Z3tanDv3_Dh(
    <3 x half> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <3 x half> %angle, i32 0
    %angle1 = extractelement <3 x half> %angle, i32 1
    %angle2 = extractelement <3 x half> %angle, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.tan.f16(half %angle0)
    %2 = call half @llpc.tan.f16(half %angle1)
    %3 = call half @llpc.tan.f16(half %angle2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> tan()  =>  llpc.tan.f16
define spir_func <4 x half> @_Z3tanDv4_Dh(
    <4 x half> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <4 x half> %angle, i32 0
    %angle1 = extractelement <4 x half> %angle, i32 1
    %angle2 = extractelement <4 x half> %angle, i32 2
    %angle3 = extractelement <4 x half> %angle, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.tan.f16(half %angle0)
    %2 = call half @llpc.tan.f16(half %angle1)
    %3 = call half @llpc.tan.f16(half %angle2)
    %4 = call half @llpc.tan.f16(half %angle3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.tan.f16(half) #0

; half asin()  =>  llpc.asin.f16
define spir_func half @_Z4asinDh(
    half %x) #0
{
    %1 = call half @llpc.asin.f16(half %x)

    ret half %1
}

; <2 x half> asin()  =>  llpc.asin.f16
define spir_func <2 x half> @_Z4asinDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.asin.f16(half %x0)
    %2 = call half @llpc.asin.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> asin()  =>  llpc.asin.f16
define spir_func <3 x half> @_Z4asinDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.asin.f16(half %x0)
    %2 = call half @llpc.asin.f16(half %x1)
    %3 = call half @llpc.asin.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> asin()  =>  llpc.asin.f16
define spir_func <4 x half> @_Z4asinDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.asin.f16(half %x0)
    %2 = call half @llpc.asin.f16(half %x1)
    %3 = call half @llpc.asin.f16(half %x2)
    %4 = call half @llpc.asin.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.asin.f16(half) #0

; half acos()  =>  llpc.acos.f16
define spir_func half @_Z4acosDh(
    half %x) #0
{
    %1 = call half @llpc.acos.f16(half %x)

    ret half %1
}

; <2 x half> acos()  =>  llpc.acos.f16
define spir_func <2 x half> @_Z4acosDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.acos.f16(half %x0)
    %2 = call half @llpc.acos.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> acos()  =>  llpc.acos.f16
define spir_func <3 x half> @_Z4acosDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.acos.f16(half %x0)
    %2 = call half @llpc.acos.f16(half %x1)
    %3 = call half @llpc.acos.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> acos()  =>  llpc.acos.f16
define spir_func <4 x half> @_Z4acosDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.acos.f16(half %x0)
    %2 = call half @llpc.acos.f16(half %x1)
    %3 = call half @llpc.acos.f16(half %x2)
    %4 = call half @llpc.acos.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.acos.f16(half) #0

; half atan()  =>  llpc.atan.f16
define spir_func half @_Z4atanDh(
    half %x) #0
{
    %1 = call half @llpc.atan.f16(half %x)

    ret half %1
}

; <2 x half> atan()  =>  llpc.atan.f16
define spir_func <2 x half> @_Z4atanDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.atan.f16(half %x0)
    %2 = call half @llpc.atan.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> atan()  =>  llpc.atan.f16
define spir_func <3 x half> @_Z4atanDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.atan.f16(half %x0)
    %2 = call half @llpc.atan.f16(half %x1)
    %3 = call half @llpc.atan.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> atan()  =>  llpc.atan.f16
define spir_func <4 x half> @_Z4atanDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.atan.f16(half %x0)
    %2 = call half @llpc.atan.f16(half %x1)
    %3 = call half @llpc.atan.f16(half %x2)
    %4 = call half @llpc.atan.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.atan.f16(half) #0

; half atan2()  =>  llpc.atan2.f16
define spir_func half @_Z5atan2DhDh(
    half %y, half %x) #0
{
    %1 = call half @llpc.atan2.f16(half %y, half %x)

    ret half %1
}

; <2 x half> atan2()  =>  llpc.atan2.f16
define spir_func <2 x half> @_Z5atan2Dv2_DhDv2_Dh(
    <2 x half> %y, <2 x half> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <2 x half> %y, i32 0
    %y1 = extractelement <2 x half> %y, i32 1

    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.atan2.f16(half %y0, half %x0)
    %2 = call half @llpc.atan2.f16(half %y1, half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> atan2()  =>  llpc.atan2.f16
define spir_func <3 x half> @_Z5atan2Dv3_DhDv3_Dh(
    <3 x half> %y, <3 x half> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <3 x half> %y, i32 0
    %y1 = extractelement <3 x half> %y, i32 1
    %y2 = extractelement <3 x half> %y, i32 2

    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.atan2.f16(half %y0, half %x0)
    %2 = call half @llpc.atan2.f16(half %y1, half %x1)
    %3 = call half @llpc.atan2.f16(half %y2, half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> atan2()  =>  llpc.atan2.f16
define spir_func <4 x half> @_Z5atan2Dv4_DhDv4_Dh(
    <4 x half> %y, <4 x half> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <4 x half> %y, i32 0
    %y1 = extractelement <4 x half> %y, i32 1
    %y2 = extractelement <4 x half> %y, i32 2
    %y3 = extractelement <4 x half> %y, i32 3

    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.atan2.f16(half %y0, half %x0)
    %2 = call half @llpc.atan2.f16(half %y1, half %x1)
    %3 = call half @llpc.atan2.f16(half %y2, half %x2)
    %4 = call half @llpc.atan2.f16(half %y3, half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.atan2.f16(half, half) #0

; half sinh()  =>  llpc.sinh.f16
define spir_func half @_Z4sinhDh(
    half %x) #0
{
    %1 = call half @llpc.sinh.f16(half %x)

    ret half %1
}

; <2 x half> sinh()  =>  llpc.sinh.f16
define spir_func <2 x half> @_Z4sinhDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.sinh.f16(half %x0)
    %2 = call half @llpc.sinh.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> sinh()  =>  llpc.sinh.f16
define spir_func <3 x half> @_Z4sinhDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.sinh.f16(half %x0)
    %2 = call half @llpc.sinh.f16(half %x1)
    %3 = call half @llpc.sinh.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> sinh()  =>  llpc.sinh.f16
define spir_func <4 x half> @_Z4sinhDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.sinh.f16(half %x0)
    %2 = call half @llpc.sinh.f16(half %x1)
    %3 = call half @llpc.sinh.f16(half %x2)
    %4 = call half @llpc.sinh.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.sinh.f16(half) #0

; half cosh()  =>  llpc.cosh.f16
define spir_func half @_Z4coshDh(
    half %x) #0
{
    %1 = call half @llpc.cosh.f16(half %x)

    ret half %1
}

; <2 x half> cosh()  =>  llpc.cosh.f16
define spir_func <2 x half> @_Z4coshDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.cosh.f16(half %x0)
    %2 = call half @llpc.cosh.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> cosh()  =>  llpc.cosh.f16
define spir_func <3 x half> @_Z4coshDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.cosh.f16(half %x0)
    %2 = call half @llpc.cosh.f16(half %x1)
    %3 = call half @llpc.cosh.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> cosh()  =>  llpc.cosh.f16
define spir_func <4 x half> @_Z4coshDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.cosh.f16(half %x0)
    %2 = call half @llpc.cosh.f16(half %x1)
    %3 = call half @llpc.cosh.f16(half %x2)
    %4 = call half @llpc.cosh.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.cosh.f16(half) #0

; half tanh()  =>  llpc.tanh.f16
define spir_func half @_Z4tanhDh(
    half %x) #0
{
    %1 = call half @llpc.tanh.f16(half %x)

    ret half %1
}

; <2 x half> tanh()  =>  llpc.tanh.f16
define spir_func <2 x half> @_Z4tanhDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.tanh.f16(half %x0)
    %2 = call half @llpc.tanh.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> tanh()  =>  llpc.tanh.f16
define spir_func <3 x half> @_Z4tanhDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.tanh.f16(half %x0)
    %2 = call half @llpc.tanh.f16(half %x1)
    %3 = call half @llpc.tanh.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> tanh()  =>  llpc.tanh.f16
define spir_func <4 x half> @_Z4tanhDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.tanh.f16(half %x0)
    %2 = call half @llpc.tanh.f16(half %x1)
    %3 = call half @llpc.tanh.f16(half %x2)
    %4 = call half @llpc.tanh.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.tanh.f16(half) #0

; half asinh()  =>  llpc.asinh.f16
define spir_func half @_Z5asinhDh(
    half %x) #0
{
    %1 = call half @llpc.asinh.f16(half %x)

    ret half %1
}

; <2 x half> asinh()  =>  llpc.asinh.f16
define spir_func <2 x half> @_Z5asinhDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.asinh.f16(half %x0)
    %2 = call half @llpc.asinh.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> asinh()  =>  llpc.asinh.f16
define spir_func <3 x half> @_Z5asinhDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.asinh.f16(half %x0)
    %2 = call half @llpc.asinh.f16(half %x1)
    %3 = call half @llpc.asinh.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> asinh()  =>  llpc.asinh.f16
define spir_func <4 x half> @_Z5asinhDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.asinh.f16(half %x0)
    %2 = call half @llpc.asinh.f16(half %x1)
    %3 = call half @llpc.asinh.f16(half %x2)
    %4 = call half @llpc.asinh.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.asinh.f16(half) #0

; half acosh()  =>  llpc.acosh.f16
define spir_func half @_Z5acoshDh(
    half %x) #0
{
    %1 = call half @llpc.acosh.f16(half %x)

    ret half %1
}

; <2 x half> acosh()  =>  llpc.acosh.f16
define spir_func <2 x half> @_Z5acoshDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.acosh.f16(half %x0)
    %2 = call half @llpc.acosh.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> acosh()  =>  llpc.acosh.f16
define spir_func <3 x half> @_Z5acoshDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.acosh.f16(half %x0)
    %2 = call half @llpc.acosh.f16(half %x1)
    %3 = call half @llpc.acosh.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> acosh()  =>  llpc.acosh.f16
define spir_func <4 x half> @_Z5acoshDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.acosh.f16(half %x0)
    %2 = call half @llpc.acosh.f16(half %x1)
    %3 = call half @llpc.acosh.f16(half %x2)
    %4 = call half @llpc.acosh.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.acosh.f16(half) #0

; half atanh()  =>  llpc.atanh.f16
define spir_func half @_Z5atanhDh(
    half %x) #0
{
    %1 = call half @llpc.atanh.f16(half %x)

    ret half %1
}

; <2 x half> atanh()  =>  llpc.atanh.f16
define spir_func <2 x half> @_Z5atanhDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.atanh.f16(half %x0)
    %2 = call half @llpc.atanh.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> atanh()  =>  llpc.atanh.f16
define spir_func <3 x half> @_Z5atanhDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.atanh.f16(half %x0)
    %2 = call half @llpc.atanh.f16(half %x1)
    %3 = call half @llpc.atanh.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> atanh()  =>  llpc.atanh.f16
define spir_func <4 x half> @_Z5atanhDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.atanh.f16(half %x0)
    %2 = call half @llpc.atanh.f16(half %x1)
    %3 = call half @llpc.atanh.f16(half %x2)
    %4 = call half @llpc.atanh.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.atanh.f16(half) #0

; =====================================================================================================================
; >>>  Exponential Functions
; =====================================================================================================================

; half pow()  =>  llpc.pow.f16
define spir_func half @_Z3powDhDh(
    half %x, half %y) #0
{
    %1 = call half @llpc.pow.f16(half %x, half %y)

    ret half %1
}

; <2 x half> pow()  =>  llpc.pow.f16
define spir_func <2 x half> @_Z3powDv2_DhDv2_Dh(
    <2 x half> %x, <2 x half> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    %y0 = extractelement <2 x half> %y, i32 0
    %y1 = extractelement <2 x half> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.pow.f16(half %x0, half %y0)
    %2 = call half @llpc.pow.f16(half %x1, half %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> pow()  =>  llpc.pow.f16
define spir_func <3 x half> @_Z3powDv3_DhDv3_Dh(
    <3 x half> %x, <3 x half> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    %y0 = extractelement <3 x half> %y, i32 0
    %y1 = extractelement <3 x half> %y, i32 1
    %y2 = extractelement <3 x half> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.pow.f16(half %x0, half %y0)
    %2 = call half @llpc.pow.f16(half %x1, half %y1)
    %3 = call half @llpc.pow.f16(half %x2, half %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> pow()  =>  llpc.pow.f16
define spir_func <4 x half> @_Z3powDv4_DhDv4_Dh(
    <4 x half> %x, <4 x half> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    %y0 = extractelement <4 x half> %y, i32 0
    %y1 = extractelement <4 x half> %y, i32 1
    %y2 = extractelement <4 x half> %y, i32 2
    %y3 = extractelement <4 x half> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.pow.f16(half %x0, half %y0)
    %2 = call half @llpc.pow.f16(half %x1, half %y1)
    %3 = call half @llpc.pow.f16(half %x2, half %y2)
    %4 = call half @llpc.pow.f16(half %x3, half %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.pow.f16(half, half) #0

; half exp()  =>  llpc.exp.f16
define spir_func half @_Z3expDh(
    half %x) #0
{
    %1 = call half @llpc.exp.f16(half %x)

    ret half %1
}

; <2 x half> exp()  =>  llpc.exp.f16
define spir_func <2 x half> @_Z3expDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.exp.f16(half %x0)
    %2 = call half @llpc.exp.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> exp()  =>  llpc.exp.f16
define spir_func <3 x half> @_Z3expDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.exp.f16(half %x0)
    %2 = call half @llpc.exp.f16(half %x1)
    %3 = call half @llpc.exp.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> exp()  =>  llpc.exp.f16
define spir_func <4 x half> @_Z3expDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.exp.f16(half %x0)
    %2 = call half @llpc.exp.f16(half %x1)
    %3 = call half @llpc.exp.f16(half %x2)
    %4 = call half @llpc.exp.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.exp.f16(half) #0

; half log()  =>  llpc.log.f16
define spir_func half @_Z3logDh(
    half %x) #0
{
    %1 = call half @llpc.log.f16(half %x)

    ret half %1
}

; <2 x half> log()  =>  llpc.log.f16
define spir_func <2 x half> @_Z3logDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.log.f16(half %x0)
    %2 = call half @llpc.log.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> log()  =>  llpc.log.f16
define spir_func <3 x half> @_Z3logDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.log.f16(half %x0)
    %2 = call half @llpc.log.f16(half %x1)
    %3 = call half @llpc.log.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> log()  =>  llpc.log.f16
define spir_func <4 x half> @_Z3logDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.log.f16(half %x0)
    %2 = call half @llpc.log.f16(half %x1)
    %3 = call half @llpc.log.f16(half %x2)
    %4 = call half @llpc.log.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.log.f16(half) #0

; half exp2()  =>  llvm.exp2.f16
define spir_func half @_Z4exp2Dh(
    half %x) #0
{
    %1 = call half @llvm.exp2.f16(half %x)

    ret half %1
}

; <2 x half> exp2()  =>  llvm.exp2.f16
define spir_func <2 x half> @_Z4exp2Dv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.exp2.f16(half %x0)
    %2 = call half @llvm.exp2.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> exp2()  =>  llvm.exp2.f16
define spir_func <3 x half> @_Z4exp2Dv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.exp2.f16(half %x0)
    %2 = call half @llvm.exp2.f16(half %x1)
    %3 = call half @llvm.exp2.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> exp2()  =>  llvm.exp2.f16
define spir_func <4 x half> @_Z4exp2Dv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.exp2.f16(half %x0)
    %2 = call half @llvm.exp2.f16(half %x1)
    %3 = call half @llvm.exp2.f16(half %x2)
    %4 = call half @llvm.exp2.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llvm.exp2.f16(half) #0

; half log2()  =>  llvm.log2.f16
define spir_func half @_Z4log2Dh(
    half %x) #0
{
    %1 = call half @llvm.log2.f16(half %x)

    ret half %1
}

; <2 x half> log2()  =>  llvm.log2.f16
define spir_func <2 x half> @_Z4log2Dv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.log2.f16(half %x0)
    %2 = call half @llvm.log2.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> log2()  =>  llvm.log2.f16
define spir_func <3 x half> @_Z4log2Dv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.log2.f16(half %x0)
    %2 = call half @llvm.log2.f16(half %x1)
    %3 = call half @llvm.log2.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> log2()  =>  llvm.log2.f16
define spir_func <4 x half> @_Z4log2Dv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.log2.f16(half %x0)
    %2 = call half @llvm.log2.f16(half %x1)
    %3 = call half @llvm.log2.f16(half %x2)
    %4 = call half @llvm.log2.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llvm.log2.f16(half) #0

; half sqrt()  =>  llvm.sqrt.f16
define spir_func half @_Z4sqrtDh(
    half %x) #0
{
    %1 = call half @llvm.sqrt.f16(half %x)

    ret half %1
}

; <2 x half> sqrt()  =>  llvm.sqrt.f16
define spir_func <2 x half> @_Z4sqrtDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.sqrt.f16(half %x0)
    %2 = call half @llvm.sqrt.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> sqrt()  =>  llvm.sqrt.f16
define spir_func <3 x half> @_Z4sqrtDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.sqrt.f16(half %x0)
    %2 = call half @llvm.sqrt.f16(half %x1)
    %3 = call half @llvm.sqrt.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> sqrt()  =>  llvm.sqrt.f16
define spir_func <4 x half> @_Z4sqrtDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.sqrt.f16(half %x0)
    %2 = call half @llvm.sqrt.f16(half %x1)
    %3 = call half @llvm.sqrt.f16(half %x2)
    %4 = call half @llvm.sqrt.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llvm.sqrt.f16(half) #0

; half inverseSqrt()  =>  llpc.inverseSqrt.f16
define spir_func half @_Z11inverseSqrtDh(
    half %x) #0
{
    %1 = call half @llpc.inverseSqrt.f16(half %x)

    ret half %1
}

; <2 x half> inverseSqrt()  =>  llpc.inverseSqrt.f16
define spir_func <2 x half> @_Z11inverseSqrtDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.inverseSqrt.f16(half %x0)
    %2 = call half @llpc.inverseSqrt.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> inverseSqrt()  =>  llpc.inverseSqrt.f16
define spir_func <3 x half> @_Z11inverseSqrtDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.inverseSqrt.f16(half %x0)
    %2 = call half @llpc.inverseSqrt.f16(half %x1)
    %3 = call half @llpc.inverseSqrt.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> inverseSqrt()  =>  llpc.inverseSqrt.f16
define spir_func <4 x half> @_Z11inverseSqrtDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.inverseSqrt.f16(half %x0)
    %2 = call half @llpc.inverseSqrt.f16(half %x1)
    %3 = call half @llpc.inverseSqrt.f16(half %x2)
    %4 = call half @llpc.inverseSqrt.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.inverseSqrt.f16(half) #0

; =====================================================================================================================
; >>>  Common Functions
; =====================================================================================================================

; half fabs()  =>  llvm.fabs.f16
define spir_func half @_Z4fabsDh(
    half %x) #0
{
    %1 = call half @llvm.fabs.f16(half %x)

    ret half %1
}

; <2 x half> fabs()  =>  llvm.fabs.f16
define spir_func <2 x half> @_Z4fabsDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.fabs.f16(half %x0)
    %2 = call half @llvm.fabs.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> fabs()  =>  llvm.fabs.f16
define spir_func <3 x half> @_Z4fabsDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.fabs.f16(half %x0)
    %2 = call half @llvm.fabs.f16(half %x1)
    %3 = call half @llvm.fabs.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> fabs()  =>  llvm.fabs.f16
define spir_func <4 x half> @_Z4fabsDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.fabs.f16(half %x0)
    %2 = call half @llvm.fabs.f16(half %x1)
    %3 = call half @llvm.fabs.f16(half %x2)
    %4 = call half @llvm.fabs.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llvm.fabs.f16(half) #0

; half fsign()  =>  llpc.fsign.f16
define spir_func half @_Z5fsignDh(
    half %x) #0
{
    %1 = call half @llpc.fsign.f16(half %x)

    ret half %1
}

; <2 x half> fsign()  =>  llpc.fsign.f16
define spir_func <2 x half> @_Z5fsignDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fsign.f16(half %x0)
    %2 = call half @llpc.fsign.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> fsign()  =>  llpc.fsign.f16
define spir_func <3 x half> @_Z5fsignDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fsign.f16(half %x0)
    %2 = call half @llpc.fsign.f16(half %x1)
    %3 = call half @llpc.fsign.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> fsign()  =>  llpc.fsign.f16
define spir_func <4 x half> @_Z5fsignDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fsign.f16(half %x0)
    %2 = call half @llpc.fsign.f16(half %x1)
    %3 = call half @llpc.fsign.f16(half %x2)
    %4 = call half @llpc.fsign.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.fsign.f16(half) #0

; half floor()  =>  llvm.floor.f16
define spir_func half @_Z5floorDh(
    half %x) #0
{
    %1 = call half @llvm.floor.f16(half %x)

    ret half %1
}

; <2 x half> floor()  =>  llvm.floor.f16
define spir_func <2 x half> @_Z5floorDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.floor.f16(half %x0)
    %2 = call half @llvm.floor.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> floor()  =>  llvm.floor.f16
define spir_func <3 x half> @_Z5floorDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.floor.f16(half %x0)
    %2 = call half @llvm.floor.f16(half %x1)
    %3 = call half @llvm.floor.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> floor()  =>  llvm.floor.f16
define spir_func <4 x half> @_Z5floorDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.floor.f16(half %x0)
    %2 = call half @llvm.floor.f16(half %x1)
    %3 = call half @llvm.floor.f16(half %x2)
    %4 = call half @llvm.floor.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llvm.floor.f16(half) #0

; half trunc()  =>  llvm.trunc.f16
define spir_func half @_Z5truncDh(
    half %x) #0
{
    %1 = call half @llvm.trunc.f16(half %x)

    ret half %1
}

; <2 x half> trunc()  =>  llvm.trunc.f16
define spir_func <2 x half> @_Z5truncDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.trunc.f16(half %x0)
    %2 = call half @llvm.trunc.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> trunc()  =>  llvm.trunc.f16
define spir_func <3 x half> @_Z5truncDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.trunc.f16(half %x0)
    %2 = call half @llvm.trunc.f16(half %x1)
    %3 = call half @llvm.trunc.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> trunc()  =>  llvm.trunc.f16
define spir_func <4 x half> @_Z5truncDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.trunc.f16(half %x0)
    %2 = call half @llvm.trunc.f16(half %x1)
    %3 = call half @llvm.trunc.f16(half %x2)
    %4 = call half @llvm.trunc.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llvm.trunc.f16(half) #0

; half round()  =>  llpc.round.f16
define spir_func half @_Z5roundDh(
    half %x) #0
{
    %1 = call half @llpc.round.f16(half %x)

    ret half %1
}

; <2 x half> round()  =>  llpc.round.f16
define spir_func <2 x half> @_Z5roundDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.round.f16(half %x0)
    %2 = call half @llpc.round.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> round()  =>  llpc.round.f16
define spir_func <3 x half> @_Z5roundDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.round.f16(half %x0)
    %2 = call half @llpc.round.f16(half %x1)
    %3 = call half @llpc.round.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> round()  =>  llpc.round.f16
define spir_func <4 x half> @_Z5roundDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.round.f16(half %x0)
    %2 = call half @llpc.round.f16(half %x1)
    %3 = call half @llpc.round.f16(half %x2)
    %4 = call half @llpc.round.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.round.f16(half) #0

; half roundEven()  =>  llvm.rint.f16
define spir_func half @_Z9roundEvenDh(
    half %x) #0
{
    %1 = call half @llvm.rint.f16(half %x)

    ret half %1
}

; <2 x half> roundEven()  =>  llvm.rint.f16
define spir_func <2 x half> @_Z9roundEvenDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.rint.f16(half %x0)
    %2 = call half @llvm.rint.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> roundEven()  =>  llvm.rint.f16
define spir_func <3 x half> @_Z9roundEvenDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.rint.f16(half %x0)
    %2 = call half @llvm.rint.f16(half %x1)
    %3 = call half @llvm.rint.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> roundEven()  =>  llvm.rint.f16
define spir_func <4 x half> @_Z9roundEvenDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.rint.f16(half %x0)
    %2 = call half @llvm.rint.f16(half %x1)
    %3 = call half @llvm.rint.f16(half %x2)
    %4 = call half @llvm.rint.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llvm.rint.f16(half) #0

; half ceil()  =>  llvm.ceil.f16
define spir_func half @_Z4ceilDh(
    half %x) #0
{
    %1 = call half @llvm.ceil.f16(half %x)

    ret half %1
}

; <2 x half> ceil()  =>  llvm.ceil.f16
define spir_func <2 x half> @_Z4ceilDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.ceil.f16(half %x0)
    %2 = call half @llvm.ceil.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> ceil()  =>  llvm.ceil.f16
define spir_func <3 x half> @_Z4ceilDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.ceil.f16(half %x0)
    %2 = call half @llvm.ceil.f16(half %x1)
    %3 = call half @llvm.ceil.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> ceil()  =>  llvm.ceil.f16
define spir_func <4 x half> @_Z4ceilDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.ceil.f16(half %x0)
    %2 = call half @llvm.ceil.f16(half %x1)
    %3 = call half @llvm.ceil.f16(half %x2)
    %4 = call half @llvm.ceil.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llvm.ceil.f16(half) #0

; half fract()  =>  llpc.fract.f16
define spir_func half @_Z5fractDh(
    half %x) #0
{
    %1 = call half @llpc.fract.f16(half %x)

    ret half %1
}

; <2 x half> fract()  =>  llpc.fract.f16
define spir_func <2 x half> @_Z5fractDv2_Dh(
    <2 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fract.f16(half %x0)
    %2 = call half @llpc.fract.f16(half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> fract()  =>  llpc.fract.f16
define spir_func <3 x half> @_Z5fractDv3_Dh(
    <3 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fract.f16(half %x0)
    %2 = call half @llpc.fract.f16(half %x1)
    %3 = call half @llpc.fract.f16(half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> fract()  =>  llpc.fract.f16
define spir_func <4 x half> @_Z5fractDv4_Dh(
    <4 x half> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fract.f16(half %x0)
    %2 = call half @llpc.fract.f16(half %x1)
    %3 = call half @llpc.fract.f16(half %x2)
    %4 = call half @llpc.fract.f16(half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.fract.f16(half) #0

; half fmod()  =>  llpc.mod.f16
define spir_func half @_Z4fmodDhDh(
    half %x, half %y) #0
{
    %1 = call half @llpc.mod.f16(half %x, half %y)

    ret half %1
}

; <2 x half> fmod()  =>  llpc.mod.f16
define spir_func <2 x half> @_Z4fmodDv2_DhDv2_Dh(
    <2 x half> %x, <2 x half> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    %y0 = extractelement <2 x half> %y, i32 0
    %y1 = extractelement <2 x half> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.mod.f16(half %x0, half %y0)
    %2 = call half @llpc.mod.f16(half %x1, half %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> fmod()  =>  llpc.mod.f16
define spir_func <3 x half> @_Z4fmodDv3_DhDv3_Dh(
    <3 x half> %x, <3 x half> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    %y0 = extractelement <3 x half> %y, i32 0
    %y1 = extractelement <3 x half> %y, i32 1
    %y2 = extractelement <3 x half> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.mod.f16(half %x0, half %y0)
    %2 = call half @llpc.mod.f16(half %x1, half %y1)
    %3 = call half @llpc.mod.f16(half %x2, half %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> fmod()  =>  llpc.mod.f16
define spir_func <4 x half> @_Z4fmodDv4_DhDv4_Dh(
    <4 x half> %x, <4 x half> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    %y0 = extractelement <4 x half> %y, i32 0
    %y1 = extractelement <4 x half> %y, i32 1
    %y2 = extractelement <4 x half> %y, i32 2
    %y3 = extractelement <4 x half> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.mod.f16(half %x0, half %y0)
    %2 = call half @llpc.mod.f16(half %x1, half %y1)
    %3 = call half @llpc.mod.f16(half %x2, half %y2)
    %4 = call half @llpc.mod.f16(half %x3, half %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.mod.f16(half, half) #0

; half step()  =>  llpc.step.f16
define spir_func half @_Z4stepDhDh(
    half %edge, half %x) #0
{
    %1 = call half @llpc.step.f16(half %edge, half %x)

    ret half %1
}

; <2 x half> step()  =>  llpc.step.f16
define spir_func <2 x half> @_Z4stepDv2_DhDv2_Dh(
    <2 x half> %edge, <2 x half> %x) #0
{
    ; Extract components from source vectors
    %edge0 = extractelement <2 x half> %edge, i32 0
    %edge1 = extractelement <2 x half> %edge, i32 1

    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.step.f16(half %edge0, half %x0)
    %2 = call half @llpc.step.f16(half %edge1, half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> step()  =>  llpc.step.f16
define spir_func <3 x half> @_Z4stepDv3_DhDv3_Dh(
    <3 x half> %edge, <3 x half> %x) #0
{
    ; Extract components from source vectors
    %edge0 = extractelement <3 x half> %edge, i32 0
    %edge1 = extractelement <3 x half> %edge, i32 1
    %edge2 = extractelement <3 x half> %edge, i32 2

    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.step.f16(half %edge0, half %x0)
    %2 = call half @llpc.step.f16(half %edge1, half %x1)
    %3 = call half @llpc.step.f16(half %edge2, half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> step()  =>  llpc.step.f16
define spir_func <4 x half> @_Z4stepDv4_DhDv4_Dh(
    <4 x half> %edge, <4 x half> %x) #0
{
    ; Extract components from source vectors
    %edge0 = extractelement <4 x half> %edge, i32 0
    %edge1 = extractelement <4 x half> %edge, i32 1
    %edge2 = extractelement <4 x half> %edge, i32 2
    %edge3 = extractelement <4 x half> %edge, i32 3

    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.step.f16(half %edge0, half %x0)
    %2 = call half @llpc.step.f16(half %edge1, half %x1)
    %3 = call half @llpc.step.f16(half %edge2, half %x2)
    %4 = call half @llpc.step.f16(half %edge3, half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.step.f16(half, half) #0

; half smoothStep()  =>  llpc.smoothStep.f16
define spir_func half @_Z10smoothStepDhDhDh(
    half %edge0, half %edge1, half %x) #0
{
    %1 = call half @llpc.smoothStep.f16(half %edge0, half %edge1, half %x)

    ret half %1
}

; <2 x half> smoothStep()  =>  llpc.smoothStep.f16
define spir_func <2 x half> @_Z10smoothStepDv2_DhDv2_DhDv2_Dh(
    <2 x half> %edge0, <2 x half> %edge1, <2 x half> %x) #0
{
    ; Extract components from source vectors
    %edge00 = extractelement <2 x half> %edge0, i32 0
    %edge01 = extractelement <2 x half> %edge0, i32 1

    %edge10 = extractelement <2 x half> %edge1, i32 0
    %edge11 = extractelement <2 x half> %edge1, i32 1

    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.smoothStep.f16(half %edge00, half %edge10, half %x0)
    %2 = call half @llpc.smoothStep.f16(half %edge01, half %edge11, half %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> smoothStep()  =>  llpc.smoothStep.f16
define spir_func <3 x half> @_Z10smoothStepDv3_DhDv3_DhDv3_Dh(
    <3 x half> %edge0, <3 x half> %edge1, <3 x half> %x) #0
{
    ; Extract components from source vectors
    %edge00 = extractelement <3 x half> %edge0, i32 0
    %edge01 = extractelement <3 x half> %edge0, i32 1
    %edge02 = extractelement <3 x half> %edge0, i32 2

    %edge10 = extractelement <3 x half> %edge1, i32 0
    %edge11 = extractelement <3 x half> %edge1, i32 1
    %edge12 = extractelement <3 x half> %edge1, i32 2

    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.smoothStep.f16(half %edge00, half %edge10, half %x0)
    %2 = call half @llpc.smoothStep.f16(half %edge01, half %edge11, half %x1)
    %3 = call half @llpc.smoothStep.f16(half %edge02, half %edge12, half %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> smoothStep()  =>  llpc.smoothStep.f16
define spir_func <4 x half> @_Z10smoothStepDv4_DhDv4_DhDv4_Dh(
    <4 x half> %edge0, <4 x half> %edge1, <4 x half> %x) #0
{
    ; Extract components from source vectors
    %edge00 = extractelement <4 x half> %edge0, i32 0
    %edge01 = extractelement <4 x half> %edge0, i32 1
    %edge02 = extractelement <4 x half> %edge0, i32 2
    %edge03 = extractelement <4 x half> %edge0, i32 3

    %edge10 = extractelement <4 x half> %edge1, i32 0
    %edge11 = extractelement <4 x half> %edge1, i32 1
    %edge12 = extractelement <4 x half> %edge1, i32 2
    %edge13 = extractelement <4 x half> %edge1, i32 3

    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.smoothStep.f16(half %edge00, half %edge10, half %x0)
    %2 = call half @llpc.smoothStep.f16(half %edge01, half %edge11, half %x1)
    %3 = call half @llpc.smoothStep.f16(half %edge02, half %edge12, half %x2)
    %4 = call half @llpc.smoothStep.f16(half %edge03, half %edge13, half %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.smoothStep.f16(half, half, half) #0

; i1 isinf()  =>  llpc.isinf.f16
define spir_func i1 @_Z5isinfDh(
    half %value) #0
{
    %1 = call i1 @llpc.isinf.f16(half %value)

    ret i1 %1
}

; <2 x i1> isinf()  =>  llpc.isinf.f16
define spir_func <2 x i1> @_Z5isinfDv2_Dh(
    <2 x half> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x half> %value, i32 0
    %value1 = extractelement <2 x half> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isinf.f16(half %value0)
    %2 = call i1 @llpc.isinf.f16(half %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i1>
    %4 = load <2 x i1>, <2 x i1>* %3
    %5 = insertelement <2 x i1> %4, i1 %1, i32 0
    %6 = insertelement <2 x i1> %5, i1 %2, i32 1

    ret <2 x i1> %6
}

; <3 x i1> isinf()  =>  llpc.isinf.f16
define spir_func <3 x i1> @_Z5isinfDv3_Dh(
    <3 x half> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x half> %value, i32 0
    %value1 = extractelement <3 x half> %value, i32 1
    %value2 = extractelement <3 x half> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isinf.f16(half %value0)
    %2 = call i1 @llpc.isinf.f16(half %value1)
    %3 = call i1 @llpc.isinf.f16(half %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i1>
    %5 = load <3 x i1>, <3 x i1>* %4
    %6 = insertelement <3 x i1> %5, i1 %1, i32 0
    %7 = insertelement <3 x i1> %6, i1 %2, i32 1
    %8 = insertelement <3 x i1> %7, i1 %3, i32 2

    ret <3 x i1> %8
}

; <4 x i1> isinf()  =>  llpc.isinf.f16
define spir_func <4 x i1> @_Z5isinfDv4_Dh(
    <4 x half> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x half> %value, i32 0
    %value1 = extractelement <4 x half> %value, i32 1
    %value2 = extractelement <4 x half> %value, i32 2
    %value3 = extractelement <4 x half> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isinf.f16(half %value0)
    %2 = call i1 @llpc.isinf.f16(half %value1)
    %3 = call i1 @llpc.isinf.f16(half %value2)
    %4 = call i1 @llpc.isinf.f16(half %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i1>
    %6 = load <4 x i1>, <4 x i1>* %5
    %7 = insertelement <4 x i1> %6, i1 %1, i32 0
    %8 = insertelement <4 x i1> %7, i1 %2, i32 1
    %9 = insertelement <4 x i1> %8, i1 %3, i32 2
    %10 = insertelement <4 x i1> %9, i1 %4, i32 3

    ret <4 x i1> %10
}

declare i1 @llpc.isinf.f16(half) #0

; i1 isnan()  =>  llpc.isnan.f16
define spir_func i1 @_Z5isnanDh(
    half %value) #0
{
    %1 = call i1 @llpc.isnan.f16(half %value)

    ret i1 %1
}

; <2 x i1> isnan()  =>  llpc.isnan.f16
define spir_func <2 x i1> @_Z5isnanDv2_Dh(
    <2 x half> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x half> %value, i32 0
    %value1 = extractelement <2 x half> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isnan.f16(half %value0)
    %2 = call i1 @llpc.isnan.f16(half %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i1>
    %4 = load <2 x i1>, <2 x i1>* %3
    %5 = insertelement <2 x i1> %4, i1 %1, i32 0
    %6 = insertelement <2 x i1> %5, i1 %2, i32 1

    ret <2 x i1> %6
}

; <3 x i1> isnan()  =>  llpc.isnan.f16
define spir_func <3 x i1> @_Z5isnanDv3_Dh(
    <3 x half> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x half> %value, i32 0
    %value1 = extractelement <3 x half> %value, i32 1
    %value2 = extractelement <3 x half> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isnan.f16(half %value0)
    %2 = call i1 @llpc.isnan.f16(half %value1)
    %3 = call i1 @llpc.isnan.f16(half %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i1>
    %5 = load <3 x i1>, <3 x i1>* %4
    %6 = insertelement <3 x i1> %5, i1 %1, i32 0
    %7 = insertelement <3 x i1> %6, i1 %2, i32 1
    %8 = insertelement <3 x i1> %7, i1 %3, i32 2

    ret <3 x i1> %8
}

; <4 x i1> isnan()  =>  llpc.isnan.f16
define spir_func <4 x i1> @_Z5isnanDv4_Dh(
    <4 x half> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x half> %value, i32 0
    %value1 = extractelement <4 x half> %value, i32 1
    %value2 = extractelement <4 x half> %value, i32 2
    %value3 = extractelement <4 x half> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isnan.f16(half %value0)
    %2 = call i1 @llpc.isnan.f16(half %value1)
    %3 = call i1 @llpc.isnan.f16(half %value2)
    %4 = call i1 @llpc.isnan.f16(half %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i1>
    %6 = load <4 x i1>, <4 x i1>* %5
    %7 = insertelement <4 x i1> %6, i1 %1, i32 0
    %8 = insertelement <4 x i1> %7, i1 %2, i32 1
    %9 = insertelement <4 x i1> %8, i1 %3, i32 2
    %10 = insertelement <4 x i1> %9, i1 %4, i32 3

    ret <4 x i1> %10
}

declare i1 @llpc.isnan.f16(half) #0

; half ldexp()  =>  llvm.amdgcn.ldexp.f16
define spir_func half @_Z5ldexpDhi(
    half %x, i32 %exp) #0
{
    %1 = call half @llvm.amdgcn.ldexp.f16(half %x, i32 %exp)

    ret half %1
}

; <2 x half> ldexp()  =>  llvm.amdgcn.ldexp.f16
define spir_func <2 x half> @_Z5ldexpDv2_DhDv2_i(
    <2 x half> %x, <2 x i32> %exp) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    %exp0 = extractelement <2 x i32> %exp, i32 0
    %exp1 = extractelement <2 x i32> %exp, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.amdgcn.ldexp.f16(half %x0, i32 %exp0)
    %2 = call half @llvm.amdgcn.ldexp.f16(half %x1, i32 %exp1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> ldexp()  =>  llvm.amdgcn.ldexp.f16
define spir_func <3 x half> @_Z5ldexpDv3_DhDv3_i(
    <3 x half> %x, <3 x i32> %exp) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    %exp0 = extractelement <3 x i32> %exp, i32 0
    %exp1 = extractelement <3 x i32> %exp, i32 1
    %exp2 = extractelement <3 x i32> %exp, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.amdgcn.ldexp.f16(half %x0, i32 %exp0)
    %2 = call half @llvm.amdgcn.ldexp.f16(half %x1, i32 %exp1)
    %3 = call half @llvm.amdgcn.ldexp.f16(half %x2, i32 %exp2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> ldexp()  =>  llvm.amdgcn.ldexp.f16
define spir_func <4 x half> @_Z5ldexpDv4_DhDv4_i(
    <4 x half> %x, <4 x i32> %exp) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    %exp0 = extractelement <4 x i32> %exp, i32 0
    %exp1 = extractelement <4 x i32> %exp, i32 1
    %exp2 = extractelement <4 x i32> %exp, i32 2
    %exp3 = extractelement <4 x i32> %exp, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llvm.amdgcn.ldexp.f16(half %x0, i32 %exp0)
    %2 = call half @llvm.amdgcn.ldexp.f16(half %x1, i32 %exp1)
    %3 = call half @llvm.amdgcn.ldexp.f16(half %x2, i32 %exp2)
    %4 = call half @llvm.amdgcn.ldexp.f16(half %x3, i32 %exp3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llvm.amdgcn.ldexp.f16(half, i32) #1

; =====================================================================================================================
; >>>  Derivative Functions
; =====================================================================================================================

; half DPdx()  =>  llpc.dpdx.f16
define spir_func half @_Z4DPdxDh(
    half %p) #0
{
    %1 = call half @llpc.dpdx.f16(half %p)

    ret half %1
}

; <2 x half> DPdx()  =>  llpc.dpdx.f16
define spir_func <2 x half> @_Z4DPdxDv2_Dh(
    <2 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x half> %p, i32 0
    %p1 = extractelement <2 x half> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdx.f16(half %p0)
    %2 = call half @llpc.dpdx.f16(half %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> DPdx()  =>  llpc.dpdx.f16
define spir_func <3 x half> @_Z4DPdxDv3_Dh(
    <3 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x half> %p, i32 0
    %p1 = extractelement <3 x half> %p, i32 1
    %p2 = extractelement <3 x half> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdx.f16(half %p0)
    %2 = call half @llpc.dpdx.f16(half %p1)
    %3 = call half @llpc.dpdx.f16(half %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> DPdx()  =>  llpc.dpdx.f16
define spir_func <4 x half> @_Z4DPdxDv4_Dh(
    <4 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x half> %p, i32 0
    %p1 = extractelement <4 x half> %p, i32 1
    %p2 = extractelement <4 x half> %p, i32 2
    %p3 = extractelement <4 x half> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdx.f16(half %p0)
    %2 = call half @llpc.dpdx.f16(half %p1)
    %3 = call half @llpc.dpdx.f16(half %p2)
    %4 = call half @llpc.dpdx.f16(half %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.dpdx.f16(half) #0

; half DPdy()  =>  llpc.dpdy.f16
define spir_func half @_Z4DPdyDh(
    half %p) #0
{
    %1 = call half @llpc.dpdy.f16(half %p)

    ret half %1
}

; <2 x half> DPdy()  =>  llpc.dpdy.f16
define spir_func <2 x half> @_Z4DPdyDv2_Dh(
    <2 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x half> %p, i32 0
    %p1 = extractelement <2 x half> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdy.f16(half %p0)
    %2 = call half @llpc.dpdy.f16(half %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> DPdy()  =>  llpc.dpdy.f16
define spir_func <3 x half> @_Z4DPdyDv3_Dh(
    <3 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x half> %p, i32 0
    %p1 = extractelement <3 x half> %p, i32 1
    %p2 = extractelement <3 x half> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdy.f16(half %p0)
    %2 = call half @llpc.dpdy.f16(half %p1)
    %3 = call half @llpc.dpdy.f16(half %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> DPdy()  =>  llpc.dpdy.f16
define spir_func <4 x half> @_Z4DPdyDv4_Dh(
    <4 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x half> %p, i32 0
    %p1 = extractelement <4 x half> %p, i32 1
    %p2 = extractelement <4 x half> %p, i32 2
    %p3 = extractelement <4 x half> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdy.f16(half %p0)
    %2 = call half @llpc.dpdy.f16(half %p1)
    %3 = call half @llpc.dpdy.f16(half %p2)
    %4 = call half @llpc.dpdy.f16(half %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.dpdy.f16(half) #0

; half Fwidth()  =>  llpc.fwidth.f16
define spir_func half @_Z6FwidthDh(
    half %p) #0
{
    %1 = call half @llpc.fwidth.f16(half %p)

    ret half %1
}

; <2 x half> Fwidth()  =>  llpc.fwidth.f16
define spir_func <2 x half> @_Z6FwidthDv2_Dh(
    <2 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x half> %p, i32 0
    %p1 = extractelement <2 x half> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fwidth.f16(half %p0)
    %2 = call half @llpc.fwidth.f16(half %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> Fwidth()  =>  llpc.fwidth.f16
define spir_func <3 x half> @_Z6FwidthDv3_Dh(
    <3 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x half> %p, i32 0
    %p1 = extractelement <3 x half> %p, i32 1
    %p2 = extractelement <3 x half> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fwidth.f16(half %p0)
    %2 = call half @llpc.fwidth.f16(half %p1)
    %3 = call half @llpc.fwidth.f16(half %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> Fwidth()  =>  llpc.fwidth.f16
define spir_func <4 x half> @_Z6FwidthDv4_Dh(
    <4 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x half> %p, i32 0
    %p1 = extractelement <4 x half> %p, i32 1
    %p2 = extractelement <4 x half> %p, i32 2
    %p3 = extractelement <4 x half> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fwidth.f16(half %p0)
    %2 = call half @llpc.fwidth.f16(half %p1)
    %3 = call half @llpc.fwidth.f16(half %p2)
    %4 = call half @llpc.fwidth.f16(half %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.fwidth.f16(half) #0

; half DPdxFine()  =>  llpc.dpdxFine.f16
define spir_func half @_Z8DPdxFineDh(
    half %p) #0
{
    %1 = call half @llpc.dpdxFine.f16(half %p)

    ret half %1
}

; <2 x half> DPdxFine()  =>  llpc.dpdxFine.f16
define spir_func <2 x half> @_Z8DPdxFineDv2_Dh(
    <2 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x half> %p, i32 0
    %p1 = extractelement <2 x half> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdxFine.f16(half %p0)
    %2 = call half @llpc.dpdxFine.f16(half %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> DPdxFine()  =>  llpc.dpdxFine.f16
define spir_func <3 x half> @_Z8DPdxFineDv3_Dh(
    <3 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x half> %p, i32 0
    %p1 = extractelement <3 x half> %p, i32 1
    %p2 = extractelement <3 x half> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdxFine.f16(half %p0)
    %2 = call half @llpc.dpdxFine.f16(half %p1)
    %3 = call half @llpc.dpdxFine.f16(half %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> DPdxFine()  =>  llpc.dpdxFine.f16
define spir_func <4 x half> @_Z8DPdxFineDv4_Dh(
    <4 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x half> %p, i32 0
    %p1 = extractelement <4 x half> %p, i32 1
    %p2 = extractelement <4 x half> %p, i32 2
    %p3 = extractelement <4 x half> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdxFine.f16(half %p0)
    %2 = call half @llpc.dpdxFine.f16(half %p1)
    %3 = call half @llpc.dpdxFine.f16(half %p2)
    %4 = call half @llpc.dpdxFine.f16(half %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.dpdxFine.f16(half) #0

; half DPdyFine()  =>  llpc.dpdyFine.f16
define spir_func half @_Z8DPdyFineDh(
    half %p) #0
{
    %1 = call half @llpc.dpdyFine.f16(half %p)

    ret half %1
}

; <2 x half> DPdyFine()  =>  llpc.dpdyFine.f16
define spir_func <2 x half> @_Z8DPdyFineDv2_Dh(
    <2 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x half> %p, i32 0
    %p1 = extractelement <2 x half> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdyFine.f16(half %p0)
    %2 = call half @llpc.dpdyFine.f16(half %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> DPdyFine()  =>  llpc.dpdyFine.f16
define spir_func <3 x half> @_Z8DPdyFineDv3_Dh(
    <3 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x half> %p, i32 0
    %p1 = extractelement <3 x half> %p, i32 1
    %p2 = extractelement <3 x half> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdyFine.f16(half %p0)
    %2 = call half @llpc.dpdyFine.f16(half %p1)
    %3 = call half @llpc.dpdyFine.f16(half %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> DPdyFine()  =>  llpc.dpdyFine.f16
define spir_func <4 x half> @_Z8DPdyFineDv4_Dh(
    <4 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x half> %p, i32 0
    %p1 = extractelement <4 x half> %p, i32 1
    %p2 = extractelement <4 x half> %p, i32 2
    %p3 = extractelement <4 x half> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdyFine.f16(half %p0)
    %2 = call half @llpc.dpdyFine.f16(half %p1)
    %3 = call half @llpc.dpdyFine.f16(half %p2)
    %4 = call half @llpc.dpdyFine.f16(half %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.dpdyFine.f16(half) #0

; half FwidthFine()  =>  llpc.fwidthFine.f16
define spir_func half @_Z10FwidthFineDh(
    half %p) #0
{
    %1 = call half @llpc.fwidthFine.f16(half %p)

    ret half %1
}

; <2 x half> FwidthFine()  =>  llpc.fwidthFine.f16
define spir_func <2 x half> @_Z10FwidthFineDv2_Dh(
    <2 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x half> %p, i32 0
    %p1 = extractelement <2 x half> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fwidthFine.f16(half %p0)
    %2 = call half @llpc.fwidthFine.f16(half %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> FwidthFine()  =>  llpc.fwidthFine.f16
define spir_func <3 x half> @_Z10FwidthFineDv3_Dh(
    <3 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x half> %p, i32 0
    %p1 = extractelement <3 x half> %p, i32 1
    %p2 = extractelement <3 x half> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fwidthFine.f16(half %p0)
    %2 = call half @llpc.fwidthFine.f16(half %p1)
    %3 = call half @llpc.fwidthFine.f16(half %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> FwidthFine()  =>  llpc.fwidthFine.f16
define spir_func <4 x half> @_Z10FwidthFineDv4_Dh(
    <4 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x half> %p, i32 0
    %p1 = extractelement <4 x half> %p, i32 1
    %p2 = extractelement <4 x half> %p, i32 2
    %p3 = extractelement <4 x half> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fwidthFine.f16(half %p0)
    %2 = call half @llpc.fwidthFine.f16(half %p1)
    %3 = call half @llpc.fwidthFine.f16(half %p2)
    %4 = call half @llpc.fwidthFine.f16(half %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.fwidthFine.f16(half) #0

; half DPdxCoarse()  =>  llpc.dpdxCoarse.f16
define spir_func half @_Z10DPdxCoarseDh(
    half %p) #0
{
    %1 = call half @llpc.dpdxCoarse.f16(half %p)

    ret half %1
}

; <2 x half> DPdxCoarse()  =>  llpc.dpdxCoarse.f16
define spir_func <2 x half> @_Z10DPdxCoarseDv2_Dh(
    <2 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x half> %p, i32 0
    %p1 = extractelement <2 x half> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdxCoarse.f16(half %p0)
    %2 = call half @llpc.dpdxCoarse.f16(half %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> DPdxCoarse()  =>  llpc.dpdxCoarse.f16
define spir_func <3 x half> @_Z10DPdxCoarseDv3_Dh(
    <3 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x half> %p, i32 0
    %p1 = extractelement <3 x half> %p, i32 1
    %p2 = extractelement <3 x half> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdxCoarse.f16(half %p0)
    %2 = call half @llpc.dpdxCoarse.f16(half %p1)
    %3 = call half @llpc.dpdxCoarse.f16(half %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> DPdxCoarse()  =>  llpc.dpdxCoarse.f16
define spir_func <4 x half> @_Z10DPdxCoarseDv4_Dh(
    <4 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x half> %p, i32 0
    %p1 = extractelement <4 x half> %p, i32 1
    %p2 = extractelement <4 x half> %p, i32 2
    %p3 = extractelement <4 x half> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdxCoarse.f16(half %p0)
    %2 = call half @llpc.dpdxCoarse.f16(half %p1)
    %3 = call half @llpc.dpdxCoarse.f16(half %p2)
    %4 = call half @llpc.dpdxCoarse.f16(half %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.dpdxCoarse.f16(half) #0

; half DPdyCoarse()  =>  llpc.dpdyCoarse.f16
define spir_func half @_Z10DPdyCoarseDh(
    half %p) #0
{
    %1 = call half @llpc.dpdyCoarse.f16(half %p)

    ret half %1
}

; <2 x half> DPdyCoarse()  =>  llpc.dpdyCoarse.f16
define spir_func <2 x half> @_Z10DPdyCoarseDv2_Dh(
    <2 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x half> %p, i32 0
    %p1 = extractelement <2 x half> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdyCoarse.f16(half %p0)
    %2 = call half @llpc.dpdyCoarse.f16(half %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> DPdyCoarse()  =>  llpc.dpdyCoarse.f16
define spir_func <3 x half> @_Z10DPdyCoarseDv3_Dh(
    <3 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x half> %p, i32 0
    %p1 = extractelement <3 x half> %p, i32 1
    %p2 = extractelement <3 x half> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdyCoarse.f16(half %p0)
    %2 = call half @llpc.dpdyCoarse.f16(half %p1)
    %3 = call half @llpc.dpdyCoarse.f16(half %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> DPdyCoarse()  =>  llpc.dpdyCoarse.f16
define spir_func <4 x half> @_Z10DPdyCoarseDv4_Dh(
    <4 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x half> %p, i32 0
    %p1 = extractelement <4 x half> %p, i32 1
    %p2 = extractelement <4 x half> %p, i32 2
    %p3 = extractelement <4 x half> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.dpdyCoarse.f16(half %p0)
    %2 = call half @llpc.dpdyCoarse.f16(half %p1)
    %3 = call half @llpc.dpdyCoarse.f16(half %p2)
    %4 = call half @llpc.dpdyCoarse.f16(half %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.dpdyCoarse.f16(half) #0

; half FwidthCoarse()  =>  llpc.fwidthCoarse.f16
define spir_func half @_Z12FwidthCoarseDh(
    half %p) #0
{
    %1 = call half @llpc.fwidthCoarse.f16(half %p)

    ret half %1
}

; <2 x half> FwidthCoarse()  =>  llpc.fwidthCoarse.f16
define spir_func <2 x half> @_Z12FwidthCoarseDv2_Dh(
    <2 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x half> %p, i32 0
    %p1 = extractelement <2 x half> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fwidthCoarse.f16(half %p0)
    %2 = call half @llpc.fwidthCoarse.f16(half %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> FwidthCoarse()  =>  llpc.fwidthCoarse.f16
define spir_func <3 x half> @_Z12FwidthCoarseDv3_Dh(
    <3 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x half> %p, i32 0
    %p1 = extractelement <3 x half> %p, i32 1
    %p2 = extractelement <3 x half> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fwidthCoarse.f16(half %p0)
    %2 = call half @llpc.fwidthCoarse.f16(half %p1)
    %3 = call half @llpc.fwidthCoarse.f16(half %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> FwidthCoarse()  =>  llpc.fwidthCoarse.f16
define spir_func <4 x half> @_Z12FwidthCoarseDv4_Dh(
    <4 x half> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x half> %p, i32 0
    %p1 = extractelement <4 x half> %p, i32 1
    %p2 = extractelement <4 x half> %p, i32 2
    %p3 = extractelement <4 x half> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fwidthCoarse.f16(half %p0)
    %2 = call half @llpc.fwidthCoarse.f16(half %p1)
    %3 = call half @llpc.fwidthCoarse.f16(half %p2)
    %4 = call half @llpc.fwidthCoarse.f16(half %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.fwidthCoarse.f16(half) #0

; =====================================================================================================================
; >>>  Functions of extension AMD_shader_trinary_minmax
; =====================================================================================================================

; half FMin3AMD()  =>  llpc.fmin3.f16
define spir_func half @_Z8FMin3AMDDhDhDh(
    half %x, half %y, half %z) #0
{
    %1 = call half @llpc.fmin3.f16(half %x, half %y, half %z)

    ret half %1
}

; <2 x half> FMin3AMD()  =>  llpc.fmin3.f16
define spir_func <2 x half> @_Z8FMin3AMDDv2_DhDv2_DhDv2_Dh(
    <2 x half> %x, <2 x half> %y, <2 x half> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    %y0 = extractelement <2 x half> %y, i32 0
    %y1 = extractelement <2 x half> %y, i32 1

    %z0 = extractelement <2 x half> %z, i32 0
    %z1 = extractelement <2 x half> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fmin3.f16(half %x0, half %y0, half %z0)
    %2 = call half @llpc.fmin3.f16(half %x1, half %y1, half %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> FMin3AMD()  =>  llpc.fmin3.f16
define spir_func <3 x half> @_Z8FMin3AMDDv3_DhDv3_DhDv3_Dh(
    <3 x half> %x, <3 x half> %y, <3 x half> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    %y0 = extractelement <3 x half> %y, i32 0
    %y1 = extractelement <3 x half> %y, i32 1
    %y2 = extractelement <3 x half> %y, i32 2

    %z0 = extractelement <3 x half> %z, i32 0
    %z1 = extractelement <3 x half> %z, i32 1
    %z2 = extractelement <3 x half> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fmin3.f16(half %x0, half %y0, half %z0)
    %2 = call half @llpc.fmin3.f16(half %x1, half %y1, half %z1)
    %3 = call half @llpc.fmin3.f16(half %x2, half %y2, half %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> FMin3AMD()  =>  llpc.fmin3.f16
define spir_func <4 x half> @_Z8FMin3AMDDv4_DhDv4_DhDv4_Dh(
    <4 x half> %x, <4 x half> %y, <4 x half> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    %y0 = extractelement <4 x half> %y, i32 0
    %y1 = extractelement <4 x half> %y, i32 1
    %y2 = extractelement <4 x half> %y, i32 2
    %y3 = extractelement <4 x half> %y, i32 3

    %z0 = extractelement <4 x half> %z, i32 0
    %z1 = extractelement <4 x half> %z, i32 1
    %z2 = extractelement <4 x half> %z, i32 2
    %z3 = extractelement <4 x half> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fmin3.f16(half %x0, half %y0, half %z0)
    %2 = call half @llpc.fmin3.f16(half %x1, half %y1, half %z1)
    %3 = call half @llpc.fmin3.f16(half %x2, half %y2, half %z2)
    %4 = call half @llpc.fmin3.f16(half %x3, half %y3, half %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.fmin3.f16(half, half, half) #0

; half FMax3AMD()  =>  llpc.fmax3.f16
define spir_func half @_Z8FMax3AMDDhDhDh(
    half %x, half %y, half %z) #0
{
    %1 = call half @llpc.fmax3.f16(half %x, half %y, half %z)

    ret half %1
}

; <2 x half> FMax3AMD()  =>  llpc.fmax3.f16
define spir_func <2 x half> @_Z8FMax3AMDDv2_DhDv2_DhDv2_Dh(
    <2 x half> %x, <2 x half> %y, <2 x half> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    %y0 = extractelement <2 x half> %y, i32 0
    %y1 = extractelement <2 x half> %y, i32 1

    %z0 = extractelement <2 x half> %z, i32 0
    %z1 = extractelement <2 x half> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fmax3.f16(half %x0, half %y0, half %z0)
    %2 = call half @llpc.fmax3.f16(half %x1, half %y1, half %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> FMax3AMD()  =>  llpc.fmax3.f16
define spir_func <3 x half> @_Z8FMax3AMDDv3_DhDv3_DhDv3_Dh(
    <3 x half> %x, <3 x half> %y, <3 x half> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    %y0 = extractelement <3 x half> %y, i32 0
    %y1 = extractelement <3 x half> %y, i32 1
    %y2 = extractelement <3 x half> %y, i32 2

    %z0 = extractelement <3 x half> %z, i32 0
    %z1 = extractelement <3 x half> %z, i32 1
    %z2 = extractelement <3 x half> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fmax3.f16(half %x0, half %y0, half %z0)
    %2 = call half @llpc.fmax3.f16(half %x1, half %y1, half %z1)
    %3 = call half @llpc.fmax3.f16(half %x2, half %y2, half %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> FMax3AMD()  =>  llpc.fmax3.f16
define spir_func <4 x half> @_Z8FMax3AMDDv4_DhDv4_DhDv4_Dh(
    <4 x half> %x, <4 x half> %y, <4 x half> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    %y0 = extractelement <4 x half> %y, i32 0
    %y1 = extractelement <4 x half> %y, i32 1
    %y2 = extractelement <4 x half> %y, i32 2
    %y3 = extractelement <4 x half> %y, i32 3

    %z0 = extractelement <4 x half> %z, i32 0
    %z1 = extractelement <4 x half> %z, i32 1
    %z2 = extractelement <4 x half> %z, i32 2
    %z3 = extractelement <4 x half> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fmax3.f16(half %x0, half %y0, half %z0)
    %2 = call half @llpc.fmax3.f16(half %x1, half %y1, half %z1)
    %3 = call half @llpc.fmax3.f16(half %x2, half %y2, half %z2)
    %4 = call half @llpc.fmax3.f16(half %x3, half %y3, half %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.fmax3.f16(half, half, half) #0

; half FMid3AMD()  =>  llpc.fmid3.f16
define spir_func half @_Z8FMid3AMDDhDhDh(
    half %x, half %y, half %z) #0
{
    %1 = call half @llpc.fmid3.f16(half %x, half %y, half %z)

    ret half %1
}

; <2 x half> FMid3AMD()  =>  llpc.fmid3.f16
define spir_func <2 x half> @_Z8FMid3AMDDv2_DhDv2_DhDv2_Dh(
    <2 x half> %x, <2 x half> %y, <2 x half> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x half> %x, i32 0
    %x1 = extractelement <2 x half> %x, i32 1

    %y0 = extractelement <2 x half> %y, i32 0
    %y1 = extractelement <2 x half> %y, i32 1

    %z0 = extractelement <2 x half> %z, i32 0
    %z1 = extractelement <2 x half> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fmid3.f16(half %x0, half %y0, half %z0)
    %2 = call half @llpc.fmid3.f16(half %x1, half %y1, half %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x half>
    %4 = load <2 x half>, <2 x half>* %3
    %5 = insertelement <2 x half> %4, half %1, i32 0
    %6 = insertelement <2 x half> %5, half %2, i32 1

    ret <2 x half> %6
}

; <3 x half> FMid3AMD()  =>  llpc.fmid3.f16
define spir_func <3 x half> @_Z8FMid3AMDDv3_DhDv3_DhDv3_Dh(
    <3 x half> %x, <3 x half> %y, <3 x half> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x half> %x, i32 0
    %x1 = extractelement <3 x half> %x, i32 1
    %x2 = extractelement <3 x half> %x, i32 2

    %y0 = extractelement <3 x half> %y, i32 0
    %y1 = extractelement <3 x half> %y, i32 1
    %y2 = extractelement <3 x half> %y, i32 2

    %z0 = extractelement <3 x half> %z, i32 0
    %z1 = extractelement <3 x half> %z, i32 1
    %z2 = extractelement <3 x half> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fmid3.f16(half %x0, half %y0, half %z0)
    %2 = call half @llpc.fmid3.f16(half %x1, half %y1, half %z1)
    %3 = call half @llpc.fmid3.f16(half %x2, half %y2, half %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x half>
    %5 = load <3 x half>, <3 x half>* %4
    %6 = insertelement <3 x half> %5, half %1, i32 0
    %7 = insertelement <3 x half> %6, half %2, i32 1
    %8 = insertelement <3 x half> %7, half %3, i32 2

    ret <3 x half> %8
}

; <4 x half> FMid3AMD()  =>  llpc.fmid3.f16
define spir_func <4 x half> @_Z8FMid3AMDDv4_DhDv4_DhDv4_Dh(
    <4 x half> %x, <4 x half> %y, <4 x half> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x half> %x, i32 0
    %x1 = extractelement <4 x half> %x, i32 1
    %x2 = extractelement <4 x half> %x, i32 2
    %x3 = extractelement <4 x half> %x, i32 3

    %y0 = extractelement <4 x half> %y, i32 0
    %y1 = extractelement <4 x half> %y, i32 1
    %y2 = extractelement <4 x half> %y, i32 2
    %y3 = extractelement <4 x half> %y, i32 3

    %z0 = extractelement <4 x half> %z, i32 0
    %z1 = extractelement <4 x half> %z, i32 1
    %z2 = extractelement <4 x half> %z, i32 2
    %z3 = extractelement <4 x half> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call half @llpc.fmid3.f16(half %x0, half %y0, half %z0)
    %2 = call half @llpc.fmid3.f16(half %x1, half %y1, half %z1)
    %3 = call half @llpc.fmid3.f16(half %x2, half %y2, half %z2)
    %4 = call half @llpc.fmid3.f16(half %x3, half %y3, half %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x half>
    %6 = load <4 x half>, <4 x half>* %5
    %7 = insertelement <4 x half> %6, half %1, i32 0
    %8 = insertelement <4 x half> %7, half %2, i32 1
    %9 = insertelement <4 x half> %8, half %3, i32 2
    %10 = insertelement <4 x half> %9, half %4, i32 3

    ret <4 x half> %10
}

declare half @llpc.fmid3.f16(half, half, half) #0

attributes #0 = { nounwind }
attributes #1 = { nounwind readnone }
