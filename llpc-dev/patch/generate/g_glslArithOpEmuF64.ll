;**********************************************************************************************************************
;*
;*  Trade secret of Advanced Micro Devices, Inc.
;*  Copyright (c) 2017, Advanced Micro Devices, Inc., (unpublished)
;*
;*  All rights reserved. This notice is intended as a precaution against inadvertent publication and does not imply
;*  publication or any waiver of confidentiality. The year included in the foregoing notice is the year of creation of
;*  the work.
;*
;**********************************************************************************************************************

;**********************************************************************************************************************
;* @file  g_glslArithOpEmuF64.ll
;* @brief LLPC LLVM-IR file: contains emulation codes for GLSL arithmetic operations (float64).
;*
;* @note  This file has been generated automatically. Do not hand-modify this file. When changes are needed, modify the
;*        generating script genGlslArithOpEmuCode.py.
;**********************************************************************************************************************

target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v16:16:16-v24:32:32-v32:32:32-v48:64:64-v64:64:64-v96:128:128-v128:128:128-v192:256:256-v256:256:256-v512:512:512-v1024:1024:1024"
target triple = "spir64-unknown-unknown"

; =====================================================================================================================
; >>>  Operators
; =====================================================================================================================

; double fdiv()  =>  llpc.fdiv.f64
define spir_func double @_Z4fdivdd(
    double %y, double %x) #0
{
    %1 = call double @llpc.fdiv.f64(double %y, double %x)

    ret double %1
}

; <2 x double> fdiv()  =>  llpc.fdiv.f64
define spir_func <2 x double> @_Z4fdivDv2_dDv2_d(
    <2 x double> %y, <2 x double> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <2 x double> %y, i32 0
    %y1 = extractelement <2 x double> %y, i32 1

    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fdiv.f64(double %y0, double %x0)
    %2 = call double @llpc.fdiv.f64(double %y1, double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> fdiv()  =>  llpc.fdiv.f64
define spir_func <3 x double> @_Z4fdivDv3_dDv3_d(
    <3 x double> %y, <3 x double> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <3 x double> %y, i32 0
    %y1 = extractelement <3 x double> %y, i32 1
    %y2 = extractelement <3 x double> %y, i32 2

    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fdiv.f64(double %y0, double %x0)
    %2 = call double @llpc.fdiv.f64(double %y1, double %x1)
    %3 = call double @llpc.fdiv.f64(double %y2, double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> fdiv()  =>  llpc.fdiv.f64
define spir_func <4 x double> @_Z4fdivDv4_dDv4_d(
    <4 x double> %y, <4 x double> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <4 x double> %y, i32 0
    %y1 = extractelement <4 x double> %y, i32 1
    %y2 = extractelement <4 x double> %y, i32 2
    %y3 = extractelement <4 x double> %y, i32 3

    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fdiv.f64(double %y0, double %x0)
    %2 = call double @llpc.fdiv.f64(double %y1, double %x1)
    %3 = call double @llpc.fdiv.f64(double %y2, double %x2)
    %4 = call double @llpc.fdiv.f64(double %y3, double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llpc.fdiv.f64(double, double) #0

; =====================================================================================================================
; >>>  Exponential Functions
; =====================================================================================================================

; double sqrt()  =>  llvm.sqrt.f64
define spir_func double @_Z4sqrtd(
    double %x) #0
{
    %1 = call double @llvm.sqrt.f64(double %x)

    ret double %1
}

; <2 x double> sqrt()  =>  llvm.sqrt.f64
define spir_func <2 x double> @_Z4sqrtDv2_d(
    <2 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.sqrt.f64(double %x0)
    %2 = call double @llvm.sqrt.f64(double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> sqrt()  =>  llvm.sqrt.f64
define spir_func <3 x double> @_Z4sqrtDv3_d(
    <3 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.sqrt.f64(double %x0)
    %2 = call double @llvm.sqrt.f64(double %x1)
    %3 = call double @llvm.sqrt.f64(double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> sqrt()  =>  llvm.sqrt.f64
define spir_func <4 x double> @_Z4sqrtDv4_d(
    <4 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.sqrt.f64(double %x0)
    %2 = call double @llvm.sqrt.f64(double %x1)
    %3 = call double @llvm.sqrt.f64(double %x2)
    %4 = call double @llvm.sqrt.f64(double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llvm.sqrt.f64(double) #0

; double inverseSqrt()  =>  llpc.inverseSqrt.f64
define spir_func double @_Z11inverseSqrtd(
    double %x) #0
{
    %1 = call double @llpc.inverseSqrt.f64(double %x)

    ret double %1
}

; <2 x double> inverseSqrt()  =>  llpc.inverseSqrt.f64
define spir_func <2 x double> @_Z11inverseSqrtDv2_d(
    <2 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.inverseSqrt.f64(double %x0)
    %2 = call double @llpc.inverseSqrt.f64(double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> inverseSqrt()  =>  llpc.inverseSqrt.f64
define spir_func <3 x double> @_Z11inverseSqrtDv3_d(
    <3 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.inverseSqrt.f64(double %x0)
    %2 = call double @llpc.inverseSqrt.f64(double %x1)
    %3 = call double @llpc.inverseSqrt.f64(double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> inverseSqrt()  =>  llpc.inverseSqrt.f64
define spir_func <4 x double> @_Z11inverseSqrtDv4_d(
    <4 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.inverseSqrt.f64(double %x0)
    %2 = call double @llpc.inverseSqrt.f64(double %x1)
    %3 = call double @llpc.inverseSqrt.f64(double %x2)
    %4 = call double @llpc.inverseSqrt.f64(double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llpc.inverseSqrt.f64(double) #0

; =====================================================================================================================
; >>>  Common Functions
; =====================================================================================================================

; double fabs()  =>  llvm.fabs.f64
define spir_func double @_Z4fabsd(
    double %x) #0
{
    %1 = call double @llvm.fabs.f64(double %x)

    ret double %1
}

; <2 x double> fabs()  =>  llvm.fabs.f64
define spir_func <2 x double> @_Z4fabsDv2_d(
    <2 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.fabs.f64(double %x0)
    %2 = call double @llvm.fabs.f64(double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> fabs()  =>  llvm.fabs.f64
define spir_func <3 x double> @_Z4fabsDv3_d(
    <3 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.fabs.f64(double %x0)
    %2 = call double @llvm.fabs.f64(double %x1)
    %3 = call double @llvm.fabs.f64(double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> fabs()  =>  llvm.fabs.f64
define spir_func <4 x double> @_Z4fabsDv4_d(
    <4 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.fabs.f64(double %x0)
    %2 = call double @llvm.fabs.f64(double %x1)
    %3 = call double @llvm.fabs.f64(double %x2)
    %4 = call double @llvm.fabs.f64(double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llvm.fabs.f64(double) #0

; double fsign()  =>  llpc.fsign.f64
define spir_func double @_Z5fsignd(
    double %x) #0
{
    %1 = call double @llpc.fsign.f64(double %x)

    ret double %1
}

; <2 x double> fsign()  =>  llpc.fsign.f64
define spir_func <2 x double> @_Z5fsignDv2_d(
    <2 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fsign.f64(double %x0)
    %2 = call double @llpc.fsign.f64(double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> fsign()  =>  llpc.fsign.f64
define spir_func <3 x double> @_Z5fsignDv3_d(
    <3 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fsign.f64(double %x0)
    %2 = call double @llpc.fsign.f64(double %x1)
    %3 = call double @llpc.fsign.f64(double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> fsign()  =>  llpc.fsign.f64
define spir_func <4 x double> @_Z5fsignDv4_d(
    <4 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fsign.f64(double %x0)
    %2 = call double @llpc.fsign.f64(double %x1)
    %3 = call double @llpc.fsign.f64(double %x2)
    %4 = call double @llpc.fsign.f64(double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llpc.fsign.f64(double) #0

; double floor()  =>  llvm.floor.f64
define spir_func double @_Z5floord(
    double %x) #0
{
    %1 = call double @llvm.floor.f64(double %x)

    ret double %1
}

; <2 x double> floor()  =>  llvm.floor.f64
define spir_func <2 x double> @_Z5floorDv2_d(
    <2 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.floor.f64(double %x0)
    %2 = call double @llvm.floor.f64(double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> floor()  =>  llvm.floor.f64
define spir_func <3 x double> @_Z5floorDv3_d(
    <3 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.floor.f64(double %x0)
    %2 = call double @llvm.floor.f64(double %x1)
    %3 = call double @llvm.floor.f64(double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> floor()  =>  llvm.floor.f64
define spir_func <4 x double> @_Z5floorDv4_d(
    <4 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.floor.f64(double %x0)
    %2 = call double @llvm.floor.f64(double %x1)
    %3 = call double @llvm.floor.f64(double %x2)
    %4 = call double @llvm.floor.f64(double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llvm.floor.f64(double) #0

; double trunc()  =>  llvm.trunc.f64
define spir_func double @_Z5truncd(
    double %x) #0
{
    %1 = call double @llvm.trunc.f64(double %x)

    ret double %1
}

; <2 x double> trunc()  =>  llvm.trunc.f64
define spir_func <2 x double> @_Z5truncDv2_d(
    <2 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.trunc.f64(double %x0)
    %2 = call double @llvm.trunc.f64(double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> trunc()  =>  llvm.trunc.f64
define spir_func <3 x double> @_Z5truncDv3_d(
    <3 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.trunc.f64(double %x0)
    %2 = call double @llvm.trunc.f64(double %x1)
    %3 = call double @llvm.trunc.f64(double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> trunc()  =>  llvm.trunc.f64
define spir_func <4 x double> @_Z5truncDv4_d(
    <4 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.trunc.f64(double %x0)
    %2 = call double @llvm.trunc.f64(double %x1)
    %3 = call double @llvm.trunc.f64(double %x2)
    %4 = call double @llvm.trunc.f64(double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llvm.trunc.f64(double) #0

; double round()  =>  llpc.round.f64
define spir_func double @_Z5roundd(
    double %x) #0
{
    %1 = call double @llpc.round.f64(double %x)

    ret double %1
}

; <2 x double> round()  =>  llpc.round.f64
define spir_func <2 x double> @_Z5roundDv2_d(
    <2 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.round.f64(double %x0)
    %2 = call double @llpc.round.f64(double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> round()  =>  llpc.round.f64
define spir_func <3 x double> @_Z5roundDv3_d(
    <3 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.round.f64(double %x0)
    %2 = call double @llpc.round.f64(double %x1)
    %3 = call double @llpc.round.f64(double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> round()  =>  llpc.round.f64
define spir_func <4 x double> @_Z5roundDv4_d(
    <4 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.round.f64(double %x0)
    %2 = call double @llpc.round.f64(double %x1)
    %3 = call double @llpc.round.f64(double %x2)
    %4 = call double @llpc.round.f64(double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llpc.round.f64(double) #0

; double roundEven()  =>  llvm.rint.f64
define spir_func double @_Z9roundEvend(
    double %x) #0
{
    %1 = call double @llvm.rint.f64(double %x)

    ret double %1
}

; <2 x double> roundEven()  =>  llvm.rint.f64
define spir_func <2 x double> @_Z9roundEvenDv2_d(
    <2 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.rint.f64(double %x0)
    %2 = call double @llvm.rint.f64(double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> roundEven()  =>  llvm.rint.f64
define spir_func <3 x double> @_Z9roundEvenDv3_d(
    <3 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.rint.f64(double %x0)
    %2 = call double @llvm.rint.f64(double %x1)
    %3 = call double @llvm.rint.f64(double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> roundEven()  =>  llvm.rint.f64
define spir_func <4 x double> @_Z9roundEvenDv4_d(
    <4 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.rint.f64(double %x0)
    %2 = call double @llvm.rint.f64(double %x1)
    %3 = call double @llvm.rint.f64(double %x2)
    %4 = call double @llvm.rint.f64(double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llvm.rint.f64(double) #0

; double ceil()  =>  llvm.ceil.f64
define spir_func double @_Z4ceild(
    double %x) #0
{
    %1 = call double @llvm.ceil.f64(double %x)

    ret double %1
}

; <2 x double> ceil()  =>  llvm.ceil.f64
define spir_func <2 x double> @_Z4ceilDv2_d(
    <2 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.ceil.f64(double %x0)
    %2 = call double @llvm.ceil.f64(double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> ceil()  =>  llvm.ceil.f64
define spir_func <3 x double> @_Z4ceilDv3_d(
    <3 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.ceil.f64(double %x0)
    %2 = call double @llvm.ceil.f64(double %x1)
    %3 = call double @llvm.ceil.f64(double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> ceil()  =>  llvm.ceil.f64
define spir_func <4 x double> @_Z4ceilDv4_d(
    <4 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.ceil.f64(double %x0)
    %2 = call double @llvm.ceil.f64(double %x1)
    %3 = call double @llvm.ceil.f64(double %x2)
    %4 = call double @llvm.ceil.f64(double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llvm.ceil.f64(double) #0

; double fract()  =>  llpc.fract.f64
define spir_func double @_Z5fractd(
    double %x) #0
{
    %1 = call double @llpc.fract.f64(double %x)

    ret double %1
}

; <2 x double> fract()  =>  llpc.fract.f64
define spir_func <2 x double> @_Z5fractDv2_d(
    <2 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fract.f64(double %x0)
    %2 = call double @llpc.fract.f64(double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> fract()  =>  llpc.fract.f64
define spir_func <3 x double> @_Z5fractDv3_d(
    <3 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fract.f64(double %x0)
    %2 = call double @llpc.fract.f64(double %x1)
    %3 = call double @llpc.fract.f64(double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> fract()  =>  llpc.fract.f64
define spir_func <4 x double> @_Z5fractDv4_d(
    <4 x double> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fract.f64(double %x0)
    %2 = call double @llpc.fract.f64(double %x1)
    %3 = call double @llpc.fract.f64(double %x2)
    %4 = call double @llpc.fract.f64(double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llpc.fract.f64(double) #0

; double fmod()  =>  llpc.mod.f64
define spir_func double @_Z4fmoddd(
    double %x, double %y) #0
{
    %1 = call double @llpc.mod.f64(double %x, double %y)

    ret double %1
}

; <2 x double> fmod()  =>  llpc.mod.f64
define spir_func <2 x double> @_Z4fmodDv2_dDv2_d(
    <2 x double> %x, <2 x double> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    %y0 = extractelement <2 x double> %y, i32 0
    %y1 = extractelement <2 x double> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.mod.f64(double %x0, double %y0)
    %2 = call double @llpc.mod.f64(double %x1, double %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> fmod()  =>  llpc.mod.f64
define spir_func <3 x double> @_Z4fmodDv3_dDv3_d(
    <3 x double> %x, <3 x double> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    %y0 = extractelement <3 x double> %y, i32 0
    %y1 = extractelement <3 x double> %y, i32 1
    %y2 = extractelement <3 x double> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.mod.f64(double %x0, double %y0)
    %2 = call double @llpc.mod.f64(double %x1, double %y1)
    %3 = call double @llpc.mod.f64(double %x2, double %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> fmod()  =>  llpc.mod.f64
define spir_func <4 x double> @_Z4fmodDv4_dDv4_d(
    <4 x double> %x, <4 x double> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    %y0 = extractelement <4 x double> %y, i32 0
    %y1 = extractelement <4 x double> %y, i32 1
    %y2 = extractelement <4 x double> %y, i32 2
    %y3 = extractelement <4 x double> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.mod.f64(double %x0, double %y0)
    %2 = call double @llpc.mod.f64(double %x1, double %y1)
    %3 = call double @llpc.mod.f64(double %x2, double %y2)
    %4 = call double @llpc.mod.f64(double %x3, double %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llpc.mod.f64(double, double) #0

; double fmin()  =>  llvm.minnum.f64
define spir_func double @_Z4fmindd(
    double %x, double %y) #0
{
    %1 = call double @llvm.minnum.f64(double %x, double %y)

    ret double %1
}

; <2 x double> fmin()  =>  llvm.minnum.f64
define spir_func <2 x double> @_Z4fminDv2_dDv2_d(
    <2 x double> %x, <2 x double> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    %y0 = extractelement <2 x double> %y, i32 0
    %y1 = extractelement <2 x double> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.minnum.f64(double %x0, double %y0)
    %2 = call double @llvm.minnum.f64(double %x1, double %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> fmin()  =>  llvm.minnum.f64
define spir_func <3 x double> @_Z4fminDv3_dDv3_d(
    <3 x double> %x, <3 x double> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    %y0 = extractelement <3 x double> %y, i32 0
    %y1 = extractelement <3 x double> %y, i32 1
    %y2 = extractelement <3 x double> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.minnum.f64(double %x0, double %y0)
    %2 = call double @llvm.minnum.f64(double %x1, double %y1)
    %3 = call double @llvm.minnum.f64(double %x2, double %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> fmin()  =>  llvm.minnum.f64
define spir_func <4 x double> @_Z4fminDv4_dDv4_d(
    <4 x double> %x, <4 x double> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    %y0 = extractelement <4 x double> %y, i32 0
    %y1 = extractelement <4 x double> %y, i32 1
    %y2 = extractelement <4 x double> %y, i32 2
    %y3 = extractelement <4 x double> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.minnum.f64(double %x0, double %y0)
    %2 = call double @llvm.minnum.f64(double %x1, double %y1)
    %3 = call double @llvm.minnum.f64(double %x2, double %y2)
    %4 = call double @llvm.minnum.f64(double %x3, double %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llvm.minnum.f64(double, double) #0

; double fmax()  =>  llvm.maxnum.f64
define spir_func double @_Z4fmaxdd(
    double %x, double %y) #0
{
    %1 = call double @llvm.maxnum.f64(double %x, double %y)

    ret double %1
}

; <2 x double> fmax()  =>  llvm.maxnum.f64
define spir_func <2 x double> @_Z4fmaxDv2_dDv2_d(
    <2 x double> %x, <2 x double> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    %y0 = extractelement <2 x double> %y, i32 0
    %y1 = extractelement <2 x double> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.maxnum.f64(double %x0, double %y0)
    %2 = call double @llvm.maxnum.f64(double %x1, double %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> fmax()  =>  llvm.maxnum.f64
define spir_func <3 x double> @_Z4fmaxDv3_dDv3_d(
    <3 x double> %x, <3 x double> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    %y0 = extractelement <3 x double> %y, i32 0
    %y1 = extractelement <3 x double> %y, i32 1
    %y2 = extractelement <3 x double> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.maxnum.f64(double %x0, double %y0)
    %2 = call double @llvm.maxnum.f64(double %x1, double %y1)
    %3 = call double @llvm.maxnum.f64(double %x2, double %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> fmax()  =>  llvm.maxnum.f64
define spir_func <4 x double> @_Z4fmaxDv4_dDv4_d(
    <4 x double> %x, <4 x double> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    %y0 = extractelement <4 x double> %y, i32 0
    %y1 = extractelement <4 x double> %y, i32 1
    %y2 = extractelement <4 x double> %y, i32 2
    %y3 = extractelement <4 x double> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.maxnum.f64(double %x0, double %y0)
    %2 = call double @llvm.maxnum.f64(double %x1, double %y1)
    %3 = call double @llvm.maxnum.f64(double %x2, double %y2)
    %4 = call double @llvm.maxnum.f64(double %x3, double %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llvm.maxnum.f64(double, double) #0

; double fclamp()  =>  llpc.fclamp.f64
define spir_func double @_Z6fclampddd(
    double %x, double %minVal, double %maxVal) #0
{
    %1 = call double @llpc.fclamp.f64(double %x, double %minVal, double %maxVal)

    ret double %1
}

; <2 x double> fclamp()  =>  llpc.fclamp.f64
define spir_func <2 x double> @_Z6fclampDv2_dDv2_dDv2_d(
    <2 x double> %x, <2 x double> %minVal, <2 x double> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    %minVal0 = extractelement <2 x double> %minVal, i32 0
    %minVal1 = extractelement <2 x double> %minVal, i32 1

    %maxVal0 = extractelement <2 x double> %maxVal, i32 0
    %maxVal1 = extractelement <2 x double> %maxVal, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fclamp.f64(double %x0, double %minVal0, double %maxVal0)
    %2 = call double @llpc.fclamp.f64(double %x1, double %minVal1, double %maxVal1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> fclamp()  =>  llpc.fclamp.f64
define spir_func <3 x double> @_Z6fclampDv3_dDv3_dDv3_d(
    <3 x double> %x, <3 x double> %minVal, <3 x double> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    %minVal0 = extractelement <3 x double> %minVal, i32 0
    %minVal1 = extractelement <3 x double> %minVal, i32 1
    %minVal2 = extractelement <3 x double> %minVal, i32 2

    %maxVal0 = extractelement <3 x double> %maxVal, i32 0
    %maxVal1 = extractelement <3 x double> %maxVal, i32 1
    %maxVal2 = extractelement <3 x double> %maxVal, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fclamp.f64(double %x0, double %minVal0, double %maxVal0)
    %2 = call double @llpc.fclamp.f64(double %x1, double %minVal1, double %maxVal1)
    %3 = call double @llpc.fclamp.f64(double %x2, double %minVal2, double %maxVal2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> fclamp()  =>  llpc.fclamp.f64
define spir_func <4 x double> @_Z6fclampDv4_dDv4_dDv4_d(
    <4 x double> %x, <4 x double> %minVal, <4 x double> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    %minVal0 = extractelement <4 x double> %minVal, i32 0
    %minVal1 = extractelement <4 x double> %minVal, i32 1
    %minVal2 = extractelement <4 x double> %minVal, i32 2
    %minVal3 = extractelement <4 x double> %minVal, i32 3

    %maxVal0 = extractelement <4 x double> %maxVal, i32 0
    %maxVal1 = extractelement <4 x double> %maxVal, i32 1
    %maxVal2 = extractelement <4 x double> %maxVal, i32 2
    %maxVal3 = extractelement <4 x double> %maxVal, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fclamp.f64(double %x0, double %minVal0, double %maxVal0)
    %2 = call double @llpc.fclamp.f64(double %x1, double %minVal1, double %maxVal1)
    %3 = call double @llpc.fclamp.f64(double %x2, double %minVal2, double %maxVal2)
    %4 = call double @llpc.fclamp.f64(double %x3, double %minVal3, double %maxVal3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llpc.fclamp.f64(double, double, double) #0

; double fmix()  =>  llpc.fmix.f64
define spir_func double @_Z4fmixddd(
    double %x, double %y, double %a) #0
{
    %1 = call double @llpc.fmix.f64(double %x, double %y, double %a)

    ret double %1
}

; <2 x double> fmix()  =>  llpc.fmix.f64
define spir_func <2 x double> @_Z4fmixDv2_dDv2_dDv2_d(
    <2 x double> %x, <2 x double> %y, <2 x double> %a) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    %y0 = extractelement <2 x double> %y, i32 0
    %y1 = extractelement <2 x double> %y, i32 1

    %a0 = extractelement <2 x double> %a, i32 0
    %a1 = extractelement <2 x double> %a, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fmix.f64(double %x0, double %y0, double %a0)
    %2 = call double @llpc.fmix.f64(double %x1, double %y1, double %a1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> fmix()  =>  llpc.fmix.f64
define spir_func <3 x double> @_Z4fmixDv3_dDv3_dDv3_d(
    <3 x double> %x, <3 x double> %y, <3 x double> %a) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    %y0 = extractelement <3 x double> %y, i32 0
    %y1 = extractelement <3 x double> %y, i32 1
    %y2 = extractelement <3 x double> %y, i32 2

    %a0 = extractelement <3 x double> %a, i32 0
    %a1 = extractelement <3 x double> %a, i32 1
    %a2 = extractelement <3 x double> %a, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fmix.f64(double %x0, double %y0, double %a0)
    %2 = call double @llpc.fmix.f64(double %x1, double %y1, double %a1)
    %3 = call double @llpc.fmix.f64(double %x2, double %y2, double %a2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> fmix()  =>  llpc.fmix.f64
define spir_func <4 x double> @_Z4fmixDv4_dDv4_dDv4_d(
    <4 x double> %x, <4 x double> %y, <4 x double> %a) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    %y0 = extractelement <4 x double> %y, i32 0
    %y1 = extractelement <4 x double> %y, i32 1
    %y2 = extractelement <4 x double> %y, i32 2
    %y3 = extractelement <4 x double> %y, i32 3

    %a0 = extractelement <4 x double> %a, i32 0
    %a1 = extractelement <4 x double> %a, i32 1
    %a2 = extractelement <4 x double> %a, i32 2
    %a3 = extractelement <4 x double> %a, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.fmix.f64(double %x0, double %y0, double %a0)
    %2 = call double @llpc.fmix.f64(double %x1, double %y1, double %a1)
    %3 = call double @llpc.fmix.f64(double %x2, double %y2, double %a2)
    %4 = call double @llpc.fmix.f64(double %x3, double %y3, double %a3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llpc.fmix.f64(double, double, double) #0

; double step()  =>  llpc.step.f64
define spir_func double @_Z4stepdd(
    double %edge, double %x) #0
{
    %1 = call double @llpc.step.f64(double %edge, double %x)

    ret double %1
}

; <2 x double> step()  =>  llpc.step.f64
define spir_func <2 x double> @_Z4stepDv2_dDv2_d(
    <2 x double> %edge, <2 x double> %x) #0
{
    ; Extract components from source vectors
    %edge0 = extractelement <2 x double> %edge, i32 0
    %edge1 = extractelement <2 x double> %edge, i32 1

    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.step.f64(double %edge0, double %x0)
    %2 = call double @llpc.step.f64(double %edge1, double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> step()  =>  llpc.step.f64
define spir_func <3 x double> @_Z4stepDv3_dDv3_d(
    <3 x double> %edge, <3 x double> %x) #0
{
    ; Extract components from source vectors
    %edge0 = extractelement <3 x double> %edge, i32 0
    %edge1 = extractelement <3 x double> %edge, i32 1
    %edge2 = extractelement <3 x double> %edge, i32 2

    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.step.f64(double %edge0, double %x0)
    %2 = call double @llpc.step.f64(double %edge1, double %x1)
    %3 = call double @llpc.step.f64(double %edge2, double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> step()  =>  llpc.step.f64
define spir_func <4 x double> @_Z4stepDv4_dDv4_d(
    <4 x double> %edge, <4 x double> %x) #0
{
    ; Extract components from source vectors
    %edge0 = extractelement <4 x double> %edge, i32 0
    %edge1 = extractelement <4 x double> %edge, i32 1
    %edge2 = extractelement <4 x double> %edge, i32 2
    %edge3 = extractelement <4 x double> %edge, i32 3

    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.step.f64(double %edge0, double %x0)
    %2 = call double @llpc.step.f64(double %edge1, double %x1)
    %3 = call double @llpc.step.f64(double %edge2, double %x2)
    %4 = call double @llpc.step.f64(double %edge3, double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llpc.step.f64(double, double) #0

; double smoothStep()  =>  llpc.smoothStep.f64
define spir_func double @_Z10smoothStepddd(
    double %edge0, double %edge1, double %x) #0
{
    %1 = call double @llpc.smoothStep.f64(double %edge0, double %edge1, double %x)

    ret double %1
}

; <2 x double> smoothStep()  =>  llpc.smoothStep.f64
define spir_func <2 x double> @_Z10smoothStepDv2_dDv2_dDv2_d(
    <2 x double> %edge0, <2 x double> %edge1, <2 x double> %x) #0
{
    ; Extract components from source vectors
    %edge00 = extractelement <2 x double> %edge0, i32 0
    %edge01 = extractelement <2 x double> %edge0, i32 1

    %edge10 = extractelement <2 x double> %edge1, i32 0
    %edge11 = extractelement <2 x double> %edge1, i32 1

    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.smoothStep.f64(double %edge00, double %edge10, double %x0)
    %2 = call double @llpc.smoothStep.f64(double %edge01, double %edge11, double %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> smoothStep()  =>  llpc.smoothStep.f64
define spir_func <3 x double> @_Z10smoothStepDv3_dDv3_dDv3_d(
    <3 x double> %edge0, <3 x double> %edge1, <3 x double> %x) #0
{
    ; Extract components from source vectors
    %edge00 = extractelement <3 x double> %edge0, i32 0
    %edge01 = extractelement <3 x double> %edge0, i32 1
    %edge02 = extractelement <3 x double> %edge0, i32 2

    %edge10 = extractelement <3 x double> %edge1, i32 0
    %edge11 = extractelement <3 x double> %edge1, i32 1
    %edge12 = extractelement <3 x double> %edge1, i32 2

    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.smoothStep.f64(double %edge00, double %edge10, double %x0)
    %2 = call double @llpc.smoothStep.f64(double %edge01, double %edge11, double %x1)
    %3 = call double @llpc.smoothStep.f64(double %edge02, double %edge12, double %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> smoothStep()  =>  llpc.smoothStep.f64
define spir_func <4 x double> @_Z10smoothStepDv4_dDv4_dDv4_d(
    <4 x double> %edge0, <4 x double> %edge1, <4 x double> %x) #0
{
    ; Extract components from source vectors
    %edge00 = extractelement <4 x double> %edge0, i32 0
    %edge01 = extractelement <4 x double> %edge0, i32 1
    %edge02 = extractelement <4 x double> %edge0, i32 2
    %edge03 = extractelement <4 x double> %edge0, i32 3

    %edge10 = extractelement <4 x double> %edge1, i32 0
    %edge11 = extractelement <4 x double> %edge1, i32 1
    %edge12 = extractelement <4 x double> %edge1, i32 2
    %edge13 = extractelement <4 x double> %edge1, i32 3

    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llpc.smoothStep.f64(double %edge00, double %edge10, double %x0)
    %2 = call double @llpc.smoothStep.f64(double %edge01, double %edge11, double %x1)
    %3 = call double @llpc.smoothStep.f64(double %edge02, double %edge12, double %x2)
    %4 = call double @llpc.smoothStep.f64(double %edge03, double %edge13, double %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llpc.smoothStep.f64(double, double, double) #0

; i1 isinf()  =>  llpc.isinf.f64
define spir_func i1 @_Z5isinfd(
    double %value) #0
{
    %1 = call i1 @llpc.isinf.f64(double %value)

    ret i1 %1
}

; <2 x i1> isinf()  =>  llpc.isinf.f64
define spir_func <2 x i1> @_Z5isinfDv2_d(
    <2 x double> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x double> %value, i32 0
    %value1 = extractelement <2 x double> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isinf.f64(double %value0)
    %2 = call i1 @llpc.isinf.f64(double %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i1>
    %4 = load <2 x i1>, <2 x i1>* %3
    %5 = insertelement <2 x i1> %4, i1 %1, i32 0
    %6 = insertelement <2 x i1> %5, i1 %2, i32 1

    ret <2 x i1> %6
}

; <3 x i1> isinf()  =>  llpc.isinf.f64
define spir_func <3 x i1> @_Z5isinfDv3_d(
    <3 x double> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x double> %value, i32 0
    %value1 = extractelement <3 x double> %value, i32 1
    %value2 = extractelement <3 x double> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isinf.f64(double %value0)
    %2 = call i1 @llpc.isinf.f64(double %value1)
    %3 = call i1 @llpc.isinf.f64(double %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i1>
    %5 = load <3 x i1>, <3 x i1>* %4
    %6 = insertelement <3 x i1> %5, i1 %1, i32 0
    %7 = insertelement <3 x i1> %6, i1 %2, i32 1
    %8 = insertelement <3 x i1> %7, i1 %3, i32 2

    ret <3 x i1> %8
}

; <4 x i1> isinf()  =>  llpc.isinf.f64
define spir_func <4 x i1> @_Z5isinfDv4_d(
    <4 x double> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x double> %value, i32 0
    %value1 = extractelement <4 x double> %value, i32 1
    %value2 = extractelement <4 x double> %value, i32 2
    %value3 = extractelement <4 x double> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isinf.f64(double %value0)
    %2 = call i1 @llpc.isinf.f64(double %value1)
    %3 = call i1 @llpc.isinf.f64(double %value2)
    %4 = call i1 @llpc.isinf.f64(double %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i1>
    %6 = load <4 x i1>, <4 x i1>* %5
    %7 = insertelement <4 x i1> %6, i1 %1, i32 0
    %8 = insertelement <4 x i1> %7, i1 %2, i32 1
    %9 = insertelement <4 x i1> %8, i1 %3, i32 2
    %10 = insertelement <4 x i1> %9, i1 %4, i32 3

    ret <4 x i1> %10
}

declare i1 @llpc.isinf.f64(double) #0

; i1 isnan()  =>  llpc.isnan.f64
define spir_func i1 @_Z5isnand(
    double %value) #0
{
    %1 = call i1 @llpc.isnan.f64(double %value)

    ret i1 %1
}

; <2 x i1> isnan()  =>  llpc.isnan.f64
define spir_func <2 x i1> @_Z5isnanDv2_d(
    <2 x double> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x double> %value, i32 0
    %value1 = extractelement <2 x double> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isnan.f64(double %value0)
    %2 = call i1 @llpc.isnan.f64(double %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i1>
    %4 = load <2 x i1>, <2 x i1>* %3
    %5 = insertelement <2 x i1> %4, i1 %1, i32 0
    %6 = insertelement <2 x i1> %5, i1 %2, i32 1

    ret <2 x i1> %6
}

; <3 x i1> isnan()  =>  llpc.isnan.f64
define spir_func <3 x i1> @_Z5isnanDv3_d(
    <3 x double> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x double> %value, i32 0
    %value1 = extractelement <3 x double> %value, i32 1
    %value2 = extractelement <3 x double> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isnan.f64(double %value0)
    %2 = call i1 @llpc.isnan.f64(double %value1)
    %3 = call i1 @llpc.isnan.f64(double %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i1>
    %5 = load <3 x i1>, <3 x i1>* %4
    %6 = insertelement <3 x i1> %5, i1 %1, i32 0
    %7 = insertelement <3 x i1> %6, i1 %2, i32 1
    %8 = insertelement <3 x i1> %7, i1 %3, i32 2

    ret <3 x i1> %8
}

; <4 x i1> isnan()  =>  llpc.isnan.f64
define spir_func <4 x i1> @_Z5isnanDv4_d(
    <4 x double> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x double> %value, i32 0
    %value1 = extractelement <4 x double> %value, i32 1
    %value2 = extractelement <4 x double> %value, i32 2
    %value3 = extractelement <4 x double> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isnan.f64(double %value0)
    %2 = call i1 @llpc.isnan.f64(double %value1)
    %3 = call i1 @llpc.isnan.f64(double %value2)
    %4 = call i1 @llpc.isnan.f64(double %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i1>
    %6 = load <4 x i1>, <4 x i1>* %5
    %7 = insertelement <4 x i1> %6, i1 %1, i32 0
    %8 = insertelement <4 x i1> %7, i1 %2, i32 1
    %9 = insertelement <4 x i1> %8, i1 %3, i32 2
    %10 = insertelement <4 x i1> %9, i1 %4, i32 3

    ret <4 x i1> %10
}

declare i1 @llpc.isnan.f64(double) #0

; double fma()  =>  llvm.fma.f64
define spir_func double @_Z3fmaddd(
    double %a, double %b, double %c) #0
{
    %1 = call double @llvm.fma.f64(double %a, double %b, double %c)

    ret double %1
}

; <2 x double> fma()  =>  llvm.fma.f64
define spir_func <2 x double> @_Z3fmaDv2_dDv2_dDv2_d(
    <2 x double> %a, <2 x double> %b, <2 x double> %c) #0
{
    ; Extract components from source vectors
    %a0 = extractelement <2 x double> %a, i32 0
    %a1 = extractelement <2 x double> %a, i32 1

    %b0 = extractelement <2 x double> %b, i32 0
    %b1 = extractelement <2 x double> %b, i32 1

    %c0 = extractelement <2 x double> %c, i32 0
    %c1 = extractelement <2 x double> %c, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.fma.f64(double %a0, double %b0, double %c0)
    %2 = call double @llvm.fma.f64(double %a1, double %b1, double %c1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> fma()  =>  llvm.fma.f64
define spir_func <3 x double> @_Z3fmaDv3_dDv3_dDv3_d(
    <3 x double> %a, <3 x double> %b, <3 x double> %c) #0
{
    ; Extract components from source vectors
    %a0 = extractelement <3 x double> %a, i32 0
    %a1 = extractelement <3 x double> %a, i32 1
    %a2 = extractelement <3 x double> %a, i32 2

    %b0 = extractelement <3 x double> %b, i32 0
    %b1 = extractelement <3 x double> %b, i32 1
    %b2 = extractelement <3 x double> %b, i32 2

    %c0 = extractelement <3 x double> %c, i32 0
    %c1 = extractelement <3 x double> %c, i32 1
    %c2 = extractelement <3 x double> %c, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.fma.f64(double %a0, double %b0, double %c0)
    %2 = call double @llvm.fma.f64(double %a1, double %b1, double %c1)
    %3 = call double @llvm.fma.f64(double %a2, double %b2, double %c2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> fma()  =>  llvm.fma.f64
define spir_func <4 x double> @_Z3fmaDv4_dDv4_dDv4_d(
    <4 x double> %a, <4 x double> %b, <4 x double> %c) #0
{
    ; Extract components from source vectors
    %a0 = extractelement <4 x double> %a, i32 0
    %a1 = extractelement <4 x double> %a, i32 1
    %a2 = extractelement <4 x double> %a, i32 2
    %a3 = extractelement <4 x double> %a, i32 3

    %b0 = extractelement <4 x double> %b, i32 0
    %b1 = extractelement <4 x double> %b, i32 1
    %b2 = extractelement <4 x double> %b, i32 2
    %b3 = extractelement <4 x double> %b, i32 3

    %c0 = extractelement <4 x double> %c, i32 0
    %c1 = extractelement <4 x double> %c, i32 1
    %c2 = extractelement <4 x double> %c, i32 2
    %c3 = extractelement <4 x double> %c, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.fma.f64(double %a0, double %b0, double %c0)
    %2 = call double @llvm.fma.f64(double %a1, double %b1, double %c1)
    %3 = call double @llvm.fma.f64(double %a2, double %b2, double %c2)
    %4 = call double @llvm.fma.f64(double %a3, double %b3, double %c3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llvm.fma.f64(double, double, double) #0

; double ldexp()  =>  llvm.amdgcn.ldexp.f64
define spir_func double @_Z5ldexpdi(
    double %x, i32 %exp) #0
{
    %1 = call double @llvm.amdgcn.ldexp.f64(double %x, i32 %exp)

    ret double %1
}

; <2 x double> ldexp()  =>  llvm.amdgcn.ldexp.f64
define spir_func <2 x double> @_Z5ldexpDv2_dDv2_i(
    <2 x double> %x, <2 x i32> %exp) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x double> %x, i32 0
    %x1 = extractelement <2 x double> %x, i32 1

    %exp0 = extractelement <2 x i32> %exp, i32 0
    %exp1 = extractelement <2 x i32> %exp, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.amdgcn.ldexp.f64(double %x0, i32 %exp0)
    %2 = call double @llvm.amdgcn.ldexp.f64(double %x1, i32 %exp1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x double>
    %4 = load <2 x double>, <2 x double>* %3
    %5 = insertelement <2 x double> %4, double %1, i32 0
    %6 = insertelement <2 x double> %5, double %2, i32 1

    ret <2 x double> %6
}

; <3 x double> ldexp()  =>  llvm.amdgcn.ldexp.f64
define spir_func <3 x double> @_Z5ldexpDv3_dDv3_i(
    <3 x double> %x, <3 x i32> %exp) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x double> %x, i32 0
    %x1 = extractelement <3 x double> %x, i32 1
    %x2 = extractelement <3 x double> %x, i32 2

    %exp0 = extractelement <3 x i32> %exp, i32 0
    %exp1 = extractelement <3 x i32> %exp, i32 1
    %exp2 = extractelement <3 x i32> %exp, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.amdgcn.ldexp.f64(double %x0, i32 %exp0)
    %2 = call double @llvm.amdgcn.ldexp.f64(double %x1, i32 %exp1)
    %3 = call double @llvm.amdgcn.ldexp.f64(double %x2, i32 %exp2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x double>
    %5 = load <3 x double>, <3 x double>* %4
    %6 = insertelement <3 x double> %5, double %1, i32 0
    %7 = insertelement <3 x double> %6, double %2, i32 1
    %8 = insertelement <3 x double> %7, double %3, i32 2

    ret <3 x double> %8
}

; <4 x double> ldexp()  =>  llvm.amdgcn.ldexp.f64
define spir_func <4 x double> @_Z5ldexpDv4_dDv4_i(
    <4 x double> %x, <4 x i32> %exp) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x double> %x, i32 0
    %x1 = extractelement <4 x double> %x, i32 1
    %x2 = extractelement <4 x double> %x, i32 2
    %x3 = extractelement <4 x double> %x, i32 3

    %exp0 = extractelement <4 x i32> %exp, i32 0
    %exp1 = extractelement <4 x i32> %exp, i32 1
    %exp2 = extractelement <4 x i32> %exp, i32 2
    %exp3 = extractelement <4 x i32> %exp, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call double @llvm.amdgcn.ldexp.f64(double %x0, i32 %exp0)
    %2 = call double @llvm.amdgcn.ldexp.f64(double %x1, i32 %exp1)
    %3 = call double @llvm.amdgcn.ldexp.f64(double %x2, i32 %exp2)
    %4 = call double @llvm.amdgcn.ldexp.f64(double %x3, i32 %exp3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x double>
    %6 = load <4 x double>, <4 x double>* %5
    %7 = insertelement <4 x double> %6, double %1, i32 0
    %8 = insertelement <4 x double> %7, double %2, i32 1
    %9 = insertelement <4 x double> %8, double %3, i32 2
    %10 = insertelement <4 x double> %9, double %4, i32 3

    ret <4 x double> %10
}

declare double @llvm.amdgcn.ldexp.f64(double, i32) #1

attributes #0 = { nounwind }
attributes #1 = { nounwind readnone }
