;**********************************************************************************************************************
;*
;*  Trade secret of Advanced Micro Devices, Inc.
;*  Copyright (c) 2017, Advanced Micro Devices, Inc., (unpublished)
;*
;*  All rights reserved. This notice is intended as a precaution against inadvertent publication and does not imply
;*  publication or any waiver of confidentiality. The year included in the foregoing notice is the year of creation of
;*  the work.
;*
;**********************************************************************************************************************

;**********************************************************************************************************************
;* @file  g_glslArithOpEmuI64.ll
;* @brief LLPC LLVM-IR file: contains emulation codes for GLSL arithmetic operations (int64).
;*
;* @note  This file has been generated automatically. Do not hand-modify this file. When changes are needed, modify the
;*        generating script genGlslArithOpEmuCode.py.
;**********************************************************************************************************************

target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v16:16:16-v24:32:32-v32:32:32-v48:64:64-v64:64:64-v96:128:128-v128:128:128-v192:256:256-v256:256:256-v512:512:512-v1024:1024:1024"
target triple = "spir64-unknown-unknown"

; =====================================================================================================================
; >>>  Operators
; =====================================================================================================================

; i64 smod()  =>  llpc.mod.i64
define spir_func i64 @_Z4smodll(
    i64 %x, i64 %y) #0
{
    %1 = call i64 @llpc.mod.i64(i64 %x, i64 %y)

    ret i64 %1
}

; <2 x i64> smod()  =>  llpc.mod.i64
define spir_func <2 x i64> @_Z4smodDv2_lDv2_l(
    <2 x i64> %x, <2 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i64> %x, i32 0
    %x1 = extractelement <2 x i64> %x, i32 1

    %y0 = extractelement <2 x i64> %y, i32 0
    %y1 = extractelement <2 x i64> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.mod.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.mod.i64(i64 %x1, i64 %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i64>
    %4 = load <2 x i64>, <2 x i64>* %3
    %5 = insertelement <2 x i64> %4, i64 %1, i32 0
    %6 = insertelement <2 x i64> %5, i64 %2, i32 1

    ret <2 x i64> %6
}

; <3 x i64> smod()  =>  llpc.mod.i64
define spir_func <3 x i64> @_Z4smodDv3_lDv3_l(
    <3 x i64> %x, <3 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i64> %x, i32 0
    %x1 = extractelement <3 x i64> %x, i32 1
    %x2 = extractelement <3 x i64> %x, i32 2

    %y0 = extractelement <3 x i64> %y, i32 0
    %y1 = extractelement <3 x i64> %y, i32 1
    %y2 = extractelement <3 x i64> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.mod.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.mod.i64(i64 %x1, i64 %y1)
    %3 = call i64 @llpc.mod.i64(i64 %x2, i64 %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i64>
    %5 = load <3 x i64>, <3 x i64>* %4
    %6 = insertelement <3 x i64> %5, i64 %1, i32 0
    %7 = insertelement <3 x i64> %6, i64 %2, i32 1
    %8 = insertelement <3 x i64> %7, i64 %3, i32 2

    ret <3 x i64> %8
}

; <4 x i64> smod()  =>  llpc.mod.i64
define spir_func <4 x i64> @_Z4smodDv4_lDv4_l(
    <4 x i64> %x, <4 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i64> %x, i32 0
    %x1 = extractelement <4 x i64> %x, i32 1
    %x2 = extractelement <4 x i64> %x, i32 2
    %x3 = extractelement <4 x i64> %x, i32 3

    %y0 = extractelement <4 x i64> %y, i32 0
    %y1 = extractelement <4 x i64> %y, i32 1
    %y2 = extractelement <4 x i64> %y, i32 2
    %y3 = extractelement <4 x i64> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.mod.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.mod.i64(i64 %x1, i64 %y1)
    %3 = call i64 @llpc.mod.i64(i64 %x2, i64 %y2)
    %4 = call i64 @llpc.mod.i64(i64 %x3, i64 %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i64>
    %6 = load <4 x i64>, <4 x i64>* %5
    %7 = insertelement <4 x i64> %6, i64 %1, i32 0
    %8 = insertelement <4 x i64> %7, i64 %2, i32 1
    %9 = insertelement <4 x i64> %8, i64 %3, i32 2
    %10 = insertelement <4 x i64> %9, i64 %4, i32 3

    ret <4 x i64> %10
}

declare i64 @llpc.mod.i64(i64, i64) #0

; =====================================================================================================================
; >>>  Common Functions
; =====================================================================================================================

; i64 sabs()  =>  llpc.sabs.i64
define spir_func i64 @_Z4sabsl(
    i64 %x) #0
{
    %1 = call i64 @llpc.sabs.i64(i64 %x)

    ret i64 %1
}

; <2 x i64> sabs()  =>  llpc.sabs.i64
define spir_func <2 x i64> @_Z4sabsDv2_l(
    <2 x i64> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i64> %x, i32 0
    %x1 = extractelement <2 x i64> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.sabs.i64(i64 %x0)
    %2 = call i64 @llpc.sabs.i64(i64 %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i64>
    %4 = load <2 x i64>, <2 x i64>* %3
    %5 = insertelement <2 x i64> %4, i64 %1, i32 0
    %6 = insertelement <2 x i64> %5, i64 %2, i32 1

    ret <2 x i64> %6
}

; <3 x i64> sabs()  =>  llpc.sabs.i64
define spir_func <3 x i64> @_Z4sabsDv3_l(
    <3 x i64> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i64> %x, i32 0
    %x1 = extractelement <3 x i64> %x, i32 1
    %x2 = extractelement <3 x i64> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.sabs.i64(i64 %x0)
    %2 = call i64 @llpc.sabs.i64(i64 %x1)
    %3 = call i64 @llpc.sabs.i64(i64 %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i64>
    %5 = load <3 x i64>, <3 x i64>* %4
    %6 = insertelement <3 x i64> %5, i64 %1, i32 0
    %7 = insertelement <3 x i64> %6, i64 %2, i32 1
    %8 = insertelement <3 x i64> %7, i64 %3, i32 2

    ret <3 x i64> %8
}

; <4 x i64> sabs()  =>  llpc.sabs.i64
define spir_func <4 x i64> @_Z4sabsDv4_l(
    <4 x i64> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i64> %x, i32 0
    %x1 = extractelement <4 x i64> %x, i32 1
    %x2 = extractelement <4 x i64> %x, i32 2
    %x3 = extractelement <4 x i64> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.sabs.i64(i64 %x0)
    %2 = call i64 @llpc.sabs.i64(i64 %x1)
    %3 = call i64 @llpc.sabs.i64(i64 %x2)
    %4 = call i64 @llpc.sabs.i64(i64 %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i64>
    %6 = load <4 x i64>, <4 x i64>* %5
    %7 = insertelement <4 x i64> %6, i64 %1, i32 0
    %8 = insertelement <4 x i64> %7, i64 %2, i32 1
    %9 = insertelement <4 x i64> %8, i64 %3, i32 2
    %10 = insertelement <4 x i64> %9, i64 %4, i32 3

    ret <4 x i64> %10
}

declare i64 @llpc.sabs.i64(i64) #0

; i64 ssign()  =>  llpc.ssign.i64
define spir_func i64 @_Z5ssignl(
    i64 %x) #0
{
    %1 = call i64 @llpc.ssign.i64(i64 %x)

    ret i64 %1
}

; <2 x i64> ssign()  =>  llpc.ssign.i64
define spir_func <2 x i64> @_Z5ssignDv2_l(
    <2 x i64> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i64> %x, i32 0
    %x1 = extractelement <2 x i64> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.ssign.i64(i64 %x0)
    %2 = call i64 @llpc.ssign.i64(i64 %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i64>
    %4 = load <2 x i64>, <2 x i64>* %3
    %5 = insertelement <2 x i64> %4, i64 %1, i32 0
    %6 = insertelement <2 x i64> %5, i64 %2, i32 1

    ret <2 x i64> %6
}

; <3 x i64> ssign()  =>  llpc.ssign.i64
define spir_func <3 x i64> @_Z5ssignDv3_l(
    <3 x i64> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i64> %x, i32 0
    %x1 = extractelement <3 x i64> %x, i32 1
    %x2 = extractelement <3 x i64> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.ssign.i64(i64 %x0)
    %2 = call i64 @llpc.ssign.i64(i64 %x1)
    %3 = call i64 @llpc.ssign.i64(i64 %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i64>
    %5 = load <3 x i64>, <3 x i64>* %4
    %6 = insertelement <3 x i64> %5, i64 %1, i32 0
    %7 = insertelement <3 x i64> %6, i64 %2, i32 1
    %8 = insertelement <3 x i64> %7, i64 %3, i32 2

    ret <3 x i64> %8
}

; <4 x i64> ssign()  =>  llpc.ssign.i64
define spir_func <4 x i64> @_Z5ssignDv4_l(
    <4 x i64> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i64> %x, i32 0
    %x1 = extractelement <4 x i64> %x, i32 1
    %x2 = extractelement <4 x i64> %x, i32 2
    %x3 = extractelement <4 x i64> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.ssign.i64(i64 %x0)
    %2 = call i64 @llpc.ssign.i64(i64 %x1)
    %3 = call i64 @llpc.ssign.i64(i64 %x2)
    %4 = call i64 @llpc.ssign.i64(i64 %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i64>
    %6 = load <4 x i64>, <4 x i64>* %5
    %7 = insertelement <4 x i64> %6, i64 %1, i32 0
    %8 = insertelement <4 x i64> %7, i64 %2, i32 1
    %9 = insertelement <4 x i64> %8, i64 %3, i32 2
    %10 = insertelement <4 x i64> %9, i64 %4, i32 3

    ret <4 x i64> %10
}

declare i64 @llpc.ssign.i64(i64) #0

; i64 smin()  =>  llpc.sminnum.i64
define spir_func i64 @_Z4sminll(
    i64 %x, i64 %y) #0
{
    %1 = call i64 @llpc.sminnum.i64(i64 %x, i64 %y)

    ret i64 %1
}

; <2 x i64> smin()  =>  llpc.sminnum.i64
define spir_func <2 x i64> @_Z4sminDv2_lDv2_l(
    <2 x i64> %x, <2 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i64> %x, i32 0
    %x1 = extractelement <2 x i64> %x, i32 1

    %y0 = extractelement <2 x i64> %y, i32 0
    %y1 = extractelement <2 x i64> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.sminnum.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.sminnum.i64(i64 %x1, i64 %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i64>
    %4 = load <2 x i64>, <2 x i64>* %3
    %5 = insertelement <2 x i64> %4, i64 %1, i32 0
    %6 = insertelement <2 x i64> %5, i64 %2, i32 1

    ret <2 x i64> %6
}

; <3 x i64> smin()  =>  llpc.sminnum.i64
define spir_func <3 x i64> @_Z4sminDv3_lDv3_l(
    <3 x i64> %x, <3 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i64> %x, i32 0
    %x1 = extractelement <3 x i64> %x, i32 1
    %x2 = extractelement <3 x i64> %x, i32 2

    %y0 = extractelement <3 x i64> %y, i32 0
    %y1 = extractelement <3 x i64> %y, i32 1
    %y2 = extractelement <3 x i64> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.sminnum.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.sminnum.i64(i64 %x1, i64 %y1)
    %3 = call i64 @llpc.sminnum.i64(i64 %x2, i64 %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i64>
    %5 = load <3 x i64>, <3 x i64>* %4
    %6 = insertelement <3 x i64> %5, i64 %1, i32 0
    %7 = insertelement <3 x i64> %6, i64 %2, i32 1
    %8 = insertelement <3 x i64> %7, i64 %3, i32 2

    ret <3 x i64> %8
}

; <4 x i64> smin()  =>  llpc.sminnum.i64
define spir_func <4 x i64> @_Z4sminDv4_lDv4_l(
    <4 x i64> %x, <4 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i64> %x, i32 0
    %x1 = extractelement <4 x i64> %x, i32 1
    %x2 = extractelement <4 x i64> %x, i32 2
    %x3 = extractelement <4 x i64> %x, i32 3

    %y0 = extractelement <4 x i64> %y, i32 0
    %y1 = extractelement <4 x i64> %y, i32 1
    %y2 = extractelement <4 x i64> %y, i32 2
    %y3 = extractelement <4 x i64> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.sminnum.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.sminnum.i64(i64 %x1, i64 %y1)
    %3 = call i64 @llpc.sminnum.i64(i64 %x2, i64 %y2)
    %4 = call i64 @llpc.sminnum.i64(i64 %x3, i64 %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i64>
    %6 = load <4 x i64>, <4 x i64>* %5
    %7 = insertelement <4 x i64> %6, i64 %1, i32 0
    %8 = insertelement <4 x i64> %7, i64 %2, i32 1
    %9 = insertelement <4 x i64> %8, i64 %3, i32 2
    %10 = insertelement <4 x i64> %9, i64 %4, i32 3

    ret <4 x i64> %10
}

declare i64 @llpc.sminnum.i64(i64, i64) #0

; i64 umin()  =>  llpc.uminnum.i64
define spir_func i64 @_Z4uminll(
    i64 %x, i64 %y) #0
{
    %1 = call i64 @llpc.uminnum.i64(i64 %x, i64 %y)

    ret i64 %1
}

; <2 x i64> umin()  =>  llpc.uminnum.i64
define spir_func <2 x i64> @_Z4uminDv2_lDv2_l(
    <2 x i64> %x, <2 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i64> %x, i32 0
    %x1 = extractelement <2 x i64> %x, i32 1

    %y0 = extractelement <2 x i64> %y, i32 0
    %y1 = extractelement <2 x i64> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.uminnum.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.uminnum.i64(i64 %x1, i64 %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i64>
    %4 = load <2 x i64>, <2 x i64>* %3
    %5 = insertelement <2 x i64> %4, i64 %1, i32 0
    %6 = insertelement <2 x i64> %5, i64 %2, i32 1

    ret <2 x i64> %6
}

; <3 x i64> umin()  =>  llpc.uminnum.i64
define spir_func <3 x i64> @_Z4uminDv3_lDv3_l(
    <3 x i64> %x, <3 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i64> %x, i32 0
    %x1 = extractelement <3 x i64> %x, i32 1
    %x2 = extractelement <3 x i64> %x, i32 2

    %y0 = extractelement <3 x i64> %y, i32 0
    %y1 = extractelement <3 x i64> %y, i32 1
    %y2 = extractelement <3 x i64> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.uminnum.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.uminnum.i64(i64 %x1, i64 %y1)
    %3 = call i64 @llpc.uminnum.i64(i64 %x2, i64 %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i64>
    %5 = load <3 x i64>, <3 x i64>* %4
    %6 = insertelement <3 x i64> %5, i64 %1, i32 0
    %7 = insertelement <3 x i64> %6, i64 %2, i32 1
    %8 = insertelement <3 x i64> %7, i64 %3, i32 2

    ret <3 x i64> %8
}

; <4 x i64> umin()  =>  llpc.uminnum.i64
define spir_func <4 x i64> @_Z4uminDv4_lDv4_l(
    <4 x i64> %x, <4 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i64> %x, i32 0
    %x1 = extractelement <4 x i64> %x, i32 1
    %x2 = extractelement <4 x i64> %x, i32 2
    %x3 = extractelement <4 x i64> %x, i32 3

    %y0 = extractelement <4 x i64> %y, i32 0
    %y1 = extractelement <4 x i64> %y, i32 1
    %y2 = extractelement <4 x i64> %y, i32 2
    %y3 = extractelement <4 x i64> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.uminnum.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.uminnum.i64(i64 %x1, i64 %y1)
    %3 = call i64 @llpc.uminnum.i64(i64 %x2, i64 %y2)
    %4 = call i64 @llpc.uminnum.i64(i64 %x3, i64 %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i64>
    %6 = load <4 x i64>, <4 x i64>* %5
    %7 = insertelement <4 x i64> %6, i64 %1, i32 0
    %8 = insertelement <4 x i64> %7, i64 %2, i32 1
    %9 = insertelement <4 x i64> %8, i64 %3, i32 2
    %10 = insertelement <4 x i64> %9, i64 %4, i32 3

    ret <4 x i64> %10
}

declare i64 @llpc.uminnum.i64(i64, i64) #0

; i64 smax()  =>  llpc.smaxnum.i64
define spir_func i64 @_Z4smaxll(
    i64 %x, i64 %y) #0
{
    %1 = call i64 @llpc.smaxnum.i64(i64 %x, i64 %y)

    ret i64 %1
}

; <2 x i64> smax()  =>  llpc.smaxnum.i64
define spir_func <2 x i64> @_Z4smaxDv2_lDv2_l(
    <2 x i64> %x, <2 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i64> %x, i32 0
    %x1 = extractelement <2 x i64> %x, i32 1

    %y0 = extractelement <2 x i64> %y, i32 0
    %y1 = extractelement <2 x i64> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.smaxnum.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.smaxnum.i64(i64 %x1, i64 %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i64>
    %4 = load <2 x i64>, <2 x i64>* %3
    %5 = insertelement <2 x i64> %4, i64 %1, i32 0
    %6 = insertelement <2 x i64> %5, i64 %2, i32 1

    ret <2 x i64> %6
}

; <3 x i64> smax()  =>  llpc.smaxnum.i64
define spir_func <3 x i64> @_Z4smaxDv3_lDv3_l(
    <3 x i64> %x, <3 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i64> %x, i32 0
    %x1 = extractelement <3 x i64> %x, i32 1
    %x2 = extractelement <3 x i64> %x, i32 2

    %y0 = extractelement <3 x i64> %y, i32 0
    %y1 = extractelement <3 x i64> %y, i32 1
    %y2 = extractelement <3 x i64> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.smaxnum.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.smaxnum.i64(i64 %x1, i64 %y1)
    %3 = call i64 @llpc.smaxnum.i64(i64 %x2, i64 %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i64>
    %5 = load <3 x i64>, <3 x i64>* %4
    %6 = insertelement <3 x i64> %5, i64 %1, i32 0
    %7 = insertelement <3 x i64> %6, i64 %2, i32 1
    %8 = insertelement <3 x i64> %7, i64 %3, i32 2

    ret <3 x i64> %8
}

; <4 x i64> smax()  =>  llpc.smaxnum.i64
define spir_func <4 x i64> @_Z4smaxDv4_lDv4_l(
    <4 x i64> %x, <4 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i64> %x, i32 0
    %x1 = extractelement <4 x i64> %x, i32 1
    %x2 = extractelement <4 x i64> %x, i32 2
    %x3 = extractelement <4 x i64> %x, i32 3

    %y0 = extractelement <4 x i64> %y, i32 0
    %y1 = extractelement <4 x i64> %y, i32 1
    %y2 = extractelement <4 x i64> %y, i32 2
    %y3 = extractelement <4 x i64> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.smaxnum.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.smaxnum.i64(i64 %x1, i64 %y1)
    %3 = call i64 @llpc.smaxnum.i64(i64 %x2, i64 %y2)
    %4 = call i64 @llpc.smaxnum.i64(i64 %x3, i64 %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i64>
    %6 = load <4 x i64>, <4 x i64>* %5
    %7 = insertelement <4 x i64> %6, i64 %1, i32 0
    %8 = insertelement <4 x i64> %7, i64 %2, i32 1
    %9 = insertelement <4 x i64> %8, i64 %3, i32 2
    %10 = insertelement <4 x i64> %9, i64 %4, i32 3

    ret <4 x i64> %10
}

declare i64 @llpc.smaxnum.i64(i64, i64) #0

; i64 umax()  =>  llpc.umaxnum.i64
define spir_func i64 @_Z4umaxll(
    i64 %x, i64 %y) #0
{
    %1 = call i64 @llpc.umaxnum.i64(i64 %x, i64 %y)

    ret i64 %1
}

; <2 x i64> umax()  =>  llpc.umaxnum.i64
define spir_func <2 x i64> @_Z4umaxDv2_lDv2_l(
    <2 x i64> %x, <2 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i64> %x, i32 0
    %x1 = extractelement <2 x i64> %x, i32 1

    %y0 = extractelement <2 x i64> %y, i32 0
    %y1 = extractelement <2 x i64> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.umaxnum.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.umaxnum.i64(i64 %x1, i64 %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i64>
    %4 = load <2 x i64>, <2 x i64>* %3
    %5 = insertelement <2 x i64> %4, i64 %1, i32 0
    %6 = insertelement <2 x i64> %5, i64 %2, i32 1

    ret <2 x i64> %6
}

; <3 x i64> umax()  =>  llpc.umaxnum.i64
define spir_func <3 x i64> @_Z4umaxDv3_lDv3_l(
    <3 x i64> %x, <3 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i64> %x, i32 0
    %x1 = extractelement <3 x i64> %x, i32 1
    %x2 = extractelement <3 x i64> %x, i32 2

    %y0 = extractelement <3 x i64> %y, i32 0
    %y1 = extractelement <3 x i64> %y, i32 1
    %y2 = extractelement <3 x i64> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.umaxnum.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.umaxnum.i64(i64 %x1, i64 %y1)
    %3 = call i64 @llpc.umaxnum.i64(i64 %x2, i64 %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i64>
    %5 = load <3 x i64>, <3 x i64>* %4
    %6 = insertelement <3 x i64> %5, i64 %1, i32 0
    %7 = insertelement <3 x i64> %6, i64 %2, i32 1
    %8 = insertelement <3 x i64> %7, i64 %3, i32 2

    ret <3 x i64> %8
}

; <4 x i64> umax()  =>  llpc.umaxnum.i64
define spir_func <4 x i64> @_Z4umaxDv4_lDv4_l(
    <4 x i64> %x, <4 x i64> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i64> %x, i32 0
    %x1 = extractelement <4 x i64> %x, i32 1
    %x2 = extractelement <4 x i64> %x, i32 2
    %x3 = extractelement <4 x i64> %x, i32 3

    %y0 = extractelement <4 x i64> %y, i32 0
    %y1 = extractelement <4 x i64> %y, i32 1
    %y2 = extractelement <4 x i64> %y, i32 2
    %y3 = extractelement <4 x i64> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.umaxnum.i64(i64 %x0, i64 %y0)
    %2 = call i64 @llpc.umaxnum.i64(i64 %x1, i64 %y1)
    %3 = call i64 @llpc.umaxnum.i64(i64 %x2, i64 %y2)
    %4 = call i64 @llpc.umaxnum.i64(i64 %x3, i64 %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i64>
    %6 = load <4 x i64>, <4 x i64>* %5
    %7 = insertelement <4 x i64> %6, i64 %1, i32 0
    %8 = insertelement <4 x i64> %7, i64 %2, i32 1
    %9 = insertelement <4 x i64> %8, i64 %3, i32 2
    %10 = insertelement <4 x i64> %9, i64 %4, i32 3

    ret <4 x i64> %10
}

declare i64 @llpc.umaxnum.i64(i64, i64) #0

; i64 sclamp()  =>  llpc.sclamp.i64
define spir_func i64 @_Z6sclamplll(
    i64 %x, i64 %minVal, i64 %maxVal) #0
{
    %1 = call i64 @llpc.sclamp.i64(i64 %x, i64 %minVal, i64 %maxVal)

    ret i64 %1
}

; <2 x i64> sclamp()  =>  llpc.sclamp.i64
define spir_func <2 x i64> @_Z6sclampDv2_lDv2_lDv2_l(
    <2 x i64> %x, <2 x i64> %minVal, <2 x i64> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i64> %x, i32 0
    %x1 = extractelement <2 x i64> %x, i32 1

    %minVal0 = extractelement <2 x i64> %minVal, i32 0
    %minVal1 = extractelement <2 x i64> %minVal, i32 1

    %maxVal0 = extractelement <2 x i64> %maxVal, i32 0
    %maxVal1 = extractelement <2 x i64> %maxVal, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.sclamp.i64(i64 %x0, i64 %minVal0, i64 %maxVal0)
    %2 = call i64 @llpc.sclamp.i64(i64 %x1, i64 %minVal1, i64 %maxVal1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i64>
    %4 = load <2 x i64>, <2 x i64>* %3
    %5 = insertelement <2 x i64> %4, i64 %1, i32 0
    %6 = insertelement <2 x i64> %5, i64 %2, i32 1

    ret <2 x i64> %6
}

; <3 x i64> sclamp()  =>  llpc.sclamp.i64
define spir_func <3 x i64> @_Z6sclampDv3_lDv3_lDv3_l(
    <3 x i64> %x, <3 x i64> %minVal, <3 x i64> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i64> %x, i32 0
    %x1 = extractelement <3 x i64> %x, i32 1
    %x2 = extractelement <3 x i64> %x, i32 2

    %minVal0 = extractelement <3 x i64> %minVal, i32 0
    %minVal1 = extractelement <3 x i64> %minVal, i32 1
    %minVal2 = extractelement <3 x i64> %minVal, i32 2

    %maxVal0 = extractelement <3 x i64> %maxVal, i32 0
    %maxVal1 = extractelement <3 x i64> %maxVal, i32 1
    %maxVal2 = extractelement <3 x i64> %maxVal, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.sclamp.i64(i64 %x0, i64 %minVal0, i64 %maxVal0)
    %2 = call i64 @llpc.sclamp.i64(i64 %x1, i64 %minVal1, i64 %maxVal1)
    %3 = call i64 @llpc.sclamp.i64(i64 %x2, i64 %minVal2, i64 %maxVal2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i64>
    %5 = load <3 x i64>, <3 x i64>* %4
    %6 = insertelement <3 x i64> %5, i64 %1, i32 0
    %7 = insertelement <3 x i64> %6, i64 %2, i32 1
    %8 = insertelement <3 x i64> %7, i64 %3, i32 2

    ret <3 x i64> %8
}

; <4 x i64> sclamp()  =>  llpc.sclamp.i64
define spir_func <4 x i64> @_Z6sclampDv4_lDv4_lDv4_l(
    <4 x i64> %x, <4 x i64> %minVal, <4 x i64> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i64> %x, i32 0
    %x1 = extractelement <4 x i64> %x, i32 1
    %x2 = extractelement <4 x i64> %x, i32 2
    %x3 = extractelement <4 x i64> %x, i32 3

    %minVal0 = extractelement <4 x i64> %minVal, i32 0
    %minVal1 = extractelement <4 x i64> %minVal, i32 1
    %minVal2 = extractelement <4 x i64> %minVal, i32 2
    %minVal3 = extractelement <4 x i64> %minVal, i32 3

    %maxVal0 = extractelement <4 x i64> %maxVal, i32 0
    %maxVal1 = extractelement <4 x i64> %maxVal, i32 1
    %maxVal2 = extractelement <4 x i64> %maxVal, i32 2
    %maxVal3 = extractelement <4 x i64> %maxVal, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.sclamp.i64(i64 %x0, i64 %minVal0, i64 %maxVal0)
    %2 = call i64 @llpc.sclamp.i64(i64 %x1, i64 %minVal1, i64 %maxVal1)
    %3 = call i64 @llpc.sclamp.i64(i64 %x2, i64 %minVal2, i64 %maxVal2)
    %4 = call i64 @llpc.sclamp.i64(i64 %x3, i64 %minVal3, i64 %maxVal3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i64>
    %6 = load <4 x i64>, <4 x i64>* %5
    %7 = insertelement <4 x i64> %6, i64 %1, i32 0
    %8 = insertelement <4 x i64> %7, i64 %2, i32 1
    %9 = insertelement <4 x i64> %8, i64 %3, i32 2
    %10 = insertelement <4 x i64> %9, i64 %4, i32 3

    ret <4 x i64> %10
}

declare i64 @llpc.sclamp.i64(i64, i64, i64) #0

; i64 uclamp()  =>  llpc.uclamp.i64
define spir_func i64 @_Z6uclamplll(
    i64 %x, i64 %minVal, i64 %maxVal) #0
{
    %1 = call i64 @llpc.uclamp.i64(i64 %x, i64 %minVal, i64 %maxVal)

    ret i64 %1
}

; <2 x i64> uclamp()  =>  llpc.uclamp.i64
define spir_func <2 x i64> @_Z6uclampDv2_lDv2_lDv2_l(
    <2 x i64> %x, <2 x i64> %minVal, <2 x i64> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i64> %x, i32 0
    %x1 = extractelement <2 x i64> %x, i32 1

    %minVal0 = extractelement <2 x i64> %minVal, i32 0
    %minVal1 = extractelement <2 x i64> %minVal, i32 1

    %maxVal0 = extractelement <2 x i64> %maxVal, i32 0
    %maxVal1 = extractelement <2 x i64> %maxVal, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.uclamp.i64(i64 %x0, i64 %minVal0, i64 %maxVal0)
    %2 = call i64 @llpc.uclamp.i64(i64 %x1, i64 %minVal1, i64 %maxVal1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i64>
    %4 = load <2 x i64>, <2 x i64>* %3
    %5 = insertelement <2 x i64> %4, i64 %1, i32 0
    %6 = insertelement <2 x i64> %5, i64 %2, i32 1

    ret <2 x i64> %6
}

; <3 x i64> uclamp()  =>  llpc.uclamp.i64
define spir_func <3 x i64> @_Z6uclampDv3_lDv3_lDv3_l(
    <3 x i64> %x, <3 x i64> %minVal, <3 x i64> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i64> %x, i32 0
    %x1 = extractelement <3 x i64> %x, i32 1
    %x2 = extractelement <3 x i64> %x, i32 2

    %minVal0 = extractelement <3 x i64> %minVal, i32 0
    %minVal1 = extractelement <3 x i64> %minVal, i32 1
    %minVal2 = extractelement <3 x i64> %minVal, i32 2

    %maxVal0 = extractelement <3 x i64> %maxVal, i32 0
    %maxVal1 = extractelement <3 x i64> %maxVal, i32 1
    %maxVal2 = extractelement <3 x i64> %maxVal, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.uclamp.i64(i64 %x0, i64 %minVal0, i64 %maxVal0)
    %2 = call i64 @llpc.uclamp.i64(i64 %x1, i64 %minVal1, i64 %maxVal1)
    %3 = call i64 @llpc.uclamp.i64(i64 %x2, i64 %minVal2, i64 %maxVal2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i64>
    %5 = load <3 x i64>, <3 x i64>* %4
    %6 = insertelement <3 x i64> %5, i64 %1, i32 0
    %7 = insertelement <3 x i64> %6, i64 %2, i32 1
    %8 = insertelement <3 x i64> %7, i64 %3, i32 2

    ret <3 x i64> %8
}

; <4 x i64> uclamp()  =>  llpc.uclamp.i64
define spir_func <4 x i64> @_Z6uclampDv4_lDv4_lDv4_l(
    <4 x i64> %x, <4 x i64> %minVal, <4 x i64> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i64> %x, i32 0
    %x1 = extractelement <4 x i64> %x, i32 1
    %x2 = extractelement <4 x i64> %x, i32 2
    %x3 = extractelement <4 x i64> %x, i32 3

    %minVal0 = extractelement <4 x i64> %minVal, i32 0
    %minVal1 = extractelement <4 x i64> %minVal, i32 1
    %minVal2 = extractelement <4 x i64> %minVal, i32 2
    %minVal3 = extractelement <4 x i64> %minVal, i32 3

    %maxVal0 = extractelement <4 x i64> %maxVal, i32 0
    %maxVal1 = extractelement <4 x i64> %maxVal, i32 1
    %maxVal2 = extractelement <4 x i64> %maxVal, i32 2
    %maxVal3 = extractelement <4 x i64> %maxVal, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i64 @llpc.uclamp.i64(i64 %x0, i64 %minVal0, i64 %maxVal0)
    %2 = call i64 @llpc.uclamp.i64(i64 %x1, i64 %minVal1, i64 %maxVal1)
    %3 = call i64 @llpc.uclamp.i64(i64 %x2, i64 %minVal2, i64 %maxVal2)
    %4 = call i64 @llpc.uclamp.i64(i64 %x3, i64 %minVal3, i64 %maxVal3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i64>
    %6 = load <4 x i64>, <4 x i64>* %5
    %7 = insertelement <4 x i64> %6, i64 %1, i32 0
    %8 = insertelement <4 x i64> %7, i64 %2, i32 1
    %9 = insertelement <4 x i64> %8, i64 %3, i32 2
    %10 = insertelement <4 x i64> %9, i64 %4, i32 3

    ret <4 x i64> %10
}

declare i64 @llpc.uclamp.i64(i64, i64, i64) #0

; =====================================================================================================================
; >>>  Integer Functions
; =====================================================================================================================

; i32 findILsb()  =>  llpc.findIlsb.i64
define spir_func i32 @_Z8findILsbl(
    i64 %value) #0
{
    %1 = call i32 @llpc.findIlsb.i64(i64 %value)

    ret i32 %1
}

; <2 x i32> findILsb()  =>  llpc.findIlsb.i64
define spir_func <2 x i32> @_Z8findILsbDv2_l(
    <2 x i64> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x i64> %value, i32 0
    %value1 = extractelement <2 x i64> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findIlsb.i64(i64 %value0)
    %2 = call i32 @llpc.findIlsb.i64(i64 %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> findILsb()  =>  llpc.findIlsb.i64
define spir_func <3 x i32> @_Z8findILsbDv3_l(
    <3 x i64> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x i64> %value, i32 0
    %value1 = extractelement <3 x i64> %value, i32 1
    %value2 = extractelement <3 x i64> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findIlsb.i64(i64 %value0)
    %2 = call i32 @llpc.findIlsb.i64(i64 %value1)
    %3 = call i32 @llpc.findIlsb.i64(i64 %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> findILsb()  =>  llpc.findIlsb.i64
define spir_func <4 x i32> @_Z8findILsbDv4_l(
    <4 x i64> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x i64> %value, i32 0
    %value1 = extractelement <4 x i64> %value, i32 1
    %value2 = extractelement <4 x i64> %value, i32 2
    %value3 = extractelement <4 x i64> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findIlsb.i64(i64 %value0)
    %2 = call i32 @llpc.findIlsb.i64(i64 %value1)
    %3 = call i32 @llpc.findIlsb.i64(i64 %value2)
    %4 = call i32 @llpc.findIlsb.i64(i64 %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.findIlsb.i64(i64) #0

; i32 findUMsb()  =>  llpc.findUMsb.i64
define spir_func i32 @_Z8findUMsbl(
    i64 %value) #0
{
    %1 = call i32 @llpc.findUMsb.i64(i64 %value)

    ret i32 %1
}

; <2 x i32> findUMsb()  =>  llpc.findUMsb.i64
define spir_func <2 x i32> @_Z8findUMsbDv2_l(
    <2 x i64> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x i64> %value, i32 0
    %value1 = extractelement <2 x i64> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findUMsb.i64(i64 %value0)
    %2 = call i32 @llpc.findUMsb.i64(i64 %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> findUMsb()  =>  llpc.findUMsb.i64
define spir_func <3 x i32> @_Z8findUMsbDv3_l(
    <3 x i64> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x i64> %value, i32 0
    %value1 = extractelement <3 x i64> %value, i32 1
    %value2 = extractelement <3 x i64> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findUMsb.i64(i64 %value0)
    %2 = call i32 @llpc.findUMsb.i64(i64 %value1)
    %3 = call i32 @llpc.findUMsb.i64(i64 %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> findUMsb()  =>  llpc.findUMsb.i64
define spir_func <4 x i32> @_Z8findUMsbDv4_l(
    <4 x i64> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x i64> %value, i32 0
    %value1 = extractelement <4 x i64> %value, i32 1
    %value2 = extractelement <4 x i64> %value, i32 2
    %value3 = extractelement <4 x i64> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findUMsb.i64(i64 %value0)
    %2 = call i32 @llpc.findUMsb.i64(i64 %value1)
    %3 = call i32 @llpc.findUMsb.i64(i64 %value2)
    %4 = call i32 @llpc.findUMsb.i64(i64 %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.findUMsb.i64(i64) #0

; i32 findSMsb()  =>  llpc.findSMsb.i64
define spir_func i32 @_Z8findSMsbl(
    i64 %value) #0
{
    %1 = call i32 @llpc.findSMsb.i64(i64 %value)

    ret i32 %1
}

; <2 x i32> findSMsb()  =>  llpc.findSMsb.i64
define spir_func <2 x i32> @_Z8findSMsbDv2_l(
    <2 x i64> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x i64> %value, i32 0
    %value1 = extractelement <2 x i64> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findSMsb.i64(i64 %value0)
    %2 = call i32 @llpc.findSMsb.i64(i64 %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> findSMsb()  =>  llpc.findSMsb.i64
define spir_func <3 x i32> @_Z8findSMsbDv3_l(
    <3 x i64> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x i64> %value, i32 0
    %value1 = extractelement <3 x i64> %value, i32 1
    %value2 = extractelement <3 x i64> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findSMsb.i64(i64 %value0)
    %2 = call i32 @llpc.findSMsb.i64(i64 %value1)
    %3 = call i32 @llpc.findSMsb.i64(i64 %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> findSMsb()  =>  llpc.findSMsb.i64
define spir_func <4 x i32> @_Z8findSMsbDv4_l(
    <4 x i64> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x i64> %value, i32 0
    %value1 = extractelement <4 x i64> %value, i32 1
    %value2 = extractelement <4 x i64> %value, i32 2
    %value3 = extractelement <4 x i64> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findSMsb.i64(i64 %value0)
    %2 = call i32 @llpc.findSMsb.i64(i64 %value1)
    %3 = call i32 @llpc.findSMsb.i64(i64 %value2)
    %4 = call i32 @llpc.findSMsb.i64(i64 %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.findSMsb.i64(i64) #0

attributes #0 = { nounwind }
attributes #1 = { nounwind readnone }
