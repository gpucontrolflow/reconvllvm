;**********************************************************************************************************************
;*
;*  Trade secret of Advanced Micro Devices, Inc.
;*  Copyright (c) 2017, Advanced Micro Devices, Inc., (unpublished)
;*
;*  All rights reserved. This notice is intended as a precaution against inadvertent publication and does not imply
;*  publication or any waiver of confidentiality. The year included in the foregoing notice is the year of creation of
;*  the work.
;*
;**********************************************************************************************************************

;**********************************************************************************************************************
;* @file  g_glslArithOpEmu.ll
;* @brief LLPC LLVM-IR file: contains emulation codes for GLSL arithmetic operations (std32).
;*
;* @note  This file has been generated automatically. Do not hand-modify this file. When changes are needed, modify the
;*        generating script genGlslArithOpEmuCode.py.
;**********************************************************************************************************************

target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v16:16:16-v24:32:32-v32:32:32-v48:64:64-v64:64:64-v96:128:128-v128:128:128-v192:256:256-v256:256:256-v512:512:512-v1024:1024:1024"
target triple = "spir64-unknown-unknown"

; =====================================================================================================================
; >>>  SPIR-V specific
; =====================================================================================================================

; float quantizeToF16()  =>  llpc.quantizeToF16
define spir_func float @_Z13quantizeToF16f(
    float %x) #0
{
    %1 = call float @llpc.quantizeToF16(float %x)

    ret float %1
}

; <2 x float> quantizeToF16()  =>  llpc.quantizeToF16
define spir_func <2 x float> @_Z13quantizeToF16Dv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.quantizeToF16(float %x0)
    %2 = call float @llpc.quantizeToF16(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> quantizeToF16()  =>  llpc.quantizeToF16
define spir_func <3 x float> @_Z13quantizeToF16Dv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.quantizeToF16(float %x0)
    %2 = call float @llpc.quantizeToF16(float %x1)
    %3 = call float @llpc.quantizeToF16(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> quantizeToF16()  =>  llpc.quantizeToF16
define spir_func <4 x float> @_Z13quantizeToF16Dv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.quantizeToF16(float %x0)
    %2 = call float @llpc.quantizeToF16(float %x1)
    %3 = call float @llpc.quantizeToF16(float %x2)
    %4 = call float @llpc.quantizeToF16(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.quantizeToF16(float) #0

; =====================================================================================================================
; >>>  Operators
; =====================================================================================================================

; float fdiv()  =>  llpc.fdiv.f32
define spir_func float @_Z4fdivff(
    float %y, float %x) #0
{
    %1 = call float @llpc.fdiv.f32(float %y, float %x)

    ret float %1
}

; <2 x float> fdiv()  =>  llpc.fdiv.f32
define spir_func <2 x float> @_Z4fdivDv2_fDv2_f(
    <2 x float> %y, <2 x float> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <2 x float> %y, i32 0
    %y1 = extractelement <2 x float> %y, i32 1

    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fdiv.f32(float %y0, float %x0)
    %2 = call float @llpc.fdiv.f32(float %y1, float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> fdiv()  =>  llpc.fdiv.f32
define spir_func <3 x float> @_Z4fdivDv3_fDv3_f(
    <3 x float> %y, <3 x float> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <3 x float> %y, i32 0
    %y1 = extractelement <3 x float> %y, i32 1
    %y2 = extractelement <3 x float> %y, i32 2

    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fdiv.f32(float %y0, float %x0)
    %2 = call float @llpc.fdiv.f32(float %y1, float %x1)
    %3 = call float @llpc.fdiv.f32(float %y2, float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> fdiv()  =>  llpc.fdiv.f32
define spir_func <4 x float> @_Z4fdivDv4_fDv4_f(
    <4 x float> %y, <4 x float> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <4 x float> %y, i32 0
    %y1 = extractelement <4 x float> %y, i32 1
    %y2 = extractelement <4 x float> %y, i32 2
    %y3 = extractelement <4 x float> %y, i32 3

    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fdiv.f32(float %y0, float %x0)
    %2 = call float @llpc.fdiv.f32(float %y1, float %x1)
    %3 = call float @llpc.fdiv.f32(float %y2, float %x2)
    %4 = call float @llpc.fdiv.f32(float %y3, float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.fdiv.f32(float, float) #0

; i32 smod()  =>  llpc.mod.i32
define spir_func i32 @_Z4smodii(
    i32 %x, i32 %y) #0
{
    %1 = call i32 @llpc.mod.i32(i32 %x, i32 %y)

    ret i32 %1
}

; <2 x i32> smod()  =>  llpc.mod.i32
define spir_func <2 x i32> @_Z4smodDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %y0 = extractelement <2 x i32> %y, i32 0
    %y1 = extractelement <2 x i32> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.mod.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.mod.i32(i32 %x1, i32 %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> smod()  =>  llpc.mod.i32
define spir_func <3 x i32> @_Z4smodDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %y0 = extractelement <3 x i32> %y, i32 0
    %y1 = extractelement <3 x i32> %y, i32 1
    %y2 = extractelement <3 x i32> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.mod.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.mod.i32(i32 %x1, i32 %y1)
    %3 = call i32 @llpc.mod.i32(i32 %x2, i32 %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> smod()  =>  llpc.mod.i32
define spir_func <4 x i32> @_Z4smodDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %y0 = extractelement <4 x i32> %y, i32 0
    %y1 = extractelement <4 x i32> %y, i32 1
    %y2 = extractelement <4 x i32> %y, i32 2
    %y3 = extractelement <4 x i32> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.mod.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.mod.i32(i32 %x1, i32 %y1)
    %3 = call i32 @llpc.mod.i32(i32 %x2, i32 %y2)
    %4 = call i32 @llpc.mod.i32(i32 %x3, i32 %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.mod.i32(i32, i32) #0

; =====================================================================================================================
; >>>  Angle and Trigonometry Functions
; =====================================================================================================================

; float sin()  =>  llvm.sin.f32
define spir_func float @_Z3sinf(
    float %angle) #0
{
    %1 = call float @llvm.sin.f32(float %angle)

    ret float %1
}

; <2 x float> sin()  =>  llvm.sin.f32
define spir_func <2 x float> @_Z3sinDv2_f(
    <2 x float> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <2 x float> %angle, i32 0
    %angle1 = extractelement <2 x float> %angle, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.sin.f32(float %angle0)
    %2 = call float @llvm.sin.f32(float %angle1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> sin()  =>  llvm.sin.f32
define spir_func <3 x float> @_Z3sinDv3_f(
    <3 x float> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <3 x float> %angle, i32 0
    %angle1 = extractelement <3 x float> %angle, i32 1
    %angle2 = extractelement <3 x float> %angle, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.sin.f32(float %angle0)
    %2 = call float @llvm.sin.f32(float %angle1)
    %3 = call float @llvm.sin.f32(float %angle2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> sin()  =>  llvm.sin.f32
define spir_func <4 x float> @_Z3sinDv4_f(
    <4 x float> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <4 x float> %angle, i32 0
    %angle1 = extractelement <4 x float> %angle, i32 1
    %angle2 = extractelement <4 x float> %angle, i32 2
    %angle3 = extractelement <4 x float> %angle, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.sin.f32(float %angle0)
    %2 = call float @llvm.sin.f32(float %angle1)
    %3 = call float @llvm.sin.f32(float %angle2)
    %4 = call float @llvm.sin.f32(float %angle3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.sin.f32(float) #0

; float cos()  =>  llvm.cos.f32
define spir_func float @_Z3cosf(
    float %angle) #0
{
    %1 = call float @llvm.cos.f32(float %angle)

    ret float %1
}

; <2 x float> cos()  =>  llvm.cos.f32
define spir_func <2 x float> @_Z3cosDv2_f(
    <2 x float> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <2 x float> %angle, i32 0
    %angle1 = extractelement <2 x float> %angle, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.cos.f32(float %angle0)
    %2 = call float @llvm.cos.f32(float %angle1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> cos()  =>  llvm.cos.f32
define spir_func <3 x float> @_Z3cosDv3_f(
    <3 x float> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <3 x float> %angle, i32 0
    %angle1 = extractelement <3 x float> %angle, i32 1
    %angle2 = extractelement <3 x float> %angle, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.cos.f32(float %angle0)
    %2 = call float @llvm.cos.f32(float %angle1)
    %3 = call float @llvm.cos.f32(float %angle2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> cos()  =>  llvm.cos.f32
define spir_func <4 x float> @_Z3cosDv4_f(
    <4 x float> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <4 x float> %angle, i32 0
    %angle1 = extractelement <4 x float> %angle, i32 1
    %angle2 = extractelement <4 x float> %angle, i32 2
    %angle3 = extractelement <4 x float> %angle, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.cos.f32(float %angle0)
    %2 = call float @llvm.cos.f32(float %angle1)
    %3 = call float @llvm.cos.f32(float %angle2)
    %4 = call float @llvm.cos.f32(float %angle3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.cos.f32(float) #0

; float tan()  =>  llpc.tan.f32
define spir_func float @_Z3tanf(
    float %angle) #0
{
    %1 = call float @llpc.tan.f32(float %angle)

    ret float %1
}

; <2 x float> tan()  =>  llpc.tan.f32
define spir_func <2 x float> @_Z3tanDv2_f(
    <2 x float> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <2 x float> %angle, i32 0
    %angle1 = extractelement <2 x float> %angle, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.tan.f32(float %angle0)
    %2 = call float @llpc.tan.f32(float %angle1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> tan()  =>  llpc.tan.f32
define spir_func <3 x float> @_Z3tanDv3_f(
    <3 x float> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <3 x float> %angle, i32 0
    %angle1 = extractelement <3 x float> %angle, i32 1
    %angle2 = extractelement <3 x float> %angle, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.tan.f32(float %angle0)
    %2 = call float @llpc.tan.f32(float %angle1)
    %3 = call float @llpc.tan.f32(float %angle2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> tan()  =>  llpc.tan.f32
define spir_func <4 x float> @_Z3tanDv4_f(
    <4 x float> %angle) #0
{
    ; Extract components from source vectors
    %angle0 = extractelement <4 x float> %angle, i32 0
    %angle1 = extractelement <4 x float> %angle, i32 1
    %angle2 = extractelement <4 x float> %angle, i32 2
    %angle3 = extractelement <4 x float> %angle, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.tan.f32(float %angle0)
    %2 = call float @llpc.tan.f32(float %angle1)
    %3 = call float @llpc.tan.f32(float %angle2)
    %4 = call float @llpc.tan.f32(float %angle3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.tan.f32(float) #0

; float asin()  =>  llpc.asin.f32
define spir_func float @_Z4asinf(
    float %x) #0
{
    %1 = call float @llpc.asin.f32(float %x)

    ret float %1
}

; <2 x float> asin()  =>  llpc.asin.f32
define spir_func <2 x float> @_Z4asinDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.asin.f32(float %x0)
    %2 = call float @llpc.asin.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> asin()  =>  llpc.asin.f32
define spir_func <3 x float> @_Z4asinDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.asin.f32(float %x0)
    %2 = call float @llpc.asin.f32(float %x1)
    %3 = call float @llpc.asin.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> asin()  =>  llpc.asin.f32
define spir_func <4 x float> @_Z4asinDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.asin.f32(float %x0)
    %2 = call float @llpc.asin.f32(float %x1)
    %3 = call float @llpc.asin.f32(float %x2)
    %4 = call float @llpc.asin.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.asin.f32(float) #0

; float acos()  =>  llpc.acos.f32
define spir_func float @_Z4acosf(
    float %x) #0
{
    %1 = call float @llpc.acos.f32(float %x)

    ret float %1
}

; <2 x float> acos()  =>  llpc.acos.f32
define spir_func <2 x float> @_Z4acosDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.acos.f32(float %x0)
    %2 = call float @llpc.acos.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> acos()  =>  llpc.acos.f32
define spir_func <3 x float> @_Z4acosDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.acos.f32(float %x0)
    %2 = call float @llpc.acos.f32(float %x1)
    %3 = call float @llpc.acos.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> acos()  =>  llpc.acos.f32
define spir_func <4 x float> @_Z4acosDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.acos.f32(float %x0)
    %2 = call float @llpc.acos.f32(float %x1)
    %3 = call float @llpc.acos.f32(float %x2)
    %4 = call float @llpc.acos.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.acos.f32(float) #0

; float atan()  =>  llpc.atan.f32
define spir_func float @_Z4atanf(
    float %x) #0
{
    %1 = call float @llpc.atan.f32(float %x)

    ret float %1
}

; <2 x float> atan()  =>  llpc.atan.f32
define spir_func <2 x float> @_Z4atanDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.atan.f32(float %x0)
    %2 = call float @llpc.atan.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> atan()  =>  llpc.atan.f32
define spir_func <3 x float> @_Z4atanDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.atan.f32(float %x0)
    %2 = call float @llpc.atan.f32(float %x1)
    %3 = call float @llpc.atan.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> atan()  =>  llpc.atan.f32
define spir_func <4 x float> @_Z4atanDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.atan.f32(float %x0)
    %2 = call float @llpc.atan.f32(float %x1)
    %3 = call float @llpc.atan.f32(float %x2)
    %4 = call float @llpc.atan.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.atan.f32(float) #0

; float atan2()  =>  llpc.atan2.f32
define spir_func float @_Z5atan2ff(
    float %y, float %x) #0
{
    %1 = call float @llpc.atan2.f32(float %y, float %x)

    ret float %1
}

; <2 x float> atan2()  =>  llpc.atan2.f32
define spir_func <2 x float> @_Z5atan2Dv2_fDv2_f(
    <2 x float> %y, <2 x float> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <2 x float> %y, i32 0
    %y1 = extractelement <2 x float> %y, i32 1

    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.atan2.f32(float %y0, float %x0)
    %2 = call float @llpc.atan2.f32(float %y1, float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> atan2()  =>  llpc.atan2.f32
define spir_func <3 x float> @_Z5atan2Dv3_fDv3_f(
    <3 x float> %y, <3 x float> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <3 x float> %y, i32 0
    %y1 = extractelement <3 x float> %y, i32 1
    %y2 = extractelement <3 x float> %y, i32 2

    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.atan2.f32(float %y0, float %x0)
    %2 = call float @llpc.atan2.f32(float %y1, float %x1)
    %3 = call float @llpc.atan2.f32(float %y2, float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> atan2()  =>  llpc.atan2.f32
define spir_func <4 x float> @_Z5atan2Dv4_fDv4_f(
    <4 x float> %y, <4 x float> %x) #0
{
    ; Extract components from source vectors
    %y0 = extractelement <4 x float> %y, i32 0
    %y1 = extractelement <4 x float> %y, i32 1
    %y2 = extractelement <4 x float> %y, i32 2
    %y3 = extractelement <4 x float> %y, i32 3

    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.atan2.f32(float %y0, float %x0)
    %2 = call float @llpc.atan2.f32(float %y1, float %x1)
    %3 = call float @llpc.atan2.f32(float %y2, float %x2)
    %4 = call float @llpc.atan2.f32(float %y3, float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.atan2.f32(float, float) #0

; float sinh()  =>  llpc.sinh.f32
define spir_func float @_Z4sinhf(
    float %x) #0
{
    %1 = call float @llpc.sinh.f32(float %x)

    ret float %1
}

; <2 x float> sinh()  =>  llpc.sinh.f32
define spir_func <2 x float> @_Z4sinhDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.sinh.f32(float %x0)
    %2 = call float @llpc.sinh.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> sinh()  =>  llpc.sinh.f32
define spir_func <3 x float> @_Z4sinhDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.sinh.f32(float %x0)
    %2 = call float @llpc.sinh.f32(float %x1)
    %3 = call float @llpc.sinh.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> sinh()  =>  llpc.sinh.f32
define spir_func <4 x float> @_Z4sinhDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.sinh.f32(float %x0)
    %2 = call float @llpc.sinh.f32(float %x1)
    %3 = call float @llpc.sinh.f32(float %x2)
    %4 = call float @llpc.sinh.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.sinh.f32(float) #0

; float cosh()  =>  llpc.cosh.f32
define spir_func float @_Z4coshf(
    float %x) #0
{
    %1 = call float @llpc.cosh.f32(float %x)

    ret float %1
}

; <2 x float> cosh()  =>  llpc.cosh.f32
define spir_func <2 x float> @_Z4coshDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.cosh.f32(float %x0)
    %2 = call float @llpc.cosh.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> cosh()  =>  llpc.cosh.f32
define spir_func <3 x float> @_Z4coshDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.cosh.f32(float %x0)
    %2 = call float @llpc.cosh.f32(float %x1)
    %3 = call float @llpc.cosh.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> cosh()  =>  llpc.cosh.f32
define spir_func <4 x float> @_Z4coshDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.cosh.f32(float %x0)
    %2 = call float @llpc.cosh.f32(float %x1)
    %3 = call float @llpc.cosh.f32(float %x2)
    %4 = call float @llpc.cosh.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.cosh.f32(float) #0

; float tanh()  =>  llpc.tanh.f32
define spir_func float @_Z4tanhf(
    float %x) #0
{
    %1 = call float @llpc.tanh.f32(float %x)

    ret float %1
}

; <2 x float> tanh()  =>  llpc.tanh.f32
define spir_func <2 x float> @_Z4tanhDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.tanh.f32(float %x0)
    %2 = call float @llpc.tanh.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> tanh()  =>  llpc.tanh.f32
define spir_func <3 x float> @_Z4tanhDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.tanh.f32(float %x0)
    %2 = call float @llpc.tanh.f32(float %x1)
    %3 = call float @llpc.tanh.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> tanh()  =>  llpc.tanh.f32
define spir_func <4 x float> @_Z4tanhDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.tanh.f32(float %x0)
    %2 = call float @llpc.tanh.f32(float %x1)
    %3 = call float @llpc.tanh.f32(float %x2)
    %4 = call float @llpc.tanh.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.tanh.f32(float) #0

; float asinh()  =>  llpc.asinh.f32
define spir_func float @_Z5asinhf(
    float %x) #0
{
    %1 = call float @llpc.asinh.f32(float %x)

    ret float %1
}

; <2 x float> asinh()  =>  llpc.asinh.f32
define spir_func <2 x float> @_Z5asinhDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.asinh.f32(float %x0)
    %2 = call float @llpc.asinh.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> asinh()  =>  llpc.asinh.f32
define spir_func <3 x float> @_Z5asinhDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.asinh.f32(float %x0)
    %2 = call float @llpc.asinh.f32(float %x1)
    %3 = call float @llpc.asinh.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> asinh()  =>  llpc.asinh.f32
define spir_func <4 x float> @_Z5asinhDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.asinh.f32(float %x0)
    %2 = call float @llpc.asinh.f32(float %x1)
    %3 = call float @llpc.asinh.f32(float %x2)
    %4 = call float @llpc.asinh.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.asinh.f32(float) #0

; float acosh()  =>  llpc.acosh.f32
define spir_func float @_Z5acoshf(
    float %x) #0
{
    %1 = call float @llpc.acosh.f32(float %x)

    ret float %1
}

; <2 x float> acosh()  =>  llpc.acosh.f32
define spir_func <2 x float> @_Z5acoshDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.acosh.f32(float %x0)
    %2 = call float @llpc.acosh.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> acosh()  =>  llpc.acosh.f32
define spir_func <3 x float> @_Z5acoshDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.acosh.f32(float %x0)
    %2 = call float @llpc.acosh.f32(float %x1)
    %3 = call float @llpc.acosh.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> acosh()  =>  llpc.acosh.f32
define spir_func <4 x float> @_Z5acoshDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.acosh.f32(float %x0)
    %2 = call float @llpc.acosh.f32(float %x1)
    %3 = call float @llpc.acosh.f32(float %x2)
    %4 = call float @llpc.acosh.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.acosh.f32(float) #0

; float atanh()  =>  llpc.atanh.f32
define spir_func float @_Z5atanhf(
    float %x) #0
{
    %1 = call float @llpc.atanh.f32(float %x)

    ret float %1
}

; <2 x float> atanh()  =>  llpc.atanh.f32
define spir_func <2 x float> @_Z5atanhDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.atanh.f32(float %x0)
    %2 = call float @llpc.atanh.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> atanh()  =>  llpc.atanh.f32
define spir_func <3 x float> @_Z5atanhDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.atanh.f32(float %x0)
    %2 = call float @llpc.atanh.f32(float %x1)
    %3 = call float @llpc.atanh.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> atanh()  =>  llpc.atanh.f32
define spir_func <4 x float> @_Z5atanhDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.atanh.f32(float %x0)
    %2 = call float @llpc.atanh.f32(float %x1)
    %3 = call float @llpc.atanh.f32(float %x2)
    %4 = call float @llpc.atanh.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.atanh.f32(float) #0

; =====================================================================================================================
; >>>  Exponential Functions
; =====================================================================================================================

; float pow()  =>  llvm.pow.f32
define spir_func float @_Z3powff(
    float %x, float %y) #0
{
    %1 = call float @llvm.pow.f32(float %x, float %y)

    ret float %1
}

; <2 x float> pow()  =>  llvm.pow.f32
define spir_func <2 x float> @_Z3powDv2_fDv2_f(
    <2 x float> %x, <2 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %y0 = extractelement <2 x float> %y, i32 0
    %y1 = extractelement <2 x float> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.pow.f32(float %x0, float %y0)
    %2 = call float @llvm.pow.f32(float %x1, float %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> pow()  =>  llvm.pow.f32
define spir_func <3 x float> @_Z3powDv3_fDv3_f(
    <3 x float> %x, <3 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %y0 = extractelement <3 x float> %y, i32 0
    %y1 = extractelement <3 x float> %y, i32 1
    %y2 = extractelement <3 x float> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.pow.f32(float %x0, float %y0)
    %2 = call float @llvm.pow.f32(float %x1, float %y1)
    %3 = call float @llvm.pow.f32(float %x2, float %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> pow()  =>  llvm.pow.f32
define spir_func <4 x float> @_Z3powDv4_fDv4_f(
    <4 x float> %x, <4 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %y0 = extractelement <4 x float> %y, i32 0
    %y1 = extractelement <4 x float> %y, i32 1
    %y2 = extractelement <4 x float> %y, i32 2
    %y3 = extractelement <4 x float> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.pow.f32(float %x0, float %y0)
    %2 = call float @llvm.pow.f32(float %x1, float %y1)
    %3 = call float @llvm.pow.f32(float %x2, float %y2)
    %4 = call float @llvm.pow.f32(float %x3, float %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.pow.f32(float, float) #0

; float exp()  =>  llpc.exp.f32
define spir_func float @_Z3expf(
    float %x) #0
{
    %1 = call float @llpc.exp.f32(float %x)

    ret float %1
}

; <2 x float> exp()  =>  llpc.exp.f32
define spir_func <2 x float> @_Z3expDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.exp.f32(float %x0)
    %2 = call float @llpc.exp.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> exp()  =>  llpc.exp.f32
define spir_func <3 x float> @_Z3expDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.exp.f32(float %x0)
    %2 = call float @llpc.exp.f32(float %x1)
    %3 = call float @llpc.exp.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> exp()  =>  llpc.exp.f32
define spir_func <4 x float> @_Z3expDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.exp.f32(float %x0)
    %2 = call float @llpc.exp.f32(float %x1)
    %3 = call float @llpc.exp.f32(float %x2)
    %4 = call float @llpc.exp.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.exp.f32(float) #0

; float log()  =>  llpc.log.f32
define spir_func float @_Z3logf(
    float %x) #0
{
    %1 = call float @llpc.log.f32(float %x)

    ret float %1
}

; <2 x float> log()  =>  llpc.log.f32
define spir_func <2 x float> @_Z3logDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.log.f32(float %x0)
    %2 = call float @llpc.log.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> log()  =>  llpc.log.f32
define spir_func <3 x float> @_Z3logDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.log.f32(float %x0)
    %2 = call float @llpc.log.f32(float %x1)
    %3 = call float @llpc.log.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> log()  =>  llpc.log.f32
define spir_func <4 x float> @_Z3logDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.log.f32(float %x0)
    %2 = call float @llpc.log.f32(float %x1)
    %3 = call float @llpc.log.f32(float %x2)
    %4 = call float @llpc.log.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.log.f32(float) #0

; float exp2()  =>  llvm.exp2.f32
define spir_func float @_Z4exp2f(
    float %x) #0
{
    %1 = call float @llvm.exp2.f32(float %x)

    ret float %1
}

; <2 x float> exp2()  =>  llvm.exp2.f32
define spir_func <2 x float> @_Z4exp2Dv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.exp2.f32(float %x0)
    %2 = call float @llvm.exp2.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> exp2()  =>  llvm.exp2.f32
define spir_func <3 x float> @_Z4exp2Dv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.exp2.f32(float %x0)
    %2 = call float @llvm.exp2.f32(float %x1)
    %3 = call float @llvm.exp2.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> exp2()  =>  llvm.exp2.f32
define spir_func <4 x float> @_Z4exp2Dv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.exp2.f32(float %x0)
    %2 = call float @llvm.exp2.f32(float %x1)
    %3 = call float @llvm.exp2.f32(float %x2)
    %4 = call float @llvm.exp2.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.exp2.f32(float) #0

; float log2()  =>  llvm.log2.f32
define spir_func float @_Z4log2f(
    float %x) #0
{
    %1 = call float @llvm.log2.f32(float %x)

    ret float %1
}

; <2 x float> log2()  =>  llvm.log2.f32
define spir_func <2 x float> @_Z4log2Dv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.log2.f32(float %x0)
    %2 = call float @llvm.log2.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> log2()  =>  llvm.log2.f32
define spir_func <3 x float> @_Z4log2Dv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.log2.f32(float %x0)
    %2 = call float @llvm.log2.f32(float %x1)
    %3 = call float @llvm.log2.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> log2()  =>  llvm.log2.f32
define spir_func <4 x float> @_Z4log2Dv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.log2.f32(float %x0)
    %2 = call float @llvm.log2.f32(float %x1)
    %3 = call float @llvm.log2.f32(float %x2)
    %4 = call float @llvm.log2.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.log2.f32(float) #0

; float sqrt()  =>  llvm.sqrt.f32
define spir_func float @_Z4sqrtf(
    float %x) #0
{
    %1 = call float @llvm.sqrt.f32(float %x)

    ret float %1
}

; <2 x float> sqrt()  =>  llvm.sqrt.f32
define spir_func <2 x float> @_Z4sqrtDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.sqrt.f32(float %x0)
    %2 = call float @llvm.sqrt.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> sqrt()  =>  llvm.sqrt.f32
define spir_func <3 x float> @_Z4sqrtDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.sqrt.f32(float %x0)
    %2 = call float @llvm.sqrt.f32(float %x1)
    %3 = call float @llvm.sqrt.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> sqrt()  =>  llvm.sqrt.f32
define spir_func <4 x float> @_Z4sqrtDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.sqrt.f32(float %x0)
    %2 = call float @llvm.sqrt.f32(float %x1)
    %3 = call float @llvm.sqrt.f32(float %x2)
    %4 = call float @llvm.sqrt.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.sqrt.f32(float) #0

; float inverseSqrt()  =>  llpc.inverseSqrt.f32
define spir_func float @_Z11inverseSqrtf(
    float %x) #0
{
    %1 = call float @llpc.inverseSqrt.f32(float %x)

    ret float %1
}

; <2 x float> inverseSqrt()  =>  llpc.inverseSqrt.f32
define spir_func <2 x float> @_Z11inverseSqrtDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.inverseSqrt.f32(float %x0)
    %2 = call float @llpc.inverseSqrt.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> inverseSqrt()  =>  llpc.inverseSqrt.f32
define spir_func <3 x float> @_Z11inverseSqrtDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.inverseSqrt.f32(float %x0)
    %2 = call float @llpc.inverseSqrt.f32(float %x1)
    %3 = call float @llpc.inverseSqrt.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> inverseSqrt()  =>  llpc.inverseSqrt.f32
define spir_func <4 x float> @_Z11inverseSqrtDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.inverseSqrt.f32(float %x0)
    %2 = call float @llpc.inverseSqrt.f32(float %x1)
    %3 = call float @llpc.inverseSqrt.f32(float %x2)
    %4 = call float @llpc.inverseSqrt.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.inverseSqrt.f32(float) #0

; =====================================================================================================================
; >>>  Common Functions
; =====================================================================================================================

; float fabs()  =>  llvm.fabs.f32
define spir_func float @_Z4fabsf(
    float %x) #0
{
    %1 = call float @llvm.fabs.f32(float %x)

    ret float %1
}

; <2 x float> fabs()  =>  llvm.fabs.f32
define spir_func <2 x float> @_Z4fabsDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.fabs.f32(float %x0)
    %2 = call float @llvm.fabs.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> fabs()  =>  llvm.fabs.f32
define spir_func <3 x float> @_Z4fabsDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.fabs.f32(float %x0)
    %2 = call float @llvm.fabs.f32(float %x1)
    %3 = call float @llvm.fabs.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> fabs()  =>  llvm.fabs.f32
define spir_func <4 x float> @_Z4fabsDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.fabs.f32(float %x0)
    %2 = call float @llvm.fabs.f32(float %x1)
    %3 = call float @llvm.fabs.f32(float %x2)
    %4 = call float @llvm.fabs.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.fabs.f32(float) #0

; i32 sabs()  =>  llpc.sabs.i32
define spir_func i32 @_Z4sabsi(
    i32 %x) #0
{
    %1 = call i32 @llpc.sabs.i32(i32 %x)

    ret i32 %1
}

; <2 x i32> sabs()  =>  llpc.sabs.i32
define spir_func <2 x i32> @_Z4sabsDv2_i(
    <2 x i32> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.sabs.i32(i32 %x0)
    %2 = call i32 @llpc.sabs.i32(i32 %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> sabs()  =>  llpc.sabs.i32
define spir_func <3 x i32> @_Z4sabsDv3_i(
    <3 x i32> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.sabs.i32(i32 %x0)
    %2 = call i32 @llpc.sabs.i32(i32 %x1)
    %3 = call i32 @llpc.sabs.i32(i32 %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> sabs()  =>  llpc.sabs.i32
define spir_func <4 x i32> @_Z4sabsDv4_i(
    <4 x i32> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.sabs.i32(i32 %x0)
    %2 = call i32 @llpc.sabs.i32(i32 %x1)
    %3 = call i32 @llpc.sabs.i32(i32 %x2)
    %4 = call i32 @llpc.sabs.i32(i32 %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.sabs.i32(i32) #0

; float fsign()  =>  llpc.fsign.f32
define spir_func float @_Z5fsignf(
    float %x) #0
{
    %1 = call float @llpc.fsign.f32(float %x)

    ret float %1
}

; <2 x float> fsign()  =>  llpc.fsign.f32
define spir_func <2 x float> @_Z5fsignDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fsign.f32(float %x0)
    %2 = call float @llpc.fsign.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> fsign()  =>  llpc.fsign.f32
define spir_func <3 x float> @_Z5fsignDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fsign.f32(float %x0)
    %2 = call float @llpc.fsign.f32(float %x1)
    %3 = call float @llpc.fsign.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> fsign()  =>  llpc.fsign.f32
define spir_func <4 x float> @_Z5fsignDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fsign.f32(float %x0)
    %2 = call float @llpc.fsign.f32(float %x1)
    %3 = call float @llpc.fsign.f32(float %x2)
    %4 = call float @llpc.fsign.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.fsign.f32(float) #0

; i32 ssign()  =>  llpc.ssign.i32
define spir_func i32 @_Z5ssigni(
    i32 %x) #0
{
    %1 = call i32 @llpc.ssign.i32(i32 %x)

    ret i32 %1
}

; <2 x i32> ssign()  =>  llpc.ssign.i32
define spir_func <2 x i32> @_Z5ssignDv2_i(
    <2 x i32> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.ssign.i32(i32 %x0)
    %2 = call i32 @llpc.ssign.i32(i32 %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> ssign()  =>  llpc.ssign.i32
define spir_func <3 x i32> @_Z5ssignDv3_i(
    <3 x i32> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.ssign.i32(i32 %x0)
    %2 = call i32 @llpc.ssign.i32(i32 %x1)
    %3 = call i32 @llpc.ssign.i32(i32 %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> ssign()  =>  llpc.ssign.i32
define spir_func <4 x i32> @_Z5ssignDv4_i(
    <4 x i32> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.ssign.i32(i32 %x0)
    %2 = call i32 @llpc.ssign.i32(i32 %x1)
    %3 = call i32 @llpc.ssign.i32(i32 %x2)
    %4 = call i32 @llpc.ssign.i32(i32 %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.ssign.i32(i32) #0

; float floor()  =>  llvm.floor.f32
define spir_func float @_Z5floorf(
    float %x) #0
{
    %1 = call float @llvm.floor.f32(float %x)

    ret float %1
}

; <2 x float> floor()  =>  llvm.floor.f32
define spir_func <2 x float> @_Z5floorDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.floor.f32(float %x0)
    %2 = call float @llvm.floor.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> floor()  =>  llvm.floor.f32
define spir_func <3 x float> @_Z5floorDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.floor.f32(float %x0)
    %2 = call float @llvm.floor.f32(float %x1)
    %3 = call float @llvm.floor.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> floor()  =>  llvm.floor.f32
define spir_func <4 x float> @_Z5floorDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.floor.f32(float %x0)
    %2 = call float @llvm.floor.f32(float %x1)
    %3 = call float @llvm.floor.f32(float %x2)
    %4 = call float @llvm.floor.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.floor.f32(float) #0

; float trunc()  =>  llvm.trunc.f32
define spir_func float @_Z5truncf(
    float %x) #0
{
    %1 = call float @llvm.trunc.f32(float %x)

    ret float %1
}

; <2 x float> trunc()  =>  llvm.trunc.f32
define spir_func <2 x float> @_Z5truncDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.trunc.f32(float %x0)
    %2 = call float @llvm.trunc.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> trunc()  =>  llvm.trunc.f32
define spir_func <3 x float> @_Z5truncDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.trunc.f32(float %x0)
    %2 = call float @llvm.trunc.f32(float %x1)
    %3 = call float @llvm.trunc.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> trunc()  =>  llvm.trunc.f32
define spir_func <4 x float> @_Z5truncDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.trunc.f32(float %x0)
    %2 = call float @llvm.trunc.f32(float %x1)
    %3 = call float @llvm.trunc.f32(float %x2)
    %4 = call float @llvm.trunc.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.trunc.f32(float) #0

; float round()  =>  llpc.round.f32
define spir_func float @_Z5roundf(
    float %x) #0
{
    %1 = call float @llpc.round.f32(float %x)

    ret float %1
}

; <2 x float> round()  =>  llpc.round.f32
define spir_func <2 x float> @_Z5roundDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.round.f32(float %x0)
    %2 = call float @llpc.round.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> round()  =>  llpc.round.f32
define spir_func <3 x float> @_Z5roundDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.round.f32(float %x0)
    %2 = call float @llpc.round.f32(float %x1)
    %3 = call float @llpc.round.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> round()  =>  llpc.round.f32
define spir_func <4 x float> @_Z5roundDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.round.f32(float %x0)
    %2 = call float @llpc.round.f32(float %x1)
    %3 = call float @llpc.round.f32(float %x2)
    %4 = call float @llpc.round.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.round.f32(float) #0

; float roundEven()  =>  llvm.rint.f32
define spir_func float @_Z9roundEvenf(
    float %x) #0
{
    %1 = call float @llvm.rint.f32(float %x)

    ret float %1
}

; <2 x float> roundEven()  =>  llvm.rint.f32
define spir_func <2 x float> @_Z9roundEvenDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.rint.f32(float %x0)
    %2 = call float @llvm.rint.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> roundEven()  =>  llvm.rint.f32
define spir_func <3 x float> @_Z9roundEvenDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.rint.f32(float %x0)
    %2 = call float @llvm.rint.f32(float %x1)
    %3 = call float @llvm.rint.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> roundEven()  =>  llvm.rint.f32
define spir_func <4 x float> @_Z9roundEvenDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.rint.f32(float %x0)
    %2 = call float @llvm.rint.f32(float %x1)
    %3 = call float @llvm.rint.f32(float %x2)
    %4 = call float @llvm.rint.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.rint.f32(float) #0

; float ceil()  =>  llvm.ceil.f32
define spir_func float @_Z4ceilf(
    float %x) #0
{
    %1 = call float @llvm.ceil.f32(float %x)

    ret float %1
}

; <2 x float> ceil()  =>  llvm.ceil.f32
define spir_func <2 x float> @_Z4ceilDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.ceil.f32(float %x0)
    %2 = call float @llvm.ceil.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> ceil()  =>  llvm.ceil.f32
define spir_func <3 x float> @_Z4ceilDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.ceil.f32(float %x0)
    %2 = call float @llvm.ceil.f32(float %x1)
    %3 = call float @llvm.ceil.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> ceil()  =>  llvm.ceil.f32
define spir_func <4 x float> @_Z4ceilDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.ceil.f32(float %x0)
    %2 = call float @llvm.ceil.f32(float %x1)
    %3 = call float @llvm.ceil.f32(float %x2)
    %4 = call float @llvm.ceil.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.ceil.f32(float) #0

; float fract()  =>  llpc.fract.f32
define spir_func float @_Z5fractf(
    float %x) #0
{
    %1 = call float @llpc.fract.f32(float %x)

    ret float %1
}

; <2 x float> fract()  =>  llpc.fract.f32
define spir_func <2 x float> @_Z5fractDv2_f(
    <2 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fract.f32(float %x0)
    %2 = call float @llpc.fract.f32(float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> fract()  =>  llpc.fract.f32
define spir_func <3 x float> @_Z5fractDv3_f(
    <3 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fract.f32(float %x0)
    %2 = call float @llpc.fract.f32(float %x1)
    %3 = call float @llpc.fract.f32(float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> fract()  =>  llpc.fract.f32
define spir_func <4 x float> @_Z5fractDv4_f(
    <4 x float> %x) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fract.f32(float %x0)
    %2 = call float @llpc.fract.f32(float %x1)
    %3 = call float @llpc.fract.f32(float %x2)
    %4 = call float @llpc.fract.f32(float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.fract.f32(float) #0

; float fmod()  =>  llpc.mod.f32
define spir_func float @_Z4fmodff(
    float %x, float %y) #0
{
    %1 = call float @llpc.mod.f32(float %x, float %y)

    ret float %1
}

; <2 x float> fmod()  =>  llpc.mod.f32
define spir_func <2 x float> @_Z4fmodDv2_fDv2_f(
    <2 x float> %x, <2 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %y0 = extractelement <2 x float> %y, i32 0
    %y1 = extractelement <2 x float> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.mod.f32(float %x0, float %y0)
    %2 = call float @llpc.mod.f32(float %x1, float %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> fmod()  =>  llpc.mod.f32
define spir_func <3 x float> @_Z4fmodDv3_fDv3_f(
    <3 x float> %x, <3 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %y0 = extractelement <3 x float> %y, i32 0
    %y1 = extractelement <3 x float> %y, i32 1
    %y2 = extractelement <3 x float> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.mod.f32(float %x0, float %y0)
    %2 = call float @llpc.mod.f32(float %x1, float %y1)
    %3 = call float @llpc.mod.f32(float %x2, float %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> fmod()  =>  llpc.mod.f32
define spir_func <4 x float> @_Z4fmodDv4_fDv4_f(
    <4 x float> %x, <4 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %y0 = extractelement <4 x float> %y, i32 0
    %y1 = extractelement <4 x float> %y, i32 1
    %y2 = extractelement <4 x float> %y, i32 2
    %y3 = extractelement <4 x float> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.mod.f32(float %x0, float %y0)
    %2 = call float @llpc.mod.f32(float %x1, float %y1)
    %3 = call float @llpc.mod.f32(float %x2, float %y2)
    %4 = call float @llpc.mod.f32(float %x3, float %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.mod.f32(float, float) #0

; float fmin()  =>  llvm.minnum.f32
define spir_func float @_Z4fminff(
    float %x, float %y) #0
{
    %1 = call float @llvm.minnum.f32(float %x, float %y)

    ret float %1
}

; <2 x float> fmin()  =>  llvm.minnum.f32
define spir_func <2 x float> @_Z4fminDv2_fDv2_f(
    <2 x float> %x, <2 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %y0 = extractelement <2 x float> %y, i32 0
    %y1 = extractelement <2 x float> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.minnum.f32(float %x0, float %y0)
    %2 = call float @llvm.minnum.f32(float %x1, float %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> fmin()  =>  llvm.minnum.f32
define spir_func <3 x float> @_Z4fminDv3_fDv3_f(
    <3 x float> %x, <3 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %y0 = extractelement <3 x float> %y, i32 0
    %y1 = extractelement <3 x float> %y, i32 1
    %y2 = extractelement <3 x float> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.minnum.f32(float %x0, float %y0)
    %2 = call float @llvm.minnum.f32(float %x1, float %y1)
    %3 = call float @llvm.minnum.f32(float %x2, float %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> fmin()  =>  llvm.minnum.f32
define spir_func <4 x float> @_Z4fminDv4_fDv4_f(
    <4 x float> %x, <4 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %y0 = extractelement <4 x float> %y, i32 0
    %y1 = extractelement <4 x float> %y, i32 1
    %y2 = extractelement <4 x float> %y, i32 2
    %y3 = extractelement <4 x float> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.minnum.f32(float %x0, float %y0)
    %2 = call float @llvm.minnum.f32(float %x1, float %y1)
    %3 = call float @llvm.minnum.f32(float %x2, float %y2)
    %4 = call float @llvm.minnum.f32(float %x3, float %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.minnum.f32(float, float) #0

; float nmin()  =>  llpc.nmin.f32
define spir_func float @_Z4nminff(
    float %x, float %y) #0
{
    %1 = call float @llpc.nmin.f32(float %x, float %y)

    ret float %1
}

; <2 x float> nmin()  =>  llpc.nmin.f32
define spir_func <2 x float> @_Z4nminDv2_fDv2_f(
    <2 x float> %x, <2 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %y0 = extractelement <2 x float> %y, i32 0
    %y1 = extractelement <2 x float> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.nmin.f32(float %x0, float %y0)
    %2 = call float @llpc.nmin.f32(float %x1, float %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> nmin()  =>  llpc.nmin.f32
define spir_func <3 x float> @_Z4nminDv3_fDv3_f(
    <3 x float> %x, <3 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %y0 = extractelement <3 x float> %y, i32 0
    %y1 = extractelement <3 x float> %y, i32 1
    %y2 = extractelement <3 x float> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.nmin.f32(float %x0, float %y0)
    %2 = call float @llpc.nmin.f32(float %x1, float %y1)
    %3 = call float @llpc.nmin.f32(float %x2, float %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> nmin()  =>  llpc.nmin.f32
define spir_func <4 x float> @_Z4nminDv4_fDv4_f(
    <4 x float> %x, <4 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %y0 = extractelement <4 x float> %y, i32 0
    %y1 = extractelement <4 x float> %y, i32 1
    %y2 = extractelement <4 x float> %y, i32 2
    %y3 = extractelement <4 x float> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.nmin.f32(float %x0, float %y0)
    %2 = call float @llpc.nmin.f32(float %x1, float %y1)
    %3 = call float @llpc.nmin.f32(float %x2, float %y2)
    %4 = call float @llpc.nmin.f32(float %x3, float %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.nmin.f32(float, float) #0

; i32 smin()  =>  llpc.sminnum.i32
define spir_func i32 @_Z4sminii(
    i32 %x, i32 %y) #0
{
    %1 = call i32 @llpc.sminnum.i32(i32 %x, i32 %y)

    ret i32 %1
}

; <2 x i32> smin()  =>  llpc.sminnum.i32
define spir_func <2 x i32> @_Z4sminDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %y0 = extractelement <2 x i32> %y, i32 0
    %y1 = extractelement <2 x i32> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.sminnum.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.sminnum.i32(i32 %x1, i32 %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> smin()  =>  llpc.sminnum.i32
define spir_func <3 x i32> @_Z4sminDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %y0 = extractelement <3 x i32> %y, i32 0
    %y1 = extractelement <3 x i32> %y, i32 1
    %y2 = extractelement <3 x i32> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.sminnum.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.sminnum.i32(i32 %x1, i32 %y1)
    %3 = call i32 @llpc.sminnum.i32(i32 %x2, i32 %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> smin()  =>  llpc.sminnum.i32
define spir_func <4 x i32> @_Z4sminDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %y0 = extractelement <4 x i32> %y, i32 0
    %y1 = extractelement <4 x i32> %y, i32 1
    %y2 = extractelement <4 x i32> %y, i32 2
    %y3 = extractelement <4 x i32> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.sminnum.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.sminnum.i32(i32 %x1, i32 %y1)
    %3 = call i32 @llpc.sminnum.i32(i32 %x2, i32 %y2)
    %4 = call i32 @llpc.sminnum.i32(i32 %x3, i32 %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.sminnum.i32(i32, i32) #0

; i32 umin()  =>  llpc.uminnum.i32
define spir_func i32 @_Z4uminii(
    i32 %x, i32 %y) #0
{
    %1 = call i32 @llpc.uminnum.i32(i32 %x, i32 %y)

    ret i32 %1
}

; <2 x i32> umin()  =>  llpc.uminnum.i32
define spir_func <2 x i32> @_Z4uminDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %y0 = extractelement <2 x i32> %y, i32 0
    %y1 = extractelement <2 x i32> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.uminnum.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.uminnum.i32(i32 %x1, i32 %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> umin()  =>  llpc.uminnum.i32
define spir_func <3 x i32> @_Z4uminDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %y0 = extractelement <3 x i32> %y, i32 0
    %y1 = extractelement <3 x i32> %y, i32 1
    %y2 = extractelement <3 x i32> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.uminnum.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.uminnum.i32(i32 %x1, i32 %y1)
    %3 = call i32 @llpc.uminnum.i32(i32 %x2, i32 %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> umin()  =>  llpc.uminnum.i32
define spir_func <4 x i32> @_Z4uminDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %y0 = extractelement <4 x i32> %y, i32 0
    %y1 = extractelement <4 x i32> %y, i32 1
    %y2 = extractelement <4 x i32> %y, i32 2
    %y3 = extractelement <4 x i32> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.uminnum.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.uminnum.i32(i32 %x1, i32 %y1)
    %3 = call i32 @llpc.uminnum.i32(i32 %x2, i32 %y2)
    %4 = call i32 @llpc.uminnum.i32(i32 %x3, i32 %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.uminnum.i32(i32, i32) #0

; float fmax()  =>  llvm.maxnum.f32
define spir_func float @_Z4fmaxff(
    float %x, float %y) #0
{
    %1 = call float @llvm.maxnum.f32(float %x, float %y)

    ret float %1
}

; <2 x float> fmax()  =>  llvm.maxnum.f32
define spir_func <2 x float> @_Z4fmaxDv2_fDv2_f(
    <2 x float> %x, <2 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %y0 = extractelement <2 x float> %y, i32 0
    %y1 = extractelement <2 x float> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.maxnum.f32(float %x0, float %y0)
    %2 = call float @llvm.maxnum.f32(float %x1, float %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> fmax()  =>  llvm.maxnum.f32
define spir_func <3 x float> @_Z4fmaxDv3_fDv3_f(
    <3 x float> %x, <3 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %y0 = extractelement <3 x float> %y, i32 0
    %y1 = extractelement <3 x float> %y, i32 1
    %y2 = extractelement <3 x float> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.maxnum.f32(float %x0, float %y0)
    %2 = call float @llvm.maxnum.f32(float %x1, float %y1)
    %3 = call float @llvm.maxnum.f32(float %x2, float %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> fmax()  =>  llvm.maxnum.f32
define spir_func <4 x float> @_Z4fmaxDv4_fDv4_f(
    <4 x float> %x, <4 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %y0 = extractelement <4 x float> %y, i32 0
    %y1 = extractelement <4 x float> %y, i32 1
    %y2 = extractelement <4 x float> %y, i32 2
    %y3 = extractelement <4 x float> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.maxnum.f32(float %x0, float %y0)
    %2 = call float @llvm.maxnum.f32(float %x1, float %y1)
    %3 = call float @llvm.maxnum.f32(float %x2, float %y2)
    %4 = call float @llvm.maxnum.f32(float %x3, float %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.maxnum.f32(float, float) #0

; float nmax()  =>  llpc.nmax.f32
define spir_func float @_Z4nmaxff(
    float %x, float %y) #0
{
    %1 = call float @llpc.nmax.f32(float %x, float %y)

    ret float %1
}

; <2 x float> nmax()  =>  llpc.nmax.f32
define spir_func <2 x float> @_Z4nmaxDv2_fDv2_f(
    <2 x float> %x, <2 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %y0 = extractelement <2 x float> %y, i32 0
    %y1 = extractelement <2 x float> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.nmax.f32(float %x0, float %y0)
    %2 = call float @llpc.nmax.f32(float %x1, float %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> nmax()  =>  llpc.nmax.f32
define spir_func <3 x float> @_Z4nmaxDv3_fDv3_f(
    <3 x float> %x, <3 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %y0 = extractelement <3 x float> %y, i32 0
    %y1 = extractelement <3 x float> %y, i32 1
    %y2 = extractelement <3 x float> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.nmax.f32(float %x0, float %y0)
    %2 = call float @llpc.nmax.f32(float %x1, float %y1)
    %3 = call float @llpc.nmax.f32(float %x2, float %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> nmax()  =>  llpc.nmax.f32
define spir_func <4 x float> @_Z4nmaxDv4_fDv4_f(
    <4 x float> %x, <4 x float> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %y0 = extractelement <4 x float> %y, i32 0
    %y1 = extractelement <4 x float> %y, i32 1
    %y2 = extractelement <4 x float> %y, i32 2
    %y3 = extractelement <4 x float> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.nmax.f32(float %x0, float %y0)
    %2 = call float @llpc.nmax.f32(float %x1, float %y1)
    %3 = call float @llpc.nmax.f32(float %x2, float %y2)
    %4 = call float @llpc.nmax.f32(float %x3, float %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.nmax.f32(float, float) #0

; i32 smax()  =>  llpc.smaxnum.i32
define spir_func i32 @_Z4smaxii(
    i32 %x, i32 %y) #0
{
    %1 = call i32 @llpc.smaxnum.i32(i32 %x, i32 %y)

    ret i32 %1
}

; <2 x i32> smax()  =>  llpc.smaxnum.i32
define spir_func <2 x i32> @_Z4smaxDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %y0 = extractelement <2 x i32> %y, i32 0
    %y1 = extractelement <2 x i32> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.smaxnum.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.smaxnum.i32(i32 %x1, i32 %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> smax()  =>  llpc.smaxnum.i32
define spir_func <3 x i32> @_Z4smaxDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %y0 = extractelement <3 x i32> %y, i32 0
    %y1 = extractelement <3 x i32> %y, i32 1
    %y2 = extractelement <3 x i32> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.smaxnum.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.smaxnum.i32(i32 %x1, i32 %y1)
    %3 = call i32 @llpc.smaxnum.i32(i32 %x2, i32 %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> smax()  =>  llpc.smaxnum.i32
define spir_func <4 x i32> @_Z4smaxDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %y0 = extractelement <4 x i32> %y, i32 0
    %y1 = extractelement <4 x i32> %y, i32 1
    %y2 = extractelement <4 x i32> %y, i32 2
    %y3 = extractelement <4 x i32> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.smaxnum.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.smaxnum.i32(i32 %x1, i32 %y1)
    %3 = call i32 @llpc.smaxnum.i32(i32 %x2, i32 %y2)
    %4 = call i32 @llpc.smaxnum.i32(i32 %x3, i32 %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.smaxnum.i32(i32, i32) #0

; i32 umax()  =>  llpc.umaxnum.i32
define spir_func i32 @_Z4umaxii(
    i32 %x, i32 %y) #0
{
    %1 = call i32 @llpc.umaxnum.i32(i32 %x, i32 %y)

    ret i32 %1
}

; <2 x i32> umax()  =>  llpc.umaxnum.i32
define spir_func <2 x i32> @_Z4umaxDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %y0 = extractelement <2 x i32> %y, i32 0
    %y1 = extractelement <2 x i32> %y, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.umaxnum.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.umaxnum.i32(i32 %x1, i32 %y1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> umax()  =>  llpc.umaxnum.i32
define spir_func <3 x i32> @_Z4umaxDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %y0 = extractelement <3 x i32> %y, i32 0
    %y1 = extractelement <3 x i32> %y, i32 1
    %y2 = extractelement <3 x i32> %y, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.umaxnum.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.umaxnum.i32(i32 %x1, i32 %y1)
    %3 = call i32 @llpc.umaxnum.i32(i32 %x2, i32 %y2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> umax()  =>  llpc.umaxnum.i32
define spir_func <4 x i32> @_Z4umaxDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %y) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %y0 = extractelement <4 x i32> %y, i32 0
    %y1 = extractelement <4 x i32> %y, i32 1
    %y2 = extractelement <4 x i32> %y, i32 2
    %y3 = extractelement <4 x i32> %y, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.umaxnum.i32(i32 %x0, i32 %y0)
    %2 = call i32 @llpc.umaxnum.i32(i32 %x1, i32 %y1)
    %3 = call i32 @llpc.umaxnum.i32(i32 %x2, i32 %y2)
    %4 = call i32 @llpc.umaxnum.i32(i32 %x3, i32 %y3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.umaxnum.i32(i32, i32) #0

; float fclamp()  =>  llpc.fclamp.f32
define spir_func float @_Z6fclampfff(
    float %x, float %minVal, float %maxVal) #0
{
    %1 = call float @llpc.fclamp.f32(float %x, float %minVal, float %maxVal)

    ret float %1
}

; <2 x float> fclamp()  =>  llpc.fclamp.f32
define spir_func <2 x float> @_Z6fclampDv2_fDv2_fDv2_f(
    <2 x float> %x, <2 x float> %minVal, <2 x float> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %minVal0 = extractelement <2 x float> %minVal, i32 0
    %minVal1 = extractelement <2 x float> %minVal, i32 1

    %maxVal0 = extractelement <2 x float> %maxVal, i32 0
    %maxVal1 = extractelement <2 x float> %maxVal, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fclamp.f32(float %x0, float %minVal0, float %maxVal0)
    %2 = call float @llpc.fclamp.f32(float %x1, float %minVal1, float %maxVal1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> fclamp()  =>  llpc.fclamp.f32
define spir_func <3 x float> @_Z6fclampDv3_fDv3_fDv3_f(
    <3 x float> %x, <3 x float> %minVal, <3 x float> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %minVal0 = extractelement <3 x float> %minVal, i32 0
    %minVal1 = extractelement <3 x float> %minVal, i32 1
    %minVal2 = extractelement <3 x float> %minVal, i32 2

    %maxVal0 = extractelement <3 x float> %maxVal, i32 0
    %maxVal1 = extractelement <3 x float> %maxVal, i32 1
    %maxVal2 = extractelement <3 x float> %maxVal, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fclamp.f32(float %x0, float %minVal0, float %maxVal0)
    %2 = call float @llpc.fclamp.f32(float %x1, float %minVal1, float %maxVal1)
    %3 = call float @llpc.fclamp.f32(float %x2, float %minVal2, float %maxVal2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> fclamp()  =>  llpc.fclamp.f32
define spir_func <4 x float> @_Z6fclampDv4_fDv4_fDv4_f(
    <4 x float> %x, <4 x float> %minVal, <4 x float> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %minVal0 = extractelement <4 x float> %minVal, i32 0
    %minVal1 = extractelement <4 x float> %minVal, i32 1
    %minVal2 = extractelement <4 x float> %minVal, i32 2
    %minVal3 = extractelement <4 x float> %minVal, i32 3

    %maxVal0 = extractelement <4 x float> %maxVal, i32 0
    %maxVal1 = extractelement <4 x float> %maxVal, i32 1
    %maxVal2 = extractelement <4 x float> %maxVal, i32 2
    %maxVal3 = extractelement <4 x float> %maxVal, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fclamp.f32(float %x0, float %minVal0, float %maxVal0)
    %2 = call float @llpc.fclamp.f32(float %x1, float %minVal1, float %maxVal1)
    %3 = call float @llpc.fclamp.f32(float %x2, float %minVal2, float %maxVal2)
    %4 = call float @llpc.fclamp.f32(float %x3, float %minVal3, float %maxVal3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.fclamp.f32(float, float, float) #0

; float nclamp()  =>  llpc.nclamp.f32
define spir_func float @_Z6nclampfff(
    float %x, float %minVal, float %maxVal) #0
{
    %1 = call float @llpc.nclamp.f32(float %x, float %minVal, float %maxVal)

    ret float %1
}

; <2 x float> nclamp()  =>  llpc.nclamp.f32
define spir_func <2 x float> @_Z6nclampDv2_fDv2_fDv2_f(
    <2 x float> %x, <2 x float> %minVal, <2 x float> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %minVal0 = extractelement <2 x float> %minVal, i32 0
    %minVal1 = extractelement <2 x float> %minVal, i32 1

    %maxVal0 = extractelement <2 x float> %maxVal, i32 0
    %maxVal1 = extractelement <2 x float> %maxVal, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.nclamp.f32(float %x0, float %minVal0, float %maxVal0)
    %2 = call float @llpc.nclamp.f32(float %x1, float %minVal1, float %maxVal1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> nclamp()  =>  llpc.nclamp.f32
define spir_func <3 x float> @_Z6nclampDv3_fDv3_fDv3_f(
    <3 x float> %x, <3 x float> %minVal, <3 x float> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %minVal0 = extractelement <3 x float> %minVal, i32 0
    %minVal1 = extractelement <3 x float> %minVal, i32 1
    %minVal2 = extractelement <3 x float> %minVal, i32 2

    %maxVal0 = extractelement <3 x float> %maxVal, i32 0
    %maxVal1 = extractelement <3 x float> %maxVal, i32 1
    %maxVal2 = extractelement <3 x float> %maxVal, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.nclamp.f32(float %x0, float %minVal0, float %maxVal0)
    %2 = call float @llpc.nclamp.f32(float %x1, float %minVal1, float %maxVal1)
    %3 = call float @llpc.nclamp.f32(float %x2, float %minVal2, float %maxVal2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> nclamp()  =>  llpc.nclamp.f32
define spir_func <4 x float> @_Z6nclampDv4_fDv4_fDv4_f(
    <4 x float> %x, <4 x float> %minVal, <4 x float> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %minVal0 = extractelement <4 x float> %minVal, i32 0
    %minVal1 = extractelement <4 x float> %minVal, i32 1
    %minVal2 = extractelement <4 x float> %minVal, i32 2
    %minVal3 = extractelement <4 x float> %minVal, i32 3

    %maxVal0 = extractelement <4 x float> %maxVal, i32 0
    %maxVal1 = extractelement <4 x float> %maxVal, i32 1
    %maxVal2 = extractelement <4 x float> %maxVal, i32 2
    %maxVal3 = extractelement <4 x float> %maxVal, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.nclamp.f32(float %x0, float %minVal0, float %maxVal0)
    %2 = call float @llpc.nclamp.f32(float %x1, float %minVal1, float %maxVal1)
    %3 = call float @llpc.nclamp.f32(float %x2, float %minVal2, float %maxVal2)
    %4 = call float @llpc.nclamp.f32(float %x3, float %minVal3, float %maxVal3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.nclamp.f32(float, float, float) #0

; i32 sclamp()  =>  llpc.sclamp.i32
define spir_func i32 @_Z6sclampiii(
    i32 %x, i32 %minVal, i32 %maxVal) #0
{
    %1 = call i32 @llpc.sclamp.i32(i32 %x, i32 %minVal, i32 %maxVal)

    ret i32 %1
}

; <2 x i32> sclamp()  =>  llpc.sclamp.i32
define spir_func <2 x i32> @_Z6sclampDv2_iDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %minVal, <2 x i32> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %minVal0 = extractelement <2 x i32> %minVal, i32 0
    %minVal1 = extractelement <2 x i32> %minVal, i32 1

    %maxVal0 = extractelement <2 x i32> %maxVal, i32 0
    %maxVal1 = extractelement <2 x i32> %maxVal, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.sclamp.i32(i32 %x0, i32 %minVal0, i32 %maxVal0)
    %2 = call i32 @llpc.sclamp.i32(i32 %x1, i32 %minVal1, i32 %maxVal1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> sclamp()  =>  llpc.sclamp.i32
define spir_func <3 x i32> @_Z6sclampDv3_iDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %minVal, <3 x i32> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %minVal0 = extractelement <3 x i32> %minVal, i32 0
    %minVal1 = extractelement <3 x i32> %minVal, i32 1
    %minVal2 = extractelement <3 x i32> %minVal, i32 2

    %maxVal0 = extractelement <3 x i32> %maxVal, i32 0
    %maxVal1 = extractelement <3 x i32> %maxVal, i32 1
    %maxVal2 = extractelement <3 x i32> %maxVal, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.sclamp.i32(i32 %x0, i32 %minVal0, i32 %maxVal0)
    %2 = call i32 @llpc.sclamp.i32(i32 %x1, i32 %minVal1, i32 %maxVal1)
    %3 = call i32 @llpc.sclamp.i32(i32 %x2, i32 %minVal2, i32 %maxVal2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> sclamp()  =>  llpc.sclamp.i32
define spir_func <4 x i32> @_Z6sclampDv4_iDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %minVal, <4 x i32> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %minVal0 = extractelement <4 x i32> %minVal, i32 0
    %minVal1 = extractelement <4 x i32> %minVal, i32 1
    %minVal2 = extractelement <4 x i32> %minVal, i32 2
    %minVal3 = extractelement <4 x i32> %minVal, i32 3

    %maxVal0 = extractelement <4 x i32> %maxVal, i32 0
    %maxVal1 = extractelement <4 x i32> %maxVal, i32 1
    %maxVal2 = extractelement <4 x i32> %maxVal, i32 2
    %maxVal3 = extractelement <4 x i32> %maxVal, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.sclamp.i32(i32 %x0, i32 %minVal0, i32 %maxVal0)
    %2 = call i32 @llpc.sclamp.i32(i32 %x1, i32 %minVal1, i32 %maxVal1)
    %3 = call i32 @llpc.sclamp.i32(i32 %x2, i32 %minVal2, i32 %maxVal2)
    %4 = call i32 @llpc.sclamp.i32(i32 %x3, i32 %minVal3, i32 %maxVal3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.sclamp.i32(i32, i32, i32) #0

; i32 uclamp()  =>  llpc.uclamp.i32
define spir_func i32 @_Z6uclampiii(
    i32 %x, i32 %minVal, i32 %maxVal) #0
{
    %1 = call i32 @llpc.uclamp.i32(i32 %x, i32 %minVal, i32 %maxVal)

    ret i32 %1
}

; <2 x i32> uclamp()  =>  llpc.uclamp.i32
define spir_func <2 x i32> @_Z6uclampDv2_iDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %minVal, <2 x i32> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %minVal0 = extractelement <2 x i32> %minVal, i32 0
    %minVal1 = extractelement <2 x i32> %minVal, i32 1

    %maxVal0 = extractelement <2 x i32> %maxVal, i32 0
    %maxVal1 = extractelement <2 x i32> %maxVal, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.uclamp.i32(i32 %x0, i32 %minVal0, i32 %maxVal0)
    %2 = call i32 @llpc.uclamp.i32(i32 %x1, i32 %minVal1, i32 %maxVal1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> uclamp()  =>  llpc.uclamp.i32
define spir_func <3 x i32> @_Z6uclampDv3_iDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %minVal, <3 x i32> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %minVal0 = extractelement <3 x i32> %minVal, i32 0
    %minVal1 = extractelement <3 x i32> %minVal, i32 1
    %minVal2 = extractelement <3 x i32> %minVal, i32 2

    %maxVal0 = extractelement <3 x i32> %maxVal, i32 0
    %maxVal1 = extractelement <3 x i32> %maxVal, i32 1
    %maxVal2 = extractelement <3 x i32> %maxVal, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.uclamp.i32(i32 %x0, i32 %minVal0, i32 %maxVal0)
    %2 = call i32 @llpc.uclamp.i32(i32 %x1, i32 %minVal1, i32 %maxVal1)
    %3 = call i32 @llpc.uclamp.i32(i32 %x2, i32 %minVal2, i32 %maxVal2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> uclamp()  =>  llpc.uclamp.i32
define spir_func <4 x i32> @_Z6uclampDv4_iDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %minVal, <4 x i32> %maxVal) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %minVal0 = extractelement <4 x i32> %minVal, i32 0
    %minVal1 = extractelement <4 x i32> %minVal, i32 1
    %minVal2 = extractelement <4 x i32> %minVal, i32 2
    %minVal3 = extractelement <4 x i32> %minVal, i32 3

    %maxVal0 = extractelement <4 x i32> %maxVal, i32 0
    %maxVal1 = extractelement <4 x i32> %maxVal, i32 1
    %maxVal2 = extractelement <4 x i32> %maxVal, i32 2
    %maxVal3 = extractelement <4 x i32> %maxVal, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.uclamp.i32(i32 %x0, i32 %minVal0, i32 %maxVal0)
    %2 = call i32 @llpc.uclamp.i32(i32 %x1, i32 %minVal1, i32 %maxVal1)
    %3 = call i32 @llpc.uclamp.i32(i32 %x2, i32 %minVal2, i32 %maxVal2)
    %4 = call i32 @llpc.uclamp.i32(i32 %x3, i32 %minVal3, i32 %maxVal3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.uclamp.i32(i32, i32, i32) #0

; float fmix()  =>  llpc.fmix.f32
define spir_func float @_Z4fmixfff(
    float %x, float %y, float %a) #0
{
    %1 = call float @llpc.fmix.f32(float %x, float %y, float %a)

    ret float %1
}

; <2 x float> fmix()  =>  llpc.fmix.f32
define spir_func <2 x float> @_Z4fmixDv2_fDv2_fDv2_f(
    <2 x float> %x, <2 x float> %y, <2 x float> %a) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %y0 = extractelement <2 x float> %y, i32 0
    %y1 = extractelement <2 x float> %y, i32 1

    %a0 = extractelement <2 x float> %a, i32 0
    %a1 = extractelement <2 x float> %a, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fmix.f32(float %x0, float %y0, float %a0)
    %2 = call float @llpc.fmix.f32(float %x1, float %y1, float %a1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> fmix()  =>  llpc.fmix.f32
define spir_func <3 x float> @_Z4fmixDv3_fDv3_fDv3_f(
    <3 x float> %x, <3 x float> %y, <3 x float> %a) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %y0 = extractelement <3 x float> %y, i32 0
    %y1 = extractelement <3 x float> %y, i32 1
    %y2 = extractelement <3 x float> %y, i32 2

    %a0 = extractelement <3 x float> %a, i32 0
    %a1 = extractelement <3 x float> %a, i32 1
    %a2 = extractelement <3 x float> %a, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fmix.f32(float %x0, float %y0, float %a0)
    %2 = call float @llpc.fmix.f32(float %x1, float %y1, float %a1)
    %3 = call float @llpc.fmix.f32(float %x2, float %y2, float %a2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> fmix()  =>  llpc.fmix.f32
define spir_func <4 x float> @_Z4fmixDv4_fDv4_fDv4_f(
    <4 x float> %x, <4 x float> %y, <4 x float> %a) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %y0 = extractelement <4 x float> %y, i32 0
    %y1 = extractelement <4 x float> %y, i32 1
    %y2 = extractelement <4 x float> %y, i32 2
    %y3 = extractelement <4 x float> %y, i32 3

    %a0 = extractelement <4 x float> %a, i32 0
    %a1 = extractelement <4 x float> %a, i32 1
    %a2 = extractelement <4 x float> %a, i32 2
    %a3 = extractelement <4 x float> %a, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fmix.f32(float %x0, float %y0, float %a0)
    %2 = call float @llpc.fmix.f32(float %x1, float %y1, float %a1)
    %3 = call float @llpc.fmix.f32(float %x2, float %y2, float %a2)
    %4 = call float @llpc.fmix.f32(float %x3, float %y3, float %a3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.fmix.f32(float, float, float) #0

; float step()  =>  llpc.step.f32
define spir_func float @_Z4stepff(
    float %edge, float %x) #0
{
    %1 = call float @llpc.step.f32(float %edge, float %x)

    ret float %1
}

; <2 x float> step()  =>  llpc.step.f32
define spir_func <2 x float> @_Z4stepDv2_fDv2_f(
    <2 x float> %edge, <2 x float> %x) #0
{
    ; Extract components from source vectors
    %edge0 = extractelement <2 x float> %edge, i32 0
    %edge1 = extractelement <2 x float> %edge, i32 1

    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.step.f32(float %edge0, float %x0)
    %2 = call float @llpc.step.f32(float %edge1, float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> step()  =>  llpc.step.f32
define spir_func <3 x float> @_Z4stepDv3_fDv3_f(
    <3 x float> %edge, <3 x float> %x) #0
{
    ; Extract components from source vectors
    %edge0 = extractelement <3 x float> %edge, i32 0
    %edge1 = extractelement <3 x float> %edge, i32 1
    %edge2 = extractelement <3 x float> %edge, i32 2

    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.step.f32(float %edge0, float %x0)
    %2 = call float @llpc.step.f32(float %edge1, float %x1)
    %3 = call float @llpc.step.f32(float %edge2, float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> step()  =>  llpc.step.f32
define spir_func <4 x float> @_Z4stepDv4_fDv4_f(
    <4 x float> %edge, <4 x float> %x) #0
{
    ; Extract components from source vectors
    %edge0 = extractelement <4 x float> %edge, i32 0
    %edge1 = extractelement <4 x float> %edge, i32 1
    %edge2 = extractelement <4 x float> %edge, i32 2
    %edge3 = extractelement <4 x float> %edge, i32 3

    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.step.f32(float %edge0, float %x0)
    %2 = call float @llpc.step.f32(float %edge1, float %x1)
    %3 = call float @llpc.step.f32(float %edge2, float %x2)
    %4 = call float @llpc.step.f32(float %edge3, float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.step.f32(float, float) #0

; float smoothStep()  =>  llpc.smoothStep.f32
define spir_func float @_Z10smoothStepfff(
    float %edge0, float %edge1, float %x) #0
{
    %1 = call float @llpc.smoothStep.f32(float %edge0, float %edge1, float %x)

    ret float %1
}

; <2 x float> smoothStep()  =>  llpc.smoothStep.f32
define spir_func <2 x float> @_Z10smoothStepDv2_fDv2_fDv2_f(
    <2 x float> %edge0, <2 x float> %edge1, <2 x float> %x) #0
{
    ; Extract components from source vectors
    %edge00 = extractelement <2 x float> %edge0, i32 0
    %edge01 = extractelement <2 x float> %edge0, i32 1

    %edge10 = extractelement <2 x float> %edge1, i32 0
    %edge11 = extractelement <2 x float> %edge1, i32 1

    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.smoothStep.f32(float %edge00, float %edge10, float %x0)
    %2 = call float @llpc.smoothStep.f32(float %edge01, float %edge11, float %x1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> smoothStep()  =>  llpc.smoothStep.f32
define spir_func <3 x float> @_Z10smoothStepDv3_fDv3_fDv3_f(
    <3 x float> %edge0, <3 x float> %edge1, <3 x float> %x) #0
{
    ; Extract components from source vectors
    %edge00 = extractelement <3 x float> %edge0, i32 0
    %edge01 = extractelement <3 x float> %edge0, i32 1
    %edge02 = extractelement <3 x float> %edge0, i32 2

    %edge10 = extractelement <3 x float> %edge1, i32 0
    %edge11 = extractelement <3 x float> %edge1, i32 1
    %edge12 = extractelement <3 x float> %edge1, i32 2

    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.smoothStep.f32(float %edge00, float %edge10, float %x0)
    %2 = call float @llpc.smoothStep.f32(float %edge01, float %edge11, float %x1)
    %3 = call float @llpc.smoothStep.f32(float %edge02, float %edge12, float %x2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> smoothStep()  =>  llpc.smoothStep.f32
define spir_func <4 x float> @_Z10smoothStepDv4_fDv4_fDv4_f(
    <4 x float> %edge0, <4 x float> %edge1, <4 x float> %x) #0
{
    ; Extract components from source vectors
    %edge00 = extractelement <4 x float> %edge0, i32 0
    %edge01 = extractelement <4 x float> %edge0, i32 1
    %edge02 = extractelement <4 x float> %edge0, i32 2
    %edge03 = extractelement <4 x float> %edge0, i32 3

    %edge10 = extractelement <4 x float> %edge1, i32 0
    %edge11 = extractelement <4 x float> %edge1, i32 1
    %edge12 = extractelement <4 x float> %edge1, i32 2
    %edge13 = extractelement <4 x float> %edge1, i32 3

    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.smoothStep.f32(float %edge00, float %edge10, float %x0)
    %2 = call float @llpc.smoothStep.f32(float %edge01, float %edge11, float %x1)
    %3 = call float @llpc.smoothStep.f32(float %edge02, float %edge12, float %x2)
    %4 = call float @llpc.smoothStep.f32(float %edge03, float %edge13, float %x3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.smoothStep.f32(float, float, float) #0

; i1 isinf()  =>  llpc.isinf.f32
define spir_func i1 @_Z5isinff(
    float %value) #0
{
    %1 = call i1 @llpc.isinf.f32(float %value)

    ret i1 %1
}

; <2 x i1> isinf()  =>  llpc.isinf.f32
define spir_func <2 x i1> @_Z5isinfDv2_f(
    <2 x float> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x float> %value, i32 0
    %value1 = extractelement <2 x float> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isinf.f32(float %value0)
    %2 = call i1 @llpc.isinf.f32(float %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i1>
    %4 = load <2 x i1>, <2 x i1>* %3
    %5 = insertelement <2 x i1> %4, i1 %1, i32 0
    %6 = insertelement <2 x i1> %5, i1 %2, i32 1

    ret <2 x i1> %6
}

; <3 x i1> isinf()  =>  llpc.isinf.f32
define spir_func <3 x i1> @_Z5isinfDv3_f(
    <3 x float> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x float> %value, i32 0
    %value1 = extractelement <3 x float> %value, i32 1
    %value2 = extractelement <3 x float> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isinf.f32(float %value0)
    %2 = call i1 @llpc.isinf.f32(float %value1)
    %3 = call i1 @llpc.isinf.f32(float %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i1>
    %5 = load <3 x i1>, <3 x i1>* %4
    %6 = insertelement <3 x i1> %5, i1 %1, i32 0
    %7 = insertelement <3 x i1> %6, i1 %2, i32 1
    %8 = insertelement <3 x i1> %7, i1 %3, i32 2

    ret <3 x i1> %8
}

; <4 x i1> isinf()  =>  llpc.isinf.f32
define spir_func <4 x i1> @_Z5isinfDv4_f(
    <4 x float> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x float> %value, i32 0
    %value1 = extractelement <4 x float> %value, i32 1
    %value2 = extractelement <4 x float> %value, i32 2
    %value3 = extractelement <4 x float> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isinf.f32(float %value0)
    %2 = call i1 @llpc.isinf.f32(float %value1)
    %3 = call i1 @llpc.isinf.f32(float %value2)
    %4 = call i1 @llpc.isinf.f32(float %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i1>
    %6 = load <4 x i1>, <4 x i1>* %5
    %7 = insertelement <4 x i1> %6, i1 %1, i32 0
    %8 = insertelement <4 x i1> %7, i1 %2, i32 1
    %9 = insertelement <4 x i1> %8, i1 %3, i32 2
    %10 = insertelement <4 x i1> %9, i1 %4, i32 3

    ret <4 x i1> %10
}

declare i1 @llpc.isinf.f32(float) #0

; i1 isnan()  =>  llpc.isnan.f32
define spir_func i1 @_Z5isnanf(
    float %value) #0
{
    %1 = call i1 @llpc.isnan.f32(float %value)

    ret i1 %1
}

; <2 x i1> isnan()  =>  llpc.isnan.f32
define spir_func <2 x i1> @_Z5isnanDv2_f(
    <2 x float> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x float> %value, i32 0
    %value1 = extractelement <2 x float> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isnan.f32(float %value0)
    %2 = call i1 @llpc.isnan.f32(float %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i1>
    %4 = load <2 x i1>, <2 x i1>* %3
    %5 = insertelement <2 x i1> %4, i1 %1, i32 0
    %6 = insertelement <2 x i1> %5, i1 %2, i32 1

    ret <2 x i1> %6
}

; <3 x i1> isnan()  =>  llpc.isnan.f32
define spir_func <3 x i1> @_Z5isnanDv3_f(
    <3 x float> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x float> %value, i32 0
    %value1 = extractelement <3 x float> %value, i32 1
    %value2 = extractelement <3 x float> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isnan.f32(float %value0)
    %2 = call i1 @llpc.isnan.f32(float %value1)
    %3 = call i1 @llpc.isnan.f32(float %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i1>
    %5 = load <3 x i1>, <3 x i1>* %4
    %6 = insertelement <3 x i1> %5, i1 %1, i32 0
    %7 = insertelement <3 x i1> %6, i1 %2, i32 1
    %8 = insertelement <3 x i1> %7, i1 %3, i32 2

    ret <3 x i1> %8
}

; <4 x i1> isnan()  =>  llpc.isnan.f32
define spir_func <4 x i1> @_Z5isnanDv4_f(
    <4 x float> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x float> %value, i32 0
    %value1 = extractelement <4 x float> %value, i32 1
    %value2 = extractelement <4 x float> %value, i32 2
    %value3 = extractelement <4 x float> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i1 @llpc.isnan.f32(float %value0)
    %2 = call i1 @llpc.isnan.f32(float %value1)
    %3 = call i1 @llpc.isnan.f32(float %value2)
    %4 = call i1 @llpc.isnan.f32(float %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i1>
    %6 = load <4 x i1>, <4 x i1>* %5
    %7 = insertelement <4 x i1> %6, i1 %1, i32 0
    %8 = insertelement <4 x i1> %7, i1 %2, i32 1
    %9 = insertelement <4 x i1> %8, i1 %3, i32 2
    %10 = insertelement <4 x i1> %9, i1 %4, i32 3

    ret <4 x i1> %10
}

declare i1 @llpc.isnan.f32(float) #0

; float fma()  =>  llpc.fma.f32
define spir_func float @_Z3fmafff(
    float %a, float %b, float %c) #0
{
    %1 = call float @llpc.fma.f32(float %a, float %b, float %c)

    ret float %1
}

; <2 x float> fma()  =>  llpc.fma.f32
define spir_func <2 x float> @_Z3fmaDv2_fDv2_fDv2_f(
    <2 x float> %a, <2 x float> %b, <2 x float> %c) #0
{
    ; Extract components from source vectors
    %a0 = extractelement <2 x float> %a, i32 0
    %a1 = extractelement <2 x float> %a, i32 1

    %b0 = extractelement <2 x float> %b, i32 0
    %b1 = extractelement <2 x float> %b, i32 1

    %c0 = extractelement <2 x float> %c, i32 0
    %c1 = extractelement <2 x float> %c, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fma.f32(float %a0, float %b0, float %c0)
    %2 = call float @llpc.fma.f32(float %a1, float %b1, float %c1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> fma()  =>  llpc.fma.f32
define spir_func <3 x float> @_Z3fmaDv3_fDv3_fDv3_f(
    <3 x float> %a, <3 x float> %b, <3 x float> %c) #0
{
    ; Extract components from source vectors
    %a0 = extractelement <3 x float> %a, i32 0
    %a1 = extractelement <3 x float> %a, i32 1
    %a2 = extractelement <3 x float> %a, i32 2

    %b0 = extractelement <3 x float> %b, i32 0
    %b1 = extractelement <3 x float> %b, i32 1
    %b2 = extractelement <3 x float> %b, i32 2

    %c0 = extractelement <3 x float> %c, i32 0
    %c1 = extractelement <3 x float> %c, i32 1
    %c2 = extractelement <3 x float> %c, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fma.f32(float %a0, float %b0, float %c0)
    %2 = call float @llpc.fma.f32(float %a1, float %b1, float %c1)
    %3 = call float @llpc.fma.f32(float %a2, float %b2, float %c2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> fma()  =>  llpc.fma.f32
define spir_func <4 x float> @_Z3fmaDv4_fDv4_fDv4_f(
    <4 x float> %a, <4 x float> %b, <4 x float> %c) #0
{
    ; Extract components from source vectors
    %a0 = extractelement <4 x float> %a, i32 0
    %a1 = extractelement <4 x float> %a, i32 1
    %a2 = extractelement <4 x float> %a, i32 2
    %a3 = extractelement <4 x float> %a, i32 3

    %b0 = extractelement <4 x float> %b, i32 0
    %b1 = extractelement <4 x float> %b, i32 1
    %b2 = extractelement <4 x float> %b, i32 2
    %b3 = extractelement <4 x float> %b, i32 3

    %c0 = extractelement <4 x float> %c, i32 0
    %c1 = extractelement <4 x float> %c, i32 1
    %c2 = extractelement <4 x float> %c, i32 2
    %c3 = extractelement <4 x float> %c, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fma.f32(float %a0, float %b0, float %c0)
    %2 = call float @llpc.fma.f32(float %a1, float %b1, float %c1)
    %3 = call float @llpc.fma.f32(float %a2, float %b2, float %c2)
    %4 = call float @llpc.fma.f32(float %a3, float %b3, float %c3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.fma.f32(float, float, float) #0

; float ldexp()  =>  llvm.amdgcn.ldexp.f32
define spir_func float @_Z5ldexpfi(
    float %x, i32 %exp) #0
{
    %1 = call float @llvm.amdgcn.ldexp.f32(float %x, i32 %exp)

    ret float %1
}

; <2 x float> ldexp()  =>  llvm.amdgcn.ldexp.f32
define spir_func <2 x float> @_Z5ldexpDv2_fDv2_i(
    <2 x float> %x, <2 x i32> %exp) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %exp0 = extractelement <2 x i32> %exp, i32 0
    %exp1 = extractelement <2 x i32> %exp, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.amdgcn.ldexp.f32(float %x0, i32 %exp0)
    %2 = call float @llvm.amdgcn.ldexp.f32(float %x1, i32 %exp1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> ldexp()  =>  llvm.amdgcn.ldexp.f32
define spir_func <3 x float> @_Z5ldexpDv3_fDv3_i(
    <3 x float> %x, <3 x i32> %exp) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %exp0 = extractelement <3 x i32> %exp, i32 0
    %exp1 = extractelement <3 x i32> %exp, i32 1
    %exp2 = extractelement <3 x i32> %exp, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.amdgcn.ldexp.f32(float %x0, i32 %exp0)
    %2 = call float @llvm.amdgcn.ldexp.f32(float %x1, i32 %exp1)
    %3 = call float @llvm.amdgcn.ldexp.f32(float %x2, i32 %exp2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> ldexp()  =>  llvm.amdgcn.ldexp.f32
define spir_func <4 x float> @_Z5ldexpDv4_fDv4_i(
    <4 x float> %x, <4 x i32> %exp) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %exp0 = extractelement <4 x i32> %exp, i32 0
    %exp1 = extractelement <4 x i32> %exp, i32 1
    %exp2 = extractelement <4 x i32> %exp, i32 2
    %exp3 = extractelement <4 x i32> %exp, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llvm.amdgcn.ldexp.f32(float %x0, i32 %exp0)
    %2 = call float @llvm.amdgcn.ldexp.f32(float %x1, i32 %exp1)
    %3 = call float @llvm.amdgcn.ldexp.f32(float %x2, i32 %exp2)
    %4 = call float @llvm.amdgcn.ldexp.f32(float %x3, i32 %exp3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llvm.amdgcn.ldexp.f32(float, i32) #1

; =====================================================================================================================
; >>>  Integer Functions
; =====================================================================================================================

; i32 BitFieldSExtract()  =>  llpc.bitFieldSExtract.i32
define spir_func i32 @_Z16BitFieldSExtractiii(
    i32 %value, i32 %offset, i32 %bits) #0
{
    %1 = call i32 @llpc.bitFieldSExtract.i32(i32 %value, i32 %offset, i32 %bits)

    ret i32 %1
}

; <2 x i32> BitFieldSExtract()  =>  llpc.bitFieldSExtract.i32
define spir_func <2 x i32> @_Z16BitFieldSExtractDv2_iii(
    <2 x i32> %value, i32 %offset, i32 %bits) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x i32> %value, i32 0
    %value1 = extractelement <2 x i32> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.bitFieldSExtract.i32(i32 %value0, i32 %offset, i32 %bits)
    %2 = call i32 @llpc.bitFieldSExtract.i32(i32 %value1, i32 %offset, i32 %bits)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> BitFieldSExtract()  =>  llpc.bitFieldSExtract.i32
define spir_func <3 x i32> @_Z16BitFieldSExtractDv3_iii(
    <3 x i32> %value, i32 %offset, i32 %bits) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x i32> %value, i32 0
    %value1 = extractelement <3 x i32> %value, i32 1
    %value2 = extractelement <3 x i32> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.bitFieldSExtract.i32(i32 %value0, i32 %offset, i32 %bits)
    %2 = call i32 @llpc.bitFieldSExtract.i32(i32 %value1, i32 %offset, i32 %bits)
    %3 = call i32 @llpc.bitFieldSExtract.i32(i32 %value2, i32 %offset, i32 %bits)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> BitFieldSExtract()  =>  llpc.bitFieldSExtract.i32
define spir_func <4 x i32> @_Z16BitFieldSExtractDv4_iii(
    <4 x i32> %value, i32 %offset, i32 %bits) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x i32> %value, i32 0
    %value1 = extractelement <4 x i32> %value, i32 1
    %value2 = extractelement <4 x i32> %value, i32 2
    %value3 = extractelement <4 x i32> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.bitFieldSExtract.i32(i32 %value0, i32 %offset, i32 %bits)
    %2 = call i32 @llpc.bitFieldSExtract.i32(i32 %value1, i32 %offset, i32 %bits)
    %3 = call i32 @llpc.bitFieldSExtract.i32(i32 %value2, i32 %offset, i32 %bits)
    %4 = call i32 @llpc.bitFieldSExtract.i32(i32 %value3, i32 %offset, i32 %bits)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.bitFieldSExtract.i32(i32, i32, i32) #0

; i32 BitFieldUExtract()  =>  llpc.bitFieldUExtract.i32
define spir_func i32 @_Z16BitFieldUExtractiii(
    i32 %value, i32 %offset, i32 %bits) #0
{
    %1 = call i32 @llpc.bitFieldUExtract.i32(i32 %value, i32 %offset, i32 %bits)

    ret i32 %1
}

; <2 x i32> BitFieldUExtract()  =>  llpc.bitFieldUExtract.i32
define spir_func <2 x i32> @_Z16BitFieldUExtractDv2_iii(
    <2 x i32> %value, i32 %offset, i32 %bits) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x i32> %value, i32 0
    %value1 = extractelement <2 x i32> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.bitFieldUExtract.i32(i32 %value0, i32 %offset, i32 %bits)
    %2 = call i32 @llpc.bitFieldUExtract.i32(i32 %value1, i32 %offset, i32 %bits)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> BitFieldUExtract()  =>  llpc.bitFieldUExtract.i32
define spir_func <3 x i32> @_Z16BitFieldUExtractDv3_iii(
    <3 x i32> %value, i32 %offset, i32 %bits) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x i32> %value, i32 0
    %value1 = extractelement <3 x i32> %value, i32 1
    %value2 = extractelement <3 x i32> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.bitFieldUExtract.i32(i32 %value0, i32 %offset, i32 %bits)
    %2 = call i32 @llpc.bitFieldUExtract.i32(i32 %value1, i32 %offset, i32 %bits)
    %3 = call i32 @llpc.bitFieldUExtract.i32(i32 %value2, i32 %offset, i32 %bits)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> BitFieldUExtract()  =>  llpc.bitFieldUExtract.i32
define spir_func <4 x i32> @_Z16BitFieldUExtractDv4_iii(
    <4 x i32> %value, i32 %offset, i32 %bits) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x i32> %value, i32 0
    %value1 = extractelement <4 x i32> %value, i32 1
    %value2 = extractelement <4 x i32> %value, i32 2
    %value3 = extractelement <4 x i32> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.bitFieldUExtract.i32(i32 %value0, i32 %offset, i32 %bits)
    %2 = call i32 @llpc.bitFieldUExtract.i32(i32 %value1, i32 %offset, i32 %bits)
    %3 = call i32 @llpc.bitFieldUExtract.i32(i32 %value2, i32 %offset, i32 %bits)
    %4 = call i32 @llpc.bitFieldUExtract.i32(i32 %value3, i32 %offset, i32 %bits)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.bitFieldUExtract.i32(i32, i32, i32) #0

; i32 BitFieldInsert()  =>  llpc.bitFieldInsert.i32
define spir_func i32 @_Z14BitFieldInsertiiii(
    i32 %base, i32 %insert, i32 %offset, i32 %bits) #0
{
    %1 = call i32 @llpc.bitFieldInsert.i32(i32 %base, i32 %insert, i32 %offset, i32 %bits)

    ret i32 %1
}

; <2 x i32> BitFieldInsert()  =>  llpc.bitFieldInsert.i32
define spir_func <2 x i32> @_Z14BitFieldInsertDv2_iDv2_iii(
    <2 x i32> %base, <2 x i32> %insert, i32 %offset, i32 %bits) #0
{
    ; Extract components from source vectors
    %base0 = extractelement <2 x i32> %base, i32 0
    %base1 = extractelement <2 x i32> %base, i32 1

    %insert0 = extractelement <2 x i32> %insert, i32 0
    %insert1 = extractelement <2 x i32> %insert, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.bitFieldInsert.i32(i32 %base0, i32 %insert0, i32 %offset, i32 %bits)
    %2 = call i32 @llpc.bitFieldInsert.i32(i32 %base1, i32 %insert1, i32 %offset, i32 %bits)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> BitFieldInsert()  =>  llpc.bitFieldInsert.i32
define spir_func <3 x i32> @_Z14BitFieldInsertDv3_iDv3_iii(
    <3 x i32> %base, <3 x i32> %insert, i32 %offset, i32 %bits) #0
{
    ; Extract components from source vectors
    %base0 = extractelement <3 x i32> %base, i32 0
    %base1 = extractelement <3 x i32> %base, i32 1
    %base2 = extractelement <3 x i32> %base, i32 2

    %insert0 = extractelement <3 x i32> %insert, i32 0
    %insert1 = extractelement <3 x i32> %insert, i32 1
    %insert2 = extractelement <3 x i32> %insert, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.bitFieldInsert.i32(i32 %base0, i32 %insert0, i32 %offset, i32 %bits)
    %2 = call i32 @llpc.bitFieldInsert.i32(i32 %base1, i32 %insert1, i32 %offset, i32 %bits)
    %3 = call i32 @llpc.bitFieldInsert.i32(i32 %base2, i32 %insert2, i32 %offset, i32 %bits)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> BitFieldInsert()  =>  llpc.bitFieldInsert.i32
define spir_func <4 x i32> @_Z14BitFieldInsertDv4_iDv4_iii(
    <4 x i32> %base, <4 x i32> %insert, i32 %offset, i32 %bits) #0
{
    ; Extract components from source vectors
    %base0 = extractelement <4 x i32> %base, i32 0
    %base1 = extractelement <4 x i32> %base, i32 1
    %base2 = extractelement <4 x i32> %base, i32 2
    %base3 = extractelement <4 x i32> %base, i32 3

    %insert0 = extractelement <4 x i32> %insert, i32 0
    %insert1 = extractelement <4 x i32> %insert, i32 1
    %insert2 = extractelement <4 x i32> %insert, i32 2
    %insert3 = extractelement <4 x i32> %insert, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.bitFieldInsert.i32(i32 %base0, i32 %insert0, i32 %offset, i32 %bits)
    %2 = call i32 @llpc.bitFieldInsert.i32(i32 %base1, i32 %insert1, i32 %offset, i32 %bits)
    %3 = call i32 @llpc.bitFieldInsert.i32(i32 %base2, i32 %insert2, i32 %offset, i32 %bits)
    %4 = call i32 @llpc.bitFieldInsert.i32(i32 %base3, i32 %insert3, i32 %offset, i32 %bits)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.bitFieldInsert.i32(i32, i32, i32, i32) #0

; i32 BitReverse()  =>  llvm.bitreverse.i32
define spir_func i32 @_Z10BitReversei(
    i32 %value) #0
{
    %1 = call i32 @llvm.bitreverse.i32(i32 %value)

    ret i32 %1
}

; <2 x i32> BitReverse()  =>  llvm.bitreverse.i32
define spir_func <2 x i32> @_Z10BitReverseDv2_i(
    <2 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x i32> %value, i32 0
    %value1 = extractelement <2 x i32> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llvm.bitreverse.i32(i32 %value0)
    %2 = call i32 @llvm.bitreverse.i32(i32 %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> BitReverse()  =>  llvm.bitreverse.i32
define spir_func <3 x i32> @_Z10BitReverseDv3_i(
    <3 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x i32> %value, i32 0
    %value1 = extractelement <3 x i32> %value, i32 1
    %value2 = extractelement <3 x i32> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llvm.bitreverse.i32(i32 %value0)
    %2 = call i32 @llvm.bitreverse.i32(i32 %value1)
    %3 = call i32 @llvm.bitreverse.i32(i32 %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> BitReverse()  =>  llvm.bitreverse.i32
define spir_func <4 x i32> @_Z10BitReverseDv4_i(
    <4 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x i32> %value, i32 0
    %value1 = extractelement <4 x i32> %value, i32 1
    %value2 = extractelement <4 x i32> %value, i32 2
    %value3 = extractelement <4 x i32> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llvm.bitreverse.i32(i32 %value0)
    %2 = call i32 @llvm.bitreverse.i32(i32 %value1)
    %3 = call i32 @llvm.bitreverse.i32(i32 %value2)
    %4 = call i32 @llvm.bitreverse.i32(i32 %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llvm.bitreverse.i32(i32) #0

; i32 BitCount()  =>  llvm.ctpop.i32
define spir_func i32 @_Z8BitCounti(
    i32 %value) #0
{
    %1 = call i32 @llvm.ctpop.i32(i32 %value)

    ret i32 %1
}

; <2 x i32> BitCount()  =>  llvm.ctpop.i32
define spir_func <2 x i32> @_Z8BitCountDv2_i(
    <2 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x i32> %value, i32 0
    %value1 = extractelement <2 x i32> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llvm.ctpop.i32(i32 %value0)
    %2 = call i32 @llvm.ctpop.i32(i32 %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> BitCount()  =>  llvm.ctpop.i32
define spir_func <3 x i32> @_Z8BitCountDv3_i(
    <3 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x i32> %value, i32 0
    %value1 = extractelement <3 x i32> %value, i32 1
    %value2 = extractelement <3 x i32> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llvm.ctpop.i32(i32 %value0)
    %2 = call i32 @llvm.ctpop.i32(i32 %value1)
    %3 = call i32 @llvm.ctpop.i32(i32 %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> BitCount()  =>  llvm.ctpop.i32
define spir_func <4 x i32> @_Z8BitCountDv4_i(
    <4 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x i32> %value, i32 0
    %value1 = extractelement <4 x i32> %value, i32 1
    %value2 = extractelement <4 x i32> %value, i32 2
    %value3 = extractelement <4 x i32> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llvm.ctpop.i32(i32 %value0)
    %2 = call i32 @llvm.ctpop.i32(i32 %value1)
    %3 = call i32 @llvm.ctpop.i32(i32 %value2)
    %4 = call i32 @llvm.ctpop.i32(i32 %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llvm.ctpop.i32(i32) #0

; i32 findILsb()  =>  llpc.findIlsb.i32
define spir_func i32 @_Z8findILsbi(
    i32 %value) #0
{
    %1 = call i32 @llpc.findIlsb.i32(i32 %value)

    ret i32 %1
}

; <2 x i32> findILsb()  =>  llpc.findIlsb.i32
define spir_func <2 x i32> @_Z8findILsbDv2_i(
    <2 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x i32> %value, i32 0
    %value1 = extractelement <2 x i32> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findIlsb.i32(i32 %value0)
    %2 = call i32 @llpc.findIlsb.i32(i32 %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> findILsb()  =>  llpc.findIlsb.i32
define spir_func <3 x i32> @_Z8findILsbDv3_i(
    <3 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x i32> %value, i32 0
    %value1 = extractelement <3 x i32> %value, i32 1
    %value2 = extractelement <3 x i32> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findIlsb.i32(i32 %value0)
    %2 = call i32 @llpc.findIlsb.i32(i32 %value1)
    %3 = call i32 @llpc.findIlsb.i32(i32 %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> findILsb()  =>  llpc.findIlsb.i32
define spir_func <4 x i32> @_Z8findILsbDv4_i(
    <4 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x i32> %value, i32 0
    %value1 = extractelement <4 x i32> %value, i32 1
    %value2 = extractelement <4 x i32> %value, i32 2
    %value3 = extractelement <4 x i32> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findIlsb.i32(i32 %value0)
    %2 = call i32 @llpc.findIlsb.i32(i32 %value1)
    %3 = call i32 @llpc.findIlsb.i32(i32 %value2)
    %4 = call i32 @llpc.findIlsb.i32(i32 %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.findIlsb.i32(i32) #0

; i32 findUMsb()  =>  llpc.findUMsb.i32
define spir_func i32 @_Z8findUMsbi(
    i32 %value) #0
{
    %1 = call i32 @llpc.findUMsb.i32(i32 %value)

    ret i32 %1
}

; <2 x i32> findUMsb()  =>  llpc.findUMsb.i32
define spir_func <2 x i32> @_Z8findUMsbDv2_i(
    <2 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x i32> %value, i32 0
    %value1 = extractelement <2 x i32> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findUMsb.i32(i32 %value0)
    %2 = call i32 @llpc.findUMsb.i32(i32 %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> findUMsb()  =>  llpc.findUMsb.i32
define spir_func <3 x i32> @_Z8findUMsbDv3_i(
    <3 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x i32> %value, i32 0
    %value1 = extractelement <3 x i32> %value, i32 1
    %value2 = extractelement <3 x i32> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findUMsb.i32(i32 %value0)
    %2 = call i32 @llpc.findUMsb.i32(i32 %value1)
    %3 = call i32 @llpc.findUMsb.i32(i32 %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> findUMsb()  =>  llpc.findUMsb.i32
define spir_func <4 x i32> @_Z8findUMsbDv4_i(
    <4 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x i32> %value, i32 0
    %value1 = extractelement <4 x i32> %value, i32 1
    %value2 = extractelement <4 x i32> %value, i32 2
    %value3 = extractelement <4 x i32> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findUMsb.i32(i32 %value0)
    %2 = call i32 @llpc.findUMsb.i32(i32 %value1)
    %3 = call i32 @llpc.findUMsb.i32(i32 %value2)
    %4 = call i32 @llpc.findUMsb.i32(i32 %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.findUMsb.i32(i32) #0

; i32 findSMsb()  =>  llpc.findSMsb.i32
define spir_func i32 @_Z8findSMsbi(
    i32 %value) #0
{
    %1 = call i32 @llpc.findSMsb.i32(i32 %value)

    ret i32 %1
}

; <2 x i32> findSMsb()  =>  llpc.findSMsb.i32
define spir_func <2 x i32> @_Z8findSMsbDv2_i(
    <2 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <2 x i32> %value, i32 0
    %value1 = extractelement <2 x i32> %value, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findSMsb.i32(i32 %value0)
    %2 = call i32 @llpc.findSMsb.i32(i32 %value1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> findSMsb()  =>  llpc.findSMsb.i32
define spir_func <3 x i32> @_Z8findSMsbDv3_i(
    <3 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <3 x i32> %value, i32 0
    %value1 = extractelement <3 x i32> %value, i32 1
    %value2 = extractelement <3 x i32> %value, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findSMsb.i32(i32 %value0)
    %2 = call i32 @llpc.findSMsb.i32(i32 %value1)
    %3 = call i32 @llpc.findSMsb.i32(i32 %value2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> findSMsb()  =>  llpc.findSMsb.i32
define spir_func <4 x i32> @_Z8findSMsbDv4_i(
    <4 x i32> %value) #0
{
    ; Extract components from source vectors
    %value0 = extractelement <4 x i32> %value, i32 0
    %value1 = extractelement <4 x i32> %value, i32 1
    %value2 = extractelement <4 x i32> %value, i32 2
    %value3 = extractelement <4 x i32> %value, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.findSMsb.i32(i32 %value0)
    %2 = call i32 @llpc.findSMsb.i32(i32 %value1)
    %3 = call i32 @llpc.findSMsb.i32(i32 %value2)
    %4 = call i32 @llpc.findSMsb.i32(i32 %value3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.findSMsb.i32(i32) #0

; =====================================================================================================================
; >>>  Derivative Functions
; =====================================================================================================================

; float DPdx()  =>  llpc.dpdx.f32
define spir_func float @_Z4DPdxf(
    float %p) #0
{
    %1 = call float @llpc.dpdx.f32(float %p)

    ret float %1
}

; <2 x float> DPdx()  =>  llpc.dpdx.f32
define spir_func <2 x float> @_Z4DPdxDv2_f(
    <2 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x float> %p, i32 0
    %p1 = extractelement <2 x float> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdx.f32(float %p0)
    %2 = call float @llpc.dpdx.f32(float %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> DPdx()  =>  llpc.dpdx.f32
define spir_func <3 x float> @_Z4DPdxDv3_f(
    <3 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x float> %p, i32 0
    %p1 = extractelement <3 x float> %p, i32 1
    %p2 = extractelement <3 x float> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdx.f32(float %p0)
    %2 = call float @llpc.dpdx.f32(float %p1)
    %3 = call float @llpc.dpdx.f32(float %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> DPdx()  =>  llpc.dpdx.f32
define spir_func <4 x float> @_Z4DPdxDv4_f(
    <4 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x float> %p, i32 0
    %p1 = extractelement <4 x float> %p, i32 1
    %p2 = extractelement <4 x float> %p, i32 2
    %p3 = extractelement <4 x float> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdx.f32(float %p0)
    %2 = call float @llpc.dpdx.f32(float %p1)
    %3 = call float @llpc.dpdx.f32(float %p2)
    %4 = call float @llpc.dpdx.f32(float %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.dpdx.f32(float) #0

; float DPdy()  =>  llpc.dpdy.f32
define spir_func float @_Z4DPdyf(
    float %p) #0
{
    %1 = call float @llpc.dpdy.f32(float %p)

    ret float %1
}

; <2 x float> DPdy()  =>  llpc.dpdy.f32
define spir_func <2 x float> @_Z4DPdyDv2_f(
    <2 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x float> %p, i32 0
    %p1 = extractelement <2 x float> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdy.f32(float %p0)
    %2 = call float @llpc.dpdy.f32(float %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> DPdy()  =>  llpc.dpdy.f32
define spir_func <3 x float> @_Z4DPdyDv3_f(
    <3 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x float> %p, i32 0
    %p1 = extractelement <3 x float> %p, i32 1
    %p2 = extractelement <3 x float> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdy.f32(float %p0)
    %2 = call float @llpc.dpdy.f32(float %p1)
    %3 = call float @llpc.dpdy.f32(float %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> DPdy()  =>  llpc.dpdy.f32
define spir_func <4 x float> @_Z4DPdyDv4_f(
    <4 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x float> %p, i32 0
    %p1 = extractelement <4 x float> %p, i32 1
    %p2 = extractelement <4 x float> %p, i32 2
    %p3 = extractelement <4 x float> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdy.f32(float %p0)
    %2 = call float @llpc.dpdy.f32(float %p1)
    %3 = call float @llpc.dpdy.f32(float %p2)
    %4 = call float @llpc.dpdy.f32(float %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.dpdy.f32(float) #0

; float Fwidth()  =>  llpc.fwidth.f32
define spir_func float @_Z6Fwidthf(
    float %p) #0
{
    %1 = call float @llpc.fwidth.f32(float %p)

    ret float %1
}

; <2 x float> Fwidth()  =>  llpc.fwidth.f32
define spir_func <2 x float> @_Z6FwidthDv2_f(
    <2 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x float> %p, i32 0
    %p1 = extractelement <2 x float> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fwidth.f32(float %p0)
    %2 = call float @llpc.fwidth.f32(float %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> Fwidth()  =>  llpc.fwidth.f32
define spir_func <3 x float> @_Z6FwidthDv3_f(
    <3 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x float> %p, i32 0
    %p1 = extractelement <3 x float> %p, i32 1
    %p2 = extractelement <3 x float> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fwidth.f32(float %p0)
    %2 = call float @llpc.fwidth.f32(float %p1)
    %3 = call float @llpc.fwidth.f32(float %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> Fwidth()  =>  llpc.fwidth.f32
define spir_func <4 x float> @_Z6FwidthDv4_f(
    <4 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x float> %p, i32 0
    %p1 = extractelement <4 x float> %p, i32 1
    %p2 = extractelement <4 x float> %p, i32 2
    %p3 = extractelement <4 x float> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fwidth.f32(float %p0)
    %2 = call float @llpc.fwidth.f32(float %p1)
    %3 = call float @llpc.fwidth.f32(float %p2)
    %4 = call float @llpc.fwidth.f32(float %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.fwidth.f32(float) #0

; float DPdxFine()  =>  llpc.dpdxFine.f32
define spir_func float @_Z8DPdxFinef(
    float %p) #0
{
    %1 = call float @llpc.dpdxFine.f32(float %p)

    ret float %1
}

; <2 x float> DPdxFine()  =>  llpc.dpdxFine.f32
define spir_func <2 x float> @_Z8DPdxFineDv2_f(
    <2 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x float> %p, i32 0
    %p1 = extractelement <2 x float> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdxFine.f32(float %p0)
    %2 = call float @llpc.dpdxFine.f32(float %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> DPdxFine()  =>  llpc.dpdxFine.f32
define spir_func <3 x float> @_Z8DPdxFineDv3_f(
    <3 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x float> %p, i32 0
    %p1 = extractelement <3 x float> %p, i32 1
    %p2 = extractelement <3 x float> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdxFine.f32(float %p0)
    %2 = call float @llpc.dpdxFine.f32(float %p1)
    %3 = call float @llpc.dpdxFine.f32(float %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> DPdxFine()  =>  llpc.dpdxFine.f32
define spir_func <4 x float> @_Z8DPdxFineDv4_f(
    <4 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x float> %p, i32 0
    %p1 = extractelement <4 x float> %p, i32 1
    %p2 = extractelement <4 x float> %p, i32 2
    %p3 = extractelement <4 x float> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdxFine.f32(float %p0)
    %2 = call float @llpc.dpdxFine.f32(float %p1)
    %3 = call float @llpc.dpdxFine.f32(float %p2)
    %4 = call float @llpc.dpdxFine.f32(float %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.dpdxFine.f32(float) #0

; float DPdyFine()  =>  llpc.dpdyFine.f32
define spir_func float @_Z8DPdyFinef(
    float %p) #0
{
    %1 = call float @llpc.dpdyFine.f32(float %p)

    ret float %1
}

; <2 x float> DPdyFine()  =>  llpc.dpdyFine.f32
define spir_func <2 x float> @_Z8DPdyFineDv2_f(
    <2 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x float> %p, i32 0
    %p1 = extractelement <2 x float> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdyFine.f32(float %p0)
    %2 = call float @llpc.dpdyFine.f32(float %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> DPdyFine()  =>  llpc.dpdyFine.f32
define spir_func <3 x float> @_Z8DPdyFineDv3_f(
    <3 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x float> %p, i32 0
    %p1 = extractelement <3 x float> %p, i32 1
    %p2 = extractelement <3 x float> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdyFine.f32(float %p0)
    %2 = call float @llpc.dpdyFine.f32(float %p1)
    %3 = call float @llpc.dpdyFine.f32(float %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> DPdyFine()  =>  llpc.dpdyFine.f32
define spir_func <4 x float> @_Z8DPdyFineDv4_f(
    <4 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x float> %p, i32 0
    %p1 = extractelement <4 x float> %p, i32 1
    %p2 = extractelement <4 x float> %p, i32 2
    %p3 = extractelement <4 x float> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdyFine.f32(float %p0)
    %2 = call float @llpc.dpdyFine.f32(float %p1)
    %3 = call float @llpc.dpdyFine.f32(float %p2)
    %4 = call float @llpc.dpdyFine.f32(float %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.dpdyFine.f32(float) #0

; float FwidthFine()  =>  llpc.fwidthFine.f32
define spir_func float @_Z10FwidthFinef(
    float %p) #0
{
    %1 = call float @llpc.fwidthFine.f32(float %p)

    ret float %1
}

; <2 x float> FwidthFine()  =>  llpc.fwidthFine.f32
define spir_func <2 x float> @_Z10FwidthFineDv2_f(
    <2 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x float> %p, i32 0
    %p1 = extractelement <2 x float> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fwidthFine.f32(float %p0)
    %2 = call float @llpc.fwidthFine.f32(float %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> FwidthFine()  =>  llpc.fwidthFine.f32
define spir_func <3 x float> @_Z10FwidthFineDv3_f(
    <3 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x float> %p, i32 0
    %p1 = extractelement <3 x float> %p, i32 1
    %p2 = extractelement <3 x float> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fwidthFine.f32(float %p0)
    %2 = call float @llpc.fwidthFine.f32(float %p1)
    %3 = call float @llpc.fwidthFine.f32(float %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> FwidthFine()  =>  llpc.fwidthFine.f32
define spir_func <4 x float> @_Z10FwidthFineDv4_f(
    <4 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x float> %p, i32 0
    %p1 = extractelement <4 x float> %p, i32 1
    %p2 = extractelement <4 x float> %p, i32 2
    %p3 = extractelement <4 x float> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fwidthFine.f32(float %p0)
    %2 = call float @llpc.fwidthFine.f32(float %p1)
    %3 = call float @llpc.fwidthFine.f32(float %p2)
    %4 = call float @llpc.fwidthFine.f32(float %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.fwidthFine.f32(float) #0

; float DPdxCoarse()  =>  llpc.dpdxCoarse.f32
define spir_func float @_Z10DPdxCoarsef(
    float %p) #0
{
    %1 = call float @llpc.dpdxCoarse.f32(float %p)

    ret float %1
}

; <2 x float> DPdxCoarse()  =>  llpc.dpdxCoarse.f32
define spir_func <2 x float> @_Z10DPdxCoarseDv2_f(
    <2 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x float> %p, i32 0
    %p1 = extractelement <2 x float> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdxCoarse.f32(float %p0)
    %2 = call float @llpc.dpdxCoarse.f32(float %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> DPdxCoarse()  =>  llpc.dpdxCoarse.f32
define spir_func <3 x float> @_Z10DPdxCoarseDv3_f(
    <3 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x float> %p, i32 0
    %p1 = extractelement <3 x float> %p, i32 1
    %p2 = extractelement <3 x float> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdxCoarse.f32(float %p0)
    %2 = call float @llpc.dpdxCoarse.f32(float %p1)
    %3 = call float @llpc.dpdxCoarse.f32(float %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> DPdxCoarse()  =>  llpc.dpdxCoarse.f32
define spir_func <4 x float> @_Z10DPdxCoarseDv4_f(
    <4 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x float> %p, i32 0
    %p1 = extractelement <4 x float> %p, i32 1
    %p2 = extractelement <4 x float> %p, i32 2
    %p3 = extractelement <4 x float> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdxCoarse.f32(float %p0)
    %2 = call float @llpc.dpdxCoarse.f32(float %p1)
    %3 = call float @llpc.dpdxCoarse.f32(float %p2)
    %4 = call float @llpc.dpdxCoarse.f32(float %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.dpdxCoarse.f32(float) #0

; float DPdyCoarse()  =>  llpc.dpdyCoarse.f32
define spir_func float @_Z10DPdyCoarsef(
    float %p) #0
{
    %1 = call float @llpc.dpdyCoarse.f32(float %p)

    ret float %1
}

; <2 x float> DPdyCoarse()  =>  llpc.dpdyCoarse.f32
define spir_func <2 x float> @_Z10DPdyCoarseDv2_f(
    <2 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x float> %p, i32 0
    %p1 = extractelement <2 x float> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdyCoarse.f32(float %p0)
    %2 = call float @llpc.dpdyCoarse.f32(float %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> DPdyCoarse()  =>  llpc.dpdyCoarse.f32
define spir_func <3 x float> @_Z10DPdyCoarseDv3_f(
    <3 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x float> %p, i32 0
    %p1 = extractelement <3 x float> %p, i32 1
    %p2 = extractelement <3 x float> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdyCoarse.f32(float %p0)
    %2 = call float @llpc.dpdyCoarse.f32(float %p1)
    %3 = call float @llpc.dpdyCoarse.f32(float %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> DPdyCoarse()  =>  llpc.dpdyCoarse.f32
define spir_func <4 x float> @_Z10DPdyCoarseDv4_f(
    <4 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x float> %p, i32 0
    %p1 = extractelement <4 x float> %p, i32 1
    %p2 = extractelement <4 x float> %p, i32 2
    %p3 = extractelement <4 x float> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.dpdyCoarse.f32(float %p0)
    %2 = call float @llpc.dpdyCoarse.f32(float %p1)
    %3 = call float @llpc.dpdyCoarse.f32(float %p2)
    %4 = call float @llpc.dpdyCoarse.f32(float %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.dpdyCoarse.f32(float) #0

; float FwidthCoarse()  =>  llpc.fwidthCoarse.f32
define spir_func float @_Z12FwidthCoarsef(
    float %p) #0
{
    %1 = call float @llpc.fwidthCoarse.f32(float %p)

    ret float %1
}

; <2 x float> FwidthCoarse()  =>  llpc.fwidthCoarse.f32
define spir_func <2 x float> @_Z12FwidthCoarseDv2_f(
    <2 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <2 x float> %p, i32 0
    %p1 = extractelement <2 x float> %p, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fwidthCoarse.f32(float %p0)
    %2 = call float @llpc.fwidthCoarse.f32(float %p1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> FwidthCoarse()  =>  llpc.fwidthCoarse.f32
define spir_func <3 x float> @_Z12FwidthCoarseDv3_f(
    <3 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <3 x float> %p, i32 0
    %p1 = extractelement <3 x float> %p, i32 1
    %p2 = extractelement <3 x float> %p, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fwidthCoarse.f32(float %p0)
    %2 = call float @llpc.fwidthCoarse.f32(float %p1)
    %3 = call float @llpc.fwidthCoarse.f32(float %p2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> FwidthCoarse()  =>  llpc.fwidthCoarse.f32
define spir_func <4 x float> @_Z12FwidthCoarseDv4_f(
    <4 x float> %p) #0
{
    ; Extract components from source vectors
    %p0 = extractelement <4 x float> %p, i32 0
    %p1 = extractelement <4 x float> %p, i32 1
    %p2 = extractelement <4 x float> %p, i32 2
    %p3 = extractelement <4 x float> %p, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fwidthCoarse.f32(float %p0)
    %2 = call float @llpc.fwidthCoarse.f32(float %p1)
    %3 = call float @llpc.fwidthCoarse.f32(float %p2)
    %4 = call float @llpc.fwidthCoarse.f32(float %p3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.fwidthCoarse.f32(float) #0

; =====================================================================================================================
; >>>  Functions of extension AMD_shader_trinary_minmax
; =====================================================================================================================

; i32 SMin3AMD()  =>  llpc.smin3.i32
define spir_func i32 @_Z8SMin3AMDiii(
    i32 %x, i32 %y, i32 %z) #0
{
    %1 = call i32 @llpc.smin3.i32(i32 %x, i32 %y, i32 %z)

    ret i32 %1
}

; <2 x i32> SMin3AMD()  =>  llpc.smin3.i32
define spir_func <2 x i32> @_Z8SMin3AMDDv2_iDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %y, <2 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %y0 = extractelement <2 x i32> %y, i32 0
    %y1 = extractelement <2 x i32> %y, i32 1

    %z0 = extractelement <2 x i32> %z, i32 0
    %z1 = extractelement <2 x i32> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.smin3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.smin3.i32(i32 %x1, i32 %y1, i32 %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> SMin3AMD()  =>  llpc.smin3.i32
define spir_func <3 x i32> @_Z8SMin3AMDDv3_iDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %y, <3 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %y0 = extractelement <3 x i32> %y, i32 0
    %y1 = extractelement <3 x i32> %y, i32 1
    %y2 = extractelement <3 x i32> %y, i32 2

    %z0 = extractelement <3 x i32> %z, i32 0
    %z1 = extractelement <3 x i32> %z, i32 1
    %z2 = extractelement <3 x i32> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.smin3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.smin3.i32(i32 %x1, i32 %y1, i32 %z1)
    %3 = call i32 @llpc.smin3.i32(i32 %x2, i32 %y2, i32 %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> SMin3AMD()  =>  llpc.smin3.i32
define spir_func <4 x i32> @_Z8SMin3AMDDv4_iDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %y, <4 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %y0 = extractelement <4 x i32> %y, i32 0
    %y1 = extractelement <4 x i32> %y, i32 1
    %y2 = extractelement <4 x i32> %y, i32 2
    %y3 = extractelement <4 x i32> %y, i32 3

    %z0 = extractelement <4 x i32> %z, i32 0
    %z1 = extractelement <4 x i32> %z, i32 1
    %z2 = extractelement <4 x i32> %z, i32 2
    %z3 = extractelement <4 x i32> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.smin3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.smin3.i32(i32 %x1, i32 %y1, i32 %z1)
    %3 = call i32 @llpc.smin3.i32(i32 %x2, i32 %y2, i32 %z2)
    %4 = call i32 @llpc.smin3.i32(i32 %x3, i32 %y3, i32 %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.smin3.i32(i32, i32, i32) #0

; i32 SMax3AMD()  =>  llpc.smax3.i32
define spir_func i32 @_Z8SMax3AMDiii(
    i32 %x, i32 %y, i32 %z) #0
{
    %1 = call i32 @llpc.smax3.i32(i32 %x, i32 %y, i32 %z)

    ret i32 %1
}

; <2 x i32> SMax3AMD()  =>  llpc.smax3.i32
define spir_func <2 x i32> @_Z8SMax3AMDDv2_iDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %y, <2 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %y0 = extractelement <2 x i32> %y, i32 0
    %y1 = extractelement <2 x i32> %y, i32 1

    %z0 = extractelement <2 x i32> %z, i32 0
    %z1 = extractelement <2 x i32> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.smax3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.smax3.i32(i32 %x1, i32 %y1, i32 %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> SMax3AMD()  =>  llpc.smax3.i32
define spir_func <3 x i32> @_Z8SMax3AMDDv3_iDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %y, <3 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %y0 = extractelement <3 x i32> %y, i32 0
    %y1 = extractelement <3 x i32> %y, i32 1
    %y2 = extractelement <3 x i32> %y, i32 2

    %z0 = extractelement <3 x i32> %z, i32 0
    %z1 = extractelement <3 x i32> %z, i32 1
    %z2 = extractelement <3 x i32> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.smax3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.smax3.i32(i32 %x1, i32 %y1, i32 %z1)
    %3 = call i32 @llpc.smax3.i32(i32 %x2, i32 %y2, i32 %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> SMax3AMD()  =>  llpc.smax3.i32
define spir_func <4 x i32> @_Z8SMax3AMDDv4_iDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %y, <4 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %y0 = extractelement <4 x i32> %y, i32 0
    %y1 = extractelement <4 x i32> %y, i32 1
    %y2 = extractelement <4 x i32> %y, i32 2
    %y3 = extractelement <4 x i32> %y, i32 3

    %z0 = extractelement <4 x i32> %z, i32 0
    %z1 = extractelement <4 x i32> %z, i32 1
    %z2 = extractelement <4 x i32> %z, i32 2
    %z3 = extractelement <4 x i32> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.smax3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.smax3.i32(i32 %x1, i32 %y1, i32 %z1)
    %3 = call i32 @llpc.smax3.i32(i32 %x2, i32 %y2, i32 %z2)
    %4 = call i32 @llpc.smax3.i32(i32 %x3, i32 %y3, i32 %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.smax3.i32(i32, i32, i32) #0

; i32 SMid3AMD()  =>  llpc.smid3.i32
define spir_func i32 @_Z8SMid3AMDiii(
    i32 %x, i32 %y, i32 %z) #0
{
    %1 = call i32 @llpc.smid3.i32(i32 %x, i32 %y, i32 %z)

    ret i32 %1
}

; <2 x i32> SMid3AMD()  =>  llpc.smid3.i32
define spir_func <2 x i32> @_Z8SMid3AMDDv2_iDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %y, <2 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %y0 = extractelement <2 x i32> %y, i32 0
    %y1 = extractelement <2 x i32> %y, i32 1

    %z0 = extractelement <2 x i32> %z, i32 0
    %z1 = extractelement <2 x i32> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.smid3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.smid3.i32(i32 %x1, i32 %y1, i32 %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> SMid3AMD()  =>  llpc.smid3.i32
define spir_func <3 x i32> @_Z8SMid3AMDDv3_iDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %y, <3 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %y0 = extractelement <3 x i32> %y, i32 0
    %y1 = extractelement <3 x i32> %y, i32 1
    %y2 = extractelement <3 x i32> %y, i32 2

    %z0 = extractelement <3 x i32> %z, i32 0
    %z1 = extractelement <3 x i32> %z, i32 1
    %z2 = extractelement <3 x i32> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.smid3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.smid3.i32(i32 %x1, i32 %y1, i32 %z1)
    %3 = call i32 @llpc.smid3.i32(i32 %x2, i32 %y2, i32 %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> SMid3AMD()  =>  llpc.smid3.i32
define spir_func <4 x i32> @_Z8SMid3AMDDv4_iDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %y, <4 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %y0 = extractelement <4 x i32> %y, i32 0
    %y1 = extractelement <4 x i32> %y, i32 1
    %y2 = extractelement <4 x i32> %y, i32 2
    %y3 = extractelement <4 x i32> %y, i32 3

    %z0 = extractelement <4 x i32> %z, i32 0
    %z1 = extractelement <4 x i32> %z, i32 1
    %z2 = extractelement <4 x i32> %z, i32 2
    %z3 = extractelement <4 x i32> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.smid3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.smid3.i32(i32 %x1, i32 %y1, i32 %z1)
    %3 = call i32 @llpc.smid3.i32(i32 %x2, i32 %y2, i32 %z2)
    %4 = call i32 @llpc.smid3.i32(i32 %x3, i32 %y3, i32 %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.smid3.i32(i32, i32, i32) #0

; i32 UMin3AMD()  =>  llpc.umin3.i32
define spir_func i32 @_Z8UMin3AMDiii(
    i32 %x, i32 %y, i32 %z) #0
{
    %1 = call i32 @llpc.umin3.i32(i32 %x, i32 %y, i32 %z)

    ret i32 %1
}

; <2 x i32> UMin3AMD()  =>  llpc.umin3.i32
define spir_func <2 x i32> @_Z8UMin3AMDDv2_iDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %y, <2 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %y0 = extractelement <2 x i32> %y, i32 0
    %y1 = extractelement <2 x i32> %y, i32 1

    %z0 = extractelement <2 x i32> %z, i32 0
    %z1 = extractelement <2 x i32> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.umin3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.umin3.i32(i32 %x1, i32 %y1, i32 %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> UMin3AMD()  =>  llpc.umin3.i32
define spir_func <3 x i32> @_Z8UMin3AMDDv3_iDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %y, <3 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %y0 = extractelement <3 x i32> %y, i32 0
    %y1 = extractelement <3 x i32> %y, i32 1
    %y2 = extractelement <3 x i32> %y, i32 2

    %z0 = extractelement <3 x i32> %z, i32 0
    %z1 = extractelement <3 x i32> %z, i32 1
    %z2 = extractelement <3 x i32> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.umin3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.umin3.i32(i32 %x1, i32 %y1, i32 %z1)
    %3 = call i32 @llpc.umin3.i32(i32 %x2, i32 %y2, i32 %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> UMin3AMD()  =>  llpc.umin3.i32
define spir_func <4 x i32> @_Z8UMin3AMDDv4_iDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %y, <4 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %y0 = extractelement <4 x i32> %y, i32 0
    %y1 = extractelement <4 x i32> %y, i32 1
    %y2 = extractelement <4 x i32> %y, i32 2
    %y3 = extractelement <4 x i32> %y, i32 3

    %z0 = extractelement <4 x i32> %z, i32 0
    %z1 = extractelement <4 x i32> %z, i32 1
    %z2 = extractelement <4 x i32> %z, i32 2
    %z3 = extractelement <4 x i32> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.umin3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.umin3.i32(i32 %x1, i32 %y1, i32 %z1)
    %3 = call i32 @llpc.umin3.i32(i32 %x2, i32 %y2, i32 %z2)
    %4 = call i32 @llpc.umin3.i32(i32 %x3, i32 %y3, i32 %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.umin3.i32(i32, i32, i32) #0

; i32 UMax3AMD()  =>  llpc.umax3.i32
define spir_func i32 @_Z8UMax3AMDiii(
    i32 %x, i32 %y, i32 %z) #0
{
    %1 = call i32 @llpc.umax3.i32(i32 %x, i32 %y, i32 %z)

    ret i32 %1
}

; <2 x i32> UMax3AMD()  =>  llpc.umax3.i32
define spir_func <2 x i32> @_Z8UMax3AMDDv2_iDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %y, <2 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %y0 = extractelement <2 x i32> %y, i32 0
    %y1 = extractelement <2 x i32> %y, i32 1

    %z0 = extractelement <2 x i32> %z, i32 0
    %z1 = extractelement <2 x i32> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.umax3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.umax3.i32(i32 %x1, i32 %y1, i32 %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> UMax3AMD()  =>  llpc.umax3.i32
define spir_func <3 x i32> @_Z8UMax3AMDDv3_iDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %y, <3 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %y0 = extractelement <3 x i32> %y, i32 0
    %y1 = extractelement <3 x i32> %y, i32 1
    %y2 = extractelement <3 x i32> %y, i32 2

    %z0 = extractelement <3 x i32> %z, i32 0
    %z1 = extractelement <3 x i32> %z, i32 1
    %z2 = extractelement <3 x i32> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.umax3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.umax3.i32(i32 %x1, i32 %y1, i32 %z1)
    %3 = call i32 @llpc.umax3.i32(i32 %x2, i32 %y2, i32 %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> UMax3AMD()  =>  llpc.umax3.i32
define spir_func <4 x i32> @_Z8UMax3AMDDv4_iDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %y, <4 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %y0 = extractelement <4 x i32> %y, i32 0
    %y1 = extractelement <4 x i32> %y, i32 1
    %y2 = extractelement <4 x i32> %y, i32 2
    %y3 = extractelement <4 x i32> %y, i32 3

    %z0 = extractelement <4 x i32> %z, i32 0
    %z1 = extractelement <4 x i32> %z, i32 1
    %z2 = extractelement <4 x i32> %z, i32 2
    %z3 = extractelement <4 x i32> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.umax3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.umax3.i32(i32 %x1, i32 %y1, i32 %z1)
    %3 = call i32 @llpc.umax3.i32(i32 %x2, i32 %y2, i32 %z2)
    %4 = call i32 @llpc.umax3.i32(i32 %x3, i32 %y3, i32 %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.umax3.i32(i32, i32, i32) #0

; i32 UMid3AMD()  =>  llpc.umid3.i32
define spir_func i32 @_Z8UMid3AMDiii(
    i32 %x, i32 %y, i32 %z) #0
{
    %1 = call i32 @llpc.umid3.i32(i32 %x, i32 %y, i32 %z)

    ret i32 %1
}

; <2 x i32> UMid3AMD()  =>  llpc.umid3.i32
define spir_func <2 x i32> @_Z8UMid3AMDDv2_iDv2_iDv2_i(
    <2 x i32> %x, <2 x i32> %y, <2 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x i32> %x, i32 0
    %x1 = extractelement <2 x i32> %x, i32 1

    %y0 = extractelement <2 x i32> %y, i32 0
    %y1 = extractelement <2 x i32> %y, i32 1

    %z0 = extractelement <2 x i32> %z, i32 0
    %z1 = extractelement <2 x i32> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.umid3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.umid3.i32(i32 %x1, i32 %y1, i32 %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x i32>
    %4 = load <2 x i32>, <2 x i32>* %3
    %5 = insertelement <2 x i32> %4, i32 %1, i32 0
    %6 = insertelement <2 x i32> %5, i32 %2, i32 1

    ret <2 x i32> %6
}

; <3 x i32> UMid3AMD()  =>  llpc.umid3.i32
define spir_func <3 x i32> @_Z8UMid3AMDDv3_iDv3_iDv3_i(
    <3 x i32> %x, <3 x i32> %y, <3 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x i32> %x, i32 0
    %x1 = extractelement <3 x i32> %x, i32 1
    %x2 = extractelement <3 x i32> %x, i32 2

    %y0 = extractelement <3 x i32> %y, i32 0
    %y1 = extractelement <3 x i32> %y, i32 1
    %y2 = extractelement <3 x i32> %y, i32 2

    %z0 = extractelement <3 x i32> %z, i32 0
    %z1 = extractelement <3 x i32> %z, i32 1
    %z2 = extractelement <3 x i32> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.umid3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.umid3.i32(i32 %x1, i32 %y1, i32 %z1)
    %3 = call i32 @llpc.umid3.i32(i32 %x2, i32 %y2, i32 %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x i32>
    %5 = load <3 x i32>, <3 x i32>* %4
    %6 = insertelement <3 x i32> %5, i32 %1, i32 0
    %7 = insertelement <3 x i32> %6, i32 %2, i32 1
    %8 = insertelement <3 x i32> %7, i32 %3, i32 2

    ret <3 x i32> %8
}

; <4 x i32> UMid3AMD()  =>  llpc.umid3.i32
define spir_func <4 x i32> @_Z8UMid3AMDDv4_iDv4_iDv4_i(
    <4 x i32> %x, <4 x i32> %y, <4 x i32> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x i32> %x, i32 0
    %x1 = extractelement <4 x i32> %x, i32 1
    %x2 = extractelement <4 x i32> %x, i32 2
    %x3 = extractelement <4 x i32> %x, i32 3

    %y0 = extractelement <4 x i32> %y, i32 0
    %y1 = extractelement <4 x i32> %y, i32 1
    %y2 = extractelement <4 x i32> %y, i32 2
    %y3 = extractelement <4 x i32> %y, i32 3

    %z0 = extractelement <4 x i32> %z, i32 0
    %z1 = extractelement <4 x i32> %z, i32 1
    %z2 = extractelement <4 x i32> %z, i32 2
    %z3 = extractelement <4 x i32> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call i32 @llpc.umid3.i32(i32 %x0, i32 %y0, i32 %z0)
    %2 = call i32 @llpc.umid3.i32(i32 %x1, i32 %y1, i32 %z1)
    %3 = call i32 @llpc.umid3.i32(i32 %x2, i32 %y2, i32 %z2)
    %4 = call i32 @llpc.umid3.i32(i32 %x3, i32 %y3, i32 %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x i32>
    %6 = load <4 x i32>, <4 x i32>* %5
    %7 = insertelement <4 x i32> %6, i32 %1, i32 0
    %8 = insertelement <4 x i32> %7, i32 %2, i32 1
    %9 = insertelement <4 x i32> %8, i32 %3, i32 2
    %10 = insertelement <4 x i32> %9, i32 %4, i32 3

    ret <4 x i32> %10
}

declare i32 @llpc.umid3.i32(i32, i32, i32) #0

; float FMin3AMD()  =>  llpc.fmin3.f32
define spir_func float @_Z8FMin3AMDfff(
    float %x, float %y, float %z) #0
{
    %1 = call float @llpc.fmin3.f32(float %x, float %y, float %z)

    ret float %1
}

; <2 x float> FMin3AMD()  =>  llpc.fmin3.f32
define spir_func <2 x float> @_Z8FMin3AMDDv2_fDv2_fDv2_f(
    <2 x float> %x, <2 x float> %y, <2 x float> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %y0 = extractelement <2 x float> %y, i32 0
    %y1 = extractelement <2 x float> %y, i32 1

    %z0 = extractelement <2 x float> %z, i32 0
    %z1 = extractelement <2 x float> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fmin3.f32(float %x0, float %y0, float %z0)
    %2 = call float @llpc.fmin3.f32(float %x1, float %y1, float %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> FMin3AMD()  =>  llpc.fmin3.f32
define spir_func <3 x float> @_Z8FMin3AMDDv3_fDv3_fDv3_f(
    <3 x float> %x, <3 x float> %y, <3 x float> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %y0 = extractelement <3 x float> %y, i32 0
    %y1 = extractelement <3 x float> %y, i32 1
    %y2 = extractelement <3 x float> %y, i32 2

    %z0 = extractelement <3 x float> %z, i32 0
    %z1 = extractelement <3 x float> %z, i32 1
    %z2 = extractelement <3 x float> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fmin3.f32(float %x0, float %y0, float %z0)
    %2 = call float @llpc.fmin3.f32(float %x1, float %y1, float %z1)
    %3 = call float @llpc.fmin3.f32(float %x2, float %y2, float %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> FMin3AMD()  =>  llpc.fmin3.f32
define spir_func <4 x float> @_Z8FMin3AMDDv4_fDv4_fDv4_f(
    <4 x float> %x, <4 x float> %y, <4 x float> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %y0 = extractelement <4 x float> %y, i32 0
    %y1 = extractelement <4 x float> %y, i32 1
    %y2 = extractelement <4 x float> %y, i32 2
    %y3 = extractelement <4 x float> %y, i32 3

    %z0 = extractelement <4 x float> %z, i32 0
    %z1 = extractelement <4 x float> %z, i32 1
    %z2 = extractelement <4 x float> %z, i32 2
    %z3 = extractelement <4 x float> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fmin3.f32(float %x0, float %y0, float %z0)
    %2 = call float @llpc.fmin3.f32(float %x1, float %y1, float %z1)
    %3 = call float @llpc.fmin3.f32(float %x2, float %y2, float %z2)
    %4 = call float @llpc.fmin3.f32(float %x3, float %y3, float %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.fmin3.f32(float, float, float) #0

; float FMax3AMD()  =>  llpc.fmax3.f32
define spir_func float @_Z8FMax3AMDfff(
    float %x, float %y, float %z) #0
{
    %1 = call float @llpc.fmax3.f32(float %x, float %y, float %z)

    ret float %1
}

; <2 x float> FMax3AMD()  =>  llpc.fmax3.f32
define spir_func <2 x float> @_Z8FMax3AMDDv2_fDv2_fDv2_f(
    <2 x float> %x, <2 x float> %y, <2 x float> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %y0 = extractelement <2 x float> %y, i32 0
    %y1 = extractelement <2 x float> %y, i32 1

    %z0 = extractelement <2 x float> %z, i32 0
    %z1 = extractelement <2 x float> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fmax3.f32(float %x0, float %y0, float %z0)
    %2 = call float @llpc.fmax3.f32(float %x1, float %y1, float %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> FMax3AMD()  =>  llpc.fmax3.f32
define spir_func <3 x float> @_Z8FMax3AMDDv3_fDv3_fDv3_f(
    <3 x float> %x, <3 x float> %y, <3 x float> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %y0 = extractelement <3 x float> %y, i32 0
    %y1 = extractelement <3 x float> %y, i32 1
    %y2 = extractelement <3 x float> %y, i32 2

    %z0 = extractelement <3 x float> %z, i32 0
    %z1 = extractelement <3 x float> %z, i32 1
    %z2 = extractelement <3 x float> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fmax3.f32(float %x0, float %y0, float %z0)
    %2 = call float @llpc.fmax3.f32(float %x1, float %y1, float %z1)
    %3 = call float @llpc.fmax3.f32(float %x2, float %y2, float %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> FMax3AMD()  =>  llpc.fmax3.f32
define spir_func <4 x float> @_Z8FMax3AMDDv4_fDv4_fDv4_f(
    <4 x float> %x, <4 x float> %y, <4 x float> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %y0 = extractelement <4 x float> %y, i32 0
    %y1 = extractelement <4 x float> %y, i32 1
    %y2 = extractelement <4 x float> %y, i32 2
    %y3 = extractelement <4 x float> %y, i32 3

    %z0 = extractelement <4 x float> %z, i32 0
    %z1 = extractelement <4 x float> %z, i32 1
    %z2 = extractelement <4 x float> %z, i32 2
    %z3 = extractelement <4 x float> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fmax3.f32(float %x0, float %y0, float %z0)
    %2 = call float @llpc.fmax3.f32(float %x1, float %y1, float %z1)
    %3 = call float @llpc.fmax3.f32(float %x2, float %y2, float %z2)
    %4 = call float @llpc.fmax3.f32(float %x3, float %y3, float %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.fmax3.f32(float, float, float) #0

; float FMid3AMD()  =>  llpc.fmid3.f32
define spir_func float @_Z8FMid3AMDfff(
    float %x, float %y, float %z) #0
{
    %1 = call float @llpc.fmid3.f32(float %x, float %y, float %z)

    ret float %1
}

; <2 x float> FMid3AMD()  =>  llpc.fmid3.f32
define spir_func <2 x float> @_Z8FMid3AMDDv2_fDv2_fDv2_f(
    <2 x float> %x, <2 x float> %y, <2 x float> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <2 x float> %x, i32 0
    %x1 = extractelement <2 x float> %x, i32 1

    %y0 = extractelement <2 x float> %y, i32 0
    %y1 = extractelement <2 x float> %y, i32 1

    %z0 = extractelement <2 x float> %z, i32 0
    %z1 = extractelement <2 x float> %z, i32 1

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fmid3.f32(float %x0, float %y0, float %z0)
    %2 = call float @llpc.fmid3.f32(float %x1, float %y1, float %z1)

    ; Insert computed components into the destination vector
    %3 = alloca <2 x float>
    %4 = load <2 x float>, <2 x float>* %3
    %5 = insertelement <2 x float> %4, float %1, i32 0
    %6 = insertelement <2 x float> %5, float %2, i32 1

    ret <2 x float> %6
}

; <3 x float> FMid3AMD()  =>  llpc.fmid3.f32
define spir_func <3 x float> @_Z8FMid3AMDDv3_fDv3_fDv3_f(
    <3 x float> %x, <3 x float> %y, <3 x float> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <3 x float> %x, i32 0
    %x1 = extractelement <3 x float> %x, i32 1
    %x2 = extractelement <3 x float> %x, i32 2

    %y0 = extractelement <3 x float> %y, i32 0
    %y1 = extractelement <3 x float> %y, i32 1
    %y2 = extractelement <3 x float> %y, i32 2

    %z0 = extractelement <3 x float> %z, i32 0
    %z1 = extractelement <3 x float> %z, i32 1
    %z2 = extractelement <3 x float> %z, i32 2

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fmid3.f32(float %x0, float %y0, float %z0)
    %2 = call float @llpc.fmid3.f32(float %x1, float %y1, float %z1)
    %3 = call float @llpc.fmid3.f32(float %x2, float %y2, float %z2)

    ; Insert computed components into the destination vector
    %4 = alloca <3 x float>
    %5 = load <3 x float>, <3 x float>* %4
    %6 = insertelement <3 x float> %5, float %1, i32 0
    %7 = insertelement <3 x float> %6, float %2, i32 1
    %8 = insertelement <3 x float> %7, float %3, i32 2

    ret <3 x float> %8
}

; <4 x float> FMid3AMD()  =>  llpc.fmid3.f32
define spir_func <4 x float> @_Z8FMid3AMDDv4_fDv4_fDv4_f(
    <4 x float> %x, <4 x float> %y, <4 x float> %z) #0
{
    ; Extract components from source vectors
    %x0 = extractelement <4 x float> %x, i32 0
    %x1 = extractelement <4 x float> %x, i32 1
    %x2 = extractelement <4 x float> %x, i32 2
    %x3 = extractelement <4 x float> %x, i32 3

    %y0 = extractelement <4 x float> %y, i32 0
    %y1 = extractelement <4 x float> %y, i32 1
    %y2 = extractelement <4 x float> %y, i32 2
    %y3 = extractelement <4 x float> %y, i32 3

    %z0 = extractelement <4 x float> %z, i32 0
    %z1 = extractelement <4 x float> %z, i32 1
    %z2 = extractelement <4 x float> %z, i32 2
    %z3 = extractelement <4 x float> %z, i32 3

    ; Call LLVM/LLPC instrinsic, do component-wise computation
    %1 = call float @llpc.fmid3.f32(float %x0, float %y0, float %z0)
    %2 = call float @llpc.fmid3.f32(float %x1, float %y1, float %z1)
    %3 = call float @llpc.fmid3.f32(float %x2, float %y2, float %z2)
    %4 = call float @llpc.fmid3.f32(float %x3, float %y3, float %z3)

    ; Insert computed components into the destination vector
    %5 = alloca <4 x float>
    %6 = load <4 x float>, <4 x float>* %5
    %7 = insertelement <4 x float> %6, float %1, i32 0
    %8 = insertelement <4 x float> %7, float %2, i32 1
    %9 = insertelement <4 x float> %8, float %3, i32 2
    %10 = insertelement <4 x float> %9, float %4, i32 3

    ret <4 x float> %10
}

declare float @llpc.fmid3.f32(float, float, float) #0

attributes #0 = { nounwind }
attributes #1 = { nounwind readnone }
